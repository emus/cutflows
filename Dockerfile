FROM atlas/athanalysis:21.2.151
COPY . /analysis
WORKDIR /home/atlas
RUN echo "Checking out ACM" && git clone --depth 1 --branch  acm-0.1.22 https://gitlab.cern.ch/atlas-sit/acm.git
WORKDIR /analysis/build
ENV ACMHOME=/home/atlas/acm ReleaseSetupScript=/home/atlas/release_setup.sh
RUN echo 'export ACMHOME=/home/atlas/acm' >> ~/.bashrc && \
    echo 'alias acmSetup="source ${ACMHOME}/acmSetup.sh"' >> ~/TEMP && \
    sed  "s/\"/'/g" ~/TEMP  >> ~/.bash_alias && \
    source ~/.bashrc && \
    source ~/release_setup.sh &&  \
    sudo mkdir /analysis/run && \  
    sudo chown -R atlas /analysis && \
    source /home/atlas/acm/acmSetup.sh AthAnalysis,21.2.151 && \
    #cat /analysis/source/CMakeLists.txt #&& \
    acm find_packages && \
    acm compile
