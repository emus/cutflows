# ReadTree

## Installation
The easiest thing to do is to run the `first_time_setup.sh` script. Alternatively you can follow the manual steps below.


ReadTree uses Cmake for its compilation and installation. 
Make a build source and run directory 
```
mkdir run source build
```

This package requires one external package, `CutChain`, which must also be cloned into the `source` directory. 
If you need any additional packages, they can also be cloned into `source`, so long as the have a `CMakeLists.txt` file.
```
git clone ssh://git@gitlab.cern.ch:7999/SUSYMultijets0L/CutChain.git
```

From the build directory, call acmSetup AthAnalysis,21.2.151 (after calling setupATLAS, if not already done)
```
setupATLAS
cd build
acmSetup AthAnalysis,21.2.151
```
Then find the packages and compile the code:
```
acm find_packages
acm compile
```

If you restart your terminal, simply go to the top-level directory and type
```
acmSetup
```
and you will be ready to continue coding. 

## Running the code
The executable `ReadTreeTest` is used to run the code. It may take a large number of optional arguments, but a basic runing of the code can be achieved with
```
RunReadTree <optional args> --output <outputname.root> <wildcarded-list-of-inputfiles>
```
For example
```
RunReadTree -s --overwrite --output /r02/atlas/emus/histograms/data3May_1902251350/data/user.wfawcett.1902251350.data18.361862.f988_m2025_p3704_NTUP /r02/atlas/emus/ntuples/1902251350/data/user.wfawcett.1902251350.data18.361862.f988_m2025_p3704_NTUP/*.root
```

### optional arguments
```--overwrite``` Will overwrite output file if it already exists (default off). 
```-d <int>``` Turn on debug mode, the integer sets the debug level (default off).
```-s``` Turn on calculation of flying  squirrel variables e.g. MT2 (default off).
```-n <int>``` Number of events to run over (default all events).
```-F``` Apply fake weights, should be used with data to create the fake sample. 


## Creating the fakes, an example
First, it's a good idea to hadd the data ntuples, just reduces the overhead of the file opening and closing and makes thigs run faster. 
Second, there's a script 

