setupATLAS
lsetup git

# Removed for RECAST
#cd source
#git clone ssh://git@gitlab.cern.ch:7999/SUSYMultijets0L/CutChain.git
#cd ../

mkdir build run
cd build 
acmSetup AthAnalysis,21.2.151 --sourcedir=../source 
#lsetup "root 6.14.08-x86_64-slc6-gcc62-opt"
acm find_packages
acm compile 
