#include "ReadTree/FakeWeightGetter.h"

std::vector<std::string> splitString(const std::string& inStr, char delim){
  // Split a std::string along a delimiter "delim"
  // Returns a vector of the split string
  std::istringstream ss(inStr);
  std::string placeholder;
  std::vector<std::string> ret;
  while (std::getline(ss, placeholder, delim) ) ret.push_back(placeholder);
  return ret;
}


void FakeWeightGetter::print_histograms(){
  /*******************
   *
   * Diagnostic function to print the information inside the m_eff_histograms map
   *
   * *************/
  std::cout << "Information retrieved from input fake and real efficiencies" << std::endl;
  for(std::map< std::string, TGraphAsymmErrors* >::iterator iter = m_eff_histograms.begin(); iter != m_eff_histograms.end(); ++iter)
  {
    std::string name = iter->first;
    TGraphAsymmErrors* hist = iter->second; 
    std::cout << "name: " << name << std::endl;
    hist->Print();
    std::cout << std::endl;
  }
}

void FakeWeightGetter::load_efficiency_histograms(){
    /*******
     * Load and store efficiency histograms
     * basically re-maps histogram name and stores it in a map
     * ***********/
    TFile* input_file = TFile::Open( m_combined_eff.c_str() );
    for(std::string label : {"real", "fake"}){
      std::string tag = label+"efficiency";
      for(std::string flavour : {"electron", "muon"}){
        for(std::string charge : {"plus", "minus"}){
          std::string hist_tag = label+"_"+flavour+"_"+charge;
          TString hist_name = tag+"_"+flavour+charge+"_lep1Pt";

          TGraphAsymmErrors* eff_hist = 0;
          input_file->GetObject(hist_name, eff_hist); 
          m_eff_histograms[hist_tag] = eff_hist;
        }
      }
    }
    input_file->Close(); // will the pointers to the histograms be deleted? 
}

void FakeWeightGetter::load_efficiency_binning(){
  
  for(std::map<std::string,TGraphAsymmErrors*>::iterator iter = m_eff_histograms.begin(); iter != m_eff_histograms.end(); ++iter)
  {
    std::string name = iter->first;
    TGraphAsymmErrors* hist = iter->second;
    Double_t* centre = hist->GetX(); // array
    Double_t* low_arr = hist->GetEXlow();
    Double_t* high_arr = hist->GetEXhigh();
    int highest_so_far_i(0), highest_so_far_high(0);
    std::vector<std::pair<float, float> > this_hist_boundaries;
    for(int i=0; i<hist->GetN(); ++i){
      float low = centre[i] - low_arr[i];
      float high = centre[i]+high_arr[i];
      if(high > highest_so_far_high){
          highest_so_far_i = i;
          highest_so_far_high = high;
      }
      this_hist_boundaries.push_back( std::make_pair(low, high) );

    }
    // remove the upper bound
    this_hist_boundaries[highest_so_far_i].second = std::numeric_limits<float>::infinity(); // will have to be careful with this 
    m_eff_bin_boundaries[name] = this_hist_boundaries; 

    // most often we're in the lowest couple of bins. We'll check these first later.
    std::string real_or_fake = splitString(name, '_').at(0);
    if(real_or_fake == "real"){
      
      // Set variables such as m_bin_index_cache_bin0_real to the _value_ of the lower boundary of bin0 for the real histogram 

      if(m_bin_index_cache_bin0_real == -999){
        m_bin_index_cache_bin0_real = this_hist_boundaries.at(0).second;
      }
      else if(m_bin_index_cache_bin0_real != this_hist_boundaries.at(0).second){
        std::cout << "WARNING: bin boundaries for real histograms do not seem consistent!" << std::endl;
      }
      
      if(m_bin_index_cache_bin1_real == -999){
        m_bin_index_cache_bin1_real = this_hist_boundaries.at(1).second;
      }
      else if(m_bin_index_cache_bin1_real != this_hist_boundaries.at(1).second){
        std::cout << "WARNING: bin boundaries for real histograms do not seem consistent!" << std::endl;
      }
    }
    else if(real_or_fake == "fake"){
      if(m_bin_index_cache_bin0_fake == -999){
        m_bin_index_cache_bin0_fake = this_hist_boundaries.at(0).second;
      }
      else if(m_bin_index_cache_bin0_fake != this_hist_boundaries.at(0).second){
        std::cout << "WARNING: bin boundaries for real histograms do not seem consistent!" << std::endl;
      }

      if(m_bin_index_cache_bin1_fake == -999){
        m_bin_index_cache_bin1_fake = this_hist_boundaries.at(1).second;
      }
      else if(m_bin_index_cache_bin1_fake != this_hist_boundaries.at(1).second)
        std::cout << "WARNING: bin boundaries for real histograms do not seem consistent!" << std::endl;
    }


  } // end of loop over map elements
}

EfficiencyTuple FakeWeightGetter::get_efficiency(std::string real_or_fake, const Lepton& lep){
  std::string eff_hist_tag = real_or_fake+"_"+lep.flavour+"_"+lep.charge_string();
  return get_efficiency_from_hist(eff_hist_tag, lep);
}

EfficiencyTuple FakeWeightGetter::get_efficiency_from_hist(std::string hist_name, const Lepton& lep){
  /*******************
   * Extract the eff and the eff_up and eff_down from the histogram
   * Cache the result (and used the cached value if it exists)
   *
   * The efficiency should be a function of the lepton kinematics, in this case the lepton pT
   * The lepton pT is used to get the bin index of the efficiency in the relevant histogram
   * the bin index is then used to extract the efficiency. 
   * *****************/

  int eff_index = get_bin_index(hist_name, lep.fourVec.Pt() );
  if(!m_debug) std::cout << "get_bin_index: " << eff_index << std::endl;

  // Concatanate two indeces (no need to have a map of maps)
  std::string hist_name_eff_index = hist_name + std::to_string(eff_index);

  // get keys of the cache
  std::vector<std::string> cache_keys;
  for(auto const& element : m_eff_cache){
    cache_keys.push_back(element.first);
  }


  // Get efficiency tuple , use cached version if available 
  EfficiencyTuple tup;
//  if( (std::find(cache_keys.begin(), cache_keys.end(), hist_name) != cache_keys.end()) ){
//    tup = m_eff_cache[hist_name];
//  }
//  else{
  TGraphAsymmErrors* hist = m_eff_histograms[hist_name];
  float eff      = hist->GetY()[eff_index];
  float err_up   = hist->GetErrorYhigh(eff_index);
  float err_down = hist->GetErrorYlow(eff_index);

  float eff_up = eff + err_up;
  float eff_down = eff - err_down;

  tup.eff = eff;
  tup.eff_up = eff_up;
  tup.eff_down = eff_down;

  m_eff_cache[hist_name_eff_index] = tup;  
//  }

  if(!m_debug){
    std::cout << "get_efficiency_from_hist: " << tup.eff << " " << tup.eff_up << " " << tup.eff_down << std::endl;
  }
  return tup;
}

int FakeWeightGetter::get_cached_bin_index(const std::string& hist_name, float value) const{

  // Most ofter we're in the lowest couple of bins. Check the cached version of these:
  std::string real_or_fake = splitString(hist_name, '_').at(0);
  if(real_or_fake == "real"){
    if(value <= m_bin_index_cache_bin0_real) return 0;
    if(value <= m_bin_index_cache_bin1_real) return 1;
  }
  else if(real_or_fake == "fake"){
    if(value <= m_bin_index_cache_bin0_fake) return 0;
    if(value <= m_bin_index_cache_bin1_fake) return 1;
  }

  return -1; // unable to get the cached bin index 

}

int FakeWeightGetter::get_bin_index(const std::string& hist_name, float value) {

  // First try to use the cached value
  /******  WJF: remove attempt to use cached index. 18/06/19
   * Could potentially cause problems if histograms only have one bin
  int cached_index = get_cached_bin_index(hist_name, value);
  if(cached_index != -1) return cached_index;
  *********/ 


  //value /= 1000; // WJF may need to check units here  ! // removed, to try to convert all units to GeV :) 

  // Otherwise search for the bin index
  load_efficiency_binning();
  std::vector< std::pair<float, float> > bin_boundaries = m_eff_bin_boundaries[hist_name];
  for(int i=0; i<bin_boundaries.size(); ++i){
//    if( i==0 || i==1) continue; // skip first two elements, already considered above
    if( (value > bin_boundaries.at(i).first) && ( value <= bin_boundaries.at(i).second) ) return i;
  }

  std::cout << "ERROR: did not find a bin index for value: " << value << std::endl;
  std::cout << "You should investigate, check pT units? " << std::endl;
  return 0;
}

float FakeWeightGetter::fake_weight_formula(bool tight1, bool tight2, float r1, float r2, float f1, float f2 ) const{
  /********
   * Applies a fake-weight formula
   * Returns the fake weight.
   * WJF: could this be vectorized? 
   *************/

  float weight = 1.0; 
  if(tight1){
    if(tight2){
      weight = ( f1*f2*(1-r1*r2) - f1*(1-r1)*r2 - f2*r1*(1-r2) ) / ((r1-f1)*(r2-f2));
    }else{ 
      weight = (1-f1)*f2*r1*r2 / ((r1-f1)*(r2-f2));
    }
  }else{
    if(tight2){
      weight = f1*(1-f2)*r1*r2 / ((r1-f1)*(r2-f2));
    }else{
      weight = -f1*f2*r1*r2 / ((r1-f1)*(r2-f2));
    }
  }
  if(!m_debug) std::cout << "fake_weight_formula: " << weight << std::endl;
  return weight;
}


std::map<std::string, float> FakeWeightGetter::get_fake_weight(const Lepton& lep1, const Lepton& lep2){
  /********************
   * Get the fake weights for this event
   * Args: 
   *  lep1 and lep2 are the leading and subleading leptons in the event
   * Returns
   *  std::map<std::string, float> of the weights, and the variations for moving the efficiency up and down
   *  **************************/

  EfficiencyTuple r1 = get_efficiency("real", lep1);
  EfficiencyTuple r2 = get_efficiency("real", lep2);
  EfficiencyTuple f1 = get_efficiency("fake", lep1);
  EfficiencyTuple f2 = get_efficiency("fake", lep2);

  float weight                             = fake_weight_formula(lep1.isTight, lep2.isTight, r1.eff,      r2.eff,      f1.eff,      f2.eff);
  float weight_leading_realup              = fake_weight_formula(lep1.isTight, lep2.isTight, r1.eff_up,   r2.eff,      f1.eff,      f2.eff);
  float weight_subleading_realup           = fake_weight_formula(lep1.isTight, lep2.isTight, r1.eff,      r2.eff_up,   f1.eff,      f2.eff);
  float weight_leading_subleading_realup   = fake_weight_formula(lep1.isTight, lep2.isTight, r1.eff_up,   r2.eff_up,   f1.eff,      f2.eff);
  float weight_leading_realdown            = fake_weight_formula(lep1.isTight, lep2.isTight, r1.eff_down, r2.eff,      f1.eff,      f2.eff);
  float weight_subleading_realdown         = fake_weight_formula(lep1.isTight, lep2.isTight, r1.eff,      r2.eff_down, f1.eff,      f2.eff);
  float weight_leading_subleading_realdown = fake_weight_formula(lep1.isTight, lep2.isTight, r1.eff_down, r2.eff_down, f1.eff,      f2.eff);
  float weight_leading_fakeup              = fake_weight_formula(lep1.isTight, lep2.isTight, r1.eff,      r2.eff,      f1.eff_up,   f2.eff);
  float weight_subleading_fakeup           = fake_weight_formula(lep1.isTight, lep2.isTight, r1.eff,      r2.eff,      f1.eff,      f2.eff_up);
  float weight_leading_subleading_fakeup   = fake_weight_formula(lep1.isTight, lep2.isTight, r1.eff,      r2.eff,      f1.eff_up,   f2.eff_up);
  float weight_leading_fakedown            = fake_weight_formula(lep1.isTight, lep2.isTight, r1.eff,      r2.eff,      f1.eff_down, f2.eff);
  float weight_subleading_fakedown         = fake_weight_formula(lep1.isTight, lep2.isTight, r1.eff,      r2.eff,      f1.eff,      f2.eff_down);
  float weight_leading_subleading_fakedown = fake_weight_formula(lep1.isTight, lep2.isTight, r1.eff,      r2.eff,      f1.eff_down, f2.eff_down);

  std::map<std::string, float> weight_map; 
  weight_map["weight"]                             = weight;
  weight_map["weight_leading_realup"]              = weight_leading_realup;
  weight_map["weight_subleading_realup"]           = weight_subleading_realup;
  weight_map["weight_leading_subleading_realup"]   = weight_leading_subleading_realup;
  weight_map["weight_leading_realdown"]            = weight_leading_realdown;
  weight_map["weight_subleading_realdown"]         = weight_subleading_realdown;
  weight_map["weight_leading_subleading_realdown"] = weight_leading_subleading_realdown;
  weight_map["weight_leading_fakeup"]              = weight_leading_fakeup;
  weight_map["weight_subleading_fakeup"]           = weight_subleading_fakeup;
  weight_map["weight_leading_subleading_fakeup"]   = weight_leading_subleading_fakeup;
  weight_map["weight_leading_fakedown"]            = weight_leading_fakedown;
  weight_map["weight_subleading_fakedown"]         = weight_subleading_fakedown;
  weight_map["weight_leading_subleading_fakedown"] = weight_leading_subleading_fakedown;

  return weight_map;

}
