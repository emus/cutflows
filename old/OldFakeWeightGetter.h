#ifndef ReadTree_FAKEWEIGHTGETTER_H
#define ReadTree_FAKEWEIGHTGETTER_H

#include <TFile.h>
#include <TDirectory.h>
#include <TString.h>
#include <TH1.h>
#include <TGraphAsymmErrors.h>
#include <limits>
#include <map>
#include <iostream>
#include <sstream>

#include "ReadTree/PhysicsObjects.h"


/*********************
 *
 * Origionally transcribed from <old_xaodanalysis_athena>/source/tree_decoration/get_fake_weights.py
 * e.g.: /usera/wfawcett/cambridge/emus/old_xaodanalysis_athena/source/tree_decoration/get_fake_weights.py
 *
 * ************/

// useful struct 
struct EfficiencyTuple{
  float eff;
  float eff_up;
  float eff_down;
};

class FakeWeightGetter
{
  private:
    
    std::string m_combined_eff;
    std::map<std::string, TGraphAsymmErrors*> m_eff_histograms; 
    std::map<std::string, std::vector< std::pair<float, float> > > m_eff_bin_boundaries; 
    std::map<std::string, EfficiencyTuple> m_eff_cache;

    int m_bin_index_cache_bin0_real; 
    int m_bin_index_cache_bin1_real;
    int m_bin_index_cache_bin0_fake;
    int m_bin_index_cache_bin1_fake; 

    bool m_debug;

    // Functions
    int get_bin_index(const std::string& hist_name, float value);
    int get_cached_bin_index(const std::string& hist_name, float value) const;
    void load_efficiency_histograms();
    void load_efficiency_binning();
    float fake_weight_formula(bool tight1, bool tight2, float r1, float r2, float f1, float f2 ) const;
    EfficiencyTuple get_efficiency(std::string real_or_fake, const Lepton& lep);
    EfficiencyTuple get_efficiency_from_hist(std::string hist_name, const Lepton& lep);

  public:

    // constructor 
    FakeWeightGetter() {
      m_bin_index_cache_bin0_real = -999; 
      m_bin_index_cache_bin1_real = -999;
      m_bin_index_cache_bin0_fake = -999;
      m_bin_index_cache_bin1_fake = -999; 

      m_debug = false;

    }

    void initialize(std::string combined_eff){
      m_combined_eff = combined_eff;
      std::cout << "Initialized FakeWeightGetter with TFile: " << m_combined_eff << std::endl;
      load_efficiency_histograms();
      // load_efficiency_binning(); WJF: remove caching of bin indecies (possible problems with histograms that only have one bin)
    }


    // member functions
    void print_histograms();
    std::map<std::string, float> get_fake_weight(const Lepton&, const Lepton&);

};

#endif //ReadTree_FAKEWEIGHTGETTER_H
