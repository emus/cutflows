//
// * AtlasOxbridgeKineticsInterface: an ATLAS wrapper for the mT2 and oxbridgekinetics libraries
// * Originally by Teng Jian Khoo, June 2012
// * Updated by William Fawcett, April 2019
//
// * AtlasOxbridgeKineticsInterface.h
// * Base class for the various mass bound calculators
//

#ifndef _OKCOMPUTER_
#define _OKCOMPUTER_

#include <vector>
#include <iostream>
#include "TransverseMassUtils.h"
#include "TVector2.h"

using std::vector;

namespace OK {

  class AtlasOxbridgeKineticsInterface {

    public:
      AtlasOxbridgeKineticsInterface();
      virtual ~AtlasOxbridgeKineticsInterface();

      // Flush the stored info
      virtual void clear();

      // Add a visible particle, supplying the parent to which it should be associated
      void addVis(const TLorentzVector& tlv, unsigned int parent=1);
      void addVisPxPyPzE(double px, double py, double pz, double E, unsigned int parent=1);
      void addVisPxPyPzM(double px, double py, double pz, double M, unsigned int parent=1);
      void addVisPtEtaPhiE(double pt, double eta, double phi, double E, unsigned int parent=1);
      void addVisPtEtaPhiM(double pt, double eta, double phi, double M, unsigned int parent=1);
      // Count the visibles for a parent
      inline size_t getNvis(size_t parent=1) {
        if(parent==1) {
          if(!m_viscoll1) {
            std::cout << "AtlasOxbridgeKineticsInterface: WARNING -- requested size of Nvis for side " << parent 
              << " but no objects supplied!" << std::endl;
            return 0;
          }
          return m_viscoll1->size();
        }
        if(parent==2) {
          if(!m_viscoll2) {
            std::cout << "AtlasOxbridgeKineticsInterface: WARNING -- requested size of Nvis for side " << parent 
              << " but no objects supplied!" << std::endl;
            return 0;
          }
          return m_viscoll2->size();
        }
        std::cerr << "AtlasOxbridgeKineticsInterface: WARNING -- requested size of Nvis for invalid side " << parent << std::endl;
        return 0;
      }

      double getSideMass(unsigned int side);
      double getMassSum();

      // Set all the visible particles at one go
      void setVisibles(vector<TLorentzVector> parent1, vector<TLorentzVector> parent2);
      vector<TLorentzVector> getVisibles();

      // Set the ptMiss 2-vector
      void setPtMiss(TVector2 ptmiss);
      void setPtMiss(double pxmiss, double pymiss);

      // Set the invisible mass hypothesis
      void setMinvis(double Minvis);

      inline vector<TLorentzVector> getVisCollection(unsigned int parent=1) {
        if(parent==1) {
          if(m_viscoll1) return *m_viscoll1;
        }
        if(parent==2) {
          if(m_viscoll2) return *m_viscoll2;
        }
        vector<TLorentzVector> dummy;
        return dummy;
      }

      inline TLorentzVector getVisComposite(unsigned int parent=1) {
        if(parent==1) {
          if(m_viscoll1) return sum(*m_viscoll1);
        }
        if(parent==2) {
          if(m_viscoll2) return sum(*m_viscoll2);
        }
        TLorentzVector dummy(0,0,0,0);
        return dummy;
      }

    protected:

      vector<TLorentzVector>* m_viscoll1;
      vector<TLorentzVector>* m_viscoll2;
      TVector2 m_ptmiss;
      double m_Minvis;

      static inline bool descendingPt(TLorentzVector first, TLorentzVector second) {
        return first.Pt()>second.Pt();
      };

  };
}

#endif
