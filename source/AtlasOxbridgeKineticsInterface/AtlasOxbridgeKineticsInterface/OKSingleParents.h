//
// * AtlasOxbridgeKineticsInterface: an ATLAS wrapper for the mT2 and oxbridgekinetics libraries
// * Originally by Teng Jian Khoo, June 2012
// * Updated by William Fawcett, April 2019
//
// * OKSingleParents.cxx
// * Mass bound calculators for a single decaying parent
//

#ifndef _OKSINGLEPARENTS_
#define _OKSINGLEPARENTS_

#include "AtlasOxbridgeKineticsInterface.h"

namespace OK {

  class OKSingleParents : virtual public AtlasOxbridgeKineticsInterface {

    public:
      OKSingleParents();
      ~OKSingleParents();

      // Compute the squirrel variables
      virtual double calcM1T();
      virtual double calcMT1();
      virtual double calcMo1();
      virtual double calcM1o();

      // Effective Mass
      virtual double calcMeff();

      // Effective Transverse Energy
      virtual double calcETeff();
  };

}

#endif
