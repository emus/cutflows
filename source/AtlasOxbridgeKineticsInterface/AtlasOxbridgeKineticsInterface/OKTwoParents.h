//
// * AtlasOxbridgeKineticsInterface: an ATLAS wrapper for the mT2 and oxbridgekinetics libraries
// * Originally by Teng Jian Khoo, June 2012
// * Updated by William Fawcett, April 2019
//
// * OKTwoParents.h
// * Mass bound calculators for two decaying parents
//

#ifndef _OKTWOPARENTS_
#define _OKTWOPARENTS_

#include "AtlasOxbridgeKineticsInterface/AtlasOxbridgeKineticsInterface.h"

#include "Mt2/LesterNachmanBisect_Mt2_332_Calculator.h"
#include "Mt2/Frugal_MtGen_332_Calculator.h"
#include "Mt2/LesterNachmanBisect_Mt2_3322_Calculator.h"
#include "Mt2/MCT_330_Calculator.h"
#include "Mt2/Mt2Vectors.h"

namespace Mt2 {
  class LesterNachmanBisect_Mt2_332_Calculator;
  class Frugal_MtGen_332_Calculator;
  class LesterNachmanBisect_Mt2_3322_Calculator;
}

namespace OK {

  class OKTwoParents : virtual public AtlasOxbridgeKineticsInterface {

    public:
      OKTwoParents();
      ~OKTwoParents();

      // Compute the 2-parent squirrel variables
      virtual double calcM2T();
      virtual double calcMT2();
      virtual double calcMo2();
      virtual double calcM2o();

      // Asymmetric 2-parent squirrel variables
      virtual double calcAsymMT2(const double MinvisA, const double MinvisB);
      virtual double calcAsymM2T(const double MinvisA, const double MinvisB);


      // Build in a (frugal) calculation of MTgen
      // G and g correspond to T and o projections
      // Note that this will rearrange the visible collections.
      virtual double calcM2T_gen();
      virtual double calcMT2_gen();
      virtual double calcMo2_gen();
      virtual double calcM2o_gen();

      // Other 2-parent variables
      virtual double calcMCT(unsigned int projtype=0);
      // Razor
      virtual double calcGammaMRstar(unsigned int projtype=0);
      virtual double calcMRT(unsigned int projtype=0);

      // Reclustering methods
      void recluster_minMinv();
      void recluster_symMinv();
      void recluster_maxBoostedP();

      void reassign_by_encoding(const vector<TLorentzVector> &collection, unsigned long long encoding);
      inline void reassign_by_encoding(unsigned long long encoding) {
        // This is provided for the lazy, but it's better to provide the collection if this
        // is to be called multiple times. To preserve ordering, this overloaded version
        // sorts by Pt, which is time-consuming.
        reassign_by_encoding(AtlasOxbridgeKineticsInterface::getVisibles(), encoding);
      }

    private:
      // Helper methods for the MTgen variations
      double calcMTGen(double (OK::OKTwoParents::*mt2_def)(),
          const double minvis);
      void tryAlsoFrom(unsigned int index,
          const vector<TLorentzVector> &collection,
          const unsigned long long encoding,
          double (OK::OKTwoParents::*mt2_def)(),
          const double minvis,
          double &bestMt2SqSoFar);

      Mt2::Frugal_MtGen_332_Calculator* m_mtgen_calc;
      Mt2::LesterNachmanBisect_Mt2_332_Calculator* m_mt2_calc;
      Mt2::LesterNachmanBisect_Mt2_3322_Calculator* m_amt2_calc;

  };

}

#endif
