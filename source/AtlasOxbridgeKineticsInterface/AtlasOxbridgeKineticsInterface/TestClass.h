// Dear emacs, this is -*- c++ -*-
// Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
#ifndef TESTCLASS_H
#define TESTCLASS_H

// This code is only here to test that we can make use of the AtlasOxbridgeKineticsInterface
// library correctly. This file/class should be removed once proper interface
// class(es) are written.

#include <Mt2/Mt2Vectors.h>

class TestClass {

  public:
    void dummy();

  private:
    Mt2::LorentzVector m_vec;

}; // class TestClass

#endif // TESTCLASS_H
