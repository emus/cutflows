//
// * AtlasOxbridgeKineticsInterface: an ATLAS wrapper for the mT2 and oxbridgekinetics libraries
// * Originally by Teng Jian Khoo, June 2012
// * Updated by William Fawcett, April 2019
//
// * TransverseMassUtils.h
// * Define various helper functions for transverse projections
// * and easily-calculable mass bounds
//

#ifndef _TRANSVERSEMASSUTILS_
#define _TRANSVERSEMASSUTILS_

#include <vector>
#include "TLorentzVector.h"

class TVector2;

namespace Mt2 {
  class LorentzVector;
  class LorentzTransverseVector;
  class TwoVector;
}

using std::vector;

namespace OK {

  // Define the transverse projections
  TLorentzVector projectT(const TLorentzVector &tlv);
  TLorentzVector projecto(const TLorentzVector &tlv);

  // A simple sum of TLVs
  TLorentzVector sum(const vector<TLorentzVector> &collection);

  // Combinations of composition and projection in different orders
  TLorentzVector sum_then_projectT(const vector<TLorentzVector> &collection); // M_NT
  TLorentzVector projectT_then_sum(const vector<TLorentzVector> &collection); // M_TN
  TLorentzVector projecto_then_sum(const vector<TLorentzVector> &collection); // M_oN
  TLorentzVector sum_then_projecto(const vector<TLorentzVector> &collection); // M_No

  // Compute the single parent bound == M_T
  double singleParentBound(const TLorentzVector &visible, TVector2 ptmiss, double Minvis);

  // Functions to convert ROOT vectors into Mt2 vectors
  Mt2::LorentzVector tlv_to_lv(const TLorentzVector &lv);
  Mt2::LorentzTransverseVector tlv_to_ltv(const TLorentzVector &tlv);
  Mt2::TwoVector tv2_to_2v(TVector2 tv2);

  // Methods for clustering of particles
  unsigned int closestParticle(const vector<TLorentzVector> &collection,
      double (*distance_measure)
      (const TLorentzVector&, const TLorentzVector&),
      const TLorentzVector &tlv,
      double &min_distance);
  inline unsigned int closestParticle(const vector<TLorentzVector> &collection,
      double (*distance_measure)
      (const TLorentzVector &, const TLorentzVector &),
      const TLorentzVector &tlv) {
    double dummy;
    return closestParticle(collection, distance_measure, tlv, dummy);
  }

  static inline double dLund(const TLorentzVector &hemi, const TLorentzVector &jet) {
    // Due to some cancellations, this is not a symmetric operation
    double e1 = hemi.E();
    double p1 = hemi.P();
    double e2 = jet.P();
    //    double costh = cos(hemi.Vect().Angle(jet.Vect()));
    double costh = cos(hemi.DeltaPhi(jet));
    double dist = sqrt((e1-p1*costh)*e1)/(e1+e2);
    return dist;
  }

}

#endif
