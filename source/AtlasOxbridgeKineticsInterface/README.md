ATLAS Interface to the Oxbridge Kinetics Library
================================================

This repository is meant to provide a user friendly way of using the
[Oxbridge Kinetics Library](http://www.hep.phy.cam.ac.uk/~lester/mt2/)
in an ATLAS analysis project.


## Repository Information

This package is configured to be used directly as a [submodule](https://git-scm.com/docs/git-submodule) in the user's own analysis repository. See the [SoftwareTutorialAnalysisInGitReleases twiki](https://twiki.cern.ch/twiki/bin/view/AtlasComputing/SoftwareTutorialAnalysisInGitReleases) page for more details on structuring ATLAS code. 

## Variables
There are many transverse-mass-like variables provided inside the [Oxbridge Kinetics Library](http://www.hep.phy.cam.ac.uk/~lester/mt2/), and they are more fully listed in the [Connoisseur's guide to transverse projections and mass-constraining variables](https://arxiv.org/abs/1105.2977). To briefly summarise, there are two main categories of variables: single-parent and two-parent. Single-parent assumes the missing transverse momentum comes from a single parent, two-parent assumes the missing transverse momentum comes from two particles. 


*The single-parent variables available are*
- M<sub>1T</sub>, M<sub>T1</sub>
- M<sub>o1</sub>, M<sub>1o</sub>
- M<sub>eff</sub>
- E<sub>T,eff</sub>

*The two-parent variables available are*
- M<sub>T2</sub>, M<sub>2T</sub>
- M<sub>o2</sub>, M<sub>2o</sub>
- M<sub>TGen</sub>
- aM<sub>T2</sub>, aM<sub>2T</sub> (the asymmetric variant of M<sub>T2</sub>)
- M<sub>CT</sub> (the "contransverse mass")

*Some razor variables are also included*
- M<sub>T</sub><sup>R</sup>
- γ <sub>R </sub> 

Not all variables available in the [Oxbridge Kinetics Library](http://www.hep.phy.cam.ac.uk/~lester/mt2/) are implemented in this wrapper, if you find one that you want to use then please add it.

Note that the library provides various calculators for M<sub>T2</sub>, but if the user just wants to use only this variable then the header-only implementation is recommended, which is linked on the [Oxbridge Kinetics Library](http://www.hep.phy.cam.ac.uk/~lester/mt2/) webpage. The M<sub>T2</sub> calculator used in this package is the "Cheng Hang bisection 332" method. 

If you use these variables in a publication you must cite the relvant papers, see the [Oxbridge Kinetics Library](http://www.hep.phy.cam.ac.uk/~lester/mt2/) webpage for details. 




### CMake configuration 

If you want to use this library in your own project, you must edit your CMakeLists.txt file to include the following:

- Add `AtlasOxbridgeKineticsInterface` to the `PUBLIC` section of `atlas_depends_on_subdirs` function
- Add `AtlasOxbridgeKineticsInterfaceLib` to the `LINK_LIBRARIES` section of the following functions: `atlas_add_library`, `atlas_add_executable`

### Integration into your pagkage and example variable calculation

The `AtlasOxbridgeKineticsInterface` package should be checked out at the same level as your other analysis packages in your source directory. There are two main calculators, one corresponding to the single-parent and two-parent cases. 
```cpp
#include "AtlasOxbridgeKineticsInterface/OKSingleParents.h"
#include "AtlasOxbridgeKineticsInterface/OKTwoParents.h"
```

All classes functions are protected by a namespace `OK`. The calculators can be defined as follows:
```cpp
OK::OKSingleParents ok1p;
OK::OKTwoParents ok2p;
```
Once defined, for each event you can add variables visible objects (e.g. leptons) to the calculators, add the missing transverse momentum vector, and set the mass(es) of the invisible particles you're interested in. 
```cpp
// At the start of each event, clear the objects to make sure information from the previous event isn't stored
ok1p.clear();
ok2p.clear();

// add the visible objects you want
for(TLorentzVector& jet : jets){
  ok1p.addVis(jet);
  ok2p.addVis(jet);
}

// Add the missing transverse momentum components
ok1p.setPtMiss(METx,METy);
ok2p.setPtMiss(METx,METy);

// Set the mass of the invisible object you're interested in
ok1p.setMinvis(0);
ok2p.setMinvis(0); 

// (optional) boost the physics objects into their collected COM frame
ok2p.recluster_maxBoostedP();

// Extract the variables you want
float Mo1   = ok1p.calcMo1(); 
float MT2   = ok2p.calcMT2();
float MTGen = ok2p.calcMT2_gen();
float MRT   = ok2p.calcMRT(1); // integer selects the projection
float aMT2  = ok2p.calcAsymMT2(50.0, 100.0); // asymmetric MT2 where one invisible particle has twice the mass of the other
```

### Compilation
If you are using acm, to compile this do:
```
acm find_packages
acm compile
```
During the `find_packages` step, you will see a number of files being downloaded. This the AtlasOxbridgeKineticsInterface's own CMakeLists file downloading the external Oxbridge Kinetics library, the placement of the files is handeled automatically.
