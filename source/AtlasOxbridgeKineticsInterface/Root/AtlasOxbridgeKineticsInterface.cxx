//
// * AtlasOxbridgeKineticsInterface: an ATLAS wrapper for the mT2 and oxbridgekinetics libraries
// * Originally by Teng Jian Khoo, June 2012
// * Updated by William Fawcett, April 2019
//
// * AtlasOxbridgeKineticsInterface.cxx
// * Base class for the various mass bound calculators (Implementation)
//

#include "AtlasOxbridgeKineticsInterface/AtlasOxbridgeKineticsInterface.h"
#include <algorithm>

namespace OK {
  using std::cout;
  using std::cerr;
  using std::endl;

  AtlasOxbridgeKineticsInterface::AtlasOxbridgeKineticsInterface() :
    m_viscoll1(0), m_viscoll2(0),
    m_ptmiss(0.,0.), m_Minvis(0.)
  {
    clear();
    if(m_viscoll1) delete m_viscoll1;
    if(m_viscoll2) delete m_viscoll2;
  }

  AtlasOxbridgeKineticsInterface::~AtlasOxbridgeKineticsInterface() {
    clear();
    if(m_viscoll1) delete m_viscoll1;
    if(m_viscoll2) delete m_viscoll2;
  }

  // Flush the stored info
  void AtlasOxbridgeKineticsInterface::clear() {
    if(m_viscoll1) m_viscoll1->clear();
    if(m_viscoll2) m_viscoll2->clear();
    m_ptmiss.Set(0.,0.);
    m_Minvis = 0.;
  }

  // Add a visible particle, supplying the parent to which it should be associated
  void AtlasOxbridgeKineticsInterface::addVis(const TLorentzVector& tlv, unsigned int parent) {
    if(parent==1) {
      if(!m_viscoll1) m_viscoll1 = new vector<TLorentzVector>;
      m_viscoll1->push_back(tlv);
    } else if(parent==2) {
      if(!m_viscoll2) m_viscoll2 = new vector<TLorentzVector>;
      m_viscoll2->push_back(tlv);
    } else {
      cerr << "AtlasOxbridgeKineticsInterface: only 1- and 2-parent variables currently defined." << endl;
    }
  }

  void AtlasOxbridgeKineticsInterface::addVisPxPyPzE(double px, double py, double pz, double E, unsigned int parent) {
    TLorentzVector tlv;
    tlv.SetPxPyPzE(px,py,pz,E);
    addVis(tlv,parent);
  }

  void AtlasOxbridgeKineticsInterface::addVisPxPyPzM(double px, double py, double pz, double M, unsigned int parent) {
    TLorentzVector tlv;
    tlv.SetXYZM(px,py,pz,M);
    addVis(tlv,parent);
  }

  void AtlasOxbridgeKineticsInterface::addVisPtEtaPhiE(double pt, double eta, double phi, double E, unsigned int parent) {
    TLorentzVector tlv;
    tlv.SetPtEtaPhiE(pt,eta,phi,E);
    addVis(tlv,parent);
  }

  void AtlasOxbridgeKineticsInterface::addVisPtEtaPhiM(double pt, double eta, double phi, double M, unsigned int parent) {
    TLorentzVector tlv;
    tlv.SetPtEtaPhiM(pt,eta,phi,M);
    addVis(tlv,parent);
  }

  // Set all the visible particles at one go
  void AtlasOxbridgeKineticsInterface::setVisibles(std::vector<TLorentzVector> parent1, std::vector<TLorentzVector> parent2) {
    m_viscoll1 = new vector<TLorentzVector>(parent1);
    m_viscoll2 = new vector<TLorentzVector>(parent2);
  }

  // Set the ptMiss 2-vector
  void AtlasOxbridgeKineticsInterface::setPtMiss(TVector2 ptmiss) {
    m_ptmiss = ptmiss;
  }

  // Set the ptMiss 2-vector
  void AtlasOxbridgeKineticsInterface::setPtMiss(double pxmiss, double pymiss) {
    setPtMiss(TVector2(pxmiss,pymiss));
  }

  // Set the invisible mass hypothesis
  void AtlasOxbridgeKineticsInterface::setMinvis(double Minvis) {
    m_Minvis = Minvis;
  }

  // Return the full set of unpartitioned visibles, sorted by Pt
  vector<TLorentzVector> AtlasOxbridgeKineticsInterface::getVisibles() {
    vector<TLorentzVector> collection;
    if(m_viscoll1) {
      for(unsigned int iVec=0; iVec<m_viscoll1->size(); iVec++) {
        collection.push_back(m_viscoll1->at(iVec));
      }
    }
    if(m_viscoll2) {
      for(unsigned int iVec=0; iVec<m_viscoll2->size(); iVec++) {
        collection.push_back(m_viscoll2->at(iVec));
      }
    }

    std::sort(collection.begin(),collection.end(), AtlasOxbridgeKineticsInterface::descendingPt);

    return collection;
  }

  // Return masses for one or both sides
  double AtlasOxbridgeKineticsInterface::getSideMass(unsigned int parent) {
    switch (parent) {
      case 1:
        if(m_viscoll1) {
          return sum(*m_viscoll1).M();
        } else {
          return 0.;
        }
      case 2:
        if(m_viscoll2) {
          return sum(*m_viscoll2).M();
        } else {
          return 0.;
        }
    }
    return 0.;
  }

  double AtlasOxbridgeKineticsInterface::getMassSum() {
    return getSideMass(1) + getSideMass(2);
  }

}
