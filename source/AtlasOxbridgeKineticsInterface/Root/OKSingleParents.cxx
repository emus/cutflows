//
// * AtlasOxbridgeKineticsInterface: an ATLAS wrapper for the mT2 and oxbridgekinetics libraries
// * Originally by Teng Jian Khoo, June 2012
// * Updated by William Fawcett, April 2019
//
// * OKTwoParents.cxx
// * Mass bound calculators for a single decaying parent (Implementation)
//

#include "AtlasOxbridgeKineticsInterface/OKSingleParents.h"
#include "TLorentzVector.h"

namespace OK {

  OKSingleParents::OKSingleParents() {
  }

  OKSingleParents::~OKSingleParents() {
  }

  // Compute the squirrel variables
  double OKSingleParents::calcM1T() {
    if(!m_viscoll1) return -1.;
    if(m_viscoll1->size()==0) return -1;
    // Early summed, late projected (T)
    TLorentzVector visproj = sum_then_projectT(*m_viscoll1);
    return singleParentBound(visproj,m_ptmiss,m_Minvis);
  }

  double OKSingleParents::calcMT1() {
    if(!m_viscoll1) return -1.;
    if(m_viscoll1->size()==0) return -1;
    // Early projected (T), late summed
    TLorentzVector visproj = projectT_then_sum(*m_viscoll1);
    return singleParentBound(visproj,m_ptmiss,m_Minvis);
  }

  double OKSingleParents::calcMo1() {
    if(!m_viscoll1) return -1.;
    if(m_viscoll1->size()==0) return -1;
    // Early projected (o), late summed
    TLorentzVector visproj = projecto_then_sum(*m_viscoll1);
    return singleParentBound(visproj,m_ptmiss,0.);
  }

  double OKSingleParents::calcM1o() {
    if(!m_viscoll1) return -1.;
    if(m_viscoll1->size()==0) return -1;
    // Early summed, late projected (o)
    TLorentzVector visproj = sum_then_projecto(*m_viscoll1);
    return singleParentBound(visproj,m_ptmiss,0.);
  }

  double OKSingleParents::calcETeff() {
    if(!m_viscoll1) return -1.;
    if(m_viscoll1->size()==0) return -1;
    TLorentzVector visproj = sum_then_projectT(*m_viscoll1);

    double Mvis = visproj.M();
    double ptvis = visproj.Pt();
    double ETvis = sqrt(Mvis*Mvis + ptvis*ptvis);
    double ETeff = ETvis + 2*m_ptmiss.Mod();

    return ETeff;
  }

  double OKSingleParents::calcMeff() {
    if(!m_viscoll1) return -1.;
    if(m_viscoll1->size()==0) return -1;

    double Meff = m_ptmiss.Mod();
    for(unsigned int iVis=0; iVis<m_viscoll1->size(); iVis++) {
      TLorentzVector vis = m_viscoll1->at(iVis);
      Meff += vis.Pt();
    }

    return Meff;
  }

}
