//
// * AtlasOxbridgeKineticsInterface: an ATLAS wrapper for the mT2 and oxbridgekinetics libraries
// * Originally by Teng Jian Khoo, June 2012
// * Updated by William Fawcett, April 2019
//
// * OKTwoParents.cxx
// * Mass bound calculators for two decaying parents (Implementation)
//

#include "AtlasOxbridgeKineticsInterface/OKTwoParents.h"
#include <cmath>
#include <algorithm>

//#include "Mt2/ChengHanBisect_Mt2_332_Calculator.h"
#include "Mt2/LesterNachmanBisect_Mt2_3322_Calculator.h"
#include "Mt2/Frugal_MtGen_332_Calculator.h"
#include "Mt2/LesterNachmanBisect_Mt2_3322_Calculator.h"
#include "Mt2/MCT_330_Calculator.h"
#include "Mt2/Mt2Vectors.h"

namespace OK {
  using namespace Mt2;
  using std::cout;
  using std::cerr;
  using std::endl;

  OKTwoParents::OKTwoParents() :
    m_mtgen_calc(0),
    m_mt2_calc(0),
    m_amt2_calc(0)
  {
  }

  OKTwoParents::~OKTwoParents() {
    if(m_mt2_calc)   delete m_mt2_calc;
    if(m_mtgen_calc) delete m_mtgen_calc;
    if(m_amt2_calc) delete m_amt2_calc;
  }

  // Compute the 2-parent squirrel variables

  double OKTwoParents::calcM2T() {
    if(!m_viscoll1 || !m_viscoll2) return -1.;
    if(m_viscoll1->size()==0 || m_viscoll2->size()==0) return -1.;
    if(!m_mt2_calc) m_mt2_calc = new LesterNachmanBisect_Mt2_332_Calculator;
    // Early summed, late projected (T)
    TLorentzVector visproj1 = sum_then_projectT(*m_viscoll1);
    TLorentzVector visproj2 = sum_then_projectT(*m_viscoll2);
    // Translate to MT2 classes and compute
    LorentzTransverseVector ltv1 = tlv_to_ltv(visproj1);
    LorentzTransverseVector ltv2 = tlv_to_ltv(visproj2);
    TwoVector ptmiss = tv2_to_2v(m_ptmiss);
    return m_mt2_calc->mt2_332(ltv1,ltv2,ptmiss,m_Minvis);
  }

  double OKTwoParents::calcAsymM2T(const double MinvisA, const double MinvisB) {
    if(!m_viscoll1 || !m_viscoll2) return -1.;
    if(m_viscoll1->size()==0 || m_viscoll2->size()==0) return -1.;
    if(!m_mt2_calc) m_mt2_calc = new LesterNachmanBisect_Mt2_332_Calculator;
    // Early summed, late projected (T)
    TLorentzVector visproj1 = sum_then_projectT(*m_viscoll1);
    TLorentzVector visproj2 = sum_then_projectT(*m_viscoll2);
    // Translate to MT2 classes and compute
    LorentzTransverseVector ltv1 = tlv_to_ltv(visproj1);
    LorentzTransverseVector ltv2 = tlv_to_ltv(visproj2);
    TwoVector ptmiss = tv2_to_2v(m_ptmiss);
    return m_amt2_calc->mt2_3322(ltv1, ltv2, ptmiss, MinvisA, MinvisB);
  }

  double OKTwoParents::calcMT2() {
    if(!m_viscoll1 || !m_viscoll2) return -1.;
    if(m_viscoll1->size()==0 || m_viscoll2->size()==0) return -1.;
    if(!m_mt2_calc) m_mt2_calc = new LesterNachmanBisect_Mt2_332_Calculator;
    // Early projected (T), late summed
    TLorentzVector visproj1 = projectT_then_sum(*m_viscoll1);
    TLorentzVector visproj2 = projectT_then_sum(*m_viscoll2);
    // Translate to MT2 classes and compute
    LorentzTransverseVector ltv1 = tlv_to_ltv(visproj1);
    LorentzTransverseVector ltv2 = tlv_to_ltv(visproj2);
    TwoVector ptmiss = tv2_to_2v(m_ptmiss);
    return m_mt2_calc->mt2_332(ltv1,ltv2,ptmiss,m_Minvis);
  }

  double OKTwoParents::calcAsymMT2(const double MinvisA, const double MinvisB){
    if(!m_viscoll1 || !m_viscoll2) return -1.;
    if(m_viscoll1->size()==0 || m_viscoll2->size()==0) return -1.;
    if(!m_amt2_calc) m_amt2_calc = new LesterNachmanBisect_Mt2_3322_Calculator;
    // Early projected (T), late summed
    TLorentzVector visproj1 = projectT_then_sum(*m_viscoll1);
    TLorentzVector visproj2 = projectT_then_sum(*m_viscoll2);
    // Translate to MT2 classes and compute
    LorentzTransverseVector ltv1 = tlv_to_ltv(visproj1);
    LorentzTransverseVector ltv2 = tlv_to_ltv(visproj2);
    TwoVector ptmiss = tv2_to_2v(m_ptmiss);
    return m_amt2_calc->mt2_3322(ltv1, ltv2, ptmiss, MinvisA, MinvisB);
  }

  double OKTwoParents::calcMo2() {
    if(!m_viscoll1 || !m_viscoll2) return -1.;
    if(m_viscoll1->size()==0 || m_viscoll2->size()==0) return -1.;
    if(!m_mt2_calc) m_mt2_calc = new LesterNachmanBisect_Mt2_332_Calculator;
    // Early projected (o), late summed
    TLorentzVector visproj1 = projecto_then_sum(*m_viscoll1);
    TLorentzVector visproj2 = projecto_then_sum(*m_viscoll2);
    // Translate to MT2 classes and compute
    LorentzTransverseVector ltv1 = tlv_to_ltv(visproj1);
    LorentzTransverseVector ltv2 = tlv_to_ltv(visproj2);
    TwoVector ptmiss = tv2_to_2v(m_ptmiss);
    return m_mt2_calc->mt2_332(ltv1,ltv2,ptmiss,0.);
  }

  double OKTwoParents::calcM2o() {
    if(!m_viscoll1 || !m_viscoll2) return -1.;
    if(m_viscoll1->size()==0 || m_viscoll2->size()==0) return -1.;
    if(!m_mt2_calc) m_mt2_calc = new LesterNachmanBisect_Mt2_332_Calculator;
    // Early summed, late projected (o)
    TLorentzVector visproj1 = sum_then_projecto(*m_viscoll1);
    TLorentzVector visproj2 = sum_then_projecto(*m_viscoll2);
    // Translate to MT2 classes and compute
    LorentzTransverseVector ltv1 = tlv_to_ltv(visproj1);
    LorentzTransverseVector ltv2 = tlv_to_ltv(visproj2);
    TwoVector ptmiss = tv2_to_2v(m_ptmiss);
    return m_mt2_calc->mt2_332(ltv1,ltv2,ptmiss,0.);
  }

  // Calculate versions of MTGen using the squirrel transverse projections
  double OKTwoParents::calcM2T_gen() {
    return calcMTGen(&OK::OKTwoParents::calcM2T, m_Minvis);
  }

  double OKTwoParents::calcMT2_gen() {
    return calcMTGen(&OK::OKTwoParents::calcMT2, m_Minvis);
  }

  double OKTwoParents::calcMo2_gen() {
    return calcMTGen(&OK::OKTwoParents::calcMo2, 0.);
  }

  double OKTwoParents::calcM2o_gen() {
    return calcMTGen(&OK::OKTwoParents::calcM2o, 0.);
  }

  // The master method, which uses a function pointer to the
  // summation/projection convention
  // Partially ported from the Frugal_MtGen_332_Calculator
  double OKTwoParents::calcMTGen(double (OK::OKTwoParents::*mt2_def)(),
      const double minvis) {
    vector<TLorentzVector> collection;
    if(m_viscoll1) {
      for(unsigned int iVec=0; iVec<m_viscoll1->size(); iVec++) {
        collection.push_back(m_viscoll1->at(iVec));
      }
    }
    if(m_viscoll2) {
      for(unsigned int iVec=0; iVec<m_viscoll2->size(); iVec++) {
        collection.push_back(m_viscoll2->at(iVec));
      }
    }

    //Find total mom
    const TLorentzVector totalMom = sum(collection);

    // First interesting particle will always be on SOME side.
    // WLOG we choose to name the side that IT is on as "side 1".
    // One example of this is ALL ON SIDE 1 and NOTHING ON SIDE 2.
    /// We will use this example to set things going.

    // The starting point is a single conglomerate, which should not be the final answer!
    double mtGen = sum(collection).M();

    unsigned long long encoding = 0;
    tryAlsoFrom(1,
        collection,
        encoding,
        mt2_def,
        minvis,
        mtGen);

    return mtGen;
  }

  // Helper for the M2G, MG2, Mg2, M2g methods
  // Ported from the Frugal_MtGen_332_Calculator
  void OKTwoParents::tryAlsoFrom(unsigned int index,
      const vector<TLorentzVector> &collection,
      const unsigned long long encoding,
      double (OK::OKTwoParents::*mt2_def)(),
      const double minvis,
      double & bestMt2SoFar
      ) {

    for(; // start with the given assignments
        index < collection.size();
        index++
        // this determines which the first particle to be added to
        // side 2 will be.
       ) {
      unsigned long long newencoding = encoding + (1<<(index));
      reassign_by_encoding(newencoding);
      const double mT2_2Min = sqrt(pow(sum(*m_viscoll2).M() + minvis,2));
      if (mT2_2Min >= bestMt2SoFar) {
        //This partition makes 2 too heavy, so forget it.
        // Futhermore, don't bother adding anything extra to 2 as this will only make 2 heavier.
        // So just continue, initialising side 2 with the next particle up.
        continue;
      }
      const double mT2_1Min = sqrt(pow(sum(*m_viscoll1).M() + minvis,2));
      if (mT2_1Min >= bestMt2SoFar) {
        // THIS partition is not a winner, but we may yet get one by adding more things to side 2,
        // so unlike the 2 case above, we DON'T "continue" here !
      } else {
        // We had better actually calculate MT2 now and see if we get a new winner.
        const double mt2 = (this->*mt2_def)();
        if (mt2 < bestMt2SoFar) {
          bestMt2SoFar = mt2;
        }
        // might still get a better winner by adding more things to 2 ... 
        // due to the magic freedoms in the behaviour of the missing momenta splittings ... so don't continue!
      }

      tryAlsoFrom(index+1,
          collection,
          newencoding,
          mt2_def,
          minvis,
          bestMt2SoFar);
    }
  }

  // Other 2-parent variables
  double OKTwoParents::calcMCT(unsigned int projtype) {
    if(!m_viscoll1 || !m_viscoll2) return -1.;
    // The naming conventions are not so straightforward to apply
    // So just use a switch to pick which one
    TLorentzVector visproj1;
    TLorentzVector visproj2;
    switch(projtype) {
      case 0: // early sum, late projection (mass-preserving)
        visproj1 = sum_then_projectT(*m_viscoll1);
        visproj2 = sum_then_projectT(*m_viscoll2);
        break;
      case 1: // early projection, late sum (mass-preserving)
        visproj1 = projectT_then_sum(*m_viscoll1);
        visproj2 = projectT_then_sum(*m_viscoll2);
        break;
      case 2: // early projection, late sum (mass-discarding)
        visproj1 = projecto_then_sum(*m_viscoll1);
        visproj2 = projecto_then_sum(*m_viscoll2);
        break;
      case 3: // early sum, late projection (mass-discarding)
        visproj1 = sum_then_projecto(*m_viscoll1);
        visproj2 = sum_then_projecto(*m_viscoll2);
        break;
    }
    // Translate to MT2 classes and compute
    LorentzTransverseVector ltv1 = tlv_to_ltv(visproj1);
    LorentzTransverseVector ltv2 = tlv_to_ltv(visproj2);
    return mct_330(ltv1,ltv2);
  }

  // M_R sometimes breaks because of requiring unphysical boosts.
  // So does M_R*, so both ATLAS and CMS multiply by gamma_R*.
  double OKTwoParents::calcGammaMRstar(unsigned int projtype) {
    if(!m_viscoll1 || !m_viscoll2) return -1.;

    // The naming conventions are not so straightforward to apply
    // So just use a switch to pick which one
    TLorentzVector visproj1;
    TLorentzVector visproj2;
    switch(projtype) {
      case 0: // early sum, late projection (mass-preserving)
        visproj1 = sum_then_projectT(*m_viscoll1);
        visproj2 = sum_then_projectT(*m_viscoll2);
        break;
      case 1: // early projection, late sum (mass-preserving)
        visproj1 = projectT_then_sum(*m_viscoll1);
        visproj2 = projectT_then_sum(*m_viscoll2);
        break;
      case 2: // early projection, late sum (mass-discarding)
        visproj1 = projecto_then_sum(*m_viscoll1);
        visproj2 = projecto_then_sum(*m_viscoll2);
        break;
      case 3: // early sum, late projection (mass-discarding)
        visproj1 = sum_then_projecto(*m_viscoll1);
        visproj2 = sum_then_projecto(*m_viscoll2);
        break;
    }

    // Really, this is just the transverse energy
    double M12 = (visproj1+visproj2).M();
    double pt12 = (visproj1+visproj2).Pt();
    double gammaMRstar = sqrt(M12*M12 + pt12*pt12);

    return gammaMRstar;
  }

  double OKTwoParents::calcMRT(unsigned int projtype) {
    if(!m_viscoll1 || !m_viscoll2) return -1.;

    // The naming conventions are not so straightforward to apply
    // So just use a switch to pick which one
    TLorentzVector visproj1;
    TLorentzVector visproj2;
    switch(projtype) {
      case 0: // early sum, late projection (mass-preserving)
        visproj1 = sum_then_projectT(*m_viscoll1);
        visproj2 = sum_then_projectT(*m_viscoll2);
        break;
      case 1: // early projection, late sum (mass-preserving)
        visproj1 = projectT_then_sum(*m_viscoll1);
        visproj2 = projectT_then_sum(*m_viscoll2);
        break;
      case 2: // early projection, late sum (mass-discarding)
        visproj1 = projecto_then_sum(*m_viscoll1);
        visproj2 = projecto_then_sum(*m_viscoll2);
        break;
      case 3: // early sum, late projection (mass-discarding)
        visproj1 = sum_then_projecto(*m_viscoll1);
        visproj2 = sum_then_projecto(*m_viscoll2);
        break;
    }

    double mpt = m_ptmiss.Mod();
    double pt_scalar_sum = visproj1.Pt()+visproj2.Pt();
    double pt_vector_sum = (visproj1+visproj2).Pt();
    double dphi = m_ptmiss.DeltaPhi( (visproj1+visproj2).Vect().XYvector() );
    double MRT = sqrt( 0.5 * (mpt*pt_scalar_sum - mpt * pt_vector_sum * cos(dphi)) );

    return MRT;
  }

  void OKTwoParents::reassign_by_encoding(const vector<TLorentzVector> &collection, unsigned long long encoding) {
    unsigned long long maxencoding = (1<<(collection.size()))-1;
    if(encoding >= maxencoding) {
      cerr << "OKTwoParents: Invalid encoding: " << encoding << " -- this must be less than " << maxencoding << endl;
      cerr << "OKTwoParents: Will do nothing." << endl;
      return;
    } else if(encoding == 0) {
      cerr << "OKTwoParents: Invalid encoding: " << encoding << " -- must have at least one particle per side" << endl;
      cerr << "OKTwoParents: Will do nothing." << endl;
      return;
    }

    if(m_viscoll1) m_viscoll1->clear();
    if(m_viscoll2) m_viscoll2->clear();

    for(unsigned int ipart=0; ipart<collection.size(); ipart++) {
      AtlasOxbridgeKineticsInterface::addVis(collection.at(ipart),( (encoding>>ipart) & 1)+1);
    }

    if( m_viscoll1->size() + m_viscoll2->size() > collection.size() )
      cerr << "OKTwoParents: Oops, I messed up in reassignment." << endl;
  }

  void OKTwoParents::recluster_minMinv() {
    vector<TLorentzVector> collection;
    if(m_viscoll1) {
      for(unsigned int iVec=0; iVec<m_viscoll1->size(); iVec++) {
        collection.push_back(m_viscoll1->at(iVec));
      }
    }
    if(m_viscoll2) {
      for(unsigned int iVec=0; iVec<m_viscoll2->size(); iVec++) {
        collection.push_back(m_viscoll2->at(iVec));
      }
    }
    if(collection.size()<2) return;
    // Final mass sum of two subcollections must be less than the mass of the collection
    float smallest_minvsum = sum(collection).M();
    unsigned long long bestencoding = 1;
    for(unsigned long long encoding = 1; encoding < (1u<<(collection.size()-1)); encoding++) {
      // only need half the encodings because the other half are complementary
      reassign_by_encoding(collection, encoding);
      float sum1(sum(*m_viscoll1).M()), sum2(sum(*m_viscoll2).M());
      float minvsum = sum1 + sum2;
      if(minvsum < smallest_minvsum) {
        smallest_minvsum = minvsum;
        bestencoding = encoding;
      }
    }
    reassign_by_encoding(collection,bestencoding);
  }

  void OKTwoParents::recluster_symMinv() {
    vector<TLorentzVector> collection;
    if(m_viscoll1) {
      for(unsigned int iVec=0; iVec<m_viscoll1->size(); iVec++) {
        collection.push_back(m_viscoll1->at(iVec));
      }
    }
    if(m_viscoll2) {
      for(unsigned int iVec=0; iVec<m_viscoll2->size(); iVec++) {
        collection.push_back(m_viscoll2->at(iVec));
      }
    }
    if(collection.size()<2) return;
    // Final mass difference of two subcollections must be less than the mass of the collection
    double smallest_minvdiff = sum(collection).M();
    unsigned long long bestencoding = 1;
    for(unsigned long long encoding = 1; encoding < (1u<<(collection.size()-1)); encoding++) {
      // only need half the encodings because the other half are complementary
      reassign_by_encoding(collection, encoding);
      double minvdiff = fabs( sum(*m_viscoll1).M() - sum(*m_viscoll2).M() );
      if(minvdiff < smallest_minvdiff) {
        smallest_minvdiff = minvdiff;
        bestencoding = encoding;
      }
    }
    reassign_by_encoding(collection,bestencoding);
  }


  void OKTwoParents::recluster_maxBoostedP() {
    vector<TLorentzVector> collection;
    vector<TLorentzVector> boosted_collection;
    if(m_viscoll1) {
      for(unsigned int iVec=0; iVec<m_viscoll1->size(); iVec++) {
        collection.push_back(m_viscoll1->at(iVec));
        boosted_collection.push_back(m_viscoll1->at(iVec));
      }
    }
    if(m_viscoll2) {
      for(unsigned int iVec=0; iVec<m_viscoll2->size(); iVec++) {
        collection.push_back(m_viscoll2->at(iVec));
        boosted_collection.push_back(m_viscoll2->at(iVec));
      }
    }
    if(collection.size()<2) return;
    // Switch to CoM frame
    TVector3 boost = sum(collection).BoostVector();
    for(auto& tlv : boosted_collection) {
      tlv.Boost(-boost);
    }
    float max_boostedP = 0.;
    unsigned long long bestencoding = 1;
    for(unsigned long long encoding = 1; encoding < (1u<<(boosted_collection.size()-1)); encoding++) {
      // only need half the encodings because the other half are complementary
      reassign_by_encoding(boosted_collection, encoding);
      float boostedP = sum(*m_viscoll1).P();
      if(boostedP > max_boostedP) {
        max_boostedP = boostedP;
        bestencoding = encoding;
      }
    }
    // finish by reassigning the unboosted jets
    reassign_by_encoding(collection,bestencoding);
  }

}
