// Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration

// Local include(s):
#include "AtlasOxbridgeKineticsInterface/TestClass.h"

void TestClass::dummy() {

  m_vec.setEPxPyPz( 1.0, 2.0, 3.0, 4.0 );
  return;
}
