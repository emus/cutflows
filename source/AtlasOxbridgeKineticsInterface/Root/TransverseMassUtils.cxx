//
// * AtlasOxbridgeKineticsInterface: an ATLAS wrapper for the mT2 and oxbridgekinetics libraries
// * Originally by Teng Jian Khoo, June 2012
// * Updated by William Fawcett, April 2019
//
// * TransverseMassUtils.cxx
// * Define various helper functions for transverse projections
// * and easily-calculable mass bounds (Implementation)
//

#include "AtlasOxbridgeKineticsInterface/TransverseMassUtils.h"
#include "Mt2/Mt2Vectors.h"
#include "TVector2.h"

namespace OK {
  using namespace Mt2;

  // Define the transverse projections
  TLorentzVector projectT(const TLorentzVector &tlv) {
    double newE = sqrt(tlv.E()*tlv.E() - tlv.Pz()*tlv.Pz());
    TLorentzVector newtlv(tlv.Px(), tlv.Py(), 0., newE);
    return newtlv;
  }

  TLorentzVector projecto(const TLorentzVector &tlv) {
    TLorentzVector newtlv(tlv.Px(), tlv.Py(), 0., tlv.Pt());
    return newtlv;
  }

  // A simple sum of TLVs
  TLorentzVector sum(const vector<TLorentzVector> &collection) {
    TLorentzVector composite(0.,0.,0.,0.);
    for(unsigned int iVis=0; iVis<collection.size(); iVis++) {
      composite += collection.at(iVis);
    }
    return composite;
  }

  // Combinations of composition and projection in different orders
  TLorentzVector sum_then_projectT(const vector<TLorentzVector> &collection) {
    // Project late, preserving mass
    TLorentzVector composite(0.,0.,0.,0.);
    for(unsigned int iVis=0; iVis<collection.size(); iVis++) {
      composite += collection.at(iVis);
    }
    return projectT(composite);
  }

  TLorentzVector projectT_then_sum(const vector<TLorentzVector> &collection) {
    TLorentzVector composite(0.,0.,0.,0.);
    for(unsigned int iVis=0; iVis<collection.size(); iVis++) {
      // Project early, preserving mass
      composite += projectT(collection.at(iVis));
    }
    return composite;
  }

  TLorentzVector projecto_then_sum(const vector<TLorentzVector> &collection) {
    TLorentzVector composite(0.,0.,0.,0.);
    for(unsigned int iVis=0; iVis<collection.size(); iVis++) {
      // Project early, discarding mass
      composite += projecto(collection.at(iVis));
    }
    return composite;
  }

  TLorentzVector sum_then_projecto(const vector<TLorentzVector> &collection) {
    // Project late, discarding mass
    TLorentzVector composite(0.,0.,0.,0.);
    for(unsigned int iVis=0; iVis<collection.size(); iVis++) {
      composite += collection.at(iVis);
    }
    return projecto(composite);
  }

  // Compute the single parent bound == M_T

  double singleParentBound(const TLorentzVector &visible, TVector2 ptmiss, double Minvis) {

    TLorentzVector visproj = projectT(visible);
    TLorentzVector invisproj;
    invisproj.SetXYZM(ptmiss.Px(), ptmiss.Py(), 0., Minvis);

    TLorentzVector sumproj = visproj+invisproj;
    double MTsquared = sumproj.E()*sumproj.E() - sumproj.Pt()*sumproj.Pt();

    return sqrt(MTsquared);
  }

  // Functions to convert ROOT vectors into Mt2 vectors
  LorentzVector tlv_to_lv(const TLorentzVector &tlv) {
    LorentzVector lv;
    lv.setEPxPyPz(tlv.E(),tlv.Px(),tlv.Py(),tlv.Pz());
    return lv;
  }

  LorentzTransverseVector tlv_to_ltv(const TLorentzVector &tlv) {
    LorentzTransverseVector ltv = tlv_to_lv(tlv).getLorentzTransverseVector();
    return ltv;
  }

  TwoVector tv2_to_2v(TVector2 tv2) {
    TwoVector tv(tv2.Px(),tv2.Py());
    return tv;
  }

  unsigned int closestParticle(const vector<TLorentzVector> &collection,
      double (*distance_measure)
      (const TLorentzVector&, const TLorentzVector&),
      const TLorentzVector &tlv,
      double &min_distance) {

    int closestid = 0;
    min_distance = 1e9;
    for(unsigned int ipart=0; ipart<collection.size(); ipart++) {
      double distance = (*distance_measure)(tlv, collection.at(ipart));
      if(distance < min_distance) {
        min_distance = distance;
        closestid = ipart;
      }
    }

    return closestid;
  }

}
