#ifndef CutChain_CHAIN_H
#define CutChain_CHAIN_H

#include <functional>
#include <vector>
#include <string>
#include <memory>

#include <TDirectory.h>
#include <fstream>

class TH1;

namespace CutChain
{
  class Cut;
  class EndNode;
  
  class Chain
  {
    friend class EndNode;
    public:
      // ----------------------------------------
      // Constructor
      // ----------------------------------------
      Chain(const std::string& startCutName = "Number of events");
      
      ~Chain();

      // Explicitly suppress copy constructors
      Chain(const Chain&) = delete;
      Chain operator=(const Chain&) = delete;

      std::unique_ptr<Cut> startCut;

      const std::vector<std::string>& regionNames() const { return m_regionNames; }

      bool passEvent(float weight = 1., bool debug = false);

      void initializeAll();

      void saveOutput(bool concise, TDirectory* outFile, int NevtsAOD, float XStimesBR);

      // Alternative output method where you provide the initial cutflow
      void saveOutput(bool concise, TDirectory* outFile, TH1* h_cutflow, TH1* h_cutflowWeighted);

      bool suppressAllPlots;

      bool onlyOutputFinal;

      void print();

      void printTikzFile(std::ofstream& fout) const;

    private:

      TDirectory* currentOutDir;

      std::vector<std::string> m_regionNames;
  };
}


#endif //CutChain_CHAIN_H
