#ifndef CutChain_CHAINSEGMENT_H
#define CutChain_CHAINSEGMENT_H

#include <functional>
#include <vector>
#include <memory>
#include "CutChain/Chain.h"
#include "CutChain/CutBase.h"

namespace CutChain
{
  class Cut;
  class Node;
  class TObjWrapper;
  
  class ChainSegment
  {
    friend class Cut;
    friend class Node;
    public:
      // ----------------------------------------
      // Constructor
      // ----------------------------------------
      ChainSegment(const std::string& name, std::function<bool()> cutFunc, bool doPlotting = true);

      ~ChainSegment();

      // Explicitly suppress copy constructors
      ChainSegment(const ChainSegment&) = delete;
      ChainSegment operator=(const ChainSegment&) = delete;

      // All functions apply to the last cut added to the segment
      void addTObj(TObjWrapper& tobj, bool printWarnings = true);
      void modifyAllTObjs(TObjEnum setting);
      bool modifyTObj(const std::string& tobjName, TObjEnum setting);

      // Add a cut after this cut in the chain. If a cut or a node already exists there it will be deleted 
      void addCut(const std::string& name, std::function<bool()> cutFunc, bool doPlotting = true);

      // Copy a chain segment in after this cut. If a cut or a node already exists there it will be deleted. A pointer to the last cut in the segment is returned
      void addChainSegment(const ChainSegment& segment);

      // Provide a function that increases the weight of any event passed through this cut 
      // (eventWeight = previousEventWeight * weightFunc();)
      void increaseWeight(std::function<float()> weightFunc);
    private:
      Chain m_dummyChain;
      Cut* copyAfterCut(Cut* cut) const;

      Cut* copyAfterNode(const std::string& subchainName, Node* node) const;

      std::unique_ptr<Cut> m_firstCut;
      Cut* m_lastCut;
  };
}



      


#endif //CutChain_CHAINSEGMENT_H
