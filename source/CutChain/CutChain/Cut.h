#ifndef CutChain_CUT_H
#define CutChain_CUT_H

#include "CutChain/CutBase.h"

#include <string>
#include <functional>
#include <tuple>
#include <map>
#include <exception>

namespace CutChain {
  // Forward declarations
  // Typedef and enum for extra variables in the cutflow
  typedef std::tuple<std::string, std::function<bool()>, long, double , double, double> extraCutflowVar;
  namespace ECV { enum ECV{ NAME = 0, FUNC = 1, NPASSED = 2, NPASSED_ERR = 3, NPASSED_WEIGHT = 4, NPASSED_WEIGHT_ERR = 5 }; }

  class EndNode;
  class Node;
  class ChainSegment;
  class Chain;

  class Cut : public CutBase
  {
    public:
    friend class Node;
    friend class ChainSegment;
    friend class Chain;
    friend class TObjWrapper;

      //Exceptions
      struct chainNameAlreadyUsed : public std::exception {
        chainNameAlreadyUsed(const std::string& chainName) : chainName(chainName) {}
        std::string chainName;
        const char *  what() const throw() {
          return ("Chain name "+chainName+" is already in use (will cause a name conflict in the final TFile)").c_str();
        }
      };

      // Destructor
      ~Cut();
      // Explicitly suppress copy contructors
      Cut(const Cut&) = delete;
      Cut operator=(const Cut&) = delete;

      // Inherited from CutBase
      void addTObj(const TObjWrapper& tobj, CutEnum passAlong = HERE, bool printWarnings = true) override;
      void modifyAllTObjs(TObjEnum setting, CutEnum passAlong = HERE) override;
      bool modifyTObj(const std::string& tobjName, TObjEnum setting, CutEnum = HERE) override;

      // Add a cut after this cut in the chain. If a cut or a node already exists there it will be deleted 
      Cut* addCut(const std::string& name, std::function<bool()> cutFunc, bool doPlotting = true);

      // Copy a chain segment in after this cut. If a cut or a node already exists there it will be deleted. A pointer to the last cut in the segment is returned
      Cut* addChainSegment(const ChainSegment& segment);

      // Adds a node after this cut in the chain. If a cut or a node already exists there it will be deleted.
      Node* splitChain();

      // Finishes this branch here. The supplied string is the name of the region. This will throw chainNameAlreadyUsed if the name already exists in the parent Chain object.
      EndNode* finishChain(const std::string& regionName);

      // gets the pointer to the next cut. If it doesn't exist or isn't of the right type targetCut becomes null and false is returned.
      bool getCut(Cut*& targetCut);
      bool getCut(Node*& targetCut);
      bool getCut(EndNode*& targetCut);

      // Tries to create a new cut. If it succeeds fill the new cut into targetCut and return true
      // Otherwise if this has a nextCut (of type Cut*) AND which has the same name (it's not possible to check if the function is the same - BE WARNED!) it will fill it into targetCut and return true
      // Otherwise targetCut will be null and it will return false
      bool getAddCut(Cut*& targetCut, const std::string& name, std::function<bool()> cutFunc, bool doPlotting = true);
      
      // Equivalent functions for splitChain and finishChain
      bool getSplitChain(Node*& targetCut);
      bool getFinishChain(EndNode*& targetCut, const std::string& regionName);

      // Add an extra variable (to be plotted *after* this one) to the cutflow
      void addExtraCutflowVar(const std::string& varName, std::function<bool()> varFunc);

      // Provide a function that increases the weight of any event passed through this cut 
      // (eventWeight = previousEventWeight * weightFunc();)
      void increaseWeight(std::function<float()> weightFunc) { m_weightFunc = weightFunc; }

      // Get the weight applied at this cut (previousEventWeight * weightFunc() ).
      // This is only guaranteed to be valid in a call of passEvent or passNMinusMHists
      float getWeight() const {return m_weight;}

      void setIsIgnored(bool isIgnored) { m_isIgnored = isIgnored; }

      // Find all cuts in the given search pattern with the given name
      std::vector<Cut*> findCuts(const std::string& cutName, CutEnum passAlong = BOTH) override;
      std::vector<Cut*> findCuts(const std::regex& cutRegex, CutEnum passAlong = BOTH) override;

      long getNPassedEvents() { return m_nPassedEvents; }
      double getNPassedEventsError() { return m_nPassedEventsError; }
      double getNPassedEventsWeighted() { return m_nPassedEventsWeighted; }
      double getNPassedEventsWeightedError() { return m_nPassedEventsWeightedError; }
      std::string getName() const { return m_name; }
      std::string getFullName(std::string name = "") const override;

      void enablePlotting(bool plotHistograms = true) { 
        m_plotHistograms = plotHistograms;
        if (plotHistograms) modifyAllTObjs(ENABLE);
        else modifyAllTObjs(SUPPRESS);
      }

      virtual Cut* getPreviousCut(bool countThis = false) override;
      virtual Node* getPreviousNode(bool countThis = false) override;

      virtual const Cut* getPreviousCut(bool countThis = false) const override;
      virtual const Node* getPreviousNode(bool countThis = false) const override;
      
    private:
      // ------------------------------------------
      // Constructors
      // ------------------------------------------

      Cut(Chain& parent, CutBase* previousCut, const std::string& name, std::function<bool()> cutFunc, const std::vector<std::unique_ptr<TObjWrapper>>& tobjWrappers, bool doPlotting);

      std::string m_name;

      std::function<bool()> m_cutFunc;

      CutBase* m_previousCut;
      // The cut 'owns' the cut downstream from it - this means that deleting the start of the chain deletes everything!
      std::unique_ptr<CutBase> m_nextCut;

      float m_weight;
      std::function<float()> m_weightFunc;

      // Cutflow variables
      long m_nPassedEvents;
      double m_nPassedEventsError;
      double m_nPassedEventsWeighted;
      double m_nPassedEventsWeightedError;
      std::vector<extraCutflowVar> m_extraCutflowVars;

      // This map contains the TObjWrappers that have ignored cuts
      std::map<const Cut*, std::vector<TObjWrapper*>> m_tobjWrappersNMinusM;

      bool m_isIgnored;
      bool m_plotHistograms;

      bool passEvent(float weight = 1., bool debug = false) override;

      // If some cuts can be ignored (in order to make plots without them) then this function will be called after the first one fails passEvent
      void passNMinusMHists(float weight = 1., std::vector<const Cut*> ignoredCuts = std::vector<const Cut*>(), bool debug = false ) override;

      void saveOutput(bool concise, TDirectory* outDir, TH1* cutflow, TH1* cutflowWeighted, int cutNum) const override;

      void print() const override;
      void printTikzFile(std::ofstream& fout, int tabLevel) const override;

      void initializeAll() override;
      void cleanupTObjs() override;

      void setSubchainName(const std::string& subchainName, CutBase* nextCut) override;
  };
}

#endif //CutChain_CUT_H
