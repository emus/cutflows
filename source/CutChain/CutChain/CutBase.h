#ifndef CutChain_CUTBASE_H
#define CutChain_CUTBASE_H

#include <vector>
#include <TDirectory.h>
#include <TH1.h>
#include <memory>
#include <regex>

#include <exception>
#include <fstream>

#include "CutChain/TObjWrapper.h"

// required forward declarations
namespace CutChain
{

  // Enum to define how functions can pass along the chain
  // HERE: only applied to the cut on which it's called
  // UPSTREAM: applied to this cut and all directly upstream of it (doesn't backtrack along other branches that split upstream)
  // DOWNSTREAM: applied to this cut and all defined cuts/branches downstream
  // BOTH: applies both upstream and downstream
  enum CutEnum{ HERE, UPSTREAM, DOWNSTREAM, BOTH };

  // Enum to define how functions interact with TObjWrappers
  // SUPPRESS: ignore on this cut (don't initialize, execute or save). Will be inherited as active by following cuts
  // ENABLE: reverse suppress
  // REMOVE: deletes the TObjWrapper from this cut.
  enum TObjEnum{ SUPPRESS, ENABLE, REMOVE };

  class Chain;
  class Cut;
  class Node;
  class EndNode;

  // Base class for the Cut and Node objects
  class CutBase 
  {
    friend class Cut;
    friend class Node;
    friend class EndNode;
    public:
      struct chainNotFinished : public std::exception {
        chainNotFinished() {}
        const char* what() const throw() {
          return "Chain not finished!";
        }
      };

      CutBase(Chain& parent);

      virtual ~CutBase();

      // Maintain a link to the parent chain
      Chain& parent;

      virtual void addTObj(const TObjWrapper& tobj, CutEnum passAlong = HERE, bool printWarnings = true) = 0;
      virtual void modifyAllTObjs(TObjEnum setting, CutEnum passAlong = HERE) = 0;
      // returns false if the object isn't found on any cut
      virtual bool modifyTObj(const std::string& tobjName, TObjEnum setting, CutEnum = HERE) = 0;

      // Find all cuts in the given search pattern with the given name
      virtual std::vector<Cut*> findCuts(const std::string& cutName, CutEnum passAlong = BOTH) = 0;
      virtual std::vector<Cut*> findCuts(const std::regex& cutName, CutEnum passAlong = BOTH) = 0;

      virtual Cut* getPreviousCut(bool countThis = false) = 0;
      virtual Node* getPreviousNode(bool countThis = false) = 0;
      virtual const Cut* getPreviousCut(bool countThis = false) const = 0;
      virtual const Node* getPreviousNode(bool countThis = false) const = 0;

      std::vector<std::unique_ptr<TObjWrapper>> m_tobjWrappers;
      virtual std::string getFullName(std::string name = "") const = 0;

    protected:
      virtual bool passEvent(float weight = 1., bool debug = false) = 0;

      // If some cuts can be ignored (in order to make plots without them) then this function will be called after the first one fails passEvent
      virtual void passNMinusMHists(float weight = 1., std::vector<const Cut*> ignoredCuts = std::vector<const Cut*>(), bool debug=false) = 0;

      virtual void saveOutput(bool concise, TDirectory* outDir, TH1* cutflow, TH1* cutflowWeighted, int cutNum) const = 0;

      virtual void print() const = 0;
      virtual void printTikzFile(std::ofstream& fout, int tabLevel) const = 0;

      virtual void initializeAll() = 0;
      virtual void cleanupTObjs() = 0;


      virtual void setSubchainName(const std::string& subchainName, CutBase* nextCut) = 0;
  };
}

#endif //CutChain_CUTBASE_H
