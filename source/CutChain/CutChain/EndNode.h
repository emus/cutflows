#ifndef CutChain_ENDNODE_H
#define CutChain_ENDNODE_H

#include "CutChain/CutBase.h"

namespace CutChain
{
  class Cut;
  class Node;
  class EndNode : public CutBase
  {
    friend class Cut;
    friend class Node;
    public:
      // Destructor
      ~EndNode();

      // Explicitly suppress copy constructors
      EndNode(const EndNode&) = delete;
      EndNode operator=(const EndNode&) = delete;

      void addTObj(const TObjWrapper& tobj, CutEnum passAlong = HERE, bool printWarnings = true) override;
      void modifyAllTObjs(TObjEnum setting, CutEnum passAlong = HERE) override;
      // returns false if the object isn't found on any cut
      bool modifyTObj(const std::string& tobjName, TObjEnum setting, CutEnum = HERE) override;

      // Find all cuts in the given search pattern with the given name
      std::vector<Cut*> findCuts(const std::string& cutName, CutEnum passAlong = BOTH) override;
      std::vector<Cut*> findCuts(const std::regex& cutRegex, CutEnum passAlong = BOTH) override;

      std::string getFullName(std::string name = "") const override;

      virtual Cut* getPreviousCut(bool countThis = false) override;
      virtual Node* getPreviousNode(bool countThis = false) override;

      virtual const Cut* getPreviousCut(bool countThis = false) const override;
      virtual const Node* getPreviousNode(bool countThis = false) const override;

    protected:
      // ----------------------------------------
      // Constructor
      // ----------------------------------------
      EndNode(Chain& parent, CutBase* previousCut, const std::string& regionName);

      bool passEvent(float weight = 1., bool debug = false) override;

      // If some cuts can be ignored (in order to make plots without them) then this function will be called after the first one fails passEvent
      void passNMinusMHists(float weight = 1., std::vector<const Cut*> ignoredCuts = std::vector<const Cut*>(), bool debug = false) override;

      void saveOutput(bool concise, TDirectory* outDir, TH1* cutflow, TH1* cutflowWeighted, int cutNum) const override;

      void print() const override;

      void initializeAll() override;
      void cleanupTObjs() override { m_tobjWrappers.clear(); }
      void printTikzFile(std::ofstream& fout, int tabLevel) const override;

      void setSubchainName(const std::string& subchainName, CutBase* nextCut = 0) override;

      CutBase* m_previousCut;

      std::string m_name;
  };
}

#endif //CutChain_ENDNODE_H
