#ifndef CutChain_NODE_H
#define CutChain_NODE_H

#include "CutChain/CutBase.h"

#include <exception>
#include <map>

namespace CutChain
{
  class ChainSegment;
  class Cut;
  class Node : public CutBase
  {
    friend class Cut;
    public:

      // Exceptions
      struct subchainNameAlreadyUsed : public std::exception {
        subchainNameAlreadyUsed(const std::string& subchainName) : subchainName(subchainName) {}
        std::string subchainName;
        const char * what() const throw() {
          return ("Subchain name "+subchainName+" is already in use (will cause a name conflict in the final TFile)").c_str();
        }
      };

      // Destructor
      ~Node();
      // Explicitly suppress the copy constructors
      Node(const Node&) = delete;
      Node operator=(const Node&) = delete;

      void addTObj(const TObjWrapper& tobj, CutEnum passAlong = HERE, bool printWarnings = true) override;
      void modifyAllTObjs(TObjEnum setting, CutEnum passAlong = HERE) override;
      // returns false if the object isn't found on any cut
      bool modifyTObj(const std::string& tobjName, TObjEnum setting, CutEnum = HERE) override;

      // Make a subchain starting with the specified new cut 
      Cut* addSubchain(const std::string& chainName, const std::string& cutName, std::function<bool()> cutFunc, bool doPlotting = true);

      bool hasSubchain(const std::string& chainName) { return m_nextCuts.count(chainName); }

      // Tries to fill targetCut with the named subchain. If the subchain doesn't exist or starts with the wrong type (dynamic cast fails) it returns false and targetCut will be a null pointer. Returns true if the chain is successfully found
      bool getSubchain(Cut*& targetCut, const std::string& chainName);
      bool getSubchain(Node*& targetCut, const std::string& chainName);
      bool getSubchain(EndNode*& targetCut, const std::string& chainName);

      // Tries to create a new subchain. If it succeeds fill the new cut into targetCut and return true
      // Otherwise if the subchain already exists, starts with type Cut* AND has the same name (it's not possible to check if the function is the same - BE WARNED!) it will fill the first cut of the subchain into targetCut and return true
      // Otherwise targetCut will be null and it will return false
      bool getAddSubchain(Cut*& targetCut, const std::string& chainName, const std::string& cutName, std::function<bool()> cutFunc, bool doPlotting = true);

      // Equivalent functions for Node and EndNode
      bool getSplitChain(Node*& targetCut, const std::string& chainName);
      bool getFinishChain(EndNode*& targetCut, const std::string& chainName);

      // Add a subchain using a chain segment
      Cut* addSubchainSegment(const std::string& chainName, const ChainSegment& segment);

      // Adds a node after this cut in the chain.
      Node* splitChain(const std::string& chainName);

      // Finishes this branch here.
      EndNode* finishChain(const std::string& regionName);

      // Find all cuts in the given search pattern with the given name
      std::vector<Cut*> findCuts(const std::string& cutName, CutEnum passAlong = BOTH) override;
      std::vector<Cut*> findCuts(const std::regex& cutRegex, CutEnum passAlong = BOTH) override;

      std::string getFullName(std::string name = "") const override;

      virtual Cut* getPreviousCut(bool countThis = false) override;
      virtual Node* getPreviousNode(bool countThis = false) override;

      virtual const Cut* getPreviousCut(bool countThis = false) const override;
      virtual const Node* getPreviousNode(bool countThis = false) const override;

    private:
      // ----------------------------------------
      // Constructor
      // ----------------------------------------

      Node(Chain& parent, CutBase* previousCut, const std::vector<std::unique_ptr<TObjWrapper>>& tobjWrappers);

      bool passEvent(float weight = 1., bool debug = false) override;

      // If some cuts can be ignored (in order to make plots without them) then this function will be called after the first one fails passEvent
      void passNMinusMHists(float weight = 1., std::vector<const Cut*> ignoredCuts = std::vector<const Cut*>(), bool debug = false) override;

      void saveOutput(bool concise, TDirectory* outDir, TH1* cutflow, TH1* cutflowWeighted, int cutNum) const override;

      void print() const override;
      void printTikzFile(std::ofstream& fout, int tabLevel) const override;

      void initializeAll() override;
      void cleanupTObjs() override;

      CutBase* m_previousCut;

      std::map<std::string, std::unique_ptr<CutBase> > m_nextCuts;

      void setSubchainName(const std::string& subchainName, CutBase* nextCut) override;
  };
}

#endif //CutChain_NODE_H
