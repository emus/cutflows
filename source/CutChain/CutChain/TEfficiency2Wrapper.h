#ifndef CutChain_TEFFICIENCY2WRAPPER_H
#define CutChain_TEFFICIENCY2WRAPPER_H

#include "CutChain/TObjWrapper.h"
#include <TH2.h>
#include <functional>
#include <memory>
#include <utility>

namespace CutChain
{
  class TEfficiency2Wrapper : public TObjWrapper
  {
    public:
      TEfficiency2Wrapper(const std::string& name, std::function<TH2*()> initialize, std::function<void(TEfficiency2Wrapper&)> execute, std::vector<std::string> ignoredCuts = std::vector<std::string>() );

      TEfficiency2Wrapper(const TEfficiency2Wrapper& oldTEfficiency);

      ~TEfficiency2Wrapper();

      void copyToCut(CutBase* targetCut) const;
      
      void fill(bool decision, double value1, double value2);

      std::pair<TH2*, TH2*> operator->() { return std::make_pair(m_pass.get(), m_total.get() ); }
      std::pair<const TH2*, const TH2*> operator->() const { return std::make_pair(m_pass.get(), m_total.get() ); }

    private:

      //std::unique_ptr<TEfficiency> m_teff;
      std::unique_ptr<TH2> m_pass;
      std::unique_ptr<TH2> m_total;


      void doInitialize();
      std::function<TH2*()> m_initialize;

      void doExecute();
      std::function<void(TEfficiency2Wrapper&)> m_execute;

      void saveOutput(TDirectory* outDir);
  };
}

#endif //CutChain_TEFFICIENCY2WRAPPER_H
