#ifndef CutChain_TEFFICIENCYWRAPPER_H
#define CutChain_TEFFICIENCYWRAPPER_H

#include "CutChain/TObjWrapper.h"
#include <TH1.h>
#include <functional>
#include <memory>
#include <utility>

namespace CutChain
{
  class TEfficiencyWrapper : public TObjWrapper
  {
    public:
      TEfficiencyWrapper(const std::string& name, std::function<TH1*()> initialize, std::function<void(TEfficiencyWrapper&)> execute, std::vector<std::string> ignoredCuts = std::vector<std::string>() );

      TEfficiencyWrapper(const TEfficiencyWrapper& oldTEfficiency);

      ~TEfficiencyWrapper();

      void copyToCut(CutBase* targetCut) const;
      
      void fill(bool decision, double value);

      std::pair<TH1*, TH1*> operator->() { return std::make_pair(m_pass.get(), m_total.get() ); }
      std::pair<const TH1*, const TH1*> operator->() const { return std::make_pair(m_pass.get(), m_total.get() ); }

    private:

      //std::unique_ptr<TEfficiency> m_teff;
      std::unique_ptr<TH1> m_pass;
      std::unique_ptr<TH1> m_total;


      void doInitialize();
      std::function<TH1*()> m_initialize;

      void doExecute();
      std::function<void(TEfficiencyWrapper&)> m_execute;

      void saveOutput(TDirectory* outDir);
  };
}

#endif //CutChain_TEFFICIENCYWRAPPER_H
