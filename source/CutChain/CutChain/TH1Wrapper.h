#ifndef CutChain_TH1WRAPPER_H
#define CutChain_TH1WRAPPER_H

#include "CutChain/TObjWrapper.h"
#include <TH1.h>
#include <functional>
#include <memory>

namespace CutChain
{
  class TH1Wrapper : public TObjWrapper
  {
    public:
      TH1Wrapper(const std::string& name, std::function<TH1*()> initialize, std::function<void(TH1Wrapper&)> execute, std::vector<std::string> ignoredCuts = std::vector<std::string>() );

      TH1Wrapper(const TH1Wrapper& oldTH1);

      ~TH1Wrapper();

      void copyToCut(CutBase* targetCut) const;
      
      void fill(double value, double weight = 1.);

      TH1* operator->() { return m_th1.get(); }
      const TH1* operator->() const { return m_th1.get(); }

    private:

      std::unique_ptr<TH1> m_th1;

      void doInitialize();
      std::function<TH1*()> m_initialize;

      void doExecute();
      std::function<void(TH1Wrapper&)> m_execute;

      void saveOutput(TDirectory* outDir);
  };
}

#endif //CutChain_TH1WRAPPER_H
