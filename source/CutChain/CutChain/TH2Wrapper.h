#ifndef CutChain_TH2WRAPPER_H
#define CutChain_TH2WRAPPER_H

#include "CutChain/TObjWrapper.h"
#include <TH2.h>
#include <functional>
#include <memory>

namespace CutChain
{
  class TH2Wrapper : public TObjWrapper
  {
    public:
      TH2Wrapper(const std::string& name, std::function<TH2*()> initialize, std::function<void(TH2Wrapper&)> execute, std::vector<std::string> ignoredCuts = std::vector<std::string>() );

      TH2Wrapper(const TH2Wrapper& oldTH2);

      ~TH2Wrapper();

      void copyToCut(CutBase* targetCut) const;
      
      void fill(double valueX, double valueY, double weight = 1.);

      TH2* operator->() { return m_th2.get(); }
      const TH2* operator->() const { return m_th2.get(); }

    private:

      std::unique_ptr<TH2> m_th2;

      void doInitialize();
      std::function<TH2*()> m_initialize;

      void doExecute();
      std::function<void(TH2Wrapper&)> m_execute;

      void saveOutput(TDirectory* outDir);
  };
}

#endif //CutChain_TH2WRAPPER_H
