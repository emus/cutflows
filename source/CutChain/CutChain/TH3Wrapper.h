#ifndef CutChain_TH3WRAPPER_H
#define CutChain_TH3WRAPPER_H

#include "CutChain/TObjWrapper.h"
#include <TH3.h>
#include <functional>
#include <memory>

namespace CutChain
{
  class TH3Wrapper : public TObjWrapper
  {
    public:
      TH3Wrapper(const std::string& name, std::function<TH3*()> initialize, std::function<void(TH3Wrapper&)> execute, std::vector<std::string> ignoredCuts = std::vector<std::string>() );

      TH3Wrapper(const TH3Wrapper& oldTH3);

      ~TH3Wrapper();

      void copyToCut(CutBase* targetCut) const;
      
      void fill(double valueX, double valueY, double valueZ, double weight = 1.);

      TH3* operator->() { return m_th3.get(); }
      const TH3* operator->() const { return m_th3.get(); }

    private:

      std::unique_ptr<TH3> m_th3;

      void doInitialize();
      std::function<TH3*()> m_initialize;

      void doExecute();
      std::function<void(TH3Wrapper&)> m_execute;

      void saveOutput(TDirectory* outDir);
  };
}

#endif //CutChain_TH3WRAPPER_H
