#ifndef CutChain_TOBJWRAPPER_H
#define CutChain_TOBJWRAPPER_H

#include <vector>
#include <string>
#include <memory>
#include <regex>

#include <TDirectory.h>

namespace CutChain
{
  class CutBase;
  class Cut;
  class TObjWrapper
  {
    friend class Cut;
    friend class EndNode;
    public:
      TObjWrapper(const std::string& name, std::vector<std::string> ignoredCutNames);
      //TObjWrapper(const std::string& name, std::vector<std::regex> ignoredCutNames);
      
      virtual ~TObjWrapper();

      const std::string& getName() const { return m_name; }

      virtual void copyToCut(CutBase* targetCut) const = 0;
      bool enabled;

      static const char* s_getUniqueHistName();

      Cut* containingCut;

      static unsigned int s_nCreatedTObjs;
      static unsigned int s_nInitialisedTObjs;
      static unsigned int s_nSavedTObjs;
      static unsigned int s_nTotalObjs;
      static unsigned int s_nTObjsToWrite;
      static bool s_printDebug;
      static bool s_printCleanup;

      void printAndIncrementInitCounter();
      void printAndIncrementSavedCounter();

    protected:

      std::string m_name;

      void initialize();
      virtual void doInitialize() = 0;
      void execute();
      virtual void doExecute() = 0;
      virtual void saveOutput(TDirectory* outDir) = 0;

      std::vector<std::string> m_ignoredCutNames;
      std::vector<const Cut*> m_ignoredCuts;
      std::vector<const Cut*> findIgnoredCuts();

      static int s_histUID;

      void copyObjectToCut(std::unique_ptr<TObjWrapper> object, CutBase* targetCut) const;
  };
}

#endif //CutChain_TOBJWRAPPER_H
