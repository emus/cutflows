#ifndef CutChain_TTREEWRAPPER_H
#define CutChain_TTREEWRAPPER_H

#include "CutChain/TObjWrapper.h"
#include <TTree.h>
#include <functional>
#include <memory>

namespace CutChain
{
  class TTreeWrapper : public TObjWrapper
  {
    public:
      TTreeWrapper(const std::string& name, std::function<TTree*()> initialize, std::function<void(TTreeWrapper&)> execute, std::vector<std::string> ignoredCuts = std::vector<std::string>() );

      TTreeWrapper(const TTreeWrapper& oldTTree);

      ~TTreeWrapper();

      void copyToCut(CutBase* targetCut) const;

      TTree* operator->() { return m_tree.get(); }
      const TTree* operator->() const { return m_tree.get(); }

    private:

      std::unique_ptr<TTree> m_tree;

      void doInitialize();
      std::function<TTree*()> m_initialize;

      void doExecute();
      std::function<void(TTreeWrapper&)> m_execute;

      void saveOutput(TDirectory* outDir);
  };
}

#endif //CutChain_TTREEWRAPPER_H
