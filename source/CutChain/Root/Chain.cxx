#include "CutChain/Chain.h"
#include "CutChain/Cut.h"
#include "CutChain/TObjWrapper.h"
#include <iostream>

#include <TH1.h>

CutChain::Chain::Chain(const std::string& startCutName) : startCut(new Cut(*this, 0, startCutName, [] () -> bool { return true; }, std::vector<std::unique_ptr<TObjWrapper>>(), true) ), suppressAllPlots(false)
{}

CutChain::Chain::~Chain() {
  TObjWrapper::s_printCleanup = TObjWrapper::s_printDebug;
}

bool CutChain::Chain::passEvent(float weight, bool debug)
{
  return startCut->passEvent(weight, debug);
}

void CutChain::Chain::initializeAll()
{
  startCut->cleanupTObjs();
  TObjWrapper::s_nTotalObjs = TObjWrapper::s_nCreatedTObjs;
  TObjWrapper::s_nTObjsToWrite = TObjWrapper::s_nTotalObjs;
  startCut->initializeAll();
  if (CutChain::TObjWrapper::s_printDebug) {
    std::cout << std::endl;
  }
}

void CutChain::Chain::print()
{
  startCut->print();
}

void CutChain::Chain::printTikzFile(std::ofstream& fout) const
{
  startCut->printTikzFile(fout, 0);
}

void CutChain::Chain::saveOutput(bool concise, TDirectory* outFile, int NevtsAOD, float XStimesBR)
{
  currentOutDir = outFile;
  currentOutDir->mkdir("FinalRegions");
  TH1* cutflow = new TH1D(TObjWrapper::s_getUniqueHistName(), "Cutflow", 3, 1, 4);
  cutflow->SetBinContent(1, NevtsAOD);
  cutflow->GetXaxis()->SetBinLabel(1,"Nevts in AOD from CBK");
  cutflow->Sumw2();
  //cutflow->SetBit(TH1::kCanRebin);
  TH1* cutflowWeighted = new TH1D(TObjWrapper::s_getUniqueHistName(), "Weighted Cutflow", 3, 1, 4);
  cutflowWeighted->SetBinContent(1, XStimesBR);
  cutflowWeighted->GetXaxis()->SetBinLabel(1, "XStimesBR");
  cutflowWeighted->Sumw2();
  //cutflowWeighted->SetBit(TH1::kCanRebin);
  startCut->saveOutput(concise, outFile->mkdir("fullCutflow"), cutflow, cutflowWeighted, 2);
  if (CutChain::TObjWrapper::s_printDebug) std::cout << std::endl;
}

void CutChain::Chain::saveOutput(bool concise, TDirectory* outFile, TH1* h_cutflow, TH1* h_cutflowWeighted)
{
  currentOutDir = outFile;
  currentOutDir->mkdir("FinalRegions");
  h_cutflow->LabelsDeflate("X");
  h_cutflowWeighted->LabelsDeflate("X");
  startCut->saveOutput(concise, outFile->mkdir("fullCutflow"), h_cutflow, h_cutflowWeighted, h_cutflow->GetNbinsX() + 1);
}
