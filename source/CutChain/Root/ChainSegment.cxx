#include "CutChain/ChainSegment.h"
#include "CutChain/Cut.h"
#include "CutChain/Node.h"
#include "CutChain/TObjWrapper.h"
#include <iostream>

CutChain::ChainSegment::ChainSegment(const std::string& name, std::function<bool()> cutFunc, bool doPlotting)
  : m_dummyChain("dummy"), m_firstCut(new Cut(m_dummyChain, 0, name, cutFunc, std::vector<std::unique_ptr<TObjWrapper>>(), doPlotting) )
{
  m_lastCut = m_firstCut.get();
}

CutChain::ChainSegment::~ChainSegment() {}


void CutChain::ChainSegment::addTObj(TObjWrapper& tobj, bool printWarnings)
{
  bool foundObj = false;
  for (const auto& oldTObj : m_lastCut->m_tobjWrappers) if (tobj.getName() == oldTObj->getName() ) foundObj = true;
  if (foundObj) {
    if (printWarnings) std::cout << "WARNING. Cut " << m_lastCut->m_name << " already has a TObjWrapper called " << tobj.getName() << std::endl;
  }
  else {
    tobj.copyToCut(m_lastCut);
  }
}

void CutChain::ChainSegment::modifyAllTObjs(CutChain::TObjEnum setting) {
  if (setting == REMOVE) {
    m_lastCut->m_tobjWrappers.clear();
  }
  else {
    for (auto& tobj : m_lastCut->m_tobjWrappers) tobj->enabled = setting == ENABLE;
  }
}

bool CutChain::ChainSegment::modifyTObj(const std::string& tobjName, CutChain::TObjEnum setting)
{
  bool foundObj = false;
  for (auto tobjItr = m_lastCut->m_tobjWrappers.begin(); tobjItr != m_lastCut->m_tobjWrappers.end(); ++tobjItr) {
    if ( (*tobjItr)->getName() == tobjName) {
      foundObj = true;
      switch (setting) {
        case REMOVE:
          m_lastCut->m_tobjWrappers.erase(tobjItr);
          break;
        case SUPPRESS:
          (*tobjItr)->enabled = false;
          break;
        case ENABLE:
          (*tobjItr)->enabled = true;
          break;
        default:
          break;
      }
      break;
    }
  }
  return foundObj;
}

void CutChain::ChainSegment::increaseWeight(std::function<float()> weightFunc)
{
  m_lastCut->m_weightFunc = weightFunc;
}

void CutChain::ChainSegment::addCut(const std::string& name, std::function<bool()> cutFunc, bool doPlotting)
{
  m_lastCut = m_lastCut->addCut(name, cutFunc, doPlotting);
}

void CutChain::ChainSegment::addChainSegment(const CutChain::ChainSegment& segment)
{
  m_lastCut = segment.copyAfterCut(m_lastCut);
}

CutChain::Cut* CutChain::ChainSegment::copyAfterCut(CutChain::Cut* cut) const
{
  Cut* thisCut = m_firstCut.get();
  Cut* thatCut = cut;
  while (thisCut) {
    thatCut = thatCut->addCut(thisCut->m_name, thisCut->m_cutFunc, thisCut->m_plotHistograms);
    for (auto& tobj : thisCut->m_tobjWrappers) {
      thatCut->addTObj(*tobj, HERE, false);
    }
    thatCut->m_weightFunc = thisCut->m_weightFunc;
    thisCut = dynamic_cast<Cut*>(thisCut->m_nextCut.get() );
  }
  return thatCut;
}

CutChain::Cut* CutChain::ChainSegment::copyAfterNode(const std::string& subchainName, CutChain::Node* node) const
{
  Cut* thisCut = m_firstCut.get();
  Cut* thatCut = node->addSubchain(subchainName, thisCut->m_name, thisCut->m_cutFunc, thisCut->m_plotHistograms);
  for (auto& tobj : thisCut->m_tobjWrappers) {
    thatCut->addTObj(*tobj, HERE, false);
  }
  thatCut->m_weightFunc = thisCut->m_weightFunc;
  thisCut = dynamic_cast<Cut*>(thisCut->m_nextCut.get() );
  while (thisCut) {
    thatCut = thatCut->addCut(thisCut->m_name, thisCut->m_cutFunc, thisCut->m_plotHistograms);
    for (auto& tobj : thisCut->m_tobjWrappers) {
      thatCut->addTObj(*tobj, HERE, false);
    }
    thatCut->m_weightFunc = thisCut->m_weightFunc;
    thisCut = dynamic_cast<Cut*>(thisCut->m_nextCut.get() );
  }
  return thatCut;
}
