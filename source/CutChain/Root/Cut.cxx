#include "CutChain/Cut.h"
#include "CutChain/Node.h"
#include "CutChain/EndNode.h"
#include "CutChain/ChainSegment.h"
#include "CutChain/Chain.h"
#include "CutChain/TObjWrapper.h"

#include <sstream>
#include <iostream>
#include <set>

// ----------------------------------------------
// Constructors
// ----------------------------------------------

// Marco: Test for git repository

CutChain::Cut::Cut(CutChain::Chain& parent, CutChain::CutBase* previousCut, const std::string& name, std::function<bool()> cutFunc, const std::vector<std::unique_ptr<CutChain::TObjWrapper>>& tobjWrappers, bool doPlotting)
  : CutBase(parent), m_name(name), m_cutFunc(cutFunc), m_previousCut(previousCut), m_weight(1), m_weightFunc([] () -> float { return 1.; } ), 
    m_nPassedEvents(0), m_nPassedEventsError(0), m_nPassedEventsWeighted(0), m_nPassedEventsWeightedError(0), m_isIgnored(false), m_plotHistograms(doPlotting)
{
  for (const auto& tobj : tobjWrappers) {
    tobj->copyToCut(this);
  }
  if (!doPlotting || parent.suppressAllPlots || parent.onlyOutputFinal) modifyAllTObjs(CutChain::SUPPRESS);
}

CutChain::Cut::~Cut() {
}

void CutChain::Cut::addTObj(const CutChain::TObjWrapper& tobj, CutChain::CutEnum passAlong, bool printWarnings)
{
  if (!parent.suppressAllPlots) {
    bool foundObj = false;
    for (const auto& oldTObj : m_tobjWrappers) if (tobj.getName() == oldTObj->getName() ) foundObj = true;
    if (foundObj) {
      if (printWarnings) std::cout << "WARNING. Cut " << m_name << " already has a TObjWrapper called " << tobj.getName() << std::endl;
    }
    else {
      tobj.copyToCut(this);
      if (!m_plotHistograms) modifyTObj(tobj.getName(), SUPPRESS);
    }
    if ( (passAlong == BOTH || passAlong == UPSTREAM) && m_previousCut != 0) {
      m_previousCut->addTObj(tobj, UPSTREAM);
    }
    if ( (passAlong == BOTH || passAlong == DOWNSTREAM) && m_nextCut != 0) {
      m_nextCut->addTObj(tobj, DOWNSTREAM);
    }
  }
}

void CutChain::Cut::modifyAllTObjs(CutChain::TObjEnum setting, CutChain::CutEnum passAlong) {
  if (setting == REMOVE) {
    m_tobjWrappers.clear();
  }
  else {
    for (auto& tobj : m_tobjWrappers) tobj->enabled = setting == ENABLE;
  }
  if ( (passAlong == BOTH || passAlong == UPSTREAM) && m_previousCut != 0) {
    m_previousCut->modifyAllTObjs(setting, UPSTREAM);
  }
  if ( (passAlong == BOTH || passAlong == DOWNSTREAM) && m_nextCut != 0) {
    m_nextCut->modifyAllTObjs(setting, DOWNSTREAM);
  }
}

bool CutChain::Cut::modifyTObj(const std::string& tobjName, CutChain::TObjEnum setting, CutChain::CutEnum passAlong)
{
  bool foundObj = false;
  for (auto tobjItr = m_tobjWrappers.begin(); tobjItr != m_tobjWrappers.end(); ++tobjItr) {
    if ( (*tobjItr)->getName() == tobjName) {
      foundObj = true;
      switch (setting) {
        case REMOVE:
          m_tobjWrappers.erase(tobjItr);
          break;
        case SUPPRESS:
          (*tobjItr)->enabled = false;
          break;
        case ENABLE:
          (*tobjItr)->enabled = true;
          break;
        default:
          break;
      }
      break;
    }
  }
  if ( (passAlong == BOTH || passAlong == UPSTREAM) && m_previousCut != 0) {
    foundObj = m_previousCut->modifyTObj(tobjName, setting, UPSTREAM) || foundObj;
  }
  if ( (passAlong == BOTH || passAlong == DOWNSTREAM) && m_nextCut != 0) {
    foundObj = m_nextCut->modifyTObj(tobjName, setting, DOWNSTREAM) || foundObj;
  }
  return foundObj;
}

CutChain::Cut* CutChain::Cut::addCut(const std::string& name, std::function<bool()> cutFunc, bool doPlotting)
{
  Cut* newCut = new Cut(parent, this, name, cutFunc, m_tobjWrappers, doPlotting);
  m_nextCut.reset(newCut);
  return newCut;
}

CutChain::Cut* CutChain::Cut::addChainSegment(const CutChain::ChainSegment& segment)
{
  return segment.copyAfterCut(this);
}

CutChain::Node* CutChain::Cut::splitChain() {
  Node* newNode = new Node(parent, this, m_tobjWrappers);
  m_nextCut.reset(newNode);
  return newNode;
}

CutChain::EndNode* CutChain::Cut::finishChain(const std::string& name) {
  if (find(parent.regionNames().begin(), parent.regionNames().end(), name) != parent.regionNames().end() ) {
    throw chainNameAlreadyUsed(name);
  }
  else {
    EndNode* newEndNode = new EndNode(parent, this, name);
    m_nextCut.reset(newEndNode);
    return newEndNode;
  }
}

bool CutChain::Cut::getCut(CutChain::Cut*& targetCut)
{
  if (m_nextCut) {
    targetCut = dynamic_cast<Cut*>(m_nextCut.get() );
    return targetCut != 0;
  }
  else {
    targetCut = 0;
    return false;
  }
}

bool CutChain::Cut::getCut(CutChain::Node*& targetCut)
{
  if (m_nextCut) {
    targetCut = dynamic_cast<Node*>(m_nextCut.get() );
    return targetCut != 0;
  }
  else {
    targetCut = 0;
    return false;
  }
}

bool CutChain::Cut::getCut(CutChain::EndNode*& targetCut)
{
  if (m_nextCut) {
    targetCut = dynamic_cast<EndNode*>(m_nextCut.get() );
    return targetCut != 0;
  }
  else {
    targetCut = 0;
    return false;
  }
}

bool CutChain::Cut::getAddCut(CutChain::Cut*& targetCut, const std::string& name, std::function<bool()> cutFunc, bool doPlotting)
{
  if (m_nextCut) {
    if (!getCut(targetCut) ) return false;
    else {
      if (targetCut->getName() == name) return true;
      else {
        targetCut = 0;
        return false;
      }
    }
  }
  else {
    targetCut = addCut(name, cutFunc, doPlotting);
    return true;
  }
}

bool CutChain::Cut::getSplitChain(CutChain::Node*& targetCut)
{
  if (m_nextCut) return getCut(targetCut);
  else {
    targetCut = splitChain();
    return true;
  }
}

bool CutChain::Cut::getFinishChain(CutChain::EndNode*& targetCut, const std::string& regionName) {
  if (m_nextCut) {
    if (!getCut(targetCut) ) return false;
    if (targetCut->m_name == regionName) return true;
    else {
      targetCut = 0;
      return false;
    }
  }
  else {
    targetCut = finishChain(regionName);
    return true;
  }
}

void CutChain::Cut::addExtraCutflowVar(const std::string& varName, std::function<bool()> varFunc)
{
  for (auto cutflowItr = m_extraCutflowVars.begin(); cutflowItr != m_extraCutflowVars.end(); ++cutflowItr) {
    if (std::get<ECV::NAME>(*cutflowItr) == varName) return;
  }
  m_extraCutflowVars.push_back(std::make_tuple(varName, varFunc, 0, 0., 0., 0) );
}

std::vector<CutChain::Cut*> CutChain::Cut::findCuts(const std::string& cutName, CutChain::CutEnum passAlong)
{
  std::vector<Cut*> foundCuts;
  if (m_name == cutName) foundCuts.push_back(this);
  if ( (passAlong == BOTH || passAlong == UPSTREAM) && m_previousCut != 0) {
    auto theseCuts = m_previousCut->findCuts(cutName, UPSTREAM);
    foundCuts.insert(foundCuts.end(), theseCuts.begin(), theseCuts.end() );
  }
  if ( (passAlong == BOTH || passAlong == DOWNSTREAM) && m_nextCut != 0) {
    auto theseCuts = m_previousCut->findCuts(cutName, DOWNSTREAM);
    foundCuts.insert(foundCuts.end(), theseCuts.begin(), theseCuts.end() );
  }
  return foundCuts;
}

std::vector<CutChain::Cut*> CutChain::Cut::findCuts(const std::regex& cutRegex, CutChain::CutEnum passAlong)
{
  std::vector<Cut*> foundCuts;
  if (std::regex_match(m_name, cutRegex) ) foundCuts.push_back(this);
  if ( (passAlong == BOTH || passAlong == UPSTREAM) && m_previousCut != 0) {
    auto theseCuts = m_previousCut->findCuts(cutRegex, UPSTREAM);
    foundCuts.insert(foundCuts.end(), theseCuts.begin(), theseCuts.end() );
  }
  if ( (passAlong == BOTH || passAlong == DOWNSTREAM) && m_nextCut != 0) {
    auto theseCuts = m_previousCut->findCuts(cutRegex, DOWNSTREAM);
    foundCuts.insert(foundCuts.end(), theseCuts.begin(), theseCuts.end() );
  }
  return foundCuts;
}

float sumw2(float err1, float err2) {
  return sqrt(err1*err1 + err2*err2);
}

bool CutChain::Cut::passEvent(float weight, bool debug) {
  if (debug) std::cout << getFullName() << std::endl;
  m_weight = weight * m_weightFunc();
  if (m_cutFunc() ) {
    if (m_plotHistograms) for (auto& tobj : m_tobjWrappers) tobj->execute();
    ++m_nPassedEvents;
    m_nPassedEventsError = sumw2(m_nPassedEventsError, 1.);
    m_nPassedEventsWeighted += m_weight;
    m_nPassedEventsWeightedError = sumw2(m_nPassedEventsWeightedError, m_weight);
    for (auto& var : m_extraCutflowVars) {
      if (std::get<ECV::FUNC>(var)() ) {
        ++std::get<ECV::NPASSED>(var);
        std::get<ECV::NPASSED_ERR>(var) = sumw2(std::get<ECV::NPASSED_ERR>(var), 1.);
        std::get<ECV::NPASSED_WEIGHT>(var) += m_weight;
        std::get<ECV::NPASSED_WEIGHT_ERR>(var) = sumw2(std::get<ECV::NPASSED_WEIGHT_ERR>(var), m_weight);
      }
    }
    return m_nextCut->passEvent(m_weight, debug);
  }
  else {
    if (m_isIgnored) {
      auto nMinusMItr = m_tobjWrappersNMinusM.find(this);
      if (nMinusMItr != m_tobjWrappersNMinusM.end() ) { // if it isn't in the map then no histograms ignoring this cut are declared *on* the cut
        for (auto& tobj : nMinusMItr->second ) {
          tobj->execute();
        }
      }
      m_nextCut->passNMinusMHists(m_weight, {this}, debug);
    }
    return false;
  }
}

void CutChain::Cut::passNMinusMHists(float weight, std::vector<const Cut*> ignoredCuts, bool debug)
{
  if (debug) std::cout << m_name << std::endl;
  m_weight = weight * m_weightFunc();
  if (m_cutFunc() ) {
    std::set<TObjWrapper*> histsToPlot;
    for (const Cut* cut : ignoredCuts) {
      auto nMinusMItr = m_tobjWrappersNMinusM.find(cut);
      if (nMinusMItr != m_tobjWrappersNMinusM.end() ) { // if it isn't in the map then no histograms ignoring this cut are declared *on* the cut
        for (auto& tobj : nMinusMItr->second ) {
          tobj->execute();
        }
      }
    }
#if 0
    std::vector<TObjWrapper*> histsToPlot = m_tobjWrappersNMinusM.at(ignoredCuts.at(0) );
    auto tobjItr = histsToPlot.begin();
    while (tobjItr != histsToPlot.end() ) {
      for (auto cutItr = ignoredCuts.begin() + 1; cutItr != ignoredCuts.end(); ++cutItr) {
        std::vector<TObjWrapper*> tempVector = m_tobjWrappersNMinusM.at(*cutItr);
        if (std::find(tempVector.begin(), tempVector.end(), *tobjItr) == tempVector.end() ) {
          tobjItr = histsToPlot.erase(tobjItr);
          tobjItr--;
          break;
        }
      }
      tobjItr++;
    }
#endif
    for (auto tobj : histsToPlot) tobj->execute();
    m_nextCut->passNMinusMHists(m_weight, ignoredCuts, debug);
  }
  else if (m_isIgnored) {
    ignoredCuts.push_back(this);
    m_nextCut->passNMinusMHists(m_weight, ignoredCuts, debug);
  }
}

void CutChain::Cut::saveOutput(bool concise, TDirectory* outDir, TH1* cutflow, TH1* cutflowWeighted, int cutNum) const
{
  if (!parent.suppressAllPlots && !parent.onlyOutputFinal && m_plotHistograms) {
    std::string rootName(m_name);
    std::replace(rootName.begin(), rootName.end(), ' ', '+');
    TDirectory* tobjDir = outDir->mkdir(rootName.c_str() );
    for (auto& tobj : m_tobjWrappers) tobj->saveOutput(tobjDir);
  }
  cutflow->SetBinContent(cutNum, m_nPassedEvents);
  cutflow->SetBinError(cutNum, m_nPassedEventsError);
  cutflow->GetXaxis()->SetBinLabel(cutNum, m_name.c_str() );
  cutflowWeighted->SetBinContent(cutNum, m_nPassedEventsWeighted);
  cutflowWeighted->SetBinError(cutNum, m_nPassedEventsWeightedError);
  cutflowWeighted->GetXaxis()->SetBinLabel(cutNum, m_name.c_str() );
  ++cutNum;

  for (auto var : m_extraCutflowVars) {
    cutflow->SetBinContent(cutNum, std::get<ECV::NPASSED>(var) );
    cutflow->SetBinError(cutNum, std::get<ECV::NPASSED_ERR>(var) );
    cutflow->GetXaxis()->SetBinLabel(cutNum, std::get<ECV::NAME>(var).c_str() );
    cutflowWeighted->SetBinContent(cutNum, std::get<ECV::NPASSED_WEIGHT>(var) );
    cutflowWeighted->SetBinError(cutNum, std::get<ECV::NPASSED_WEIGHT_ERR>(var) );
    cutflowWeighted->GetXaxis()->SetBinLabel(cutNum, std::get<ECV::NAME>(var).c_str() );
    ++cutNum;
  }
  m_nextCut->saveOutput(concise, outDir, cutflow, cutflowWeighted, cutNum);
}

void CutChain::Cut::print() const
{
  std::cout << m_name << "\t";
  if (m_nextCut) m_nextCut->print();
}

void CutChain::Cut::initializeAll()
{
  if (m_plotHistograms) {
    for (auto& tobj : m_tobjWrappers) {
      tobj->initialize();
      for (const Cut* cut : tobj->findIgnoredCuts() ) {
        m_tobjWrappersNMinusM[cut].push_back(tobj.get() );
      }
    }
  }
  if (m_nextCut) m_nextCut->initializeAll();
  else {
    std::cerr << "ERROR! Cut " << m_name << " is the last cut in branch (branch isn't finished)" << std::endl;
    std::cerr << "Full path to cut: " << getFullName() << std::endl;
    throw chainNotFinished();
  }
}

void CutChain::Cut::cleanupTObjs()
{
  if (!m_plotHistograms) m_tobjWrappers.clear();
  for (auto& cutPair : m_tobjWrappersNMinusM) {
    if (!m_plotHistograms) cutPair.second.clear();
    cutPair.second.erase(std::remove_if(cutPair.second.begin(), cutPair.second.end(), [] (TObjWrapper* obj) { return !obj->enabled; } ), cutPair.second.end());
  }
  m_tobjWrappers.erase(std::remove_if(m_tobjWrappers.begin(), m_tobjWrappers.end(), [] (std::unique_ptr<TObjWrapper>& obj) { return !obj->enabled; } ), m_tobjWrappers.end());
  if (m_nextCut) m_nextCut->cleanupTObjs();
}

std::string CutChain::Cut::getFullName(std::string name) const
{
  name = m_name + "/" + name;
  if (m_previousCut) return m_previousCut->getFullName(name);
  else return name;
}

void CutChain::Cut::printTikzFile(std::ofstream& fout, int tabLevel) const
{
  for (int i = 0; i < tabLevel; ++i) fout << " ";
  fout << "child { node (Cut) {" << m_name << "}\n";
  m_nextCut->printTikzFile(fout, tabLevel+1);
}

void CutChain::Cut::setSubchainName(const std::string& subchainName, CutBase*) {
  if (m_previousCut) m_previousCut->setSubchainName(subchainName, this);
}

CutChain::Cut* CutChain::Cut::getPreviousCut(bool countThis)
{
  if (countThis) return this;
  else return m_previousCut->getPreviousCut(true);
}

CutChain::Node* CutChain::Cut::getPreviousNode(bool)
{
  return m_previousCut->getPreviousNode(true);
}

const CutChain::Cut* CutChain::Cut::getPreviousCut(bool countThis) const
{
  if (countThis) return this;
  else return m_previousCut->getPreviousCut(true);
}

const CutChain::Node* CutChain::Cut::getPreviousNode(bool) const
{
  return m_previousCut->getPreviousNode(true);
}
