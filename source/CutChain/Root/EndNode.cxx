#include "CutChain/EndNode.h"
#include "CutChain/Chain.h"
#include "CutChain/Cut.h"

#include <iostream>

// 

CutChain::EndNode::EndNode(CutChain::Chain& parent, CutChain::CutBase* previousCut, const std::string& regionName)
  : CutBase(parent), m_previousCut(previousCut), m_name(regionName) 
{
  if (!parent.suppressAllPlots) m_previousCut->modifyAllTObjs(CutChain::ENABLE);
  parent.m_regionNames.push_back(regionName);
  // Rename the subchain to match the region name
  setSubchainName(regionName);
}

CutChain::EndNode::~EndNode() {}

void CutChain::EndNode::addTObj(const TObjWrapper& tobj, CutChain::CutEnum passAlong, bool printWarnings)
{
  if (passAlong == HERE || passAlong == DOWNSTREAM) return;
  else m_previousCut->addTObj(tobj, passAlong, printWarnings);
}

void CutChain::EndNode::modifyAllTObjs(CutChain::TObjEnum setting, CutChain::CutEnum passAlong)
{
  if (passAlong == HERE || passAlong == DOWNSTREAM) return;
  else m_previousCut->modifyAllTObjs(setting, passAlong);
}

bool CutChain::EndNode::modifyTObj(const std::string& tobjName, CutChain::TObjEnum setting, CutChain::CutEnum passAlong)
{
  if (passAlong == HERE || passAlong == DOWNSTREAM) return false;
  else return m_previousCut->modifyTObj(tobjName, setting, passAlong);
}

std::vector<CutChain::Cut*> CutChain::EndNode::findCuts(const std::string& cutName, CutChain::CutEnum passAlong)
{
  if (passAlong == HERE || passAlong == DOWNSTREAM) return std::vector<Cut*>();
  else return m_previousCut->findCuts(cutName, passAlong);
}

std::vector<CutChain::Cut*> CutChain::EndNode::findCuts(const std::regex& cutRegex, CutChain::CutEnum passAlong)
{
  if (passAlong == HERE || passAlong == DOWNSTREAM) return std::vector<Cut*>();
  else return m_previousCut->findCuts(cutRegex, passAlong);
}

bool CutChain::EndNode::passEvent(float, bool)
{
  return true;
}

void CutChain::EndNode::passNMinusMHists(float, std::vector<const Cut*>, bool debug)
{
  return;
}

std::string CutChain::EndNode::getFullName(std::string name) const
{
  name = m_name + "/" + name;
  if (m_previousCut) return m_previousCut->getFullName(name);
  else return name;
}

void CutChain::EndNode::saveOutput(bool concise, TDirectory* outDir, TH1* cutflow, TH1* cutflowWeighted, int) const
{
  std::string rootName(m_name);
  std::replace(rootName.begin(), rootName.end(), ' ', '+');
  TDirectory* parentOutDir = parent.currentOutDir->GetDirectory("FinalRegions")->mkdir(rootName.c_str() );
  cutflow->LabelsDeflate("X");
  outDir->WriteTObject(cutflow, "h_cutflow");
  parentOutDir->WriteTObject(cutflow, "h_cutflow");
  long nPassedEvents = cutflow->GetBinContent(cutflow->GetNbinsX() );
  delete cutflow;

  cutflowWeighted->LabelsDeflate("X");
  outDir->WriteTObject(cutflowWeighted, "h_cutflowWeighted");
  parentOutDir->WriteTObject(cutflowWeighted, "h_cutflowWeighted");
  delete cutflowWeighted;

  if (!parent.suppressAllPlots) for (auto& tobj : m_previousCut->m_tobjWrappers) tobj->saveOutput(parentOutDir);

  if (!concise) {
    //std::cout << m_name << ": " << nPassedEvents << std::flush << std::endl;
    std::cout << m_name << ": " << nPassedEvents << std::string(100, ' ') << std::endl;
  }
}

void CutChain::EndNode::setSubchainName(const std::string& subchainName, CutBase*)
{
  if (m_previousCut) m_previousCut->setSubchainName(subchainName, this);
}

void CutChain::EndNode::print() const
{
  std::cout << "--> " << m_name;
}

void CutChain::EndNode::printTikzFile(std::ofstream& fout, int) const
{
  fout << "}\n";
}

void CutChain::EndNode::initializeAll()
{
  TObjWrapper::s_nTObjsToWrite += m_previousCut->m_tobjWrappers.size();
}

CutChain::Cut* CutChain::EndNode::getPreviousCut(bool)
{
  return m_previousCut->getPreviousCut(true);
}
CutChain::Node* CutChain::EndNode::getPreviousNode(bool)
{
  return m_previousCut->getPreviousNode(true);
}

const CutChain::Cut* CutChain::EndNode::getPreviousCut(bool) const
{
  return m_previousCut->getPreviousCut(true);
}
const CutChain::Node* CutChain::EndNode::getPreviousNode(bool) const
{
  return m_previousCut->getPreviousNode(true);
}
