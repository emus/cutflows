#include "CutChain/Node.h"
#include "CutChain/EndNode.h"
#include "CutChain/Cut.h"
#include "CutChain/ChainSegment.h"
#include "CutChain/Chain.h"
#include "CutChain/TObjWrapper.h"
#include <algorithm>
#include <iostream>

CutChain::Node::~Node() {}

CutChain::Node::Node(Chain& parent, CutBase* previousCut, const std::vector<std::unique_ptr<TObjWrapper>>& tobjWrappers)
  : CutBase(parent), m_previousCut(previousCut)
{
  for (const auto& tobj : tobjWrappers) {
    tobj->copyToCut(this);
  }
}

void CutChain::Node::addTObj(const CutChain::TObjWrapper& tobj, CutChain::CutEnum passAlong, bool printWarnings)
{
  if (!parent.suppressAllPlots) {
    bool foundObj = false;
    for (const auto& oldTObj : m_tobjWrappers) if (tobj.getName() == oldTObj->getName() ) foundObj = true;
    if (foundObj) {
      if (printWarnings) std::cout << "WARNING. Node already has a TObjWrapper called " << tobj.getName() << std::endl;
    }
    else {
      tobj.copyToCut(this);
    }
    if ( (passAlong == BOTH || passAlong == UPSTREAM) && m_previousCut != 0) {
      m_previousCut->addTObj(tobj, UPSTREAM);
    }
    if (passAlong == BOTH || passAlong == DOWNSTREAM) {
      for (auto& cutPair : m_nextCuts) cutPair.second->addTObj(tobj, DOWNSTREAM);
    }
  }
}

void CutChain::Node::modifyAllTObjs(CutChain::TObjEnum setting, CutChain::CutEnum passAlong) {
  if (setting == REMOVE) {
    m_tobjWrappers.clear();
  }
  else {
    for (auto& tobj : m_tobjWrappers) tobj->enabled = setting == ENABLE;
  }
  if ( (passAlong == BOTH || passAlong == UPSTREAM) && m_previousCut != 0) {
    m_previousCut->modifyAllTObjs(setting, UPSTREAM);
  }
  if (passAlong == BOTH || passAlong == DOWNSTREAM) {
    for (auto& cutPair : m_nextCuts) cutPair.second->modifyAllTObjs(setting, DOWNSTREAM);
  }
}

bool CutChain::Node::modifyTObj(const std::string& tobjName, CutChain::TObjEnum setting, CutChain::CutEnum passAlong)
{
  bool foundObj = false;
  for (auto tobjItr = m_tobjWrappers.begin(); tobjItr != m_tobjWrappers.end(); ++tobjItr) {
    if ( (*tobjItr)->getName() == tobjName) {
      foundObj = true;
      switch (setting) {
        case REMOVE:
          m_tobjWrappers.erase(tobjItr);
          break;
        case SUPPRESS:
          (*tobjItr)->enabled = false;
          break;
        case ENABLE:
          (*tobjItr)->enabled = true;
          break;
        default:
          break;
      }
      break;
    }
  }
  if ( (passAlong == BOTH || passAlong == UPSTREAM) && m_previousCut != 0) {
    foundObj = m_previousCut->modifyTObj(tobjName, setting, UPSTREAM) || foundObj;
  }
  if (passAlong == BOTH || passAlong == DOWNSTREAM) {
    for (auto& cutPair : m_nextCuts) foundObj = cutPair.second->modifyTObj(tobjName, setting, DOWNSTREAM) || foundObj;
  }
  return foundObj;
}

CutChain::Cut* CutChain::Node::addSubchain(const std::string& chainName, const std::string& name, std::function<bool()> cutFunc, bool doPlotting)
{
  if (m_nextCuts.count(chainName) ) throw subchainNameAlreadyUsed(chainName);
  Cut* newCut = new Cut(parent, this, name, cutFunc, m_tobjWrappers, doPlotting);
  m_nextCuts[chainName] = std::unique_ptr<CutBase>(newCut);
  return newCut;
}

bool CutChain::Node::getSubchain(CutChain::Cut*& targetCut, const std::string& chainName)
{
  if (hasSubchain(chainName) ) {
    targetCut = dynamic_cast<Cut*>(m_nextCuts.at(chainName).get() );
    return targetCut != 0;
  }
  else {
    targetCut = 0;
    return false;
  }
}

bool CutChain::Node::getSubchain(CutChain::Node*& targetCut, const std::string& chainName)
{
  if (hasSubchain(chainName) ) {
    targetCut = dynamic_cast<Node*>(m_nextCuts.at(chainName).get() );
    return targetCut != 0;
  }
  else{
    targetCut = 0;
    return false;
  }
}

bool CutChain::Node::getSubchain(CutChain::EndNode*& targetCut, const std::string& chainName)
{
  if (hasSubchain(chainName) ) {
    targetCut = dynamic_cast<EndNode*>(m_nextCuts.at(chainName).get() );
    return targetCut != 0;
  }
  else {
    targetCut = 0;
    return false;
  }
}

bool CutChain::Node::getAddSubchain(CutChain::Cut*& targetCut, const std::string& chainName, const std::string& cutName, std::function<bool()> cutFunc, bool doPlotting)
{
  if (hasSubchain(chainName) ) {
    if (!getSubchain(targetCut, chainName) ) return false;
    if (targetCut->getName() == cutName) return true;
    else {
      targetCut = 0;
      return false;
    }
  }
  targetCut = addSubchain(chainName, cutName, cutFunc, doPlotting);
  return true;
}

bool CutChain::Node::getSplitChain(Node*& targetCut, const std::string& chainName)
{
  if (hasSubchain(chainName) ) return getSubchain(targetCut, chainName);
  else {
    targetCut = splitChain(chainName);
    return true;
  }
}

bool CutChain::Node::getFinishChain(EndNode*& targetCut, const std::string& chainName)
{
  if (hasSubchain(chainName) ) return getSubchain(targetCut, chainName);
  else {
    targetCut = finishChain(chainName);
    return true;
  }
}

CutChain::Cut* CutChain::Node::addSubchainSegment(const std::string& chainName, const CutChain::ChainSegment& segment)
{
  if (m_nextCuts.count(chainName) ) throw subchainNameAlreadyUsed(chainName);
  return segment.copyAfterNode(chainName, this);
}

CutChain::Node* CutChain::Node::splitChain(const std::string& chainName) {
  if (m_nextCuts.count(chainName) ) throw subchainNameAlreadyUsed(chainName);
  Node* newNode = new Node(parent, this, m_tobjWrappers);
  m_nextCuts[chainName] = std::unique_ptr<CutBase>(newNode);
  return newNode;
}

CutChain::EndNode* CutChain::Node::finishChain(const std::string& name) {
  if (m_nextCuts.count(name) ) throw subchainNameAlreadyUsed(name);
  if (find(parent.regionNames().begin(), parent.regionNames().end(), name) != parent.regionNames().end() ) {
    throw Cut::chainNameAlreadyUsed(name);
  }
  EndNode* newEndNode = new EndNode(parent, this, name);
  m_nextCuts[name] = std::unique_ptr<CutBase>(newEndNode);
  return newEndNode;
}

std::vector<CutChain::Cut*> CutChain::Node::findCuts(const std::string& cutName, CutChain::CutEnum passAlong)
{
  std::vector<Cut*> foundCuts;
  if ( (passAlong == BOTH || passAlong == UPSTREAM) && m_previousCut != 0) {
    auto theseCuts = m_previousCut->findCuts(cutName, UPSTREAM);
    foundCuts.insert(foundCuts.end(), theseCuts.begin(), theseCuts.end() );
  }
  if (passAlong == BOTH || passAlong == DOWNSTREAM) {
    for (auto& cutPair : m_nextCuts) {
      auto theseCuts = cutPair.second->findCuts(cutName, DOWNSTREAM);
      foundCuts.insert(foundCuts.end(), theseCuts.begin(), theseCuts.end() );
    }
  }
  return foundCuts;
}

std::vector<CutChain::Cut*> CutChain::Node::findCuts(const std::regex& cutRegex, CutChain::CutEnum passAlong)
{
  std::vector<Cut*> foundCuts;
  if ( (passAlong == BOTH || passAlong == UPSTREAM) && m_previousCut != 0) {
    auto theseCuts = m_previousCut->findCuts(cutRegex, UPSTREAM);
    foundCuts.insert(foundCuts.end(), theseCuts.begin(), theseCuts.end() );
  }
  if (passAlong == BOTH || passAlong == DOWNSTREAM) {
    for (auto& cutPair : m_nextCuts) {
      auto theseCuts = cutPair.second->findCuts(cutRegex, DOWNSTREAM);
      foundCuts.insert(foundCuts.end(), theseCuts.begin(), theseCuts.end() );
    }
  }
  return foundCuts;
}

bool CutChain::Node::passEvent(float weight, bool debug) {
  if (debug) std::cout << "in node" << std::endl;
  bool anyPassed = false;
  for (auto& cutPair : m_nextCuts) anyPassed = cutPair.second->passEvent(weight, debug) || anyPassed;
  return anyPassed;
}

void CutChain::Node::passNMinusMHists(float weight, std::vector<const Cut*> ignoredCuts, bool debug)
{
  for (auto& cutPair : m_nextCuts) cutPair.second->passNMinusMHists(weight, ignoredCuts, debug);
}

void CutChain::Node::saveOutput(bool concise, TDirectory* outDir, TH1* cutflow, TH1* cutflowWeighted, int cutNum) const
{
  for (auto& cutPair : m_nextCuts) {
    TH1* newCutflow = (TH1*)cutflow->Clone(TObjWrapper::s_getUniqueHistName() );
    TH1* newCutflowWeighted = (TH1*)cutflowWeighted->Clone(TObjWrapper::s_getUniqueHistName() );
    std::string rootName(cutPair.first);
    std::replace(rootName.begin(), rootName.end(), ' ', '+');
    TDirectory* newOutDir = outDir->mkdir(cutPair.first.c_str() );
    cutPair.second->saveOutput(concise, newOutDir, newCutflow, newCutflowWeighted, cutNum);
  }
}

void CutChain::Node::print() const
{
  for(auto& cutPair : m_nextCuts) {
    std::cout << std::endl;
    cutPair.second->print();
  }
}

void CutChain::Node::printTikzFile(std::ofstream& fout, int tabLevel) const
{
  for (int i = 0; i < tabLevel; ++i) fout << " ";
  fout << "child { node (Node) {}\n";
  ++tabLevel;
  for (auto& cutPair : m_nextCuts) {
    for (int i = 0; i < tabLevel; ++i) fout << " ";
    fout << "child { node (Subchain) {" << cutPair.first << "}\n";
    cutPair.second->printTikzFile(fout, tabLevel+1);
  }
}

void CutChain::Node::initializeAll()
{
  if (m_nextCuts.size() == 0) {
    std::cerr << "ERROR! Node has no following cuts => branch isn't finished!" << std::endl;
    std::cerr << "Full path to cut: " << getFullName() << std::endl;
    throw chainNotFinished();
  }
  for (auto& cutPair : m_nextCuts) cutPair.second->initializeAll();
}

void CutChain::Node::cleanupTObjs()
{
  m_tobjWrappers.clear();
  for (auto& cutPair : m_nextCuts) cutPair.second->cleanupTObjs();
}

std::string CutChain::Node::getFullName(std::string name) const
{
  if (m_previousCut) return m_previousCut->getFullName(name);
  else return name;
}

void CutChain::Node::setSubchainName(const std::string& subchainName, CutBase* nextCut)
{
  for (auto pairItr = m_nextCuts.begin(); pairItr != m_nextCuts.end(); ++pairItr) {
    if (pairItr->second.get() == nextCut) {
      if (pairItr->first == subchainName) return;
      m_nextCuts[subchainName] = std::move(pairItr->second);
      m_nextCuts.erase(pairItr);
      return;
    }
  }
  // Should we print something here as a warning!? TODO
}

CutChain::Cut* CutChain::Node::getPreviousCut(bool)
{
  return m_previousCut->getPreviousCut(true);
}

CutChain::Node* CutChain::Node::getPreviousNode(bool countThis)
{
  if (countThis) return this;
  else return m_previousCut->getPreviousNode(true);
}

const CutChain::Cut* CutChain::Node::getPreviousCut(bool) const
{
  return m_previousCut->getPreviousCut(true);
}

const CutChain::Node* CutChain::Node::getPreviousNode(bool countThis) const
{
  if (countThis) return this;
  else return m_previousCut->getPreviousNode(true);
}
