#include "CutChain/TEfficiency2Wrapper.h"
#include "CutChain/Cut.h"
#include <iostream>
#include <TEfficiency.h>

CutChain::TEfficiency2Wrapper::TEfficiency2Wrapper(const std::string& name, std::function<TH2*()> initialize, std::function<void(TEfficiency2Wrapper&)> execute, std::vector<std::string> ignoredCuts)
  : TObjWrapper(name, ignoredCuts), m_initialize(initialize), m_execute(execute)
{}

CutChain::TEfficiency2Wrapper::TEfficiency2Wrapper(const TEfficiency2Wrapper& oldTEfficiency)
  : TObjWrapper(oldTEfficiency.m_name, oldTEfficiency.m_ignoredCutNames), m_initialize(oldTEfficiency.m_initialize), m_execute(oldTEfficiency.m_execute)
{}

CutChain::TEfficiency2Wrapper::~TEfficiency2Wrapper() {
}

void CutChain::TEfficiency2Wrapper::copyToCut(CutChain::CutBase* targetCut) const
{
  TEfficiency2Wrapper* newTEfficiency = new TEfficiency2Wrapper(*this);
  copyObjectToCut(std::unique_ptr<TObjWrapper>(newTEfficiency), targetCut);
}

void CutChain::TEfficiency2Wrapper::fill(bool decision, double value1, double value2)
{
  if (m_pass && m_total) {
    m_total->Fill(value1, value2, containingCut->getWeight() );
    if (decision) m_pass->Fill(value1, value2, containingCut->getWeight() );
  }
  else {
    std::cerr << "TEfficiency2Wrapper " << containingCut->getFullName() << ":" <<  m_name << "has an invalid histogram pointers!" << std::endl;
    throw;
  }
}

void CutChain::TEfficiency2Wrapper::doInitialize() 
{ 
  m_pass.reset(m_initialize() );
  m_pass->SetName(s_getUniqueHistName() );
  m_pass->SetDirectory(0);
  m_pass->Sumw2();
  m_total.reset(m_initialize() );
  m_total->SetName(s_getUniqueHistName() );
  m_total->SetDirectory(0);
  m_total->Sumw2();

  printAndIncrementInitCounter();
}

std::vector<double> getXBins(const TH2* hist) {
  std::vector<double> binsX;
  TH1D* histX = hist->ProjectionX("", 0, -1, "");
  const TAxis* axis = histX->GetXaxis();
  for (unsigned int ii = 0; ii <= axis->GetNbins(); ++ii) {
    binsX.push_back(axis->GetBinUpEdge(ii) );
  }
  return binsX;
}

std::vector<double> getYBins(const TH2* hist) {
  std::vector<double> binsY;
  TH1D* histY = hist->ProjectionY("", 0, -1, "");
  const TAxis* axis = histY->GetXaxis();
  for (unsigned int ii = 0; ii <= axis->GetNbins(); ++ii) {
    binsY.push_back(axis->GetBinUpEdge(ii) );
  }
  return binsY;
}

void CutChain::TEfficiency2Wrapper::doExecute() { m_execute(*this); }
#if 0
TH1D constructCumulativeHistogram(const TH1* hist) {
  auto bins = getBins(hist);
  TH1D newHist( (hist->GetName()+std::string("_cumulative") ).c_str(), (hist->GetTitle()+std::string(";")+hist->GetXaxis()->GetTitle() ).c_str(), bins.size() - 1, &bins[0]);
  unsigned int overflow = newHist.GetNbinsX() + 2;
  for (unsigned int ii = 0; ii < overflow; ++ii) {
    double error(0);
    newHist.SetBinContent(ii, hist->IntegralAndError(ii, overflow, error) );
    newHist.SetBinError(ii, error);
  }
  return newHist;
}
#endif
void CutChain::TEfficiency2Wrapper::saveOutput(TDirectory* outDir)
{
  if (!enabled) return;
  TEfficiency* eff = new TEfficiency(*m_pass, *m_total );

  outDir->WriteTObject(eff, m_name.c_str() );
  printAndIncrementSavedCounter();
}
