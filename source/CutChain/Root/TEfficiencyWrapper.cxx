#include "CutChain/TEfficiencyWrapper.h"
#include "CutChain/Cut.h"
#include <iostream>
#include <TEfficiency.h>

CutChain::TEfficiencyWrapper::TEfficiencyWrapper(const std::string& name, std::function<TH1*()> initialize, std::function<void(TEfficiencyWrapper&)> execute, std::vector<std::string> ignoredCuts)
  : TObjWrapper(name, ignoredCuts), m_initialize(initialize), m_execute(execute)
{}

CutChain::TEfficiencyWrapper::TEfficiencyWrapper(const TEfficiencyWrapper& oldTEfficiency)
  : TObjWrapper(oldTEfficiency.m_name, oldTEfficiency.m_ignoredCutNames), m_initialize(oldTEfficiency.m_initialize), m_execute(oldTEfficiency.m_execute)
{}

CutChain::TEfficiencyWrapper::~TEfficiencyWrapper() {
}

void CutChain::TEfficiencyWrapper::copyToCut(CutChain::CutBase* targetCut) const
{
  TEfficiencyWrapper* newTEfficiency = new TEfficiencyWrapper(*this);
  copyObjectToCut(std::unique_ptr<TObjWrapper>(newTEfficiency), targetCut);
}

void CutChain::TEfficiencyWrapper::fill(bool decision, double value)
{
  if (m_pass && m_total) {
    m_total->Fill(value, containingCut->getWeight() );
    if (decision) m_pass->Fill(value, containingCut->getWeight() );
  }
  else {
    std::cerr << "TEfficiencyWrapper " << containingCut->getFullName() << ":" <<  m_name << "has an invalid histogram pointers!" << std::endl;
    throw;
  }
}

void CutChain::TEfficiencyWrapper::doInitialize() 
{ 
  m_pass.reset(m_initialize() );
  m_pass->SetName(s_getUniqueHistName() );
  m_pass->SetDirectory(0);
  m_pass->Sumw2();
  m_total.reset(m_initialize() );
  m_total->SetName(s_getUniqueHistName() );
  m_total->SetDirectory(0);
  m_total->Sumw2();

  printAndIncrementInitCounter();
}

std::vector<double> getBins(const TH1* hist) {
  std::vector<double> bins;
  const TAxis* axis = hist->GetXaxis();
  for (unsigned int ii = 0; ii <= axis->GetNbins(); ++ii) {
    bins.push_back(axis->GetBinUpEdge(ii) );
  }
  return bins;
}

void CutChain::TEfficiencyWrapper::doExecute() { m_execute(*this); }
#if 0
TH1D constructCumulativeHistogram(const TH1* hist) {
  auto bins = getBins(hist);
  TH1D newHist( (hist->GetName()+std::string("_cumulative") ).c_str(), (hist->GetTitle()+std::string(";")+hist->GetXaxis()->GetTitle() ).c_str(), bins.size() - 1, &bins[0]);
  unsigned int overflow = newHist.GetNbinsX() + 2;
  for (unsigned int ii = 0; ii < overflow; ++ii) {
    double error(0);
    newHist.SetBinContent(ii, hist->IntegralAndError(ii, overflow, error) );
    newHist.SetBinError(ii, error);
  }
  return newHist;
}
#endif
void CutChain::TEfficiencyWrapper::saveOutput(TDirectory* outDir)
{
  if (!enabled) return;
  TEfficiency* eff = new TEfficiency(*m_pass, *m_total );
  TH1* cumulativePassed = m_pass->GetCumulative(false, s_getUniqueHistName() );
  TH1* cumulativeTotal = m_total->GetCumulative(false, s_getUniqueHistName() );
  TEfficiency* newEff = new TEfficiency(*cumulativePassed, *cumulativeTotal);

  outDir->WriteTObject(eff, m_name.c_str() );
  outDir->WriteTObject(newEff, (m_name + "_cumulative").c_str() );
  printAndIncrementSavedCounter();
}
