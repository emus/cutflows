#include "CutChain/TH1Wrapper.h"
#include "CutChain/Cut.h"
#include <iostream>

CutChain::TH1Wrapper::TH1Wrapper(const std::string& name, std::function<TH1*()> initialize, std::function<void(TH1Wrapper&)> execute, std::vector<std::string> ignoredCuts)
  : TObjWrapper(name, ignoredCuts), m_initialize(initialize), m_execute(execute)
{}

CutChain::TH1Wrapper::TH1Wrapper(const TH1Wrapper& oldTH1)
  : TObjWrapper(oldTH1.m_name, oldTH1.m_ignoredCutNames), m_initialize(oldTH1.m_initialize), m_execute(oldTH1.m_execute)
{}

CutChain::TH1Wrapper::~TH1Wrapper() {
}

void CutChain::TH1Wrapper::copyToCut(CutChain::CutBase* targetCut) const
{
  TH1Wrapper* newTH1 = new TH1Wrapper(*this);
  copyObjectToCut(std::unique_ptr<TObjWrapper>(newTH1), targetCut);
}

void CutChain::TH1Wrapper::fill(double value, double weight)
{
  if (m_th1) {
    m_th1->Fill(value, containingCut->getWeight() * weight);
  }
  else {
    std::cerr << "TH1Wrapper " << containingCut->getFullName() << ":" <<  m_name << "has an invalid histogram pointer: " << m_th1.get() << "!" << std::endl;
    throw;
  }
}

void CutChain::TH1Wrapper::doInitialize()
{
  m_th1.reset(m_initialize() );
  m_th1->SetName(s_getUniqueHistName() );
  m_th1->SetDirectory(0);
  if (!m_th1->GetSumw2N() ) m_th1->Sumw2();
  printAndIncrementInitCounter();
}

void CutChain::TH1Wrapper::doExecute() { m_execute(*this); }

void CutChain::TH1Wrapper::saveOutput(TDirectory* outDir)
{
  if (!enabled) return;
  outDir->WriteTObject(m_th1.get(), m_name.c_str() );
  printAndIncrementSavedCounter();
}
