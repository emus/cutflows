#include "CutChain/TH2Wrapper.h"
#include "CutChain/Cut.h"
#include <iostream>

CutChain::TH2Wrapper::TH2Wrapper(const std::string& name, std::function<TH2*()> initialize, std::function<void(TH2Wrapper&)> execute, std::vector<std::string> ignoredCuts)
  : TObjWrapper(name, ignoredCuts), m_initialize(initialize), m_execute(execute)
{}

CutChain::TH2Wrapper::TH2Wrapper(const TH2Wrapper& oldTH2)
  : TObjWrapper(oldTH2.m_name, oldTH2.m_ignoredCutNames), m_initialize(oldTH2.m_initialize), m_execute(oldTH2.m_execute)
{}

CutChain::TH2Wrapper::~TH2Wrapper() {
}

void CutChain::TH2Wrapper::copyToCut(CutChain::CutBase* targetCut) const
{
  TH2Wrapper* newTH2 = new TH2Wrapper(*this);
  copyObjectToCut(std::unique_ptr<TObjWrapper>(newTH2), targetCut);
}

void CutChain::TH2Wrapper::fill(double valueX, double valueY, double weight)
{
  m_th2->Fill(valueX, valueY, containingCut->getWeight() * weight);
}

void CutChain::TH2Wrapper::doInitialize()
{
  m_th2.reset(m_initialize() );
  m_th2->SetName(s_getUniqueHistName() );
  m_th2->SetDirectory(0);
  if (!m_th2->GetSumw2N() ) m_th2->Sumw2();
  printAndIncrementInitCounter();
}

void CutChain::TH2Wrapper::doExecute() { m_execute(*this); }

void CutChain::TH2Wrapper::saveOutput(TDirectory* outDir)
{
  if (!enabled) return;
  outDir->WriteTObject(m_th2.get(), m_name.c_str() );
  printAndIncrementSavedCounter();
}
