#include "CutChain/TH3Wrapper.h"
#include "CutChain/Cut.h"
#include <iostream>

CutChain::TH3Wrapper::TH3Wrapper(const std::string& name, std::function<TH3*()> initialize, std::function<void(TH3Wrapper&)> execute, std::vector<std::string> ignoredCuts)
  : TObjWrapper(name, ignoredCuts), m_initialize(initialize), m_execute(execute)
{}

CutChain::TH3Wrapper::TH3Wrapper(const TH3Wrapper& oldTH3)
: TObjWrapper(oldTH3.m_name, oldTH3.m_ignoredCutNames), m_initialize(oldTH3.m_initialize), m_execute(oldTH3.m_execute)
{}

CutChain::TH3Wrapper::~TH3Wrapper() {
}

void CutChain::TH3Wrapper::copyToCut(CutChain::CutBase* targetCut) const
{
  TH3Wrapper* newTH3 = new TH3Wrapper(*this);
  copyObjectToCut(std::unique_ptr<TObjWrapper>(newTH3), targetCut);
}

void CutChain::TH3Wrapper::fill(double valueX, double valueY, double valueZ, double weight)
{
  m_th3->Fill(valueX, valueY, valueZ, containingCut->getWeight() * weight);
}

void CutChain::TH3Wrapper::doInitialize()
{
  m_th3.reset(m_initialize() );
  m_th3->SetName(s_getUniqueHistName() );
  m_th3->SetDirectory(0);
  if (!m_th3->GetSumw2N() ) m_th3->Sumw2();
  printAndIncrementInitCounter();
}

void CutChain::TH3Wrapper::doExecute() { m_execute(*this); }

void CutChain::TH3Wrapper::saveOutput(TDirectory* outDir)
{
  if (!enabled) return;
  outDir->WriteTObject(m_th3.get(), m_name.c_str() );
  printAndIncrementSavedCounter();
}
