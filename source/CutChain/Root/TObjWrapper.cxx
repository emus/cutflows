#include "CutChain/TObjWrapper.h"
#include "CutChain/Cut.h"
#include <sstream>
#include <iostream>

unsigned int CutChain::TObjWrapper::s_nCreatedTObjs = 0;
unsigned int CutChain::TObjWrapper::s_nInitialisedTObjs = 0;
unsigned int CutChain::TObjWrapper::s_nSavedTObjs = 0;
unsigned int CutChain::TObjWrapper::s_nTotalObjs = 0;
unsigned int CutChain::TObjWrapper::s_nTObjsToWrite = 0;
bool CutChain::TObjWrapper::s_printDebug = false;
bool CutChain::TObjWrapper::s_printCleanup = false;

void CutChain::TObjWrapper::printAndIncrementInitCounter()
{
  ++s_nInitialisedTObjs;
  if (s_printDebug)
    std::cout << "Initialised " << s_nInitialisedTObjs << "/" << s_nTotalObjs << " objects: " << containingCut->getName() << ": " << m_name << std::string(100, ' ') << "\r";
}

void CutChain::TObjWrapper::printAndIncrementSavedCounter()
{
  ++s_nSavedTObjs;
  if (s_printDebug)
    std::cout << "Saved " << s_nSavedTObjs << "/" << s_nTObjsToWrite << " objects: " << containingCut->getName() << ": " << m_name << std::string(100, ' ') << "\r";
}

CutChain::TObjWrapper::TObjWrapper(const std::string& name, std::vector<std::string> ignoredCutNames)
  : enabled(true), m_name(name), m_ignoredCutNames(ignoredCutNames)
{++s_nCreatedTObjs;}

CutChain::TObjWrapper::~TObjWrapper() {
  --s_nCreatedTObjs;
  if (s_printCleanup) 
    std::cout << "Deleted " << s_nTotalObjs - s_nCreatedTObjs << "/" << s_nTotalObjs << " objects\r";
}

std::vector<const CutChain::Cut*> CutChain::TObjWrapper::findIgnoredCuts()
{
  for (const std::string& name : m_ignoredCutNames) {
    std::vector<Cut*> foundCuts = containingCut->findCuts(std::regex(name), CutChain::UPSTREAM);
    for (Cut* cut : foundCuts) {
      m_ignoredCuts.push_back(cut);
      cut->setIsIgnored(true);
    }
  }
  return m_ignoredCuts;
}

int CutChain::TObjWrapper::s_histUID = 0;

const char* CutChain::TObjWrapper::s_getUniqueHistName()
{
  std::stringstream strStream;
  strStream << "h_JBUID_" << s_histUID;
  ++s_histUID;
  return strStream.str().c_str();
}

void CutChain::TObjWrapper::copyObjectToCut(std::unique_ptr<CutChain::TObjWrapper> object, CutChain::CutBase* targetCut) const
{
  // Note - targetCut might not be a Cut!! This is fine as it will only be used when it is (i.e. object.containingCut will be null in other cases)
  object->containingCut = dynamic_cast<Cut*>(targetCut);
  targetCut->m_tobjWrappers.push_back(std::move(object) );
}

void CutChain::TObjWrapper::initialize()
{
  if (enabled) doInitialize();
}

void CutChain::TObjWrapper::execute()
{
  if (enabled) doExecute();
}
