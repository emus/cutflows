#include "CutChain/TTreeWrapper.h"
#include "CutChain/Cut.h"
#include <iostream>

CutChain::TTreeWrapper::TTreeWrapper(const std::string& name, std::function<TTree*()> initialize, std::function<void(TTreeWrapper&)> execute, std::vector<std::string> ignoredCuts)
  : TObjWrapper(name, ignoredCuts), m_initialize(initialize), m_execute(execute)
{}

CutChain::TTreeWrapper::TTreeWrapper(const TTreeWrapper& oldTTree)
  : TObjWrapper(oldTTree.m_name, oldTTree.m_ignoredCutNames), m_initialize(oldTTree.m_initialize), m_execute(oldTTree.m_execute)
{}

CutChain::TTreeWrapper::~TTreeWrapper() {}

void CutChain::TTreeWrapper::copyToCut(CutChain::CutBase* targetCut) const
{
  TTreeWrapper* newTTree = new TTreeWrapper(*this);
  copyObjectToCut(std::unique_ptr<TObjWrapper>(newTTree), targetCut);
}

void CutChain::TTreeWrapper::doInitialize()
{
  m_tree.reset(m_initialize() );
  m_tree->SetName(s_getUniqueHistName() );
  m_tree->SetDirectory(0);
  printAndIncrementInitCounter();
}

void CutChain::TTreeWrapper::doExecute() { m_execute(*this); }

void CutChain::TTreeWrapper::saveOutput(TDirectory* outDir)
{
  if (!enabled) return;
  outDir->WriteTObject(m_tree.get(), m_name.c_str() );
  printAndIncrementSavedCounter();
}
