======================
CutChain Documentation
======================

:authors: Jon Burr
:contact: jonathan.thomas.burr@cern.ch

.. contents:: Table of contents

Introduction
============

CutChain is a set of classes that allow you to perform an analysis cutflow. The package's model assumes that you'll have a set of control/signal (etc) regions that share many identical cuts which then diverge later on in the cutflow. The analysis is then structured as a tree of cuts which then branches when the sequences of cuts required for the various regions diverge. When running, the program checks each cut in turn. If the cut is passed then control passes to the next cut in the chain. If the cut is failed or if all the cuts in a branch are passed then control passes to another branch until all branches in the event have been processed. Histograms (and other objects) can also be added to the chain which will be filled if the event passes.

Brief introduction to function lambdas
======================================

It might be possible to avoid using function lambdas in CutChain but it would make your life very difficult and miss most of the point of using the package so I'll give a (fairly) brief introduction to what they are and how to use them. This won't be a full discussion of how they work - there are plenty of places findable on Google that can give you that information...
A simple example of a function lambda declaration is this:

::
  
  auto isEven = [] (int i) -> bool { return i % 2 == 0; };

First, what are the various parts of this declaration?


``auto isEven``
    This declares a variable called isEven. ``auto`` just lets the compiler deduce what its type should be. This is necessary when dealing with function lambdas as each lambda is technically its own unique type as created by the compiler (it inherits from std::function otherwise it wouldn't be so useful).

``[]``
    This is the 'capture specification'. In simple function lambdas such as this one it simply tells the compiler that you're creating a function lambda and to do all the requisite wizardry. I'll come back to the more complicated usages later.

``(int i)``
    This is the argument list of the lambda. This is exactly the same as the argument list in a standard function declaration

``-> bool``
    This is a special piece of C++11 syntax that tells the compiler that return value should be of type ``bool``. This isn't strictly necessary - the compiler can deduce that the return value is Boolean. This might not always be true and in any case it's usually best to be specific. If you have multiple return values present then the compiler may have difficulties (especially if you're using a non C++14 compliant compiler)

``{...}``
    This is just the function body. This is also exactly the same as the function body in a standard function declaration

This object can be called *exactly* like a function::

    bool result = isEven(2); // true
    result = isEven(1); // false
  
  
The capture specification
-------------------------
  
This is where the power of function lambdas really shows. This part of the declaration allows the lambda to 'capture' variables from the enclosing scope and use them within itself. There are two main sorts of lambda capture: by value and by reference. The first is quite simple. Let's imagine we want to create a cut on the number of jets:

::

    int nJetCut = 6;
    auto cut = [nJetCut] (int nJets) -> bool { return nJets >= nJetCut; };

[nJetCut] tells the compiler that it needs to capture the external variable ``nJetCut`` by value and use it in the function. nJetCut could be set to anything and the function lambda will pick it up. However once the lambda has picked it up (i.e. it's been put in the [] capture specification) that is the value that that lambda will use for the entirety of its lifetime. At some later point in your code you could call 'cut(nJets) and it will perform 'nJets >= nJetCut' using as nJetCut whatever it was set to when you declared the variable. Any variable that is in scope at when the lambda is declared can be captured.
The second type (by reference) is much, much more powerful but also means you have to be a little bit more careful. (This is also the main way you're intended to use function lambdas in CutChain). Let's look at this function lambda

::

    int nJetCut = 6;
    int nJets = 5;
    auto cut = [nJetCut, &nJets] () -> bool { return nJets >= nJetCut; };

[&nJets] tells the compiler that it needs to capture the external variable ``nJets`` by reference and use it in the function. If we call 'cut()' just after this declaration it will obvious return false (5 is clearly less than 6...). If however we were to call 'nJets = 8; cut()' that function call would now return true. This is because our lambda is keeping a reference to the variable nJets so it will pick up any changes to that variable.
This is where you have to be careful. Let's say we define this function:

::

  auto getLambda() {
    int nJetCut = 6;
    int nJets = 5;
    return [nJetCut, &nJets] () -> bool { return nJets >= nJetCut; };
  }

This is a terrible idea. If you try to use the returned lambda bad things will happen, anywhere from a segfault to completely random behaviour. This is because the variable nJets that the lambda has a reference to no longer exists. This kind of thing is called a dangling pointer or dangling reference and is bad... The above scenario is an obvious one but not a likely one. One you may actually have to watch out for would be this:

::

    int nJetCut = 6;
    std::vector<int> numbersOfJets( {4,6,9,10} );
    int& nJets(numbersOfJets.at(1) );
    auto cut = [nJetCut, &nJets] () -> bool { return nJets >= nJetCut; };
    if (cut() ) { ... }
    nJets.push_back(13);
    if (cut() ) { ... }

Now - what happens here? As before we create ``nJetCut`` and ``nJets``. This time however nJets is a reference to an element in a vector. The first if statement executes correctly (the block executes as 6 is indeed greater than 6). The second if statement? I have no idea - this is now undefined behaviour. Pushing back an entry into a vector can move the locations of all the vector's entries in memory which leaves all former references or pointers to them as dangling references/pointers. You could be lucky and the vector doesn't have to resize (and therefore move all of its elements) and everything works as expected. If the vector does resize then a few things could happen. You could be lucky - the program might realise that something is up and throw a segfault. If you're unlucky the memory pointed to by nJets might still be interpretable as an ``int`` and the program will go right ahead and do that - this means that nJets could now be practically any value. 
This is not to say you can't use vectors. You can capture the whole vector by reference. You can even capture an individual element by reference too - just so long as you don't do anything the vector that invalidates references to its elements. These are well documented (http://stackoverflow.com/questions/6438086/iterator-invalidation-rules). Note that these restrictions also apply to other STL containers and similar objects. (These problems aren't unique to function lambdas - it's just in case you aren't familiar with habitually using lambdas you may not be aware of these things).

There is one other way of capturing variables. Let's consider the case where we are declaring and using this lambda within a class and we wish to capture by reference a member variable of this class. Let's assume for now that we have some class MyClass that has member variables 'int nJets' and 'std::function<bool()> cut' (the latter one is a nice C++11 wrapper class that makes function pointers understandable...)

::

    class MyClass {
      public:
      MyClass(int nJetCut = 6) : nJets(0), cut([nJetCut, this] () -> bool { return nJets >= nJetCut; };) {}
      int nJets;
      std::function<bool()> cut;
    };

What have we done here? Well - instead of capturing nJets by reference we now capture the ``this`` pointer of the class. This now gives us access to all of the class' member variables. As ``this`` is a pointer this is very like having captured them by reference - nJets as used by cut will always be the current value of nJets. If we were to try and use this now

::

    MyClass class1(); // Uses nJets >= 6
    class1.cut(); // False - nJets = 0; 0 < 6
    class1.nJets = 8;
    class1.cut(); // True - nJets = 8; 8 >= 6

    MyClass class2(4); // Uses nJets >= 4
    class2.cut(); // False - nJets = 0; 0 < 4
    class2.nJets = 5;
    class2.cut(); // True - nJets = 5; 5 >= 4

    class1.cut(); // Still true - nJets = 8; class2.nJets and class1.nJets are different things so changing one doesn't affect the other.

One final comment. It is possible to give the capture specification a 'default' capture. This means if you begin your capture specification with '&' or '=' then the compiler will automatically capture any variables that you use in your lambda body without naming in the capture specification (or in the argument list). Don't do this. There isn't really any good reason not to explicitly capture everything you use and it makes it that much harder to accidentally capture a variable you didn't mean to (or capture something by reference when you wanted it by value or vice versa).


What does a lambda actually look like?
--------------------------------------

So what is the compiler actually doing when you make a function lambda? What it essentially does is make a mini class for you that contains all the information you need to use it. (This is meant to help you understand what is happening in the program - it's not going to be very accurate). I'll summarise how the various lambda classes used as examples here would look. (The class and variable names are compiler generated and not usually human readable - but you should never have to read them so this is fine :) )

::

    // auto isEven = [] (int i) -> bool { return i % 2 == 0; }
    class lambda1 : public std::function<bool(int)> 
    {
      lambda1() {}
      bool operator() (int i) override { return i % 2 == 0; }
    };
    auto isEven = lambda1();

-----------------------------------

::

    // int nJetCut = 6;
    // auto cut = [nJetCut] (int nJets) -> bool { return nJets >= nJetCut; };
    class lambda2 : public std::function<bool(int)>
    {
      lambda2(int nJetCut) : nJetCut(nJetCut) {}
      int nJetCut;
      bool operator() (int nJets) override { return nJets >= nJetCut; }
    };
    int nJetCut = 6;
    auto cut = lambda2(nJetCut);
    
-----------------------------------

::

    // int nJetCut = 6;
    // int nJets;
    // auto cut = [nJetCut, &nJets] () -> bool { return nJets >= nJetCut; }
    class lambda3 : public std::function<bool()>
    {
      lambda3(int nJetCut, int& nJets) : nJetCut(nJetCut), nJets(nJets) {}
      int nJetCut;
      int& nJets;
      bool operator() () override { return nJets >= nJetCut; }
    };
    int nJetCut = 6;
    int nJets;
    auto cut = lambda3(nJetCut, nJets);

---------------------------------

::

    // class MyClass {
    //   public:
    //   MyClass(int nJetCut = 6) : nJets(0), cut([nJetCut, this] () -> bool { return nJets >= nJetCut; };) {}
    //   int nJets;
    //   std::function<bool()> cut;
    // };

    class MyClass {
      class lambda4 : public std::function<bool()>
      {
        lambda4(int nJetCut, MyClass* myClassInstance) : nJetCut(nJetCut), myClassInstance(myClassInstance) {}
        int nJetCut;
        MyClass* myClassInstance;
        bool operator() () override { return (*myClassInstance).nJets >= nJetCut; }
      };
      MyClass(int nJetCut = 6) : nJets(0), cut(lambda4(nJetCut, this) ) {}
      int nJets;
      std::function<bool()> cut;
    };

(The inheritance of the lambda-classes isn't quite right but hopefully you get the idea...)

Anyway - this should complete the (not so) brief introduction to function lambdas - I may at some point in the future move this to a separate file...

Using CutChain
==============

For the rest of this introduction I'm going to assume that you are defining your chain within a class that has a variable called ``event`` that contains all the variables you need for your cutflow (I suggest that you use the package like this though you don't have to).

Cuts are loosely contained within a ``Chain`` object. (All CutChain classes are contained within the CutChain namespace - for the following code snippets I'll assume that ``using namespace CutChain;`` is present somewhere in the code).
The Chain object contains the first cut in your cut chain - accessible through:

::

  Chain chain;
  Cut* firstCut = chain.startCut.get(); // The startCut variable is a std::unique_ptr - this means that chain owns all the following cuts and all the memory is managed correctly.

In order to add a cut (Let's assume that the ``event`` object has a ``passGRL`` member) you call

::

  Cut* nextCut = firstCut->addCut("passGRL", [this] () -> bool{ return event.passGRL; }, true);

This returns a pointer to the cut you've just added. The arguments are (in order), the name of the cut as it will appear in the cutflow and output file structure, a std::function<bool()> object (which is your function lambda that decides whether the cut is passed or not) and a final optional Boolean variable. If this is set to false then no plots (or other objects) will be made for this cut. It is true by default. Note that we capture ``event`` through the ``this`` pointer in the lambda.

Let's say that next we want to split the chain and apply cuts on different jet multiplicities (``nJets`` in our ``event`` object). First we make a ``Node`` object

::

  Node* jetMultNode = nextCut->splitChain();

Then we build the branches (probably easiest done in a for loop).

::

  for (int nJetCut : {4,5,6,7,8} ) {
    std::string cutName = std::to_string(nJetCut) + "jets";
    nextCut = jetMultNode->addSubchain(cutName, cutName, [nJetCut, this] () -> bool { return event.nJets >= nJetCut; }, true);

    // Build the rest of the branch here inside the loop
  }

The arguments supplied to ``CutChain::Node::addSubchain`` are (in order), the name of the subchain (as it will appear in the output file structure), then the last three are the same as for ``CutChain::Cut::addCut``.
You can also apply a further split after a node (if you want...) by calling ``Node* newNode = jetMultNode->splitChain(subchainName)``.

If you try to add a cut or node after a cut that already has a cut following it or to a node that already has a subchain of that name it will throw an error. However it is possible that you might want to add a cut that already exists (as you work down the chain until you reach a later branching point). In this case you can use the '``getAddCut``, ``getAddSubchain``, ``getSplitChain``...' methods. These take as their first argument the ``Cut*`` or ``Node*`` into which you wish to put a pointer to the created or retrieved ``Cut*`` or ``Node*`` (i.e. the object that would be the return value of ``addCut`` etc) and then the exact same arguments as the other methods. If the cut/node exists then a the supplied pointer will be set to point to it, otherwise it will be created. If you're trying to get/create a cut then the function will also check the name of the cut you're trying to get to the name of the cut that already exists there (if it exists). If they are different it will return false and the pointer will be set to 0. 
For example the splitting (done above could otherwise be done as

::

  for (int nJetCut : {4,5,6,7,8} ) {
    std::string cutName = std::to_string(nJetCut) + "jets";
    if (!jetMultNode->getAddSubchain(nextCut, cutName, cutName, [nJetCut, this] () -> bool { return event.nJets >= nJetCut; }, true) ) throw; // Throw an error if getAddSubchain returns false as it means that something's gone wrong

    // Build the rest of the branch here inside the loop
  }

Adding histograms etc
---------------------

Histograms etc are added to the chain through the use of wrapper classes that inherit from the ``TObjWrapper`` class. They are then added through the ``addTObj`` method.

::

  nextCut->addTObj(TH1Wrapper("h_nJets",
      [] () -> TH1* { return new TH1F("h_nJets", "Number of Jets; nJets", 15, 0, 15); },
      [this] (TH1Wrapper th1) { th1.fill(event.nJets); } ) );

The ``TH1Wrapper`` constructor takes (in order) the name of the histogram to be created, a function that tells it how to make the histogram and a function that tells it how to fill the histogram. Currently ``TH1Wrapper`` and ``TH2Wrapper`` are implemented and defined


I'll add further sections to this soon (including sections on how to define and add your own ``TObjWrappers``).
