#ifndef READTREE_BITFLAGS_H
#define READTREE_BITFLAGS_H
#include <boost/preprocessor/seq/for_each.hpp>
#include <boost/preprocessor/stringize.hpp>
#include <string>
#include <cstdint>

namespace ReadTreeBitFlags {
  //constexpr unsigned int ENUM_BASE = __COUNTER__ + 1;
  template <typename T>
    inline std::string toString(const T& val);
  template <typename T>
    inline T fromString(const std::string& val);
  typedef uint32_t bitflag_t;
}

// NB - as this uses 1ul this has an implicit limit of 32 different flags
#define RTBF_ENUM_ITEM_DECL(a, b, elem) elem = ReadTreeBitFlags::bitflag_t(1) << ( __COUNTER__ - ENUM_BASE),
#define RTBF_ENUM_ITEM_CASE(a, name, elem) case name::elem: return BOOST_PP_STRINGIZE(elem);
#define RTBF_ENUM_ITEM_IFELSE(a, name, elem) if (val == BOOST_PP_STRINGIZE(elem)) return name::elem;
#define RTBF_DECL_ENUM(name, seq)                                              \
  namespace name {                                                             \
    constexpr unsigned int ENUM_BASE = __COUNTER__ + 1;                        \
    enum name { BOOST_PP_SEQ_FOR_EACH(RTBF_ENUM_ITEM_DECL, _, seq) };          \
  }                                                                            \
  namespace ReadTreeBitFlags {                                                 \
    template <>                                                                \
      inline std::string toString(const name::name& val)                       \
      {                                                                        \
        switch(val) {                                                          \
          BOOST_PP_SEQ_FOR_EACH(RTBF_ENUM_ITEM_CASE, name, seq)                \
          default: return "unknown";                                           \
        }                                                                      \
      }                                                                        \
    template<>                                                                 \
      inline name::name fromString(const std::string& val)                     \
      {                                                                        \
        BOOST_PP_SEQ_FOR_EACH(RTBF_ENUM_ITEM_IFELSE, name, seq)                \
        return name::name(0);                                                  \
      }                                                                        \
  }

#endif //ReadTreeBitFlags
