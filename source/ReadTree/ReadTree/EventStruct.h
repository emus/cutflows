#ifndef ReadTree_EVENTSTRUCT_H
#define ReadTree_EVENTSTRUCT_H

#include <TTreeReaderValue.h>
#include <TTreeReader.h>
#include <TTreeReaderArray.h>

#include <TLorentzVector.h>
#include <vector>
#include <iostream>
#include "ReadTree/PhysicsObjects.h"
#include "ReadTree/FakeWeightGetter.h"
#include "ReadTree/MuonBiasTools.h"


#include "AtlasOxbridgeKineticsInterface/OKSingleParents.h"
#include "AtlasOxbridgeKineticsInterface/OKTwoParents.h"

// Test for FakeBkgTool 
#include "AsgTools/AnaToolHandle.h"
#include "AsgAnalysisInterfaces/IFakeBkgTool.h"
#include "AsgAnalysisInterfaces/ILinearFakeBkgTool.h"

#include "FakeBkgTools/LhoodMMEvent.h"
#include "FakeBkgTools/LhoodMM_tools.h"

#include "TH3.h"
#include "TGraph2D.h"


// IFF Truth classification types, copied from IFFTruthClassifier/IFFTruthClassifierDefs.h
// Note, could probably have used just an enum (and not an enum class), to avoid static cast
namespace IFF {
  enum class Type : int {
    Unknown = 0,
    KnownUnknown = 1,
    IsoElectron = 2,
    ChargeFlipIsoElectron = 3,
    PromptMuon = 4,
    PromptPhotonConversion = 5,
    ElectronFromMuon = 6,
    TauDecay = 7,
    BHadronDecay = 8,
    CHadronDecay= 9,
    LightFlavorDecay = 10,
  };
}  // namespace IFF

// TODO share the enum class between these things... Maybe we should have some central 'configured tool' class?
enum class OutputSetting {
  Off = 0,
  Quick = 1,
  Full = 2
};

/* OutputSetting getOutputSetting(int input); */

// match to SUSYTools one for now
// if we don't mind an extra dependency on ST, just grab from there?
enum class DataSource : int{
  Uninitialized = -1,
  Data = 0,
  FullSim = 1,
  AtlfastII = 2
};



struct Squirrels {

  double M_side1;
  double M_side2;
  size_t Nvis_side1;
  size_t Nvis_side2;

  // One parent
  double M1T;
  double MT1;
  double Mo1;
  double M1o;

  // Two parent
  double M2T;
  double MT2;
  double Mo2;
  double M2o;
  double MCT;

  // Asymmetric two-parent variables
  std::vector<double> aMT2;
  std::vector<double> aM2T;

  void clear() {
    M_side1 = M_side2 = 0;
    Nvis_side1 = Nvis_side2 = 0;
    M1T = MT1 = Mo1 = M1o = 0;
    M2T = MT2 = Mo2 = M2o = MCT = 0;
    aMT2 = {0, 0, 0, 0, 0, 0};
    aM2T = {0, 0, 0, 0, 0, 0};
  }

  // Skipping MTgen as unlikely to be helpful
};


class EventStruct
{
  public:

    // Constructor
    EventStruct(std::string muon_reco_bias_type, std::string muon_trigger_bias_type, std::string muon_efficiency_histos, std::string muon_efficiency_trig_histos) : 
      m_cutchainOutputMode(OutputSetting::Full), 
      m_muon_reco_bias_correction_tool(muon_reco_bias_type, muon_efficiency_histos, muon_efficiency_trig_histos),
      m_muon_trigger_bias_correction_tool(muon_trigger_bias_type, muon_efficiency_histos, muon_efficiency_trig_histos),
      m_asmTool("CP::AsymptMatrixTool/Tool1"),
      m_lhmTool("CP::LhoodMM_tools")
      /*m_lhmTool_eMmuP("CP::LhoodMM_tools1")*/
//      m_ttbarweighthists(ttbarweighthistos)
  {
      }

    // Initializer list
    OutputSetting m_cutchainOutputMode;
    MuonBiasCorrectionTool m_muon_reco_bias_correction_tool;
    MuonBiasCorrectionTool m_muon_trigger_bias_correction_tool;
    asg::AnaToolHandle<CP::ILinearFakeBkgTool> m_asmTool; 
    CP::LhoodMM_tools m_lhmTool;

    // Destructor
    ~EventStruct() {}

    void connect(TTree* tree);
    /*void connectTruth(TTree* truthTree) { truthEvent.connect(truthTree); }*/
    bool CalculateEventValues(bool applyHighPtIso);
    double CalculateFakeWeights();
    double CalculateDibosonScaleWeight(std::string inputTreeName);
    double CalculatePDFWeight(std::string inputTreeName);
    double CalculateISRWeight(std::string inputTreeName);
    void CalculateEventWeight(bool skipPRW, bool applyFakeWeights, bool applyMuonBias, std::string inputTreeName, bool useLikelihoodMM, bool applyttbarWeights, bool applyPDFWeight, bool applyISRWeight, bool applyDibosonScaleWeight);
    std::map<std::string, double> CalculateMuonBiasTest(std::string);
    // squirrels 
    void CalculateSquirrels();
    void CalculateSquirrelsObjects(Squirrels&, std::string);
    /*void CalculateSquirrelsWithPtEtaCuts(float jetptcut, float etacut, Squirrels& squirrel);*/

    bool debug;
    int LLSR;
    std::string fakeSyst;
    std::string resolutionMode;
    /*std::string sherpa_weightName;*/




    /////////////////////////////////////
    // Variables set during alg.configure()
    /////////////////////////////////////
    bool m_isMC;
    bool m_isData;
    /*bool m_isSherpa;*/
    int m_dsid;
    float m_lumiweight;
    float m_fake_weight; 
    float m_IFF_fake_weight;

    OutputSetting m_outputMode;
    /*DataSource    m_dataSource;*/
    std::string   m_prwConfName;
    std::string   m_prwLumiCalcName;
    std::string   m_systOption;
    bool          m_doTruth;
    bool          m_isTruth;
    bool          m_disableWeights;
    std::string   m_STConfFile;
    int           m_JESNPSet;

    std::string   m_fatJetName;
    bool          m_hasFatJets;

    /*OutputSetting m_photonMode;*/
    bool          m_outputPhotonCut;
    /*OutputSetting m_tauMode;*/
    bool          m_doPhotonOR;
    bool          m_doTauOR;
    bool          m_usePhotonMET;
    bool          m_useTauMET;
    std::string   m_gitCommitID;

    double       scaling;


    // Holly's muon bias weights
    double m_muon_recoweight;
    double m_muon_triggerweight;
    double m_muon_reco_etaphibias_up;
    double m_muon_reco_etaphibias_down;
    double m_muon_reco_ptscale_up;
    double m_muon_reco_ptscale_down;
    double m_muon_trigger_etaphibias_up;
    double m_muon_trigger_etaphibias_down;
    double m_muon_trigger_ptscale_up;
    double m_muon_trigger_ptscale_down;

    /////////////////////////////
    // Fake weights
    /////////////////////////////

    FakeWeightGetter m_fake_weight_getter;
    void initializeFakeWeightHistos(std::string);

    void InitialisettbarWeight(std::string);
    void InitialisettbarWeight2D(std::string);
    std::map<std::string, double> CalculateTtbarWeight(double, int);
    std::map<std::string, double> CalculateTtbarWeight2d(double, double, int);


    /////////////////////////////
    // IFF Fake background tool 
    /////////////////////////////


    // Test for FakeBkgTool
    StatusCode ExecuteMatrixMethod(); // FakeBkgTool
    StatusCode InitializeFakeBkgTools(); // WJF, new: initialize for ATLAS tools 
    StatusCode InitialiseLikelihoodMMTool(CP::LhoodMM_tools*, std::string, std::string); // 
    StatusCode FinalizeFakeBkgTools(); // WJF: test ATLAS tools

    // Attempt to implement IFF fake weight calculators (could be a private member)  
    /*CP::LhoodMM_tools m_lhmTool_eMmuP;*/
    int LL_event_count;
    float m_channel;

    float m_EventCount;
    float m_lep1Pt; 
    float m_lep2Pt; 
    float m_lep1Phi; 
    float m_lep2Phi; 
    float m_lep1Eta; 
    float m_lep2Eta; 

    float m_ele1Pt;
    float m_muon1Pt;

    float HT_jets;
    float HT_all;

    float m_sum_mt_met;
    float m_sum_mt_jet1;
    float m_sum_mass_lepton_jet1;

    float m_mass_muon_jet1;
    float m_mass_electron_jet1;

    float m_max_mt_jet1;
    float m_max_mass_lepton_jet1;
    float m_mt2;
    float m_met;
    float m_metsig;
    float m_jet1Pt;
    float m_jet2Pt;
    float m_jet1Phi;
    float m_jet1Eta;

    float m_max_mt_met;
    float m_NJets; // not really float but register2DHistogram isn't overloaded for ints ... 

    float m_lep1Pt_ePmuM;
    float m_lep1Pt_eMmuP;

    TH1F* LL_lep1Pt_ePmuM;
    TH1F* LL_lep1Pt_eMmuP;

    TH1F* LL_lep1Pt; 
    TH1F* LL_ele1Pt;
    TH1F* LL_muon1Pt;

    TH1F* LL_sumMT_histo;


    TH1F* LL_yield;
    TH2F* LL_EventCount_channel;

    TH2F* LL_lep1Pt_channel;
    TH2F* LL_lep2Pt_channel;
    TH2F* LL_ele1Pt_channel;
    TH2F* LL_muon1Pt_channel;

    TH2F* LL_sumMT_channel;
    TH2F* LL_MT2_channel;

    TH2F* LL_jet1Pt_channel;
    TH2F* LL_jet2Pt_channel;

    TH2F* LL_EtMissSignificance_channel;
    TH2F* LL_EtMiss_channel;
    TH2F* LL_sum_pt_leps_jet1_channel;

    TH2F* LL_max_mt_met_channel;
    TH2F* LL_lep1Phi_channel;
    TH2F* LL_lep2Phi_channel;
    TH2F* LL_jet1Phi_channel;
    TH2F* LL_jet1Eta_channel;
    TH2F* LL_lep1Eta_channel;
    TH2F* LL_lep2Eta_channel;

    TH2F* LL_sumMtJet1_channel;
    TH2F* LL_sum_mass_lepton_jet1_channel;
    TH2F* LL_max_mt_jet1_channel;
    TH2F* LL_max_mass_lepton_jet1_channel;
    TH2F* LL_NJets_channel;

    TH3F* LL_lep1Pt_EtMiss_channel; 
    TH3F* LL_HTjets_EtMiss_channel; 
    TH3F* LL_HTall_EtMiss_channel; 

    TH3F* LL_max_mt_met_v_mt2_channel;
    TH3F* LL_sumljpt_v_metsig_channel; 
    TH3F* LL_metsig_v_mt2_channel;





    /////////////////////////////
    // Common per-event variables
    /////////////////////////////


    // isMC only variables
    Float_t                mc_event_weight = 1.0;
    std::vector<Float_t> * mc_pdf_weights = 0;
    std::vector<Float_t> * mc_scale_weights = 0;
    Float_t mc_isr_mur05muf05_weight = -1.0;
    Float_t mc_isr_mur20muf20_weight = -1.0;
    Float_t mc_isr_var3cup_weight    = -1.0;
    Float_t mc_isr_var3cdown_weight  = -1.0;
    Float_t ttbar_weight  = -1.0;
    Double_t               sherpaWeight = 1;
    Float_t                GenFiltHT = 1;
    Float_t                GenFiltMET = 1;
    //Float_t                xsectTimesEff = 1;
    Float_t                pileup = 1;
    Float_t                m_pileup_weight = 1;
    Float_t                totalEventWeight;
    ULong64_t              EventNumber;
    /*Int_t                  LumiBlock;*/
    Float_t                triggerGlobalEfficiencySF = 1.0;
    Float_t                triggerGlobalEfficiency = 1.0;


    /////////////////////////////
    // Electron variables
    /////////////////////////////

    std::vector<Float_t> *elePt = 0;
    std::vector<Float_t> *eleEta = 0;
    std::vector<Float_t> *elePhi = 0;
    std::vector<Float_t> *eleE = 0;
    std::vector<Int_t>   *eleCharge = 0;
    std::vector<Char_t>  *eleIsTighterNearlySignal = 0;
    std::vector<Char_t>  *elePassCFT = 0;
    std::vector<Char_t>  *eleIsInclusiveLooserNearlySignal = 0;
    std::vector<Char_t>  *eleIsSingleTriggerMatched = 0; // old
    std::vector<Char_t>  *eleIsDiElectronTriggerMatched = 0;
    std::vector<Char_t>  *eleIsEMuTriggerMatched = 0;
    std::vector<Float_t> *eled0sig = 0;
    std::vector<Float_t> *elez0sinTheta = 0;
    std::vector<Char_t>  *eleIsGradient = 0;
    std::vector<Char_t>  *eleIsFCTight = 0;
    std::vector<Char_t>  *eleIsFCLoose = 0;
    std::vector<Char_t>  *eleIsFCHighPtCaloOnly = 0;
    std::vector<Char_t>  *eleIsIsolated = 0;
    /*std::vector<Char_t>  *eleIsPrompt = 0;*/
    std::vector<Float_t> *eleScaleFactor = 0;
    std::vector<Float_t> *eleTruthCharge=0;
    std::vector<Int_t>   *eleTruthType=0;
    std::vector<Int_t>   *eleTruthOrigin=0;
    std::vector<Float_t> *eleChargeIDSF=0;
    std::vector<Float_t> *eleChargeFlipTaggerSF=0;
    /*std::vector<Float_t> *eleTrigSF=0;*/
    std::vector<Float_t> *eleLooseSF=0;
    std::vector<Float_t> *eleIsoSF=0;
    std::vector<Int_t>   *eleIFFTruthClassification=0;

    

    /////////////////////////////
    // Muon variables
    /////////////////////////////

    std::vector<Float_t> *muPt = 0;
    std::vector<Float_t> *muEta = 0;
    std::vector<Float_t> *muPhi = 0;
    std::vector<Float_t> *muE = 0;
    std::vector<Int_t>   *muCharge = 0;
    std::vector<Char_t>  *muIsIsolated = 0;
    std::vector<Char_t>  *muIsFCLoose = 0;
    std::vector<Char_t>  *muIsFCTight = 0;
    std::vector<Char_t>  *muIsTight_VarRad = 0;
    std::vector<Char_t>  *muIsHighPtTrackOnly = 0;

    std::vector<Char_t>  *muIsSignal = 0;
    /*std::vector<Char_t>  *muIsPrompt = 0;*/
    std::vector<Char_t>  *muIsSingleTriggerMatched = 0; // old
    std::vector<Char_t>  *muIsDiMuonTriggerMatched = 0;
    std::vector<Char_t>  *muIsEMuTriggerMatched = 0;
    std::vector<Float_t> *mud0sig = 0;
    std::vector<Float_t> *muz0sinTheta = 0;


    // MC 
    std::vector<Float_t> *muScaleFactor = 0;
    std::vector<Float_t> *muIsoSF=0;
    std::vector<Float_t> *muTruthCharge=0;
    std::vector<Int_t>   *muTruthType=0;
    std::vector<Int_t>   *muTruthOrigin=0;
    std::vector<Int_t>   *muIFFTruthClassification=0;
    
    
    /////////////////////////////
    // Jet variables
    /////////////////////////////

    std::vector<Float_t>  *jetPt = 0;
    std::vector<Float_t>  *jetEta = 0;
    std::vector<Float_t>  *jetPhi = 0;
    std::vector<Float_t>  *jetMass = 0;
    std::vector<Char_t>   *jetIsSignal = 0;



    /////////////////////////////
    // Photon  variables
    /////////////////////////////
    
    std::vector<Float_t>  *photonPt = 0;
    std::vector<Float_t>  *photonEta = 0;
    std::vector<Float_t>  *photonPhi = 0;
    std::vector<Char_t>   *isSignalPhoton = 0;
    std::vector<Char_t>   *photonPassOR = 0;
    std::vector<Char_t>   *photonPassJetPhotonOR = 0;
    std::vector<Double_t> *photonResolutionAsJet = 0;


    /////////////////////////////
    // MET  variables
    /////////////////////////////

    Float_t                EtMiss;
    Float_t                EtMissSignificance;
    Float_t                EtMissPhi;

    /*TruthEventStruct truthEvent;*/


    /////////////////////////////
    // Calculated variables
    /////////////////////////////
    
    std::vector<PO::Lepton> leptons;
    std::vector<PO::Electron> baseline_electrons;
    std::vector<PO::Muon> baseline_muons;
    std::vector<PO::Jet> baseline_jets;

    std::vector<PO::Electron> loose_electrons;
    std::vector<PO::Muon> loose_muons;
    std::vector< std::reference_wrapper<PO::Lepton> > loose_leptons; 

    std::vector<PO::Electron> tight_electrons;
    std::vector<PO::Muon> tight_muons;
    std::vector< std::reference_wrapper<PO::Lepton> > tight_leptons; 

  


    std::vector<PO::Jet> signalJets;

    float mll;
    float dPhiBoost;
    TVector2 met_twovec;
    float dPhi_ll; // delta phi between two leading leptons 
    TLorentzVector met_tlv; 

    float sum_mt_met; 
    float sum_mt_jet1;
    float sum_mass_lepton_jet1;
    float sum_dphi_lepton_jet1;
    float sum_pt_leps_jet1;

    float max_mt_met;
    float max_mt_jet1;
    float max_mass_lepton_jet1;
    float max_dphi_lepton_jet1;

    float mass_muon_jet1;
    float mass_electron_jet1;

    unsigned int count_loose_muons;
    unsigned int count_loose_electrons;

    TFile* muon_file;
    TTree* muon_tree;
    unsigned int count_tight_muons;
    unsigned int count_tight_electrons;

    /////////////////////////
    // Flying squirrels
    /////////////////////////
    OK::OKSingleParents ok1p;
    OK::OKTwoParents ok2p;

    /*Squirrels sqVars_j50eta20;*/
    /*Squirrels sqVars_j80eta20;*/
    /*Squirrels squirrels;*/
    Squirrels squirrel_all;
    Squirrels squirrel_leptons;


  private:
    std::vector<TGraph2D*> m_ttbarWeightGraphs2D;
    std::vector<TH2D*> m_ttbarWeightHists2D;
    std::vector<TH1D*> m_ttbarWeightHists;
    /*mutable std::map<int, std::vector<unsigned int>> nJets_eta20;*/
    /*mutable std::map<int, std::vector<unsigned int>> nFatJets_eta20;*/


};


#endif //ReadTree_EVENTSTRUCT_H
