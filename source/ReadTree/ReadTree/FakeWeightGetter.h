#ifndef ReadTree_FAKEWEIGHTGETTER_H
#define ReadTree_FAKEWEIGHTGETTER_H

#include <TFile.h>
#include <TDirectory.h>
#include <TString.h>
#include <TKey.h>
#include <TH1.h>
#include <TGraphAsymmErrors.h>
#include <limits>
#include <map>
#include <iostream>
#include <sstream>

#include "ReadTree/PhysicsObjects.h"


/*********************
 *
 * Origionally transcribed from <old_xaodanalysis_athena>/source/tree_decoration/get_fake_weights.py
 * e.g.: /usera/wfawcett/cambridge/emus/old_xaodanalysis_athena/source/tree_decoration/get_fake_weights.py
 *
 * ************/

// useful struct 
struct EfficiencyTuple{
  float eff;
  float eff_up;
  float eff_down;
};

class FakeWeightGetter
{
  private:
    std::string m_efficiency_file;
    std::map< std::string, TGraphAsymmErrors*> m_efficiency_graphs; 
    std::map< std::string, TGraphAsymmErrors*> m_probability_graphs; 
    std::map< std::string, TGraphAsymmErrors*> m_abcd_graphs; 
    std::map<std::string, TGraph> m_efficiency_graphs_down;
    std::map<std::string, TGraph> m_efficiency_graphs_up;
    bool m_debug;
    void load_efficiency_graphs();
    void load_probability_graphs();
    void load_abcd_graphs();
    float fake_weight_formula(bool tight1, bool tight2, float r1, float r2, float f1, float f2 ) const;
    EfficiencyTuple get_efficiency(std::string classification, const PO::Lepton& lepton /*, std::string variable*/);
    
    float BayesianProbability(const PO::Lepton& lep, std::string);
    float WillIdea2ElectronWeight(const PO::Lepton& lep);

  public:

    // constructor 
    FakeWeightGetter() {
      m_debug = false;
    }

    void initialize(std::string combined_eff){
      m_efficiency_file = combined_eff;
      std::cout << "Initialized FakeWeightGetter with TFile: " << m_efficiency_file << std::endl;
      load_efficiency_graphs();
      load_probability_graphs();
      load_abcd_graphs();
    }


    void enable_debug(){m_debug=true;}

    // member functions
    float BayesianFakeProbability(const PO::Lepton& lep); 
    float BayesianRealProbability(const PO::Lepton& lep); 
    float WillFakeProb2(const PO::Lepton&, const PO::Lepton&);
    void print_histograms();
    std::map<std::string, float> get_fake_weight(const PO::Lepton&, const PO::Lepton&, int njets);
    std::map<std::string, float> get_fake_weight(const PO::Lepton&, const PO::Lepton&);

};

#endif //ReadTree_FAKEWEIGHTGETTER_H
