#ifndef ReadTree_MUONBIASTOOLS_H
#define ReadTree_MUONBIASTOOLS_H

#include <string>
#include <map>
#include "TFile.h"
#include "TH1.h"
#include "TH1F.h"
#include "TH2.h"
#include "TH2F.h"
#include "TH2Poly.h"
#include <iostream>

/*********
 * Possibly transcribed from either
 * muon_efficiency/source/charge_efficiency_correction/muon_efficiency_bias_correction_tool.py
 * muon_efficiency/source/muon_efficiency/charge_efficiency_correction/muon_efficiency_bias_correction_tool.py
 * with the former being an almost superset of the latter
 *
 * *****************/

/*#include "TPad.h"*/
/*#include "TCanvas.h"*/

class EfficiencyPair{
  // essentially a named pair
  public:
    double eff;
    double eff_err;
    EfficiencyPair() : eff(-10), eff_err(-10) {};
    EfficiencyPair(double eff_in, double eff_err_in){
      eff = eff_in;
      eff_err = eff_err_in;
    }

    // doesn't need to be a friend (as it doesn't need to access any pivate data members), but made it a friend anyway just in case this class needs to be extended
    friend std::ostream& operator<<(std::ostream& os, const EfficiencyPair& EffPair){
      os << "EfficiencyPair(" << EffPair.eff << ", " << EffPair.eff_err << ")";
      return os;
    }
};

class HistEfficiencyTuple{
  public:
    TH1* eff_hist;
    TH1* eff_hist_err;

    HistEfficiencyTuple(){
      eff_hist = NULL;
      eff_hist_err = NULL;
    }
    HistEfficiencyTuple(TH1* eff_in, TH1* err_in){
      eff_hist = eff_in;
      eff_hist_err = err_in;
    }

    /*friend std::string find_root_type(TH1* obj);*/

    int GetNbinsSimple() const{
      // Simple function used to get the number of bins in a simple way
      // Note this method is not always used, e.g. for a TH2 
      int nbins(0);
      TH2Poly* bias_poly = dynamic_cast<TH2Poly*>( eff_hist->Clone() );
      if(bias_poly)  nbins = bias_poly->GetNumberOfBins();
      else           nbins = eff_hist->GetNbinsX()+1;
      return nbins;
    }

    friend std::ostream& operator<<(std::ostream& os, const HistEfficiencyTuple& tup);

    EfficiencyPair GetBinContent(int bin) const{
      return EfficiencyPair(eff_hist->GetBinContent(bin), eff_hist_err->GetBinContent(bin));
    }

    HistEfficiencyTuple operator*(const HistEfficiencyTuple& t){


      // Need to use RTTI 
      TH2Poly* combined_poly = dynamic_cast<TH2Poly*>( eff_hist->Clone() );
      TH2Poly* combined_poly_err = dynamic_cast<TH2Poly*>( eff_hist_err->Clone() );

      //if(eff_hist->InheritsFrom("TH2Poly") && t.eff_hist->InheritsFrom("TH2Poly")){
      int nbins(0);
      if( combined_poly && combined_poly_err ){
        nbins = combined_poly->GetNumberOfBins();
      }
      else if(eff_hist->InheritsFrom("TH2") && t.eff_hist->InheritsFrom("TH2")){
        nbins = eff_hist->GetNbinsX() * eff_hist->GetNbinsY();
      }
      else{
        nbins = eff_hist->GetNbinsX();
      }


      TH1* combined_hist     = dynamic_cast<TH1*>(eff_hist->Clone());
      TH1* combined_hist_err = dynamic_cast<TH1*>(eff_hist_err->Clone());
      

      for(int i=1; i<nbins+1; ++i){
        double val1 = eff_hist->GetBinContent(i);
        double val2 = t.eff_hist->GetBinContent(i);
        double unc1 = eff_hist_err->GetBinContent(i);
        double unc2 = t.eff_hist_err->GetBinContent(i);

        double val = val1*val2;
        double unc(0.0);
        if(val1 > 0 && val2 >0){
          unc = val * sqrt( (unc1/val1)*(unc1/val1) + (unc2/val2)*(unc2/val2) );
        }
        else{
          unc =  0.0;
        }

        combined_hist->SetBinContent(i, val);
        combined_hist_err->SetBinContent(i, unc);

      }

      return HistEfficiencyTuple(combined_hist, combined_hist_err);
    }

};

class EfficiencyTool
{
  private:
    std::string m_input_filename;
    TFile* m_input_file;
    std::vector<int> m_charges;

    std::string get_probes() const; 
    std::string get_analysis() const; 
    std::string get_matches() const;

    HistEfficiencyTuple combined_efficiency_from_histograms(TH1* probe_calo, TH1* match_calo, TH1* probe_ms, TH1* match_ms) const;
    HistEfficiencyTuple one_component_efficiency_from_histograms(TH1* probe, TH1* match) const;

    void get_input_filename(){
      // TODO: make configurable
      if(m_efficiency_type == "Reco") m_input_filename = m_reco_filename; //"/r10/atlas/hpacey/ChargeFlavourAsymm_Muons/histograms/output_histograms_data.root";
      else if(m_efficiency_type == "Trig") m_input_filename = m_trigger_filename; // "/r10/atlas/hpacey/ChargeFlavourAsymm_Muons/histograms/output_histograms_data_trigger.root";
      else std::cout << "ERROR: unable to get input file" << std::endl;
      if(m_debug) std::cout << "EfficiencyTool: will use file " << m_input_filename << std::endl;
    }

    void open_file(){
      m_input_file = TFile::Open(m_input_filename.c_str(), "READ");
      if(!m_input_file->IsOpen()){
        std::cout << "ERROR: unable to open TFile " << m_input_filename << std::endl;
      }
    }

  protected:
    std::string m_efficiency_type;
    bool m_do_id_correction;
    bool m_do_poly_hist;
    std::string m_reco_filename;
    std::string m_trigger_filename;
    bool m_debug;
    
  public:
    EfficiencyTool(std::string efficiency_type, bool do_id_correction, bool do_poly_hist, std::string reco_filename, std::string trigger_filename) : 
      m_efficiency_type(efficiency_type), m_do_id_correction(do_id_correction), m_do_poly_hist(do_poly_hist), m_reco_filename(reco_filename), m_trigger_filename(trigger_filename)
  {
      m_debug = false; // can turn on/off
      if(m_debug) std::cout << "EfficiencyTool constructor called..." << std::endl;
      m_charges = {1, -1};
      get_input_filename();
      open_file();
      if(m_debug) std::cout << "*** EfficiencyTool constructor finished" << std::endl;
    }

    std::map<std::string, HistEfficiencyTuple> load_efficiency_histograms(std::string hist_tag) const;  
    std::map<std::string, HistEfficiencyTuple> load_efficiency_histograms(std::string hist_tag, std::pair<double, double> pt_bin) const; 
    TH1* load_histogram(std::string hist_name) const;
    std::string get_short_hist_name(std::string hist_type, int charge, std::pair<double, double> pt_bin) const; 
    std::string get_histogram_name(std::string probe_type, std::string match_type, std::string probe_match, std::string hist_tag, int charge, std::pair<double, double> pt_bin, bool full_path) const;
    std::string get_histogram_name(std::string probe_type, std::string match_type, std::string probe_match, std::string hist_tag, int charge, std::pair<double, double> pt_bin) const;

};

class PtBiasScaleTool : protected EfficiencyTool
{

  private: 
    // variables
    std::map<std::pair<double, double>, std::pair<double, double> > m_pt_bin_scale;

    // functions
    std::map<std::string, HistEfficiencyTuple> get_efficiency_histograms_ptbins(std::string) const;
    std::map<std::string, HistEfficiencyTuple> get_bias_histograms( std::map<std::string, HistEfficiencyTuple> ) const;
    std::vector< std::pair<double, double> > get_pt_bins() const;
    std::map<std::pair<double, double>, std::pair<double, double> > get_mean_sd_by_ptbin(std::map<std::string, HistEfficiencyTuple>) const;

  
  public:
    PtBiasScaleTool(std::string efficiency_type, bool do_id_correction, bool do_poly_hist, std::string reco_filename, std::string trigger_filename) : 
      EfficiencyTool(efficiency_type, do_id_correction, do_poly_hist, reco_filename, trigger_filename){
      if(m_debug) std::cout << "PtBiasScaleTool constructor called..." << std::endl;
      std::string this_hist_tag = m_do_poly_hist ? "EtaPhiTH2Poly_TH2Poly_EtaPhyTH2Poly" : "EtaVar";
      std::map<std::string, HistEfficiencyTuple> efficiency_histograms = get_efficiency_histograms_ptbins(this_hist_tag);
      std::map<std::string, HistEfficiencyTuple> bias_histograms = get_bias_histograms(efficiency_histograms);
      m_pt_bin_scale = get_mean_sd_by_ptbin(bias_histograms);
      if(m_debug) std::cout << "*** PtBiasScaleTool constructor finished" << std::endl;
    }

    // functions
    std::pair<double, double> ptscale_and_error(double pt_gev) const; 
    void activate_debug() { m_debug = true; }

    // print info on the member variables of this class
    void print() const;
};

class BinIndexCache{
  public:
    double eta;
    double phi;
    int bin_index;
  
    BinIndexCache(double eta_in, double phi_in, int bin_index_in){
      eta = eta_in;
      phi = phi_in;
      bin_index = bin_index_in;
    }
    BinIndexCache() : eta(-999.0), phi(-999.0), bin_index(-999) {};

    friend std::ostream& operator<<(std::ostream& os, const BinIndexCache& bic){
      os << "BinIndexCache(" << bic.eta << ", " << bic.phi << ", " << bic.bin_index << ")";
      return os;
    }
};


class EtaPhiEfficiencyTool : protected EfficiencyTool
{
  private:
    // variables
    std::map<std::string, HistEfficiencyTuple> m_efficiency_histograms;
    TH1* m_bin_template_hist;
    std::map<std::string, EfficiencyPair > m_efficiency_cache; //key is made if the follwing way:   std::string key = std::to_string(bin_index) + "_" + std::to_string(charge);  

  public:
    EtaPhiEfficiencyTool(std::string efficiency_type, bool do_id_correction, bool do_poly_hist, std::string reco_filename, std::string trigger_filename) : 
      EfficiencyTool(efficiency_type, do_id_correction, do_poly_hist, reco_filename, trigger_filename){
      if(m_debug) std::cout << "EtaPhiEfficiencyTool constructor called..." << std::endl;
      std::string this_hist_tag;
      this_hist_tag = m_do_poly_hist ? "EtaPhiTH2Poly_TH2Poly_EtaPhiTH2Poly" : "EtaPhi"; // need this variable be stored? 
      m_efficiency_histograms = load_efficiency_histograms(this_hist_tag);
      m_bin_template_hist = get_bin_template_hist();
      if(m_debug) std::cout << "*** EtaPhiEfficiencyTool constructor finished" << std::endl;
    };

    // variables
    BinIndexCache m_last_bin_index_fetch;
    
    // functions 
    TH1* get_bin_template_hist();

    EfficiencyPair get_efficiency(double eta, double phi, int charge);
    int get_bin_index(double eta, double phi);
    bool out_of_bounds(double eta, double phi);
    void activate_debug() { m_debug = true; }
    void print() const;
    

};


class MuonBiasCorrectionTool{

  private:
    std::string m_efficiency_type;
    bool m_do_ID_correction;
    bool m_do_poly_hist;
    EtaPhiEfficiencyTool m_etaPhiTool;
    PtBiasScaleTool m_ptBiasTool;
    bool m_debug;
    bool m_initialized; 


  public:


    MuonBiasCorrectionTool(std::string efficiency_type, std::string reco_filename, std::string trigger_filename, bool do_ID_correction = true, bool do_poly_hist = false) :
        m_etaPhiTool(efficiency_type, do_ID_correction, do_poly_hist, reco_filename, trigger_filename), 
        m_ptBiasTool(efficiency_type, do_ID_correction, do_poly_hist, reco_filename, trigger_filename)
    {
        m_debug = false;
        if(m_debug) std::cout << "MuonBiasCorrectionTool constructor called..." << std::endl;
        if(!(efficiency_type == "Reco" || efficiency_type == "Trig")){
            std::cout << "ERROR: supplied efficiency string type must be either \"Reco\" or \"Trig\", you supplied: " << efficiency_type << std::endl;
            exit(1);
        }
        m_efficiency_type = efficiency_type;
        m_do_ID_correction = do_ID_correction;
        m_do_poly_hist = do_poly_hist;
        m_initialized = false;
        if(m_debug) std::cout << "MuonBiasCorrectionTool constructor finished..." << std::endl;
    }


    std::map<std::string, double> get_weights(double eta, double phi, double pt_gev, int charge);
    void activate_debug(){ 
      m_debug = true;
      m_ptBiasTool.activate_debug();
      m_etaPhiTool.activate_debug();
    }

    void print() const {
      m_ptBiasTool.print();
      m_etaPhiTool.print();
    }

};

#endif //ReadTree_MUONBIASTOOLS_H
