#ifndef ReadTree_PHYSICSOBJECTS_H
#define ReadTree_PHYSICSOBJECTS_H

#include "TLorentzVector.h"

// Some utility classes to make looping over objects easier
// These are very rough and ready so feel free to make a few modifications to make them easier to use
//

namespace PO{ // PO: Physics Object 

  class Lepton
  {
    public:
      Lepton() : charge(0), passOR(true), baseline(true), isLoose(false), isTight(false), isIsolated(false), isPerfect(0), isTriggerMatched(false), isPrompt(false), IFFClassification(-1), scaleFactor(1.0), isolationScaleFactor(1.0), flavour("lepton"), dphi_met(-1.0), dphi_jet1(-1.0), mt_jet1(-1.0), mt_met(-1.0), isTruthChargeMatched(false), isTagged(false), d0sig(-999), z0sinTheta(-999), Pt(-1), Eta(-10), Phi(-10), Energy(-1)  {}

      TLorentzVector fourVec;
      TVector2 twoVec;
      int charge;

      // true 
      bool passOR;
      bool baseline;

      // should be set 
      bool isLoose;
      bool isTight;
      bool isIsolated;
      bool isPerfect;
      bool isTriggerMatched;
      bool isPrompt;
      int IFFClassification;
   
      float scaleFactor;
      float isolationScaleFactor;
      std::string flavour; // "electron" or "muon"  

      float dphi_met; 
      float dphi_jet1;
      float mt_jet1;
      float mt_met; 
      float mass_with_jet1;

      // Truth 
      float truthCharge;
      int truthType;
      int truthOrigin;

      bool isTruthChargeMatched;
      bool isTagged;

      float d0sig;
      float z0sinTheta;

      float Pt, Eta, Phi, Energy;

      void print() const{ 
        std::cout << charge << " " <<  flavour << ": " 
                  << "pT " << Pt 
                  << "\teta " << Eta 
                  << "\tphi " << Phi 
                  << "\tE " << Energy 
                  << ", baseline: " << baseline
                  << ", isLoose: " << isLoose 
                  << ", isTight: " << isTight 
                  << ", isIsolated: " << isIsolated 
                  << ", isPerfect: " << isPerfect 
                  << ", isTriggerMatched: " << isTriggerMatched 
                  /*<< " isPrompt: " << isPrompt */
                  /*<< " IFFClassification: " << IFFClassification */
                  << std::endl; 
      }

      std::string charge_string() const{
        if(charge == 1) return "plus";
        else if(charge == -1) return "minus";
        else if(charge == 0) return "neutral";
        else{
          std::cout << "ERROR: lepton charge not -1, 0, 1. Investiate!" << std::endl;
          return "";
        }
      }


      // removed in favour of member variable
      /*bool isPerfect() const{*/
      /*return isIsolated && isTight; */
      /*}*/

      // "nearly signal" definition is passOR, pass baseline and have the "looser" signal definition 
      bool isInclusiveLoose() const{
        return passOR && baseline && isLoose;
      }

      bool operator < (const Lepton& lep) const{
        return Pt < lep.Pt;
      }

      TLorentzVector getFourVec() const{
        return fourVec;
      }

      // probably needs to be a friend since the class has inheritances
      friend std::ostream& operator<<(std::ostream& os, const Lepton& lep){
        os << lep.flavour << "(" << lep.charge << ", " << lep.Pt << " GeV, " << lep.Eta << ", tight: " << lep.isTight << ")";
        return os;
      }

      float pt() const { return fourVec.Pt(); }
      float eta() const { return fourVec.Eta(); }
      float phi() const { return fourVec.Phi(); }



      /*protected:*/
      /*std::string name("Lepton");*/

  };


  class Electron : public Lepton 
  {
    public:
      Electron() : passCFT(false),  chargeIDScaleFactor(1.0), chargeFlipTaggerScaleFactor(1.0), /*trigScaleFactor(1.0),*/ looseScaleFactor(1.0), totalScaleFactor(1.0)  {}


      bool passCFT;

      float chargeIDScaleFactor;
      float chargeFlipTaggerScaleFactor;
      /*float trigScaleFactor;*/
      float looseScaleFactor;
      float totalScaleFactor;

      void print() const {
        std::cout << "Electron: pT " << Pt << "\teta " << Eta << "\tphi " << Phi << "\tM " << fourVec.M() << "\tE " << Energy << "\t charge " << charge << std::endl; 
      }


      // Note, no trigger SF applied here, since there's a global trigger SF
      float calculateTotalScaleFactor(){
        
        float electronWeight = 1.0; 
        electronWeight *= chargeIDScaleFactor * chargeFlipTaggerScaleFactor;

        // ID scale factor
        if(isTight){
          electronWeight *= scaleFactor;
        }
        else{
          electronWeight *= looseScaleFactor;
        }

        // isolastion SF currently not applied to loose-not-tight electrons
        if(isTight){
          electronWeight *= isolationScaleFactor; 
        }

        return electronWeight;

      }

      /*private:*/
      /*float mass = 5.10998910e-4; // GeV*/
  };

  class Muon : public Lepton 
  {
    public:
      Muon() {}


      void print() const{ 
        std::cout << "pT " << Pt 
                  << "\teta " << Eta 
                  << "\tphi " << Phi 
                  << "\tE " << Energy 
                  << "\tcharge: " << charge
                  << ", baseline: " << baseline
                  << ", isLoose: " << isLoose 
                  << ", isTight: " << isTight 
                  << ", isIsolated: " << isIsolated 
                  << ", isPerfect: " << isPerfect 
                  << ", isTriggerMatched: " << isTriggerMatched 
                  /*<< " isPrompt: " << isPrompt */
                  /*<< " IFFClassification: " << IFFClassification */
                  << std::endl; 
      }
      /*private:*/
      /*float mass 0.105658; // GeV */
  };

  class Jet
  {
    public:
      Jet() : baseline(true), signal(false),  dPhiMET(100.), inDeadTile(false), passOR(true), passJVT(true), bad(false), Pt(-1), Eta(-10), Phi(-10), Energy(-10) {}

      TLorentzVector fourVec;
      bool baseline;
      bool signal;
      /*bool isBjet;*/
      float dPhiMET;
      bool inDeadTile;

      // set in constructor  
      bool passOR;
      bool passJVT;
      bool bad;

      /********
      float JVT;
      float JVF;
      float JVTSF;
      float bTagScaleFactor;
      float EMFrac;
      float ChFrac;
      float bchCorrCell;
      *******/
      float Pt, Eta, Phi, Energy;

      void print(){ std::cout << "Jet: pT " << Pt << "\teta " << Eta << "\tphi " << Phi << "\tM " << fourVec.M() << std::endl; }

      bool operator < (const Jet& jet) const{
        return Pt < jet.Pt;
      }

      TLorentzVector getFourVec() const{
        return fourVec;
      }

      float pt() const { return fourVec.Pt(); }
      float eta() const { return fourVec.Eta(); }
      float phi() const { return fourVec.Phi(); }
  };


  /**********

  class Tau
  {
    public:
      Tau()
        : numTrk(0)
      {}
      TLorentzVector fourVec;
      unsigned int numTrk;
      void print(){ std::cout << "Tau: pT " << Pt << "\teta " << Eta << "\tphi " << Phi << "\tM " << fourVec.M() << std::endl; }
  };

  class Photon
  {
    public:
      Photon() {}
      TLorentzVector fourVec;
      bool signal;
      bool passOR;
      bool photonPassJetPhotonOR;
      bool isGood = true;
      double resolutionAsJet;
      static bool hasResolution;
      void print(){ std::cout << "Photon: pT " << Pt << "\teta " << Eta << "\tphi " << Phi << "\tM " << fourVec.M() << std::endl; }
  };
  *************/

} // end of namespace PO 


#endif //ReadTree_PHYSICSOBJECTS_H
