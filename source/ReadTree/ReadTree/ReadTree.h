#ifndef READTREE_H
#define READTREE_H

#include <TFile.h>
#include <TTree.h>
#include <TTreeReader.h>
#include <TTreeReaderArray.h>
#include <TTreeReaderValue.h>
#include <TChain.h>

#include "CutChain/Chain.h"
#include "CutChain/Cut.h"
#include "CutChain/Node.h"
#include "CutChain/TObjWrapper.h"

#include <vector>
#include <memory>
#include <type_traits>

#include "PathResolver/PathResolver.h"

#include "ReadTree/EventStruct.h"
/*#include "ReadTree/Region/Region.h"*/

#include "ReadTree/BitFlags.h" // make this the last include to be safe (but it should be fine)



// Evil macros
RTBF_DECL_ENUM(HistTags, (Vital)(Speedy)(UltraSpeedy)(RTN)(Trigger)(Squirrel) )
RTBF_DECL_ENUM(CutPositionTags, (Final)(First)(Penultimate)(PenultimateMETSig)(Full)(GenFiltHT) )

class ReadTree
{
  private:

    EventStruct m_event;
    std::string m_inputTreeName;

    void makeFakeEstimateRegions(CutChain::Node*, std::string, bool);
    void makeFakeValidationRegions(CutChain::Node*, std::string, bool);
    void makeFakeEfficiencyRegions(CutChain::Node*, std::string, bool);
    void makeRealEfficiencyRegions(CutChain::Node*, std::string, bool);
    void makeABCDValidRegions(CutChain::Node*, std::string, bool);
    void makeSRVRCuts(CutChain::Node*, std::string, std::string, bool);
    void makeSRCuts(CutChain::Node*, std::string, bool);
    void makeCRVRCuts(CutChain::Node*, std::string, bool);
    void makeCRCuts(CutChain::Node*, std::string, bool);
    /*void makeVRCuts(CutChain::Node*, std::string);*/
    void makeTurnOnsOnly(CutChain::Node* leptonNode);

  public:
    typedef ReadTreeBitFlags::bitflag_t bitflag_t;

    ReadTree(TTree* tree, TTree* configTree, TTree* truthTree = 0, std::string muon_efficiency_histos="", std::string muon_efficiency_trig_histos="", std::string inputTreeName="Nominal");
    ~ReadTree() {};



    // Some member functions 
    bool configure(TFile* oFile, std::string outDir,  float lumiweight, std::string e_tag, float xsection, float kfactor, float efficiency);
    //void connect();
    void Loop();
    void extractXS(TFile* oFile, std::string e_tag, float xsection, float kfactor, float efficiency);
    std::vector<float> read_xsection_file(std::string e_tag); 

    void DefineChain();


    // Chain definition functions
    /********
    CutChain::Cut* makeCutsFromSubRegion(CutChain::Node* node, const Region& region, const RegionObjectBase* sub);
    CutChain::Cut* makePreliminaryCuts(CutChain::Node* leptonNode, const Region& region);
    CutChain::Cut* makeJetCuts(CutChain::Node* jetCutBP, const Region& region);
    CutChain::Cut* makeBJetCuts(CutChain::Node* bJetBP, const Region& region);
    CutChain::Cut* makeMJSigmaCuts(CutChain::Node* MJBP, const Region& region);
    CutChain::Cut* makeSumMHemiCuts(CutChain::Node* MHBP, const Region& region);
    CutChain::Cut* makeMinMHemiCuts(CutChain::Node* MMBP, const Region& region);
    CutChain::Cut* makeM2TCuts(CutChain::Node* MTBP, const Region& region);
    ***************/


    // Functions to make the histograms
    template <typename... Ts>
    void makeTaggedPlot(CutChain::Cut* cut, bitflag_t tags, Ts... args) {
      if (tags & m_histMask) cut->addTObj(args...);
    }

    void makePlots(CutChain::Cut* cut, CutChain::CutEnum passAlong = CutChain::CutEnum::HERE);


    // Declare a constant reference to the private member variable
    // This is just for convenience - as the chain is run using only ReadTree member functions it can access the (non const) member variable if it really wants to
    // However - please don't! The chain itself should *not* modify EventStruct as the whole chain uses the same object (so changing it in one place will affect it
    // in another when you may not expect it to!)
    //const EventStruct& event;
    const EventStruct& event;


    // Configurables (set by job options/command line)
    // Note take care to match with the initializer list in the ReadTree constructor
    unsigned int startEvent;
    bool debug;
    int LLSR;
    unsigned int nEvents;
    bool doSquirrels;
    bool applyHighPtIso;
    std::string fakeSyst;
    bool applyFakeWeights;
    bool applyttbarWeights;
    bool applyMuonBias;
    bool applyDibosonScaleWeight;
    bool applyPDFWeight;
    bool applyISRWeight;
    bool applyPromptLeptonCut;
    bool applyPromptRejectionCut;
    bool useLikelihoodMM;
    bool m_plot_extra_lepton_variables; // plotting variable 
    /*bool disableWeights;*/
    bool skipPRW;
    /*bool useTopTagging;*/
    /*bool usePhotons;*/
    double m_scaling; //Amount by which to scale weights
    bool isTruth;
    bool onlyOutputFinal;
    std::string tikzFile;
    std::string fakeWeightHistos; 
    std::string ttbarWeightHistFile;

    std::string m_outDir;


    // keep these as doubles
    double totinitialNevts;
    double totinitialSumWeights;
    double totinitialSumWeights_muR05_muF05;

    // other
    bool m_isMC;
    /*std::string resolutionMode;*/

    CutChain::Chain chain;

    bool setHistMask(const std::string& maskString);
    bool setCutPositionMask(const std::string& maskString);

  private:

    std::unique_ptr<TTree> m_input;
    TTree* m_configTree;
    TTree* m_truthTree;

    /*std::vector<Region::LeptonMode> lregEnumVec; // vector for ease of iteration over lepton modes*/

    // Configurables (set by config file)
    bool m_isConfigured = false;
    DataSource    m_dataSource;
    bool          m_isData;
    bool          m_isSherpa;
    int           m_dsid;
    float         m_lumiweight;
    std::string   m_prwConfName;
    std::string   m_prwLumiCalcName;
    std::string   m_systOption;
    bool          m_doTruth;
    std::string   m_STConfFile;
    int           m_JESNPSet;

    std::string   m_fatJetName;
    bool          m_hasFatJets;

    OutputSetting m_outputMode;
    /*OutputSetting m_eleMode;*/
    /*OutputSetting m_muMode;*/
    /*OutputSetting m_jetMode;*/
    /*OutputSetting m_photonMode;*/
    bool          m_outputPhotonCut;
    /*OutputSetting m_tauMode;*/
    /*OutputSetting m_trackMode;*/
    /*OutputSetting m_EtMissMode;*/
    /*OutputSetting m_fatJetMode;*/
    bool          m_doPhotonOR;
    bool          m_doTauOR;
    bool          m_usePhotonMET;
    bool          m_useTauMET;

    bitflag_t m_histMask;
    bitflag_t m_cutMask;

    OutputSetting m_cutchainOutputMode;

    // Configurables extracted from x-section file
    float m_xsection = 0;
    float m_kfactor = 0 ;
    float m_efficiency = 0;

};

#endif //ReadTree_EVENTSTRUCT_H
