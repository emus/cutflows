#include "ReadTree/EventStruct.h"

// Function declarations
//float calculateRTN(const std::vector<PO::Jet> &jets, unsigned int N);



// Function to return a std::vector of TLV from a std::vector of physics object (Jets, Muons etc)
template <class physicsObjectVector>
std::vector<TLorentzVector> getTLVvector(physicsObjectVector objVec){
  std::vector<TLorentzVector> tempVec;
  for( const auto& iobj : objVec ){
    tempVec.push_back(iobj.fourVec);
  }
  return tempVec;
}

// Function to calculate delta phi properly ...
// Potentially superior to TLorentzVector implementation
// will always return the positive delta phi 
// and most likely faster
// (TLorentzVector may return the negative delta phi) 
inline double deltaPhi( double phi1, double phi2){
    return acos(cos(phi1 - phi2));
}

// Function to calculate MT 
inline float MT(const TLorentzVector& p1, const TLorentzVector& p2){
  float d_phi = deltaPhi(p1.Phi(), p2.Phi() ); 
  return sqrt( 2 * p1.Pt() * p2.Pt() * ( 1 - cos(d_phi) ));
}

inline float MT(const PO::Lepton& lep1, const double MET, const double METPhi){
  float d_phi = deltaPhi(lep1.Phi, METPhi);
  return sqrt(2 * lep1.Pt * MET * (1 - cos(d_phi) )); 
}

inline float invariant_mass_squared(const PO::Lepton& lep1,  const PO::Lepton& lep2){
  // Because the ROOT version of this is clearly SHIT
  // Assumes ultra-relativistic limit
  float d_eta = cosh( lep1.eta() - lep2.eta() );
  float d_phi = cos( lep1.phi() - lep2.phi() );
  return 2 * lep1.pt() * lep2.pt() * (d_eta - d_phi);
}


bool EventStruct::CalculateEventValues(bool applyHighPtIso)
{

  ///////////////////////////////////
  // Note, it is important to clear all vectors and re-initialize all floating-point values
  // at the start of this function
  ///////////////////////////////////

  if (debug) std::cout << "clearing vectors" << std::endl;
  count_loose_muons = 0;
  count_loose_electrons = 0;
  HT_jets = 0;
  HT_all = 0;
  met_tlv.SetPtEtaPhiM(0,0,0,0);
  mll = -1.0;
  dPhi_ll = - 1.0;

  baseline_electrons.clear();
  baseline_muons.clear();
  baseline_jets.clear();

  loose_muons.clear();
  loose_electrons.clear();
  loose_leptons.clear();

  // shouldn't really be needed as the selection will be done in DefineChain 
  // tight = signal 
  tight_muons.clear();
  tight_electrons.clear();
  tight_leptons.clear();
  
  signalJets.clear();

  count_tight_muons = 0;
  count_tight_electrons = 0;


  /////////////////////////////////////////////
  // Fill physics objects
  /////////////////////////////////////////////

  if (debug) std::cout << "Filling objects" << std::endl;

  // 
  // MET
  //

  // potentially useful for delta phi
  met_tlv.SetPtEtaPhiM(EtMiss, 0, EtMissPhi, 0);
  met_twovec.SetMagPhi(EtMiss, EtMissPhi);
  ////////////////////////////////
  // Jets
  ////////////////////////////////


  if (debug) std::cout << "with " << jetPt->size() << " jets" << std::endl;
  baseline_jets.reserve(jetPt->size());
  for (unsigned int i = 0; i < jetPt->size(); ++i) {
    PO::Jet jet;

    jet.fourVec.SetPtEtaPhiM(jetPt->at(i), jetEta->at(i), jetPhi->at(i), jetMass->at(i) );
    jet.Pt = jetPt->at(i);
    jet.Eta = jetEta->at(i);
    jet.Phi = jetPhi->at(i);
    jet.signal = jetIsSignal->at(i);

    jet.dPhiMET = deltaPhi(EtMissPhi, jet.Phi );

    // check if in dead tile
    //jet.inDeadTile = (-0.1 < jet.Eta && jet.Eta < 1.0 && 0.8 < jet.Phi && jet.Phi < 1.1); // 2015 region -- now corrected by the dead tile tool
    jet.inDeadTile = jet.inDeadTile || (-0.1 < jet.Eta && jet.Eta < 1.0 && -1.45 < jet.Phi && jet.Phi < -1.10); // 2016 region 1
    jet.inDeadTile = jet.inDeadTile || (-1.0 < jet.Eta && jet.Eta < 0.1 && 0.3 < jet.Phi && jet.Phi < 0.6); // 2016 region 2

    // push all jets into jet vector (only isSignal written to ntuple)
    baseline_jets.push_back(jet);

    if(jet.signal) signalJets.push_back(jet);

    if(jet.signal){
      HT_jets += jet.pt();
      HT_all += jet.pt();
    }

  } // end of for loop over jets

  // sort in descending pT order
  std::sort(baseline_jets.rbegin(), baseline_jets.rend());
  std::sort(signalJets.rbegin(), signalJets.rend());

  PO::Jet leadingJet;
  if(signalJets.size() > 0) leadingJet = signalJets.at(0);

  //////////////////////////////////////////
  // Electrons
  //////////////////////////////////////////


  baseline_electrons.reserve( elePt->size() ); 
  if (debug) std::cout << "with " << elePt->size() << " electrons" << std::endl;
  //if(elePt->size() > 0 ) std::cout << "with " << elePt->size() << " electrons" << std::endl;
  for (unsigned int i = 0; i < elePt->size(); ++i) {
    PO::Electron ele;
    ele.fourVec.SetPtEtaPhiE(elePt->at(i), eleEta->at(i), elePhi->at(i), eleE->at(i) );
    ele.twoVec.SetMagPhi(elePt->at(i), elePhi->at(i));
    ele.Pt = elePt->at(i);
    ele.Eta = eleEta->at(i);
    ele.Phi = elePhi->at(i);
    ele.Energy = eleE->at(i);
    ele.charge = eleCharge->at(i);
    ele.passCFT = elePassCFT->at(i);
    ele.d0sig =  eled0sig->at(i);
    ele.z0sinTheta = elez0sinTheta->at(i);
    // Artificially get rid of big tails 
    if(ele.d0sig > 50) ele.d0sig = 50;
    if(ele.d0sig < -50) ele.d0sig = -50;
    if(ele.z0sinTheta > 50) ele.z0sinTheta = 50;
    if(ele.z0sinTheta < -50) ele.z0sinTheta = -50;

    // Isolation 
    if(applyHighPtIso){
      // High pT isolation only required for high pT electrons (> 200 GeV)
      // This mimmics the behaviour in SUSYTools
      if(elePt->at(i) > 200.0 && eleIsFCHighPtCaloOnly->at(i)) ele.isIsolated = true;
      else if(elePt->at(i) < 200 && eleIsFCTight->at(i)) ele.isIsolated = true; 
      else ele.isIsolated = false; 
    }
    else{
      if(eleIsFCTight->at(i)) ele.isIsolated = true;
      else ele.isIsolated = false;
    }


    ele.isLoose = ele.passCFT && eleIsInclusiveLooserNearlySignal->at(i) && elePt->at(i)>25.0; //&& (eleIsFCLoose->at(i) || eleIsFCHighPtCaloOnly->at(i));
    ele.isTight = ele.isInclusiveLoose() && eleIsTighterNearlySignal->at(i) && ele.isIsolated && fabs(ele.d0sig) < 3.0 && fabs(ele.z0sinTheta) < 0.3 && elePt->at(i)>25.0;
    ele.flavour = "electron"; // to distinguish between "lepton"
    ele.isPerfect = ele.isTight; 
    if(signalJets.size() > 0){
      ele.dphi_jet1 = deltaPhi( ele.Phi, leadingJet.Phi );
      ele.mt_jet1 = MT(ele.fourVec, leadingJet.fourVec);
      ele.mass_with_jet1 = (ele.fourVec + leadingJet.fourVec).M();
    }
    ele.mt_met = MT(ele, EtMiss, EtMissPhi); 
//    if (ele.isTight){
//      std::cout<<"ELEINFO "<<elePt->at(i)<<eleEta->at(i)<<elePhi->at(i)<<eleE->at(i)<<ele.mt_met<<std::endl;
//    }
    ele.isTriggerMatched = (eleIsDiElectronTriggerMatched->at(i) || eleIsEMuTriggerMatched->at(i)); // TODO: implement in ntuple
    ele.dphi_met = deltaPhi(ele.Phi, EtMissPhi);

    ele.isTagged = (elePt->at(i)>50 && ele.isTight && ele.isTriggerMatched);  

    if (m_isMC) {
      // Scale factors
      ele.scaleFactor = eleScaleFactor->at(i);  // from m_SUSYTools_tighter->GetSignalElecSF(*electron, true, true, false, false));
      ele.looseScaleFactor = eleLooseSF->at(i); // from m_SUSYTools_looser->GetSignalElecSF(*electron, true, true, false, false));
      ele.chargeIDScaleFactor = eleChargeIDSF->at(i);
      ele.chargeFlipTaggerScaleFactor = eleChargeFlipTaggerSF->at(i);
      //ele.trigScaleFactor = eleTrigSF->at(i);
      ele.isolationScaleFactor = eleIsoSF->at(i);
      //ele.isolationScaleFactor = 1.0;
      // Truth information
      ele.isPrompt = (eleIFFTruthClassification->at(i) == static_cast<int>(IFF::Type::IsoElectron)); 
      ele.IFFClassification = eleIFFTruthClassification->at(i);
      ele.truthCharge = eleTruthCharge->at(i);
      ele.truthType = eleTruthType->at(i);
      ele.truthOrigin = eleTruthOrigin->at(i);
      ele.isTruthChargeMatched = (ele.truthCharge == ele.charge);
    }
    else {
      ele.isPrompt = true; // for fake estimate
      ele.isTruthChargeMatched = true; // for data
    }

    // Calculate the electron's total scale factor given it's properties
    ele.totalScaleFactor = ele.calculateTotalScaleFactor();

    baseline_electrons.emplace_back(ele);

    if(ele.isLoose) HT_all += ele.Pt;

    if(ele.isPerfect) count_tight_electrons++;

  }

  // sort electrons in descenting Pt order
  std::sort(baseline_electrons.rbegin(), baseline_electrons.rend());
  



  //////////////////////////////////////////
  // Muons
  //////////////////////////////////////////
  

  baseline_muons.reserve( muPt->size() );
  if (debug) std::cout << "with " << muPt->size() << " muons" << std::endl;
  for (unsigned int i = 0; i < muPt->size(); ++i) {
    PO::Muon mu;
    mu.fourVec.SetPtEtaPhiE(muPt->at(i), muEta->at(i), muPhi->at(i), muE->at(i) );
    mu.twoVec.SetMagPhi(muPt->at(i), muPhi->at(i));
    mu.Pt = muPt->at(i);
    mu.Eta = muEta->at(i);
    mu.Phi = muPhi->at(i);
    mu.Energy = muE->at(i);

    mu.charge = muCharge->at(i);

//    mu.isLoose = muIsSignal->at(i); // never had a different ID for muons //muIsInclusiveLooserNearlySignal->at(i);
    mu.isLoose = muIsSignal->at(i) && fabs(muEta->at(i)) < 2.47 && muPt->at(i)>25.0;  // Note muIsSignal contains pT>25 GeV and |eta|<2.47 already, but I'm paranoid
    mu.d0sig =  mud0sig->at(i);
    mu.z0sinTheta = muz0sinTheta->at(i);

    // Artificially get rid of big tails 
    if(mu.d0sig > 50) mu.d0sig = 50;
    if(mu.d0sig < -50) mu.d0sig = -50;
    if(mu.z0sinTheta > 50) mu.z0sinTheta = 50;
    if(mu.z0sinTheta < -50) mu.z0sinTheta = -50;


    // Isolation
    if(applyHighPtIso){
      if(muPt->at(i) > 200.0 && muIsHighPtTrackOnly->at(i)) mu.isIsolated = true;
      else if(muPt->at(i) < 200 && muIsTight_VarRad->at(i)) mu.isIsolated = true;
      else mu.isIsolated = false;
    }else{
      if( muIsFCTight->at(i) ) mu.isIsolated = true;
      else mu.isIsolated = false;
    }


    mu.isTight = mu.isInclusiveLoose() && muIsSignal->at(i) && mu.isIsolated && fabs(mu.z0sinTheta) < 0.3 && fabs(mu.d0sig) < 3 && muPt->at(i)>25.0 ;
    mu.flavour = "muon" ; // to distinguish between "lepton"
    mu.isPerfect = mu.isTight;// && fabs(mu.d0sig) < 2 && fabs(mu.z0sinTheta) < 0.2;
    if(signalJets.size() > 0){
      mu.dphi_jet1 = deltaPhi( mu.Phi, leadingJet.Phi );
      mu.mt_jet1 = MT(mu.fourVec, leadingJet.fourVec);
      mu.mass_with_jet1 = (mu.fourVec + leadingJet.fourVec).M();
    }
    mu.mt_met = MT(mu, EtMiss, EtMissPhi); 
    mu.dphi_met = deltaPhi( mu.Phi, EtMissPhi);
    mu.isTriggerMatched = (muIsDiMuonTriggerMatched->at(i) || muIsEMuTriggerMatched->at(i));
    mu.isTagged = (muPt->at(i)>50 && mu.isTight && mu.isTriggerMatched);  

    if (m_isMC) {
      mu.isPrompt = (muIFFTruthClassification->at(i) == static_cast<int>(IFF::Type::PromptMuon));
      mu.IFFClassification = muIFFTruthClassification->at(i);
      mu.scaleFactor = muScaleFactor->at(i);
      mu.truthCharge = muTruthCharge->at(i);
      mu.truthOrigin = muTruthOrigin->at(i);
      mu.truthType = muTruthType->at(i);
      mu.isolationScaleFactor = muIsoSF->at(i);
      mu.isTruthChargeMatched = (mu.truthCharge == mu.charge);
    }
    else {
      mu.isPrompt = true; // for fake estimate
      mu.isTruthChargeMatched = true; // by definition for data
    }

    if(debug){
      std::cout << "muon " << i << " of " << muPt->size() << std::endl;
      mu.print();
    }

    if(mu.isLoose) HT_all += mu.Pt;

    //baseline_muons.emplace_back(mu);
    baseline_muons.push_back(mu);
    if(mu.isPerfect) count_tight_muons++;
  }

  // sort muons in descenting Pt order
  std::sort(baseline_muons.rbegin(), baseline_muons.rend());

  //////////////////////////////////////////
  // Now calculate some stuff yeahhhh 
  //////////////////////////////////////////
  

  
  if(debug) std::cout << "sorting muons" << std::endl;
  for(PO::Muon& mu : baseline_muons){
    if(mu.isInclusiveLoose()){ 
      count_loose_muons++;
      loose_muons.push_back(mu);
      loose_leptons.push_back(mu);
      if(mu.isPerfect) tight_leptons.push_back(mu);
      if(mu.isPerfect) tight_muons.push_back(mu);
    }
  }
  if(debug) std::cout << "There are " << count_loose_muons << " inclusive-loose muons in this event" << std::endl;


  if(debug) std::cout << "sorting electrons" << std::endl;
  for( PO::Electron& ele : baseline_electrons){
    if(ele.isInclusiveLoose()) {
      count_loose_electrons++;
      loose_electrons.push_back(ele);
      loose_leptons.push_back(ele);
      if(ele.isPerfect) tight_leptons.push_back(ele);
      if(ele.isPerfect) tight_electrons.push_back(ele);
    }
  }
  if(debug) std::cout << "There are " << count_loose_electrons << " inclusive-loose electrons in this event" << std::endl;



  // reverse sort leptons (need lambda as no conversion to Lepton happens, can't be deduced from reference_wrapper) 
  std::sort(loose_leptons.rbegin(), loose_leptons.rend(), 
      [] (const std::reference_wrapper<PO::Lepton> &a, const std::reference_wrapper<PO::Lepton> &b) -> bool { return a.get().Pt < b.get().Pt;} );

  std::sort(tight_leptons.rbegin(), tight_leptons.rend(), 
      [] (const std::reference_wrapper<PO::Lepton> &a, const std::reference_wrapper<PO::Lepton> &b) -> bool { return a.get().Pt < b.get().Pt;} );
 
  if(debug) std::cout << "There are " << loose_leptons.size() << " loose leptons in this event." << std::endl;
  if(debug) std::cout << "There are " << tight_leptons.size() << " tight leptons in this event." << std::endl;
  

  // Calculate some event-level variables 
  if( loose_leptons.size() >= 2){
    const PO::Lepton& lep1 = loose_leptons.at(0).get(); 
    const PO::Lepton& lep2 = loose_leptons.at(1).get(); 

    // Assumes that there is 1 electron and 1 muon! 
    if(loose_muons.size() > 1){
        mass_muon_jet1 = loose_muons.at(0).mass_with_jet1;
    }
    else{
        mass_muon_jet1=-1;
    }

    if(loose_electrons.size() > 1){
        mass_electron_jet1 = loose_electrons.at(0).mass_with_jet1;
    }
    else{
        mass_electron_jet1 = -1;
    }

    //float bad_mll     = (lep1.fourVec + lep2.fourVec).M();
    //std::cout << bad_mll << " ";
    float new_mll2 = invariant_mass_squared( lep1, lep2); 
    mll = pow(new_mll2, 0.5); 
    dPhi_ll = deltaPhi(lep1.Phi, lep2.Phi );

    TVector2 ptllboost = met_twovec + lep1.twoVec + lep2.twoVec;
    dPhiBoost = deltaPhi(EtMissPhi, ptllboost.Phi());

    sum_mt_met           = lep1.mt_met         + lep2.mt_met;
    sum_mt_jet1          = lep1.mt_jet1        + lep2.mt_jet1;
    sum_mass_lepton_jet1 = lep1.mass_with_jet1 + lep2.mass_with_jet1;
    sum_dphi_lepton_jet1 = lep1.dphi_jet1   + lep2.dphi_jet1;

    max_mt_met           = std::max(lep1.mt_met,         lep2.mt_met);
    max_mt_jet1          = std::max(lep1.mt_jet1,        lep2.mt_jet1);
    max_mass_lepton_jet1 = std::max(lep1.mass_with_jet1, lep2.mass_with_jet1);
    max_dphi_lepton_jet1 = std::max(lep1.dphi_jet1,   lep2.dphi_jet1);

    if(signalJets.size() > 0){
      sum_pt_leps_jet1 = lep1.Pt + lep2.Pt + signalJets.at(0).Pt;
    }
    else{
      sum_pt_leps_jet1 = lep1.Pt + lep2.Pt;
    }

  }
  else {
    mass_muon_jet1 = -1.0;
    mass_electron_jet1 = -1.0;

    mll = -1.0;
    dPhi_ll = - 1.0;
    dPhiBoost = - 1.0;

    sum_mt_met = -1.0;
    sum_mt_jet1 = -1.0;
    sum_mass_lepton_jet1 = - 1.0;
    sum_dphi_lepton_jet1 = -999;
    sum_pt_leps_jet1 = -1.0;

    max_mt_met = -1.0;
    max_mt_jet1 = -1.0;
    max_mass_lepton_jet1 = -1.0;
    max_dphi_lepton_jet1 = -1.0;
  }

  if(debug) std::cout << "Finished CalculateEventValues successfully" << std::endl;
  return true; // end of function
}
