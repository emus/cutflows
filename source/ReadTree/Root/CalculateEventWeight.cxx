#include "ReadTree/EventStruct.h"



double EventStruct::CalculatePDFWeight(std::string inputTreeName){
  // obtains the pdf weight for a given number in the error set
  // gets this number by looking at the systname
//  std::cout<<"Geting PDF weight"<<std::endl;
  std::string snumber;
  if (inputTreeName.find("ttbar")!=std::string::npos){ 
    snumber = inputTreeName.replace(0,9,"");//ttbar_PDFX, singletop_PDFX 
  } else {
    snumber = inputTreeName.replace(0,13,"");
  }
  int number = std::stoi(snumber);
  double pdfweight = mc_pdf_weights->at(number-112);
  return pdfweight;
}


double EventStruct::CalculateDibosonScaleWeight(std::string inputTreeName){
  // obtains the diboson weight for a given number in the error set
  // Remember order: (muR,muF) = (0.5,0.5), (0.5,1.0), (1.0,0.5), (2.0,2.0), (2.0,1.0), (1.0,2.0)
  // gets this number by looking at the systname
//  std::cout<<"Geting DibosonScale weight"<<std::endl;
  std::string snumber;
  snumber = inputTreeName.replace(0,14,"");//diboson_scale_X 
  int number = std::stoi(snumber);
  double dibosonscaleweight = mc_scale_weights->at(number-1);
  return dibosonscaleweight;
}


double EventStruct::CalculateISRWeight(std::string inputTreeName){
  // obtains the pdf weight for a given number in the error set
  // gets this number by looking at the systname
//  std::cout<<"Geting ISR weight"<<std::endl;
  std::string snumber;
  double isrweight = 0.0;
  if (inputTreeName.find("up")!=std::string::npos){ 
    isrweight = (mc_isr_mur05muf05_weight * mc_isr_var3cup_weight)/mc_event_weight;
//    isrweight = isrweight*2.0282/2.28388;    
  } else {
    isrweight = (mc_isr_mur20muf20_weight * mc_isr_var3cdown_weight)/mc_event_weight;
  }
  return isrweight;
}


double EventStruct::CalculateFakeWeights(){
  //
  // Only Calculates the fake weights. Application is done in "CalculateEventWeights" 
  // 

  double fake_weight = 1.0;
  if(loose_leptons.size() >= 2){
    PO::Lepton& lep1 = loose_leptons.at(0).get();
    PO::Lepton& lep2 = loose_leptons.at(1).get();

    if(lep1.flavour == lep2.flavour) return 1; // not sure why I did this
    if(lep1.charge == lep2.charge) return 1;

    if(debug) std::cout << "about to calculate fake weights" << std::endl;
    // normal matrix method 
    std::map<std::string, float> weights = m_fake_weight_getter.get_fake_weight(lep1, lep2);
    fake_weight = weights["weight"];

    if(debug){
    std::cout << std::setprecision(5) << weights["weight"] << "," 
      << (int)lep1.isTight << "," << lep1.Pt << "," << lep1.charge << "," << lep1.flavour << ","
      << (int)lep2.isTight << "," << lep2.Pt << "," << lep2.charge << "," << lep2.flavour << std::endl;
    }
    //std::cout << "my calc: " << fake_weight << std::endl;
    //lep1.print();
    //lep2.print();

    /******************
    // 
    // Baysian fake probability idea
    //
    // At the moment, only assuming electron fakes
    float prob_real_1 = 1.0;
    float prob_real_2 = 1.0; 
    if(lep1.flavour == "electron"){
      m_fake_weight_getter.BayesianRealProbability(lep1); // p(tight | real)
    }
    if(lep2.flavour == "electron"){
      m_fake_weight_getter.BayesianRealProbability(lep2); // p(tight | real)
    }
    float prob_both_real = prob_real_1*prob_real_2; 
    float prob_not_both_real = 1 - prob_both_real; // equivalen to "prob either fake"
    fake_weight = prob_not_both_real;
    **********/

  
  }

  return fake_weight;

}

void EventStruct::CalculateEventWeight(
    bool skipPRW, 
    bool applyFakeWeights,
    bool applyMuonBias,
    std::string inputTreeName,
    bool useLikelihoodMM,
    bool applyttbarWeights,
    bool applyPDFWeight,
    bool applyISRWeight,
    bool applyDibosonScaleWeight){

  /******************
   * Calculates all of the relevent fake weights and applies them.
   * Normally these are only applied to MC.
   * With the exception of the fake weight, which is only applied to data in order to create the fake sample.
   * ************************/


  // Start 
  totalEventWeight = 1.0; 

  
  /**************************
   * Notes
   * https://twiki.cern.ch/twiki/bin/view/AtlasProtected/LatestRecommendationsElectronIDRun2#Efficiencies_for_central_electro
   *
   * chargeFlipTaggerScaleFactor
   * ---------------------------
   * https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/ElectronChargeFlipTaggerTool 
   * Please apply the ECIDSloose efficiency scale factor in addition to your reco, ID, isolation and trigger scale factors, see LatestRecommendationsElectronIDRun2#Efficiencies_for_central_electro. 
   * Moreover, you need to apply corrections for the charge mis-ID efficiency as described in LatestRecommendationsElectronIDRun2#Scale_factors_for_electrons_char.
   *
   * ************************/

  if (m_isMC){

    float ElectronWeight = 1.0;
    if (loose_electrons.size()>0){
      PO::Electron& e1 = loose_electrons.at(0);
      ElectronWeight = e1.totalScaleFactor; 
      if (loose_electrons.size()>1){
        PO::Electron& e2 = loose_electrons.at(1);
        ElectronWeight *= e2.totalScaleFactor; 
      }
    }

    float MuonWeight = 1.0; 
    if (loose_muons.size()>0){
      PO::Muon& m1 = loose_muons.at(0);
      MuonWeight = (m1.isTight ? m1.isolationScaleFactor : 1.0) * m1.scaleFactor;
      if (loose_muons.size()>1){
        PO::Muon& m2 = loose_muons.at(1);
        MuonWeight = MuonWeight * (m2.isTight ? m2.isolationScaleFactor : 1.0) * m2.scaleFactor;
      }
    }

    // Note scaling includes cross-section and k-factors 
    if (applyPDFWeight){
      double mc_pdfweight = CalculatePDFWeight(inputTreeName);
      totalEventWeight = mc_pdfweight * scaling * triggerGlobalEfficiencySF; 
    } else if (applyDibosonScaleWeight){
      double mc_dibosonscaleweight = CalculateDibosonScaleWeight(inputTreeName);
      totalEventWeight = mc_dibosonscaleweight * scaling * triggerGlobalEfficiencySF; 
    } else if (applyISRWeight){
      double mc_isrweight = CalculateISRWeight(inputTreeName);
      totalEventWeight = mc_isrweight * scaling * triggerGlobalEfficiencySF; 
    } else {
      totalEventWeight = mc_event_weight * scaling * triggerGlobalEfficiencySF; 
    }



    // Add lumi-weight for MC sub-campaigns (m_lumiweight should = 1 for data anyway)  
    totalEventWeight *= m_lumiweight;

    if(!skipPRW) totalEventWeight *= m_pileup_weight;

    totalEventWeight *= ElectronWeight;
    totalEventWeight *= MuonWeight;

    if(debug){
      std::cout << "ElectronWeight: "              << ElectronWeight            << std::endl;
      std::cout << "MuonWeight: "                  << MuonWeight                << std::endl;
    }
  } // end if(m_isMC) 

  // Muon bias weights (can be applied to data?)
  m_muon_recoweight              = 1.0;
  m_muon_triggerweight           = 1.0;
  m_muon_reco_etaphibias_up      = 1.0;
  m_muon_reco_etaphibias_down    = 1.0;
  m_muon_reco_ptscale_up         = 1.0;
  m_muon_reco_ptscale_down       = 1.0;
  m_muon_trigger_etaphibias_up   = 1.0;
  m_muon_trigger_etaphibias_down = 1.0;
  m_muon_trigger_ptscale_up      = 1.0;
  m_muon_trigger_ptscale_down    = 1.0;
  
  if(!applyFakeWeights && applyMuonBias){
//    std::cout<<"applying muon bias weights"<<std::endl;
    std::map<std::string, double> muon_recoweights = CalculateMuonBiasTest("Reco");
    m_muon_reco_etaphibias_up   = muon_recoweights["etaphibias_up"];
    m_muon_reco_etaphibias_down = muon_recoweights["etaphibias_down"];
    m_muon_reco_ptscale_up      = muon_recoweights["ptscale_up"];
    m_muon_reco_ptscale_down    = muon_recoweights["ptscale_down"];
    m_muon_recoweight           = muon_recoweights["nominal"];
//    std::cout<<std::to_string(muon_recoweight)<<std::endl;
    if(inputTreeName.compare("MUON_RECO_etaphibias__1up")        == 0) {m_muon_recoweight = m_muon_reco_etaphibias_up;}
    else if(inputTreeName.compare("MUON_RECO_etaphibias__1down") == 0) {m_muon_recoweight = m_muon_reco_etaphibias_down;}
    else if(inputTreeName.compare("MUON_RECO_ptscale__1up")      == 0) {m_muon_recoweight = m_muon_reco_ptscale_up;}
    else if(inputTreeName.compare("MUON_RECO_ptscale__1down")    == 0) {m_muon_recoweight = m_muon_reco_ptscale_down;}
//    std::cout<<"after syst:"<<std::endl;
//    std::cout<<std::to_string(muon_recoweight)<<std::endl;
    totalEventWeight *= m_muon_recoweight;
    std::map<std::string, double> muon_triggerweights = CalculateMuonBiasTest("Trig");
    m_muon_trigger_etaphibias_up   = muon_triggerweights["etaphibias_up"];
    m_muon_trigger_etaphibias_down = muon_triggerweights["etaphibias_down"];
    m_muon_trigger_ptscale_up      = muon_triggerweights["ptscale_up"];
    m_muon_trigger_ptscale_down    = muon_triggerweights["ptscale_down"];
    m_muon_triggerweight           = muon_triggerweights["nominal"];
    if(inputTreeName.compare("MUON_TRIGGER_etaphibias__1up")        == 0) {m_muon_triggerweight = m_muon_trigger_etaphibias_up;}
    else if(inputTreeName.compare("MUON_TRIGGER_etaphibias__1down") == 0) {m_muon_triggerweight = m_muon_trigger_etaphibias_down;}
    else if(inputTreeName.compare("MUON_TRIGGER_ptscale__1up")      == 0) {m_muon_triggerweight = m_muon_trigger_ptscale_up;}
    else if(inputTreeName.compare("MUON_TRIGGER_ptscale__1down")    == 0) {m_muon_triggerweight = m_muon_trigger_ptscale_down;}
    totalEventWeight *= m_muon_triggerweight;
//    std::cout<<m_muon_recoweight<<m_muon_triggerweight<<std::endl;
    if(debug) std::cout << "Applied muon bias weights" << std::endl;
  }



  // Can apply fake-weights to data
  m_fake_weight = 1.0;
  if(applyFakeWeights){
    if(m_isMC) std::cout << "WARNING: applying fake weights to MC, you probably don't want to do this!" << std::endl;

    if(useLikelihoodMM){
      StatusCode result = ExecuteMatrixMethod();
      m_fake_weight = m_IFF_fake_weight;  
    }
    else{
      m_fake_weight = CalculateFakeWeights();
    }
    totalEventWeight *= m_fake_weight;
  }

  // Apply the ttbar weight
//  double ttbar_weight = 1.0;
  if (applyttbarWeights){
    if(debug) std::cout << "about to apply ttbar weight" << std::endl;
    if(loose_leptons.size() >= 1 && loose_electrons.size() > 0){
      PO::Lepton& lep1  = loose_leptons.at(0).get();
      PO::Electron& ele = loose_electrons.at(0);
      double echarge    = ele.charge;
      int isemuM        = (echarge>0.0 ? 1 : 0);
      //double NJets = signalJets.size(); 
      std::map<std::string, double> ttbarweights = CalculateTtbarWeight2d(lep1.fourVec.Pt(), EtMiss, isemuM);
      //std::map<std::string, double> ttbarweights = CalculateTtbarWeight(lep1.fourVec.Pt(), isemuM);
      ttbar_weight = ttbarweights["Nominal"];
      totalEventWeight *= ttbar_weight;
      //      std::cout<<totalEventWeight<<std::endl;

      // apply systematic uncertainty as an additional weight 
      if (inputTreeName.compare("ttbar_WeightClosure")==0) {
        totalEventWeight *= (1.0 + ttbarweights["ttbar_WeightClosure"]);
        //         std::cout<<"applying ttbar modelling systematic"<<std::endl;
        //         std::cout<<totalEventWeight<<std::endl;
      }
    }
    if(debug) std::cout << "Applied ttbar weight" << std::endl;
  }


  if(debug){
    std::cout << "*****Calculated Event weight*****"       << std::endl;
    std::cout << "mc_event_weight: "             << mc_event_weight           << std::endl;
    std::cout << "triggerGlobalEfficiencySF"     << triggerGlobalEfficiencySF << std::endl;
    std::cout << "fake_weight: "                 << m_fake_weight             << std::endl;
    std::cout << "scaling: "                     << scaling                   << std::endl;
    std::cout << "pileupWeight: "                << m_pileup_weight           << std::endl;
    std::cout << "lumi weight (mc subcampaign) " << m_lumiweight              << std::endl;
    std::cout << "ttbar weight: "                << ttbar_weight              << std::endl;
    std::cout << "m_muon_recoweight: "           << m_muon_recoweight         << std::endl;
    std::cout << "muon_triggerweight: "          << m_muon_triggerweight      << std::endl;        
    std::cout << "totalEventWeight: "            << totalEventWeight          << std::endl;
    std::cout << "********************************" << std::endl;
  }

}
