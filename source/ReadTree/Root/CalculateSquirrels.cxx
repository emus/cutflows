#include "ReadTree/EventStruct.h"

void EventStruct::CalculateSquirrels(){
  CalculateSquirrelsObjects(squirrel_all, "all") ;
  CalculateSquirrelsObjects(squirrel_leptons, "leptons");
}

void EventStruct::CalculateSquirrelsObjects(Squirrels& squirrels, std::string config){

  if (debug) std::cout << "calculating squirrel variables... " << std::endl;
  if (debug) std::cout << "clearing squirrel" << std::endl;
  squirrels.clear();

  if (debug) std::cout << "clearing calculators" << std::endl;
  ok1p.clear();
  ok2p.clear();

  if (loose_leptons.size() < 2) {
    if (debug) std::cout << "There are only " << loose_leptons.size() << " leptons, which is not enough -- squirrel quitting early" << std::endl;
    return;
  }

  if (debug) std::cout << "filling calculators" << std::endl;
  float METx = EtMiss*cos(EtMissPhi);
  float METy = EtMiss*sin(EtMissPhi);


  if(config == "leptons"){
    // Assign everything to side 1 (default)
    // We'll let ok2p deal with the lepton assignments on its own
    //for(const auto& jet : jetsAndLeptons) {
    for(const auto& lep : loose_leptons){
      if(debug) std::cout << "Add lepton with pt " << lep.get().Pt << std::endl;
      ok1p.addVis(lep.get().fourVec);
      ok2p.addVis(lep.get().fourVec);
    }
  }
  else if(config == "all"){
    // add all objects to the computers
    for(const auto& lep : loose_leptons){
      ok1p.addVis(lep.get().fourVec);
      ok2p.addVis(lep.get().fourVec);
    }
    for(const auto& jet : signalJets){
      ok1p.addVis(jet.getFourVec());
      ok2p.addVis(jet.getFourVec());
    }
  }
  else{
    std::cout << "ERROR, config must be either all or leptons" << std::endl;
  }

  ok1p.setPtMiss(METx,METy);
  ok2p.setPtMiss(METx,METy);
  ok1p.setMinvis(0); // chi parameter in mt2 
  ok2p.setMinvis(0);

  squirrels.M1T = ok1p.calcM1T();
  squirrels.MT1 = ok1p.calcMT1();
  squirrels.Mo1 = ok1p.calcMo1();
  squirrels.M1o = ok1p.calcM1o();

  if (debug) {
    std::cout << "M1T: " << squirrels.M1T << std::endl;
    std::cout << "MT1: " << squirrels.MT1 << std::endl;
    std::cout << "Mo1: " << squirrels.Mo1 << std::endl;
    std::cout << "M1o: " << squirrels.M1o << std::endl;
  }

  ok2p.recluster_maxBoostedP();

  squirrels.M_side1 = ok2p.getSideMass(1);
  squirrels.M_side2 = ok2p.getSideMass(2);
  squirrels.Nvis_side1 = ok2p.getNvis(1);
  squirrels.Nvis_side2 = ok2p.getNvis(2);

  if (debug) {
    std::cout << "After clustering, have"
	      << " Side 1: M = " << squirrels.M_side1 << ", Nvis = " << squirrels.Nvis_side1 
	      << " Side 2: M = " << squirrels.M_side2 << ", Nvis = " << squirrels.Nvis_side2
	      << std::endl;
  }

  if(squirrels.Nvis_side1==0 || squirrels.Nvis_side2==0) {
    if (debug) std::cout << "less than two jets -- minMinv squirrel stops at one parent" << std::endl;
  } else {

    squirrels.M2T = ok2p.calcM2T();
    squirrels.MT2 = ok2p.calcMT2();
    squirrels.Mo2 = ok2p.calcMo2();
    squirrels.M2o = ok2p.calcM2o();
    squirrels.MCT = ok2p.calcMCT();

    squirrels.aMT2 = {
        ok2p.calcAsymMT2(100, 10),
        ok2p.calcAsymMT2(10, 100),
        ok2p.calcAsymMT2(100, 0),
        ok2p.calcAsymMT2(300, 0),
        ok2p.calcAsymMT2(500, 0),
        ok2p.calcAsymMT2(1000, 0)
    };


    if (debug) {
      std::cout << "M2T: " << squirrels.M2T << std::endl;
      std::cout << "MT2: " << squirrels.MT2 << std::endl;
      std::cout << "Mo2: " << squirrels.Mo2 << std::endl;
      std::cout << "M2o: " << squirrels.M2o << std::endl;
      std::cout << "MCT: " << squirrels.MCT << std::endl;
      std::cout << "size of aMT2" << squirrels.aMT2.size() << std::endl;
    }
  }
}

