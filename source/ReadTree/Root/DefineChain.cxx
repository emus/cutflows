#include "ReadTree/ReadTree.h"
/********
#include "ReadTree/Region/Region.h"
#include "ReadTree/Region/RegionJets.h"
#include "ReadTree/Region/RegionBjets.h"
#include "ReadTree/Region/RegionMJSigma.h"
#include "ReadTree/Region/RegionSumMHemi.h"
#include "ReadTree/Region/RegionMinMHemi.h"
#include "ReadTree/Region/RegionM2T.h"
#include "ReadTree/Region/RegionMo2.h"
#include "ReadTree/Region/RegionMETSig.h"
#include "ReadTree/Region/RegionPhoton.h"
*******/

#include "CutChain/Cut.h"
#include "CutChain/Node.h"
#include "CutChain/ChainSegment.h"
#include "CutChain/TH1Wrapper.h"
#include "CutChain/TH2Wrapper.h"
#include "CutChain/TH3Wrapper.h"
#include "CutChain/TTreeWrapper.h"
#include "CutChain/TEfficiencyWrapper.h"
#include "CutChain/TEfficiency2Wrapper.h"

#include <iostream>
#include <fstream>
#include <tuple>

#define BOOL_CHECK_ERR(EXP)                                   \
  do {                                                        \
    if (!EXP) {                                               \
      std::cerr << #EXP " evaluates to false!" << std::endl;  \
      throw;                                                  \
    }                                                         \
  } while (false)



void ReadTree::DefineChain()
{
  using namespace CutChain;
  using namespace CutPositionTags;
  Cut* activeCut = chain.startCut.get();
  if (! (m_cutMask & (First | Full) ) ) activeCut->enablePlotting(false); //*disable* plotting if neither first nor full are set

  // Note: may only be called once per cut!
  // Note: calculation of the event weight is done in CalculatEventWeights
  
  if(event.m_isMC || applyFakeWeights || applyMuonBias){ // only weight MC or data
    activeCut->increaseWeight([this] () {
        return event.totalEventWeight; 
        });
  }

  /////////////////////////////////
  // Make histograms 
  /////////////////////////////////
  makePlots(activeCut);


  /////////////////////////////////
  // Apply GenFilt cuts 
  // GenFiltHT MUST be the first cut
  // Cut applied to everything
  /////////////////////////////////

  bool doExtrattbar = false; // i.e. only running 410472
  if (doExtrattbar){
    std::cout << "DSID: " << m_dsid << std::endl;
    if(m_dsid==407341 || m_dsid==101232){ 
      activeCut = activeCut->addCut("GenFiltHT", [this] () {return event.GenFiltHT < 600;}, (m_cutMask & Full));
  //    std::cout << "applying GenFiltHT cut" << std::endl;
    }
    else{
      activeCut = activeCut->addCut("GenFiltHT", [this] () {return event.GenFiltHT > -1;}, (m_cutMask & Full) ); // doesn't cut any events, but I want the directory structure to be consistent. Maybe this isn't the most efficient way, but I can't think of anything better at the moment ... :/
  //    std::cout << "not applying GenFiltHT cut" << std::endl;
    }
    if(m_dsid==142857){
      activeCut = activeCut->addCut("GenFiltMET", [this] () {return event.GenFiltMET < 200;}, (m_cutMask & Full));
  //    std::cout << "applying GenFiltMET cut" << std::endl;
    }
    else{
      activeCut = activeCut->addCut("GenFiltMET", [this] () {return event.GenFiltMET > -1;}, (m_cutMask & Full) ); // doesn't cut any events, but I want the directory structure to be consistent. Maybe this isn't the most efficient way, but I can't think of anything better at the moment ... :/
  //    std::cout << "not applying GenFiltMET cut" << std::endl;
    }
  }

  ////////////////////////////////////
  // Baseline lepton pair
  ////////////////////////////////////
  //Cut* twoLeptons = activeCut->addCut("Exactly_two_leptons", [this] () { return (event.loose_leptons.size() == 2); } );
  activeCut->addExtraCutflowVar("Exactly_two_leptons", [this] () { return (event.loose_leptons.size() == 2); } );
  Cut* twoLeptons = activeCut; 

  
  ////////////////////////////////////
  // Two prompt leptons (will remove non-prompt MC sources according to IFF) 
  //////////////////////////////////// 
  if(applyPromptLeptonCut){
    twoLeptons = twoLeptons->addCut("two_prompt_leptons", [this] () {return (event.loose_leptons.at(0).get().isPrompt && event.loose_leptons.at(1).get().isPrompt);}, (m_cutMask & Full));
  }
  if(applyPromptRejectionCut){
    // reject events with two prompt leptons
    twoLeptons = twoLeptons->addCut("two_nonPrompt_leptons", [this] () {return !(event.loose_leptons.at(0).get().isPrompt && event.loose_leptons.at(1).get().isPrompt);}, (m_cutMask & Full));
  }
  Node* twoLeptons_node = twoLeptons->splitChain();


  ////////////////////////////////////
  // Chain for different-flavour leptons
  ////////////////////////////////////

  //Cut* twoOppositeFlavourLeptons = twoLeptons_node->addSubchain("2DFL", "2DFL", [this] () -> bool {return (event.count_loose_muons==1 && event.count_loose_electrons==1); } );
  Cut* twoOppositeFlavourLeptons = twoLeptons_node->addSubchain("2DFL", "2DFL", [this] () -> bool {return (event.loose_muons.size() == 1 && event.loose_electrons.size()==1); }, (m_cutMask & Full) );
  Node* twoOppositeFlavourLeptons_node = twoOppositeFlavourLeptons->splitChain();

  Cut* OSePmuM = twoOppositeFlavourLeptons_node->addSubchain("OSePmuM", "OSePmuM", [this] () -> bool { return (event.loose_electrons.at(0).charge ==  1 && event.loose_muons.at(0).charge == -1); }, (m_cutMask & Full) );
  Cut* OSeMmuP = twoOppositeFlavourLeptons_node->addSubchain("OSeMmuP", "OSeMmuP", [this] () -> bool { return (event.loose_electrons.at(0).charge == -1 && event.loose_muons.at(0).charge ==  1); }, (m_cutMask & Full) );


  // Split charge-flavour chains to prepare for adding regions
  Node* OSePmuM_node = OSePmuM->splitChain();
  Node* OSeMmuP_node = OSeMmuP->splitChain();

  //Make SRs (always different flavour)
  makeSRCuts(OSePmuM_node, "OSePmuM", Full);
  makeSRCuts(OSeMmuP_node, "OSeMmuP", Full);

  // Same sign different flavour -- used for validation  of fakes 
  bool make_fake_estimate_regions = false;
  if(make_fake_estimate_regions){
    Cut* SSePmuP = twoOppositeFlavourLeptons_node->addSubchain("SSePmuP", "SSePmuP", [this] () -> bool { return (event.loose_electrons.at(0).charge ==  1 && event.loose_muons.at(0).charge == 1); }, (m_cutMask & Full) );
    Cut* SSeMmuM = twoOppositeFlavourLeptons_node->addSubchain("SSeMmuM", "SSeMmuM", [this] () -> bool { return (event.loose_electrons.at(0).charge == -1 && event.loose_muons.at(0).charge ==  -1); }, (m_cutMask & Full) );

    Node* SSePmuP_node = SSePmuP->splitChain();
    Node* SSeMmuM_node = SSeMmuM->splitChain();

    //makeFakeValidationRegions(SSePmuP_node, "SSePmuP");
    //makeFakeValidationRegions(SSeMmuM_node, "SSeMmuM");


    makeFakeEstimateRegions(SSePmuP_node, "SSePmuP", Full);
    makeFakeEstimateRegions(SSeMmuM_node, "SSeMmuM", Full);
    
    makeFakeEstimateRegions(OSePmuM_node, "OSePmuM", Full);
    makeFakeEstimateRegions(OSeMmuP_node, "OSeMmuP", Full);
      

    ////////////////////////////////////
    // Chain for two same flavour leptons
    ////////////////////////////////////
    
    Cut* twoSameFlavourLeptons = twoLeptons_node->addSubchain("2SFL", "2SFL", [this] () -> bool{return ((event.count_loose_muons == 0 && event.count_loose_electrons == 2) || (event.count_loose_muons == 2 && event.count_loose_electrons == 0)); }, (m_cutMask & Full) );
    Node* twoSameFlavourLeptons_node = twoSameFlavourLeptons->splitChain();

    Cut* OSll   = twoSameFlavourLeptons_node->addSubchain("OSll",   "OSll",   [this] () -> bool {return (event.loose_leptons.at(0).get().charge == -1* event.loose_leptons.at(1).get().charge);}, (m_cutMask & Full) );

    ////////////////////////////////////
    // Chain for two electrons
    ////////////////////////////////////

    Cut* twoElectrons = twoLeptons_node->addSubchain("2e", "2e", [this] () -> bool{return (event.count_loose_muons == 0 && event.count_loose_electrons == 2); }, (m_cutMask & Full) );
    Node* twoElectrons_node = twoElectrons->splitChain();

    Cut* OSee   = twoElectrons_node->addSubchain("OSee",   "OSee",   [this] () -> bool {return (event.loose_electrons.at(0).charge == -1* event.loose_electrons.at(1).charge);}, (m_cutMask & Full) );
    Cut* SSePeP = twoElectrons_node->addSubchain("SSePeP", "SSePeP", [this] () -> bool {return (event.loose_electrons.at(0).charge == 1 && event.loose_electrons.at(1).charge == 1);}, (m_cutMask & Full) );
    Cut* SSeMeM = twoElectrons_node->addSubchain("SSeMeM", "SSeMeM", [this] () -> bool {return (event.loose_electrons.at(0).charge == -1 &&  event.loose_electrons.at(1).charge == -1);}, (m_cutMask & Full) );


    ////////////////////////////////////
    // Chain for two muons
    ////////////////////////////////////

    Cut* twoMuons = twoLeptons_node->addSubchain("2mu", "2mu", [this] () -> bool {return (event.count_loose_muons == 2 && event.count_loose_electrons == 0); }, (m_cutMask & Full) );
    Node* twoMuons_node = twoMuons->splitChain();

    Cut* OSmumu   = twoMuons_node->addSubchain("OSmumu"  , "OSmumu",   [this] () -> bool {return (event.loose_muons.at(0).charge == -1* event.loose_muons.at(1).charge);}, (m_cutMask & Full) );
    Cut* SSmuPmuP = twoMuons_node->addSubchain("SSmuPmuP", "SSmuPmuP", [this] () -> bool {return (event.loose_muons.at(0).charge == 1 && event.loose_muons.at(1).charge == 1);}, (m_cutMask & Full) );
    Cut* SSmuMmuM = twoMuons_node->addSubchain("SSmuMmuM", "SSmuMmuM", [this] () -> bool {return (event.loose_muons.at(0).charge == -1 &&  event.loose_muons.at(1).charge == -1);}, (m_cutMask & Full) );

    // Want these regions for validation of the fake estimation
    //makeSRCuts(SSePmuP_node, "SSePmuP");   
    //makeSRCuts(SSeMmuM_node, "SSeMmuM");
    //

    Node* OSll_node = OSll->splitChain();
    Node* OSee_node = OSee->splitChain();
    Node* SSePeP_node = SSePeP->splitChain();
    Node* SSeMeM_node = SSeMeM->splitChain();
    Node* OSmumu_node = OSmumu->splitChain();
    Node* SSmuPmuP_node = SSmuPmuP->splitChain();
    Node* SSmuMmuM_node = SSmuMmuM->splitChain();

    makeCRVRCuts(OSll_node, "OSll", Full);
    makeCRVRCuts(OSee_node, "OSee", Full);
    makeCRVRCuts(SSePeP_node, "SSePeP", Full);
    makeCRVRCuts(SSeMeM_node, "SSeMeM", Full);
    makeCRVRCuts(OSmumu_node, "OSmumu", Full);
    makeCRVRCuts(SSmuPmuP_node, "SSmuPmuP", Full);
    makeCRVRCuts(SSmuMmuM_node, "SSmuMmuM", Full);

//    bool do_efficiency(false);

      // For the fake efficiency, we only care about different flavour leptons (with the same sign)  
      makeFakeEfficiencyRegions(SSePmuP_node, "SSePmuP", Full);
      makeFakeEfficiencyRegions(SSeMmuM_node, "SSeMmuM", Full);
      makeFakeEfficiencyRegions(SSmuPmuP_node, "SSmuPmuP", Full);
      makeFakeEfficiencyRegions(SSmuMmuM_node, "SSmuMmuM", Full);
      //makeFakeEfficiencyRegions(OSePmuM_node, "OSePmuM");
      //makeFakeEfficiencyRegions(OSeMmuP_node, "OSeMmuP");
      //makeFakeEfficiencyRegions(SSePeP_node, "SSePeP");
      //makeFakeEfficiencyRegions(SSeMeM_node, "SSeMeM");

      // For the real efficiency, we only care about same-flavour opposite-sign leptons
      makeRealEfficiencyRegions(OSee_node, "OSee", Full);
      makeRealEfficiencyRegions(OSmumu_node, "OSmumu", Full);

      // considering not calculating these regions 
      //makeABCDValidRegions(SSePeP_node, "SSePeP");
      //makeABCDValidRegions(SSeMeM_node, "SSeMeM");
      //makeABCDValidRegions(OSee_node, "OSee");

  /**
  else{
    // finish otherwise unfinished chains
    OSll->finishChain("OSll");
    OSee->finishChain("OSee");
    OSmumu->finishChain("OSmumu");

    SSmuPmuP->finishChain("SSmuPmuP");
    SSmuMmuM->finishChain("SSmuMmuM");

    SSeMeM->finishChain("SSeMeM");
    SSePeP->finishChain("SSePeP");
  }
  ***********/
 
  
  } // end of make_fake_estimate_regions


    /*********
     *
     * WJF: these were for the ratio of ratios plots that I made ... 
     *
    // regions with leading lepton of specific charge/flavour
    Cut* leadingLepton_eP = OSePmuM_node->addSubchain("leadingLepton_eP", "leadingLepton_eP", [this] () -> bool { return (event.loose_leptons.at(0).get().flavour == "electron"); } );
    Cut* leadingLepton_muM = OSePmuM_node->addSubchain("leadingLepton_muM", "leadingLepton_muM", [this] () -> bool { return (event.loose_leptons.at(0).get().flavour == "muon"); } );
    Cut* leadingLepton_eM = OSeMmuP_node->addSubchain("leadingLepton_eM", "leadingLepton_eM", [this] () -> bool { return (event.loose_leptons.at(0).get().flavour == "electron"); } );
    Cut* leadingLepton_muP = OSeMmuP_node->addSubchain("leadingLepton_muP", "leadingLepton_muP", [this] () -> bool { return (event.loose_leptons.at(0).get().flavour == "muon"); } );

    leadingLepton_eP->finishChain("leadingLepton_eP");
    leadingLepton_muM->finishChain("leadingLepton_muM");
    leadingLepton_muP->finishChain("leadingLepton_muP");
    leadingLepton_eM->finishChain("leadingLepton_eM");
    ***************/


    //chain.print();
    std::cout << std::endl;

}


void ReadTree::makeSRVRCuts(CutChain::Node* new_node, std::string tightness, std::string prefix, bool Full){
  
    ///////////////////////////////////
    // Select node depending on tightness 
    ///////////////////////////////////
    
    CutChain::Node* current_node = new_node; // no cut, two loose leptons
    
    ///////////////////////////////////
    // Add cuts  
    ///////////////////////////////////

    std::string c1_name = prefix+"_"+tightness+"MET";
    CutChain::Cut* cut1 = current_node->addSubchain(c1_name, c1_name, [this] () -> bool {
        return (true);
        }, (m_cutMask & Full));
    cut1->finishChain(c1_name);

    if (tightness.find("VR") == std::string::npos){ // don't need VRs for these regions.
      std::string c2_name = prefix+"_"+tightness+"JET";
      CutChain::Cut* cut2 = current_node->addSubchain(c2_name, c2_name, [this] () -> bool {
          return (event.signalJets.size()>0 ) ;
          }, (m_cutMask & Full));
      cut2->finishChain(c2_name);
  
      std::string c3_name = prefix+"_"+tightness+"RPV_MT2100_METSig10";
      CutChain::Cut* cut3 = current_node->addSubchain(c3_name, c3_name, [this] () -> bool {
          return (event.squirrel_leptons.MT2>=100.0 && event.EtMissSignificance>=10.0) ;
          }, (m_cutMask & Full));
      cut3->finishChain(c3_name);
  
      std::string c4_name = prefix+"_"+tightness+"RPV_MT2100_10METSig";
      CutChain::Cut* cut4 = current_node->addSubchain(c4_name, c4_name, [this] () -> bool {
          return (event.squirrel_leptons.MT2>=100.0 && event.EtMissSignificance<10.0) ;
          }, (m_cutMask & Full));
      cut4->finishChain(c4_name);
  
      std::string c5_name = prefix+"_"+tightness+"RPV_100MT2_METSig10";
      CutChain::Cut* cut5 = current_node->addSubchain(c5_name, c5_name, [this] () -> bool {
          return (event.squirrel_leptons.MT2<100.0 && event.EtMissSignificance>=10.0) ;
          }, (m_cutMask & Full));
      cut5->finishChain(c5_name);
  
      std::string c55_name = prefix+"_"+tightness+"RPV_100MT2_10METSig";
      CutChain::Cut* cut55 = current_node->addSubchain(c55_name, c55_name, [this] () -> bool {
          return (event.squirrel_leptons.MT2<100.0 && event.EtMissSignificance<10.0) ;
          }, (m_cutMask & Full));
      cut55->finishChain(c55_name);
  
      std::string c6_name = prefix+"_"+tightness+"LQ_6METSig_sumlpt1000";
      CutChain::Cut* cut6 = current_node->addSubchain(c6_name, c6_name, [this] () -> bool {
          return (((event.signalJets.size()>1 && event.signalJets.at(1).Pt<300.0 ) || event.signalJets.size()==1 ) 
              && event.EtMissSignificance<6. && event.sum_pt_leps_jet1>=1000.0 );
          }, (m_cutMask & Full));
      cut6->finishChain(c6_name);
  
      std::string c61_name = prefix+"_"+tightness+"LQ_METSig6_sumlpt1000";
      CutChain::Cut* cut61 = current_node->addSubchain(c61_name, c61_name, [this] () -> bool {
          return (((event.signalJets.size()>1 && event.signalJets.at(1).Pt<300.0 ) || event.signalJets.size()==1 ) 
              && event.EtMissSignificance>=6. && event.sum_pt_leps_jet1>=1000.0 );
          }, (m_cutMask & Full));
      cut61->finishChain(c61_name);
  
      std::string c62_name = prefix+"_"+tightness+"LQ_6METSig_1000sumlpt";
      CutChain::Cut* cut62 = current_node->addSubchain(c62_name, c62_name, [this] () -> bool {
          return (((event.signalJets.size()>1 && event.signalJets.at(1).Pt<300.0 ) || event.signalJets.size()==1 ) 
              && event.EtMissSignificance<6. && event.sum_pt_leps_jet1<1000.0 );
          }, (m_cutMask & Full));
      cut62->finishChain(c62_name);
  
      std::string c63_name = prefix+"_"+tightness+"LQ_METSig6_1000sumlpt";
      CutChain::Cut* cut63 = current_node->addSubchain(c63_name, c63_name, [this] () -> bool {
          return (((event.signalJets.size()>1 && event.signalJets.at(1).Pt<300.0 ) || event.signalJets.size()==1 ) 
              && event.EtMissSignificance>=6. && event.sum_pt_leps_jet1<1000.0 );
          }, (m_cutMask & Full));
      cut63->finishChain(c63_name);
  
      std::string c8_name = prefix+"_"+tightness+"LQ";
      CutChain::Cut* cut8 = current_node->addSubchain(c8_name, c8_name, [this] () -> bool {
          return (((event.signalJets.size()>1 && event.signalJets.at(1).Pt<300.0 ) || event.signalJets.size()==1 ));
          }, (m_cutMask & Full));
     cut8->finishChain(c8_name);
 
    } 
 
}

void ReadTree::makeSRCuts(CutChain::Node* new_node, std::string prefix, bool Full)
{


  // note all leptons inside loose_leptons have "isLoose" 
  //


  // two perfect leptons
  CutChain::Cut* twoPerfectLeptons = new_node->addSubchain("twoPerfectLeptons", "twoPerfectLeptons", [this] () -> bool{ return (event.loose_leptons.at(0).get().isPerfect==1 && event.loose_leptons.at(1).get().isPerfect==1); }, (m_cutMask & Full) );
  CutChain::Node* twoPerfectLeptons_node = twoPerfectLeptons->splitChain();

//  // NOT two perfect leptons
//  CutChain::Cut* nottwoPerfectLeptons = new_node->addSubchain("nottwoPerfectLeptons", "nottwoPerfectLeptons", [this] () -> bool{ return (!(event.loose_leptons.at(0).get().isPerfect==1 && event.loose_leptons.at(1).get().isPerfect==1)); }, (m_cutMask & Full) );
//  CutChain::Node* nottwoPerfectLeptons_node = nottwoPerfectLeptons->splitChain();
//
//  // two imperfect leptons
//  CutChain::Cut* twoimPerfectLeptons = new_node->addSubchain("twoimPerfectLeptons", "twoimPerfectLeptons", [this] () -> bool{ return (event.loose_leptons.at(0).get().isPerfect==0 && event.loose_leptons.at(1).get().isPerfect==0); }, (m_cutMask & Full) );
//  CutChain::Node* twoimPerfectLeptons_node = twoimPerfectLeptons->splitChain();

  // stop paper region
//  std::string stopname = "srstop_"+prefix;
//  CutChain::Cut* srtwobody = twoPerfectLeptons_node->addSubchain(stopname, stopname, [this] () -> bool{ return (event.mll>20. && event.dPhiBoost<1.5 && event.EtMissSignificance>12 && event.squirrel_leptons.MT2>110. && event.signalJets.size()>0 ); }, (m_cutMask & Full) );
//  srtwobody->finishChain(stopname);


  // summt>200
  CutChain::Cut* sumMtMET200 = twoPerfectLeptons_node->addSubchain("sumMtMET200", "sumMtMET200", [this] () -> bool{ return (event.sum_mt_met>200.); }, (m_cutMask & Full) );
  CutChain::Node* sumMtMET200_node = sumMtMET200->splitChain();

//  // summt>200
//  CutChain::Cut* IncLoose_sumMtMET200 = new_node->addSubchain("IncLoose_sumMtMET200", "IncLoose_sumMtMET200", [this] () -> bool{ return (event.sum_mt_met>200.); }, (m_cutMask & Full) );
//  CutChain::Node* IncLoose_sumMtMET200_node = IncLoose_sumMtMET200->splitChain();
//
//  // summt>200
//  CutChain::Cut* Loose_sumMtMET200 = twoimPerfectLeptons_node->addSubchain("Loose_sumMtMET200", "Loose_sumMtMET200", [this] () -> bool{ return (event.sum_mt_met>200.); }, (m_cutMask & Full) );
//  CutChain::Node* Loose_sumMtMET200_node = Loose_sumMtMET200->splitChain();
//
//  // summt>200
//  CutChain::Cut* Ortho_sumMtMET200 = nottwoPerfectLeptons_node->addSubchain("Ortho_sumMtMET200", "Ortho_sumMtMET200", [this] () -> bool{ return (event.sum_mt_met>200.); }, (m_cutMask & Full) );
//  CutChain::Node* Ortho_sumMtMET200_node = Ortho_sumMtMET200->splitChain();

//  // summt>250
//  CutChain::Cut* sumMtMET250 = twoPerfectLeptons_node->addSubchain("sumMtMET250", "sumMtMET250", [this] () -> bool{ return (event.sum_mt_met>250.); }, (m_cutMask & Full) );
//  CutChain::Node* sumMtMET250_node = sumMtMET250->splitChain();
//
//  // summt>250
//  CutChain::Cut* IncLoose_sumMtMET250 = new_node->addSubchain("IncLoose_sumMtMET250", "IncLoose_sumMtMET250", [this] () -> bool{ return (event.sum_mt_met>250.); }, (m_cutMask & Full) );
//  CutChain::Node* IncLoose_sumMtMET250_node = IncLoose_sumMtMET250->splitChain();
//
//  // summt>250
//  CutChain::Cut* Loose_sumMtMET250 = twoimPerfectLeptons_node->addSubchain("Loose_sumMtMET250", "Loose_sumMtMET250", [this] () -> bool{ return (event.sum_mt_met>250.); }, (m_cutMask & Full) );
//  CutChain::Node* Loose_sumMtMET250_node = Loose_sumMtMET250->splitChain();
//
//  // summt>250
//  CutChain::Cut* Ortho_sumMtMET250 = nottwoPerfectLeptons_node->addSubchain("Ortho_sumMtMET250", "Ortho_sumMtMET250", [this] () -> bool{ return (event.sum_mt_met>250.); }, (m_cutMask & Full) );
//  CutChain::Node* Ortho_sumMtMET250_node = Ortho_sumMtMET250->splitChain();
//
  // WJF: Want both two loose leptons _AND_ two tight leptons for these regions, if only for the fake estimate
//  makeSRVRCuts( Ortho_sumMtMET200_node, "OrthoVR", prefix, Full ); 
//  makeSRVRCuts( Ortho_sumMtMET250_node, "sumMtMET250_OrthoVR", prefix, Full ); 
//  makeSRVRCuts( Loose_sumMtMET200_node, "LooseVR", prefix, Full ); 
//  makeSRVRCuts( Loose_sumMtMET250_node, "sumMtMET250_LooseVR", prefix, Full ); 
//  makeSRVRCuts( IncLoose_sumMtMET200_node, "VR", prefix, Full ); 
//  makeSRVRCuts( IncLoose_sumMtMET250_node, "sumMtMET250_VR", prefix, Full ); 
  makeSRVRCuts( sumMtMET200_node, "SR", prefix, Full ); 
//  makeSRVRCuts( sumMtMET250_node, "sumMtMET250_SR", prefix, Full ); 

  /////////////////////////
  // Control region cuts
  /////////////////////////
  
  // addExtraCutflowVar would be have been ideal here, but it wouldn't work with the splitting of the chain. If this cut could be placed with twoPerfectLeptons that would work, but we don't want thi cut applied to the SRs 
  CutChain::Cut* vetoHighLepton = twoPerfectLeptons_node->addSubchain("rejectHighPtLeadingLepton", "rejectHighPtLeadingLepton", [this] () -> bool {
      return (event.loose_leptons.at(0).get().Pt < 400.0);
      }, (m_cutMask & Full));
  CutChain::Node* vetoHighLepton_node = vetoHighLepton->splitChain();

  // control regions
  std::string c14_name = prefix+"_CRRatio";
  CutChain::Cut* c14 = vetoHighLepton_node->addSubchain(c14_name, c14_name, [this] () -> bool {
      return (event.sum_mt_met<200. ) ;
      }, (m_cutMask & Full));
  c14->finishChain(c14_name);

//  // control regions
//  std::string c15_name = prefix+"_sumMtMET150_CRRatio";
//  CutChain::Cut* c15 = vetoHighLepton_node->addSubchain(c15_name, c15_name, [this] () -> bool {
//      return (event.sum_mt_met<150. ) ;
//      }, (m_cutMask & Full));
//  c15->finishChain(c15_name);
//
//  // control regions
//  std::string c16_name = prefix+"_sumMtMET150250_CRRatio";
//  CutChain::Cut* c16 = vetoHighLepton_node->addSubchain(c16_name, c16_name, [this] () -> bool {
//      return (event.sum_mt_met<250. && event.sum_mt_met>150 ) ;
//      }, (m_cutMask & Full));
//  c16->finishChain(c16_name);
//
//  // control regions
//  std::string c17_name = prefix+"_sumMtMET150_CRJET";
//  CutChain::Cut* c17 = vetoHighLepton_node->addSubchain(c17_name, c17_name, [this] () -> bool {
//      return (event.sum_mt_met<150. ) ;
//      }, (m_cutMask & Full));
//  c17->finishChain(c17_name);
//
//  // control regions
//  std::string c18_name = prefix+"_sumMtMET150250_CRJET";
//  CutChain::Cut* c18 = vetoHighLepton_node->addSubchain(c18_name, c18_name, [this] () -> bool {
//      return (event.sum_mt_met<250. && event.sum_mt_met>150 ) ;
//      }, (m_cutMask & Full));
//  c18->finishChain(c18_name);

  // control regions
  std::string c19_name = prefix+"_CRJET";
  CutChain::Cut* c19 = vetoHighLepton_node->addSubchain(c19_name, c19_name, [this] () -> bool {
      return (event.sum_mt_met<200. && event.signalJets.size()>0 ) ;
      }, (m_cutMask & Full));
  c19->finishChain(c19_name);


}


void ReadTree::makeCRVRCuts(CutChain::Node* new_node, std::string prefix, bool Full)
{

  //std::cout<<"Making CR cuts"<<std::endl;

  // note all leptons inside loose_leptons have "isLoose" 
  // This cut is repeated here because the incoming nodes are not shared with the incoming nodes of makeSRCuts
  // This could lead to some bad behaviour if the same node is passed to both functions, I think. 
  CutChain::Cut* twoPerfectLeptons = new_node->addSubchain("twoPerfectLeptons", "twoPerfectLeptons", [this] () -> bool{ return (event.loose_leptons.at(0).get().isPerfect==1 && event.loose_leptons.at(1).get().isPerfect==1); }, (m_cutMask & Full) );

  CutChain::Node* twoPerfectLeptons_node = twoPerfectLeptons->splitChain();

  // Validation regions

  // Validation region for OSee and OSmumu
  std::string c18_name = prefix+"_VR";
  CutChain::Cut* c18 = twoPerfectLeptons_node->addSubchain(c18_name, c18_name, [this] () -> bool {
      return true; // just want two perfect leptons 
      }, (m_cutMask & Full));
  c18->finishChain(c18_name);

}

/***************
void ReadTree::makeVRCuts(CutChain::Node* new_node, std::string prefix)
{
  std::cout<<"Making VR cuts"<<std::endl;
  // note all leptons inside loose_leptons have "isLoose" 
  std::string c1_name = prefix+"_VRttbar_SF_G1j_1bj";
  CutChain::Cut* c1 = new_node->addSubchain(c1_name, c1_name, [this] () -> bool {
      return (event.loose_leptons.at(0).get().isPerfect==1 && event.loose_leptons.at(1).get().isPerfect==1 && event.signalJets.size()>1 && event.countSignalBjets==1  ) ;
      });
  c1->finishChain(c1_name);
}
***********/

void ReadTree::makeFakeEfficiencyRegions(CutChain::Node* new_node, std::string prefix, bool Full)
{


  // Note all leptons inside loose_leptons have "isLoose" 
  // Note there are always exactly two leptons 

  //////////////////////////////////
  // Will's fake efficiency regions
  //////////////////////////////////
  
  // For MC subtraction, we want both MC leptons to be prompt. For data, all leptons are prompt
  // Note prompt leppton cut move to start of the chain


  /*************
  // Muon case, maybe want *only* one tagged muon? Use XOR: !A != !B
  // Not sure if we need this? 
  std::string xor_cutname = prefix+"_exactlyOneTaggedMuon";
  CutChain::Cut * exactly_one_tagged_muon = new_node->addSubchain(xor_cutname, xor_cutname, [this] () -> bool {
      return(
          !(event.loose_leptons.at(0).get().flavour == "muon" && event.loose_leptons.at(0).get().isTagged)
           != !(event.loose_leptons.at(1).get().flavour == "muon" && event.loose_leptons.at(1).get().isTagged)
        );
      });
  exactly_one_tagged_muon->finishChain(xor_cutname);
  *************/

    /***************
     *
     * WJF: Added for additional fakes validation
     * but the eT_mu0 region is identical to this
     * Hence this bit being commented out 
     *
     *
  // one loose muon, one tight electron. Muon is inclusive loose
    CutChain::Cut* one_tight_electron = new_node->addSubchain("one_tight_electron", "one_tight_electron", [this] () -> bool {
      return(
          (event.loose_leptons.at(0).get().flavour == "electron" && event.loose_leptons.at(0).get().isTight)
           || (event.loose_leptons.at(1).get().flavour == "electron" && event.loose_leptons.at(1).get().isTight)
          );
      }, (m_cutMask & Full));
  CutChain::Node* one_tight_electron_node = one_tight_electron->splitChain();


  // at least one of the leptons is already loose (and at least one is tight since "tight" is part of the isTagged condition)
  std::string c7_name = prefix+"_oneLooseMuon";
  CutChain::Cut* c7 = one_tight_electron_node->addSubchain(c7_name, c7_name, [this] () -> bool {
      return true; 
      }, (m_cutMask & Full));
  c7->finishChain(c7_name);
  *****************************************/


  // Fake electron case, want the muon to be tagged (don't care about the electron)
  // Note "tagged" is "tight & pT > 50 GeV"
  CutChain::Cut* one_tagged_muon = new_node->addSubchain("atLeastOneTaggedMuon", "atLeastOneTaggedMuon", [this] () -> bool {
      return(
          (event.loose_leptons.at(0).get().flavour == "muon" && event.loose_leptons.at(0).get().isTagged)
           || (event.loose_leptons.at(1).get().flavour == "muon" && event.loose_leptons.at(1).get().isTagged)
          );
      }, (m_cutMask & Full));
  CutChain::Node* one_tagged_muon_node = one_tagged_muon->splitChain();

  // Fake lepton requirement estimated from the ratio of the number of probes matching the Tight requirement,
  // to those passing an inclusive loose selection.
  
  // at least one of the leptons is already loose (and at least one is tight since "tight" is part of the isTagged condition)
  std::string c5_name = prefix+"_oneLoose";
  CutChain::Cut* c5 = one_tagged_muon_node->addSubchain(c5_name, c5_name, [this] () -> bool {
      return true; 
      }, (m_cutMask & Full));
  c5->finishChain(c5_name);



  // the tag lepton is already tight, here we want the other lepton to also be tight
  std::string c6_name = prefix+"_twoTightLeptons";
  CutChain::Cut* c6 = one_tagged_muon_node->addSubchain(c6_name, c6_name, [this] () -> bool {
      return (event.loose_leptons.at(0).get().isTight && event.loose_leptons.at(1).get().isTight); 
      }, (m_cutMask & Full));
  c6->finishChain(c6_name);

}


void ReadTree::makeFakeValidationRegions(CutChain::Node* node, std::string prefix, bool Full){

  // Used for same-sign emu regions, for validation of the fake estimate

  std::string c1_name = prefix + "_eT_muT";
  CutChain::Cut* c1 = node->addSubchain(c1_name, c1_name, [this] () -> bool {
      return ( 
        ((event.loose_leptons.at(0).get().flavour == "electron" && event.loose_leptons.at(0).get().isTight)
        || (event.loose_leptons.at(1).get().flavour == "electron" && event.loose_leptons.at(1).get().isTight))
        &&
        ((event.loose_leptons.at(0).get().flavour == "muon" && !event.loose_leptons.at(0).get().isTight)
        || (event.loose_leptons.at(1).get().flavour == "muon" && !event.loose_leptons.at(1).get().isTight))
        );
      }, (m_cutMask & Full) );
  c1->finishChain(c1_name);
  
}

void ReadTree::makeFakeEstimateRegions(CutChain::Node* new_node, std::string prefix, bool Full){

  /*************************************
  Very much like the regions in "makeRealEfficiencyRegions", except:
  - not inside the Z peak
  - with the "loose and not tight" requirement

  Note, these regions are intended for cases where there is one muon and one electron ONLY
  *******************************/


  
  // tight electron, loose-not-tight muon
  // note nearly signal already has "loose"
  std::string c1_name = prefix + "_eT_muL";
  CutChain::Cut* c1 = new_node->addSubchain(c1_name, c1_name, [this] () -> bool {
      return ( 
        ((event.loose_leptons.at(0).get().flavour == "electron" && event.loose_leptons.at(0).get().isTight)
        || (event.loose_leptons.at(1).get().flavour == "electron" && event.loose_leptons.at(1).get().isTight))
        &&
        ((event.loose_leptons.at(0).get().flavour == "muon" && !event.loose_leptons.at(0).get().isTight)
        || (event.loose_leptons.at(1).get().flavour == "muon" && !event.loose_leptons.at(1).get().isTight))
        );
      }, (m_cutMask & Full) );
  c1->finishChain(c1_name);


  // tight muon, loose-not-tight electron
  // note nearly signal already has "loose"
  std::string c2_name = prefix + "_eL_muT";
  CutChain::Cut* c2 = new_node->addSubchain(c2_name, c2_name, [this] () -> bool {
      return ( 
        ((event.loose_leptons.at(0).get().flavour == "muon" && event.loose_leptons.at(0).get().isTight)
        || (event.loose_leptons.at(1).get().flavour == "muon" && event.loose_leptons.at(1).get().isTight))
        &&
        ((event.loose_leptons.at(0).get().flavour == "electron" && !event.loose_leptons.at(0).get().isTight)
        || (event.loose_leptons.at(1).get().flavour == "electron" && !event.loose_leptons.at(1).get().isTight))
        );
      }, (m_cutMask & Full) );
  c2->finishChain(c2_name);

  // loose-not-tight muon and loose-not-tight electron
  // note nearly signal already has "loose"
  std::string c3_name = prefix + "_eL_muL";
  CutChain::Cut* c3 = new_node->addSubchain(c3_name, c3_name, [this] () -> bool {
      return ( !event.loose_leptons.at(0).get().isTight && !event.loose_leptons.at(1).get().isTight );
      }, (m_cutMask & Full) );
  c3->finishChain(c3_name);

  // Tight-Tight regions
  // Careful because this can be the SR
  std::string c4_name = prefix + "_eT_muT";
  CutChain::Cut* c4 = new_node->addSubchain(c4_name, c4_name, [this] () -> bool {
      return ( event.loose_leptons.at(0).get().isTight && event.loose_leptons.at(1).get().isTight );
      }, (m_cutMask & Full) );
  c4->finishChain(c4_name);

  ////////////////////////////////////
  // Will's fake estimate idea regions
  ////////////////////////////////////

  // Tight electron, inclusive loose muon
  std::string c5_name = prefix + "_eT_mu0";
  CutChain::Cut* c5 = new_node->addSubchain(c5_name, c5_name, [this] () -> bool {
      return ( event.loose_electrons.at(0).isTight );
      }, (m_cutMask & Full) );
  c5->finishChain(c5_name);

  // Loose-not-tight electron, inclusive loose muon
  std::string c6_name = prefix + "_eL_mu0";
  CutChain::Cut* c6 = new_node->addSubchain(c6_name, c6_name, [this] () -> bool {
      return ( !event.loose_electrons.at(0).isTight );
      }, (m_cutMask & Full) );
  c6->finishChain(c6_name);


  // inclusive loose electron, tight muon
  std::string c7_name = prefix + "_e0_muT";
  CutChain::Cut* c7 = new_node->addSubchain(c7_name, c7_name, [this] () -> bool {
      return ( event.loose_muons.at(0).isTight );
      }, (m_cutMask & Full) );
  c7->finishChain(c7_name);

  // inclusive loose electron, loose-not-tight muon
  std::string c8_name = prefix + "_e0_muL";
  CutChain::Cut* c8 = new_node->addSubchain(c8_name, c8_name, [this] () -> bool {
      return ( !event.loose_muons.at(0).isTight );
      }, (m_cutMask & Full) );
  c8->finishChain(c8_name);

}

void ReadTree::makeABCDValidRegions(CutChain::Node* new_node, std::string prefix, bool Full){
  
  if (prefix == "OSee"){

    // e+ inclusive-loose, e- tight
    std::string c1_name = prefix+"_eP0_eMT";
    CutChain::Cut* c1 = new_node->addSubchain(c1_name, c1_name, [this] () -> bool {
        return (
          (event.loose_leptons.at(0).get().charge < 0 && event.loose_leptons.at(0).get().isTight)
          || (event.loose_leptons.at(1).get().charge < 0 && event.loose_leptons.at(1).get().isTight)
          ); 
        }, (m_cutMask & Full));
    c1->finishChain(c1_name);

    // e+ exclusive-loose, e- tight
    std::string c2_name = prefix+"_ePL_eMT";
    CutChain::Cut* c2 = new_node->addSubchain(c2_name, c2_name, [this] () -> bool {
        return (
          ((event.loose_leptons.at(0).get().charge < 0 && event.loose_leptons.at(0).get().isTight)
          || (event.loose_leptons.at(1).get().charge < 0 && event.loose_leptons.at(1).get().isTight))
          &&
          ( (event.loose_leptons.at(0).get().charge > 0 && !event.loose_leptons.at(0).get().isTight)
            || (event.loose_leptons.at(1).get().charge > 0 && !event.loose_leptons.at(1).get().isTight) )
          ); 
        }, (m_cutMask & Full));
    c2->finishChain(c2_name);

    // e- inclusive-loose, e+ tight
    std::string c3_name = prefix+"_ePT_eM0";
    CutChain::Cut* c3 = new_node->addSubchain(c3_name, c3_name, [this] () -> bool {
        return (
          (event.loose_leptons.at(0).get().charge > 0 && event.loose_leptons.at(0).get().isTight)
          || (event.loose_leptons.at(1).get().charge > 0 && event.loose_leptons.at(1).get().isTight)
          ); 
        }, (m_cutMask & Full));
    c3->finishChain(c3_name);

    // e- exclusive-loose, e+ tight
    std::string c4_name = prefix+"_ePT_eML";
    CutChain::Cut* c4 = new_node->addSubchain(c4_name, c4_name, [this] () -> bool {
        return (
          ((event.loose_leptons.at(0).get().charge > 0 && event.loose_leptons.at(0).get().isTight)
          || (event.loose_leptons.at(1).get().charge > 0 && event.loose_leptons.at(1).get().isTight))
          &&
          ( (event.loose_leptons.at(0).get().charge < 0 && !event.loose_leptons.at(0).get().isTight)
            || (event.loose_leptons.at(1).get().charge < 0 && !event.loose_leptons.at(1).get().isTight) )
          ); 
        }, (m_cutMask & Full));
    c4->finishChain(c4_name);

  }
  else{
    // same sign regions

    // one tight, one inclusive loose
    std::string c1_name = prefix+"_eT_e0";
    CutChain::Cut* c1 = new_node->addSubchain(c1_name, c1_name, [this] () -> bool {
        return (event.loose_leptons.at(0).get().isTight || event.loose_leptons.at(1).get().isTight); 
        }, (m_cutMask & Full));
    c1->finishChain(c1_name);

    // one tight, the other exclusive loose (only one tight)
    std::string c2_name = prefix+"_eT_eL";
    CutChain::Cut* c2 = new_node->addSubchain(c2_name, c2_name, [this] () -> bool {
        return (event.loose_leptons.at(0).get().isTight != event.loose_leptons.at(1).get().isTight); 
        }, (m_cutMask & Full));
    c2->finishChain(c2_name); 
  }



  
  // Two tight
  std::string c6_name = prefix+"_twoTightLeptons";
  CutChain::Cut* c6 = new_node->addSubchain(c6_name, c6_name, [this] () -> bool {
      return (event.loose_leptons.at(0).get().isTight && event.loose_leptons.at(1).get().isTight); 
      }, (m_cutMask & Full));
  c6->finishChain(c6_name);

}

void ReadTree::makeRealEfficiencyRegions(CutChain::Node* new_node, std::string prefix, bool Full){

  // These regions should be used to make the real efficiency estimates.
  // In principle they need only be applied to data
  // These regions requre same-flavour opposite-sign leptons with an 80 < m_ll < 100 GeV cut
  // They should be split into TT, TO, OT 
  // The isPrompt and isTruthChargeMatched should both be 1 for data. 
  
  double m_Z = 91.1876; 

  CutChain::Cut* zpeak = new_node->addSubchain("zpeak", "zpeak", [this, m_Z] () -> bool { return (event.mll> m_Z-5 && event.mll< m_Z+5); }, (m_cutMask & Full) );
  CutChain::Node* zpeak_node = zpeak->splitChain();

  ////////////////////////////////////
  // (Will's) Real efficiency regions
  ////////////////////////////////////

  // l+ tight, l- tight
  std::string c1_name = prefix+"_PTight_MTight";
  CutChain::Cut* c1 = zpeak_node->addSubchain(c1_name, c1_name, [this] () -> bool {
      return ( event.loose_leptons.at(0).get().isTight && event.loose_leptons.at(1).get().isTight);
      }, (m_cutMask & Full) );

  // l+ tight, l- loose
  std::string c2_name = prefix+"_PTight_MLoose";
  CutChain::Cut* c2 = zpeak_node->addSubchain(c2_name, c2_name, [this] () -> bool {
      return (  
          (event.loose_leptons.at(0).get().charge > 0 && event.loose_leptons.at(0).get().isTight) 
          || (event.loose_leptons.at(1).get().charge > 0 && event.loose_leptons.at(1).get().isTight) 
          ) ;
      }, (m_cutMask & Full) );

  // l+ loose, l- tight
  std::string c3_name = prefix+"_PLoose_MTight";
  CutChain::Cut* c3 = zpeak_node->addSubchain(c3_name, c3_name, [this] () -> bool {
      return (  
          (event.loose_leptons.at(0).get().charge < 0 && event.loose_leptons.at(0).get().isTight) 
          || (event.loose_leptons.at(1).get().charge < 0 && event.loose_leptons.at(1).get().isTight) 
          ) ;
      }, (m_cutMask & Full) );

  c1->finishChain(c1_name);
  c2->finishChain(c2_name);
  c3->finishChain(c3_name);

  ////////////////////////////////////
  // (Holly's) Real efficiency regions
  ////////////////////////////////////
//  std::string c5_name = prefix+"_Z_noFakesnoCF_TightMatched_Loose";
//  CutChain::Cut* c5 = zpeak_node->addSubchain(c5_name, c5_name, [this] () -> bool { 
//      return (event.loose_leptons.at(0).get().isTight==1) 
//      && (event.loose_leptons.at(0).get().isTriggerMatched==1) 
//      && (event.loose_leptons.at(0).get().isPrompt==1) 
//      && (event.loose_leptons.at(1).get().isPrompt==1) 
//      && (event.loose_leptons.at(0).get().isTruthChargeMatched) 
//      && (event.loose_leptons.at(1).get().isTruthChargeMatched); 
//      } );
//
//  std::string c6_name = prefix+"_Z_noFakesnoCF_Loose_TightMatched";
//  CutChain::Cut* c6 = zpeak_node->addSubchain(c6_name, c6_name, [this] () -> bool { 
//      return (event.loose_leptons.at(1).get().isTight==1) 
//      && (event.loose_leptons.at(1).get().isTriggerMatched==1) 
//      && (event.loose_leptons.at(0).get().isPrompt==1) 
//      && (event.loose_leptons.at(1).get().isPrompt==1) 
//      && (event.loose_leptons.at(0).get().isTruthChargeMatched) 
//      && (event.loose_leptons.at(1).get().isTruthChargeMatched); 
//      } );
//
//  std::string c7_name = prefix+"_Z_noFakesnoCF_TightMatched_Tight";
//  CutChain::Cut* c7 = zpeak_node->addSubchain(c7_name, c7_name, [this] () -> bool { 
//      return (event.loose_leptons.at(0).get().isTight==1)
//      && (event.loose_leptons.at(0).get().isTriggerMatched==1) 
//      && (event.loose_leptons.at(1).get().isTight==1) 
//      && (event.loose_leptons.at(0).get().isPrompt==1) 
//      && (event.loose_leptons.at(1).get().isPrompt==1) 
//      && (event.loose_leptons.at(0).get().isTruthChargeMatched) 
//      && (event.loose_leptons.at(1).get().isTruthChargeMatched);
//      } );
//
//  std::string c8_name = prefix+"_Z_noFakesnoCF_Tight_TightMatched";
//  CutChain::Cut* c8 = zpeak_node->addSubchain(c8_name, c8_name, [this] () -> bool { 
//      return (event.loose_leptons.at(0).get().isTight==1) 
//      && (event.loose_leptons.at(1).get().isTriggerMatched==1) 
//      && (event.loose_leptons.at(1).get().isTight==1) 
//      && (event.loose_leptons.at(0).get().isPrompt==1) 
//      && (event.loose_leptons.at(1).get().isPrompt==1) 
//      && (event.loose_leptons.at(0).get().isTruthChargeMatched) 
//      && (event.loose_leptons.at(1).get().isTruthChargeMatched);
//      } );
//
//  
//
//  c5->finishChain(c5_name);
//  c6->finishChain(c6_name);
//  c7->finishChain(c7_name);
//  c8->finishChain(c8_name);

}
