#include "ReadTree/EventStruct.h"


// some new test code to implement the fake estimate: FakeBkgTool 


void EventStruct::connect(TTree* tree)
{


  std::cout << "Connecting the TTree" << std::endl;


  // Note that if this string *isn't* MC or Data then the old sqrt{pT} definition of resolution will be used.
  /********************
   *
   * WJF 2019: currently in common tree
   *
  tree->SetBranchStatus  ( "RunNumber"        , 1                 ) ;
  tree->SetBranchAddress ( "RunNumber"        , &RunNumber        ) ;
  tree->SetBranchStatus  ( "LumiBlock",         1                 ) ;
  tree->SetBranchAddress ( "LumiBlock",         &LumiBlock        ) ;
  tree->SetBranchStatus  ( "pileup",            1                 ) ;
  tree->SetBranchAddress ( "pileup",            &pileup           ) ;
  tree->SetBranchStatus  ("bcid",               1                 ) ;
  tree->SetBranchAddress ( "bcid",              &BCID           ) ;
  ******************************/

  tree->SetBranchStatus  ( "EventNumber",       1                 ) ;
  tree->SetBranchAddress ( "EventNumber",       &EventNumber      ) ;

  if (m_isMC) {
    tree->SetBranchStatus  ("mc_isr_mur05muf05_weight",	1				) ;
    tree->SetBranchAddress ("mc_isr_mur05muf05_weight",	&mc_isr_mur05muf05_weight	) ;
    tree->SetBranchStatus  ("mc_isr_mur20muf20_weight",	1				) ;
    tree->SetBranchAddress ("mc_isr_mur20muf20_weight",	&mc_isr_mur20muf20_weight	) ;
    tree->SetBranchStatus  ("mc_isr_var3cup_weight",	1				) ;
    tree->SetBranchAddress ("mc_isr_var3cup_weight",	&mc_isr_var3cup_weight		) ;
    tree->SetBranchStatus  ("mc_isr_var3cdown_weight",	1				) ;
    tree->SetBranchAddress ("mc_isr_var3cdown_weight",	&mc_isr_var3cdown_weight	) ;
    tree->SetBranchStatus  ( "mc_scale_weights",		1				) ;
    tree->SetBranchAddress ( "mc_scale_weights",		&mc_scale_weights			) ;
    tree->SetBranchStatus  ( "mc_pdf_weights",		1				) ;
    tree->SetBranchAddress ( "mc_pdf_weights",		&mc_pdf_weights			) ;
    tree->SetBranchStatus  ( "mc_event_weight",		1				) ;
    tree->SetBranchAddress ( "mc_event_weight",		&mc_event_weight		) ;
    tree->SetBranchStatus  ( "GenFiltHT",		1				) ;
    tree->SetBranchAddress ( "GenFiltHT",		&GenFiltHT			) ;
    tree->SetBranchStatus  ( "GenFiltMET",		1				) ;
    tree->SetBranchAddress ( "GenFiltMET",		&GenFiltMET			) ;
    tree->SetBranchStatus  ( "pileup_weight",		1				) ;
    tree->SetBranchAddress ( "pileup_weight",		&m_pileup_weight		) ;

    tree->SetBranchStatus("triggerGlobalEfficiencySF", 1);
    tree->SetBranchAddress("triggerGlobalEfficiencySF", &triggerGlobalEfficiencySF );
    tree->SetBranchStatus("triggerGlobalEfficiency", 1);
    tree->SetBranchAddress("triggerGlobalEfficiency", &triggerGlobalEfficiency );

  }

  // Electrons 
  tree->SetBranchStatus  ( "elePt",                      1                   ) ;
  tree->SetBranchAddress ( "elePt",                      &elePt              ) ;
  tree->SetBranchStatus  ( "eleEta",                     1                   ) ;
  tree->SetBranchAddress ( "eleEta",                     &eleEta             ) ;
  tree->SetBranchStatus  ( "elePhi",                     1                   ) ;
  tree->SetBranchAddress ( "elePhi",                     &elePhi             ) ;
  tree->SetBranchStatus  ( "eleE",                       1                   ) ;
  tree->SetBranchAddress ( "eleE",                       &eleE               ) ;
  tree->SetBranchStatus  ( "eleCharge",                  1                   ) ;
  tree->SetBranchAddress ( "eleCharge",                  &eleCharge          ) ;
  tree->SetBranchStatus  ( "elePassCFT",                 1                   ) ;
  tree->SetBranchAddress ( "elePassCFT",                 &elePassCFT      ) ;
  tree->SetBranchStatus  ( "eleIsIsolated",              1                   ) ;
  tree->SetBranchAddress ( "eleIsIsolated",              &eleIsIsolated      ) ;
  tree->SetBranchStatus  ( "eleIsGradient",              1                   ) ;
  tree->SetBranchAddress ( "eleIsGradient",              &eleIsGradient      ) ;
  tree->SetBranchStatus  ( "eleIsFCTight",               1                   ) ;
  tree->SetBranchAddress ( "eleIsFCTight",               &eleIsFCTight      ) ;
  tree->SetBranchStatus  ( "eleIsFCLoose",               1                   ) ;
  tree->SetBranchAddress ( "eleIsFCLoose",               &eleIsFCLoose      ) ;
  tree->SetBranchStatus  ( "eleIsFCHighPtCaloOnly",              1                   ) ;
  tree->SetBranchAddress ( "eleIsFCHighPtCaloOnly",              &eleIsFCHighPtCaloOnly      ) ;
  tree->SetBranchStatus  ( "eleIsTighterNearlySignal",   1                   ) ;
  tree->SetBranchAddress ( "eleIsTighterNearlySignal",   &eleIsTighterNearlySignal          ) ;
  tree->SetBranchStatus  ( "eleIsInclusiveLooserNearlySignal", 1                   ) ;
  tree->SetBranchAddress ( "eleIsInclusiveLooserNearlySignal", &eleIsInclusiveLooserNearlySignal          ) ;
  // Old trigger matching
  tree->SetBranchStatus  ( "eleIsSingleTriggerMatched",  1                   ) ; // old
  tree->SetBranchAddress ( "eleIsSingleTriggerMatched",  &eleIsSingleTriggerMatched          ) ; // old


  // new trigger matching
  tree->SetBranchStatus ( "eleIsDiElectronTriggerMatched", 1);
  tree->SetBranchAddress ( "eleIsDiElectronTriggerMatched", &eleIsDiElectronTriggerMatched);
  tree->SetBranchStatus ( "eleIsEMuTriggerMatched", 1);
  tree->SetBranchAddress ( "eleIsEMuTriggerMatched", &eleIsEMuTriggerMatched);

  // track parameters 
  tree->SetBranchStatus ("eled0sig", 1);
  tree->SetBranchAddress ( "eled0sig", &eled0sig);
  tree->SetBranchStatus ("elez0sinTheta", 1);
  tree->SetBranchAddress ( "elez0sinTheta", &elez0sinTheta);



  if(m_isMC){
    //tree->SetBranchStatus  ( "eleIsPrompt",           1                   ) ;
    //tree->SetBranchAddress ( "eleIsPrompt",           &eleIsPrompt             ) ;
    tree->SetBranchStatus  ( "eleSF",                 1                      ) ;
    tree->SetBranchAddress ( "eleSF",                 &eleScaleFactor        ) ;
    tree->SetBranchStatus  ( "eleTruthCharge",        1 ) ;
    tree->SetBranchAddress ( "eleTruthCharge",        &eleTruthCharge );
    tree->SetBranchStatus  ( "eleTruthType",          1 ) ;
    tree->SetBranchAddress ( "eleTruthType",          &eleTruthType );
    tree->SetBranchStatus  ( "eleTruthOrigin",        1 ) ;
    tree->SetBranchAddress ( "eleTruthOrigin",        &eleTruthOrigin );
    tree->SetBranchStatus  ( "eleChargeIDSF",         1 ) ;
    tree->SetBranchAddress ( "eleChargeIDSF",         &eleChargeIDSF );
    tree->SetBranchStatus  ( "eleChargeFlipTaggerSF", 1 ) ;
    tree->SetBranchAddress ( "eleChargeFlipTaggerSF", &eleChargeFlipTaggerSF );
    //tree->SetBranchStatus  ( "eleTrigSF",             1 ) ;
    //tree->SetBranchAddress ( "eleTrigSF",             &eleTrigSF );
    tree->SetBranchStatus  ( "eleLooseSF",            1 ) ;
    tree->SetBranchAddress ( "eleLooseSF",            &eleLooseSF );
    tree->SetBranchStatus  ( "eleIsoSF",              1 ) ;
    tree->SetBranchAddress ( "eleIsoSF",              &eleIsoSF );
    // New 
    tree->SetBranchStatus  ( "eleIFFTruthClassification", 1 ) ;
    tree->SetBranchAddress ( "eleIFFTruthClassification", &eleIFFTruthClassification );
    


  }


  // Muons
  tree->SetBranchStatus  ( "muPt",                      1               ) ;
  tree->SetBranchAddress ( "muPt",                      &muPt           ) ;
  tree->SetBranchStatus  ( "muEta",                     1               ) ;
  tree->SetBranchAddress ( "muEta",                     &muEta          ) ;
  tree->SetBranchStatus  ( "muPhi",                     1               ) ;
  tree->SetBranchAddress ( "muPhi",                     &muPhi          ) ;
  tree->SetBranchStatus  ( "muE",                       1               ) ;
  tree->SetBranchAddress ( "muE",                       &muE       ) ;
  tree->SetBranchStatus  ( "muCharge",                  1               ) ;
  tree->SetBranchAddress ( "muCharge",                  &muCharge       ) ;
  tree->SetBranchStatus  ( "muIsIsolated",              1               ) ;
  tree->SetBranchAddress ( "muIsIsolated",              &muIsIsolated ) ;
  tree->SetBranchStatus  ( "muIsFCLoose",              1               ) ;
  tree->SetBranchAddress ( "muIsFCLoose",              &muIsFCLoose ) ;
  tree->SetBranchStatus  ( "muIsHighPtTrackOnly",               1                   ) ;
  tree->SetBranchAddress ( "muIsHighPtTrackOnly",               &muIsHighPtTrackOnly      ) ;
  tree->SetBranchStatus  ( "muIsFCTight",               1                   ) ;
  tree->SetBranchAddress ( "muIsFCTight",               &muIsFCTight      ) ;
  tree->SetBranchStatus  ( "muIsTight_VarRad",               1                   ) ;
  tree->SetBranchAddress ( "muIsTight_VarRad",               &muIsTight_VarRad      ) ;
  tree->SetBranchStatus  ( "muIsSignal",   1               ) ;
  tree->SetBranchAddress ( "muIsSignal",   &muIsSignal   ) ;

  // old trigger matching
  tree->SetBranchStatus  ( "muIsSingleTriggerMatched",  1               ) ;
  tree->SetBranchAddress ( "muIsSingleTriggerMatched",  &muIsSingleTriggerMatched ) ;


  // new trigger matching
  tree->SetBranchStatus ( "muIsDiMuonTriggerMatched", 1);
  tree->SetBranchAddress ( "muIsDiMuonTriggerMatched", &muIsDiMuonTriggerMatched);
  tree->SetBranchStatus ( "muIsEMuTriggerMatched", 1);
  tree->SetBranchAddress ( "muIsEMuTriggerMatched", &muIsEMuTriggerMatched);

  // Track parameters 
  tree->SetBranchStatus ("mud0sig", 1);
  tree->SetBranchAddress ( "mud0sig", &mud0sig);
  tree->SetBranchStatus ("muz0sinTheta", 1);
  tree->SetBranchAddress ( "muz0sinTheta", &muz0sinTheta);

  if (m_isMC) {
    //tree->SetBranchStatus  ( "muIsPrompt",           1                   ) ;
    //tree->SetBranchAddress ( "muIsPrompt",           &muIsPrompt             ) ;
    tree->SetBranchStatus  ( "muSF"        , 1                     ) ;
    tree->SetBranchAddress ( "muSF"        , &muScaleFactor        ) ;
    tree->SetBranchStatus  ( "muIsoSF", 1 ) ;
    tree->SetBranchAddress ( "muIsoSF", &muIsoSF );
    tree->SetBranchStatus  ( "muTruthCharge", 1 ) ;
    tree->SetBranchAddress ( "muTruthCharge", &muTruthCharge );
    tree->SetBranchStatus  ( "muTruthType", 1 ) ;
    tree->SetBranchAddress ( "muTruthType", &muTruthType );
    tree->SetBranchStatus  ( "muTruthOrigin", 1 ) ;
    tree->SetBranchAddress ( "muTruthOrigin", &muTruthOrigin );
    tree->SetBranchStatus  ( "muIFFTruthClassification", 1 ) ;
    tree->SetBranchAddress ( "muIFFTruthClassification", &muIFFTruthClassification );

  }

  // Jets
  tree->SetBranchStatus  ( "jetPt",        1                ) ;
  tree->SetBranchAddress ( "jetPt",        &jetPt           ) ;
  tree->SetBranchStatus  ( "jetEta",       1                ) ;
  tree->SetBranchAddress ( "jetEta",       &jetEta          ) ;
  tree->SetBranchStatus  ( "jetPhi",       1                ) ;
  tree->SetBranchAddress ( "jetPhi",       &jetPhi          ) ;
  tree->SetBranchStatus  ( "jetMass",      1                ) ;
  tree->SetBranchAddress ( "jetMass",      &jetMass         ) ;
  tree->SetBranchStatus  ( "jetIsSignal",  1                ) ;
  tree->SetBranchAddress ( "jetIsSignal",  &jetIsSignal     ) ;

  /****************************************
  if (!m_isTruth) {
    if (m_isMC) {
      tree->SetBranchStatus  ( "jetBTagSF"          , 1                      ) ;
      tree->SetBranchAddress ( "jetBTagSF"          , &jetBTagSF          ) ;
      tree->SetBranchAddress ( "totalBTagSF"        , &totalBTagSF        ) ;
      tree->SetBranchStatus  ( "totalBTagSF"        , 1                      ) ;
      tree->SetBranchStatus("jetJVTSF", 1);
      tree->SetBranchAddress ("jetJVTSF"           , &JVTSF );
      tree->SetBranchStatus("totalJVTSF", 1);
      tree->SetBranchAddress("totalJVTSF", &totalJVTSF);
      tree->SetBranchStatus("totalJetSF", 1);
      tree->SetBranchAddress("totalJetSF", &totalJetSF);
      tree->SetBranchStatus  ( "jetBTagSF"          , 1                   ) ;
      tree->SetBranchAddress ( "jetBTagSF"          , &jetBTagSF          ) ;
      tree->SetBranchStatus  ("jetJVTSF"            , 1                   );
      tree->SetBranchAddress ("jetJVTSF"            , &JVTSF              );
    }
    tree->SetBranchStatus  ( "jetEMFrac"            , 1                     ) ;
    tree->SetBranchAddress ( "jetEMFrac"            , &jetEMFrac            ) ;
    tree->SetBranchStatus  ( "jetChFrac"            , 1                     ) ;
    tree->SetBranchAddress ( "jetChFrac"            , &jetChFrac            ) ;
    tree->SetBranchStatus  ( "JVT"                  , 1                     ) ;
    tree->SetBranchAddress ( "JVT"                  , &JVT                  ) ;
  }
  ********************iiiiiiiiiii***/

  /********************
  if (tree->GetListOfBranches()->FindObject(resolutionName.c_str() ) ) {
    Jet::hasResolution = true;
    tree->SetBranchStatus(resolutionName.c_str(), 1);
    tree->SetBranchAddress(resolutionName.c_str(), &jetResolution);
    std::cout << "Jets have resolution!" << std::endl;
  }
  else std::cout << "No jet resolution branch! Using sqrt instead" << std::endl;
  *****************/




  // MET (TST)
  tree->SetBranchStatus  ( "EtMiss"       , 1             ) ;
  tree->SetBranchAddress ( "EtMiss"       , &EtMiss       ) ;
  tree->SetBranchStatus  ( "EtMissSignificance"       , 1             ) ;
  tree->SetBranchAddress ( "EtMissSignificance"       , &EtMissSignificance       ) ;
  tree->SetBranchStatus  ( "EtMissPhi"    , 1             ) ;
  tree->SetBranchAddress ( "EtMissPhi"    , &EtMissPhi    ) ;

  // Induvidual (TST) MET terms 
//  tree->SetBranchStatus  ( "EtMissEle"         , 1                  ) ;
//  tree->SetBranchAddress ( "EtMissEle"         , &EtMissEle         ) ;
//  tree->SetBranchStatus  ( "EtMissElePhi"      , 1                  ) ;
//  tree->SetBranchAddress ( "EtMissElePhi"      , &EtMissElePhi      ) ;
//  tree->SetBranchStatus  ( "EtMissMu"          , 1                  ) ;
//  tree->SetBranchAddress ( "EtMissMu"          , &EtMissMuon          ) ;
//  tree->SetBranchStatus  ( "EtMissMuPhi"       , 1                  ) ;
//  tree->SetBranchAddress ( "EtMissMuPhi"       , &EtMissMuonPhi       ) ;
//  tree->SetBranchStatus  ( "EtMissJet"         , 1                  ) ;
//  tree->SetBranchAddress ( "EtMissJet"         , &EtMissJet         ) ;
//  tree->SetBranchStatus  ( "EtMissJetPhi"      , 1                  ) ;
//  tree->SetBranchAddress ( "EtMissJetPhi"      , &EtMissJetPhi      ) ;

  // MET (using the CST) 
//  tree->SetBranchStatus  ( "EtMissCST"    , 1                  ) ;
//  tree->SetBranchAddress ( "EtMissCST"    , &EtMiss_withCST    ) ;
//  tree->SetBranchStatus  ( "EtMissCSTPhi" , 1                  ) ;
//  tree->SetBranchAddress ( "EtMissCSTPhi" , &EtMissPhi_withCST ) ;


}

std::map<std::string, double> EventStruct::CalculateTtbarWeight(double eventval, int isemuM){

  std::map<std::string, double> weights;
  if (eventval>400.0){
    eventval = 380.0;
  }
  int eventvalbin = m_ttbarWeightHists[0+isemuM]->GetXaxis()->FindBin(eventval);
  weights["Nominal"] = m_ttbarWeightHists[0+isemuM]->GetBinContent(eventvalbin);
  weights["ttbar_WeightClosure"] = m_ttbarWeightHists[2+isemuM]->GetBinContent(eventvalbin);
  return weights;

}


std::map<std::string, double> EventStruct::CalculateTtbarWeight2d(double eventvalx, double eventvaly, int isemuM){

  std::map<std::string, double> weights;
//  if (eventvalx>300.0){
//    eventvalx = 299.0;
//  }
//  if (eventvaly>275.0){
//    eventvaly = 274.0;
//  }
//  if (eventvalx<25.0){
//    eventvalx = 26.0;
//  }
//  if (eventvaly<50.0){
//    eventvaly = 51.0;
//  }
  if (eventvalx>290.0){
    eventvalx = 290.0;
  }
  if (eventvaly>305.0){
    eventvaly = 305.0;
  }
  if (eventvalx<30.0){
    eventvalx = 30.0;
  }
  if (eventvaly<30.0){
    eventvaly = 30.0;
  }
//  int eventvalxbin = m_ttbarWeightHists2D[0+isemuM]->GetXaxis()->FindBin(eventvalx);
//  int eventvalybin = m_ttbarWeightHists2D[0+isemuM]->GetYaxis()->FindBin(eventvaly);
//  weights["Nominal"] = m_ttbarWeightHists2D[0+isemuM]->GetBinContent(eventvalxbin, eventvalybin);
//  weights["ttbar_WeightClosure"] = m_ttbarWeightHists2D[2+isemuM]->GetBinContent(eventvalxbin,eventvalybin);
  weights["Nominal"] = m_ttbarWeightGraphs2D[0+isemuM]->Interpolate(eventvalx,eventvaly);
  weights["ttbar_WeightClosure"] = m_ttbarWeightGraphs2D[2+isemuM]->Interpolate(eventvalx,eventvaly);
  return weights;

}


void EventStruct::InitialisettbarWeight2D(std::string histo_path){

  std::cout << "EventStruct::InitialisettbarWeight2D: About to initialise ttbar weights" << std::endl;

  TFile* input_file = TFile::Open(histo_path.c_str(), "READ");
//  TH2D* ttbar_weight_hist_emuP = (TH2D*)input_file->Get("weight_emuP")->Clone("ttbarWeights_emuP"); 
//  ttbar_weight_hist_emuP->SetDirectory(0);
//  TH2D* ttbar_relunc_hist_emuP = (TH2D*)input_file->Get("reluncertainty_emuP")->Clone("ttbarRelUnc_emuP"); 
//  ttbar_relunc_hist_emuP->SetDirectory(0);
//  TH2D* ttbar_weight_hist_emuM = (TH2D*)input_file->Get("weight_emuM")->Clone("ttbarWeights_emuM"); 
//  ttbar_weight_hist_emuM->SetDirectory(0);
//  TH2D* ttbar_relunc_hist_emuM = (TH2D*)input_file->Get("reluncertainty_emuM")->Clone("ttbarRelUnc_emuM"); 
//  ttbar_relunc_hist_emuM->SetDirectory(0);
//  m_ttbarWeightHists2D.push_back(ttbar_weight_hist_emuP);
//  m_ttbarWeightHists2D.push_back(ttbar_weight_hist_emuM);
//  m_ttbarWeightHists2D.push_back(ttbar_relunc_hist_emuP);
//  m_ttbarWeightHists2D.push_back(ttbar_relunc_hist_emuM);
  TGraph2D* ttbar_weight_hist_emuP = (TGraph2D*)input_file->Get("weightgraph_emuP")->Clone("ttbarWeights_emuP"); 
  ttbar_weight_hist_emuP->SetDirectory(0);
  TGraph2D* ttbar_relunc_hist_emuP = (TGraph2D*)input_file->Get("reluncertaintygraph_emuP")->Clone("ttbarRelUnc_emuP"); 
  ttbar_relunc_hist_emuP->SetDirectory(0);
  TGraph2D* ttbar_weight_hist_emuM = (TGraph2D*)input_file->Get("weightgraph_emuM")->Clone("ttbarWeights_emuM"); 
  ttbar_weight_hist_emuM->SetDirectory(0);
  TGraph2D* ttbar_relunc_hist_emuM = (TGraph2D*)input_file->Get("reluncertaintygraph_emuM")->Clone("ttbarRelUnc_emuM"); 
  ttbar_relunc_hist_emuM->SetDirectory(0);
  m_ttbarWeightGraphs2D.push_back(ttbar_weight_hist_emuP);
  m_ttbarWeightGraphs2D.push_back(ttbar_weight_hist_emuM);
  m_ttbarWeightGraphs2D.push_back(ttbar_relunc_hist_emuP);
  m_ttbarWeightGraphs2D.push_back(ttbar_relunc_hist_emuM);
  input_file->Close();

}

void EventStruct::InitialisettbarWeight(std::string histo_path){

  std::cout << "EventStruct::InitialisettbarWeight: About to initialise ttbar weights" << std::endl;

  TFile* input_file = TFile::Open(histo_path.c_str(), "READ");
  TH1D* ttbar_weight_hist_emuP = (TH1D*)input_file->Get("weight_emuP")->Clone("ttbarWeights_emuP"); 
  ttbar_weight_hist_emuP->SetDirectory(0);
  TH1D* ttbar_relunc_hist_emuP = (TH1D*)input_file->Get("reluncertainty_emuP")->Clone("ttbarRelUnc_emuP"); 
  ttbar_relunc_hist_emuP->SetDirectory(0);
  TH1D* ttbar_weight_hist_emuM = (TH1D*)input_file->Get("weight_emuM")->Clone("ttbarWeights_emuM"); 
  ttbar_weight_hist_emuM->SetDirectory(0);
  TH1D* ttbar_relunc_hist_emuM = (TH1D*)input_file->Get("reluncertainty_emuM")->Clone("ttbarRelUnc_emuM"); 
  ttbar_relunc_hist_emuM->SetDirectory(0);
  m_ttbarWeightHists.push_back(ttbar_weight_hist_emuP);
  m_ttbarWeightHists.push_back(ttbar_weight_hist_emuM);
  m_ttbarWeightHists.push_back(ttbar_relunc_hist_emuP);
  m_ttbarWeightHists.push_back(ttbar_relunc_hist_emuM);
  input_file->Close();

}

void EventStruct::initializeFakeWeightHistos(std::string histo_path){

  std::cout << "About to initialize FakeWeightGetter" << std::endl;
  m_fake_weight_getter.initialize(histo_path);
  std::cout << "Initialiszation of FakeWeightGetter successful" << std::endl;
  //m_fake_weight_getter.print_histograms();

  //if(debug) m_fake_weight_getter.enable_debug(); 

}

std::map<std::string, double> EventStruct::CalculateMuonBiasTest(std::string type){

  std::map<std::string, double> totalweights;
  totalweights["nominal"] = 1.0;
  totalweights["etaphibias_up"] = 1.0;
  totalweights["etaphibias_down"] = 1.0;
  totalweights["ptscale_up"] = 1.0;
  totalweights["ptscale_down"] = 1.0;

  if(debug) std::cout << "About to test muon bias tool" << std::endl;

  // only want to run this if there are exactly 2 loose leptons (WJF: really only LOOSE?) 
//  if(loose_leptons.size() == 2){ // well this is bullshit, it's applying the weight to electrons too?!?!

  for(auto lep : loose_muons){
    if(debug) std::cout << "\n*** about to calculate weights ***" << std::endl;
    std::map<std::string, double> weights;
    if(type=="Reco"){
      weights = m_muon_reco_bias_correction_tool.get_weights(lep.Eta, lep.Phi, lep.Pt, lep.charge);
    } 
    else { 
      weights = m_muon_trigger_bias_correction_tool.get_weights(lep.Eta, lep.Phi, lep.Pt, lep.charge);
    }

    if(debug){ 
      std::cout << "For muon at eta " << lep.Eta << " phi " << lep.Phi << " with pT " << lep.Pt << " GeV and charge " << lep.charge; // << std::endl;
      std::cout << " returned eta-phi efficiency " << weights["eff"] << " pm " << weights["eff_err"] << ", "
        << "weight of " << weights["nominal"]  
        << " etaphi Up: " << weights["etaphibias_up"] 
        << " etaphi Down: " << weights["etaphibias_down"]
        << " ptscale Up: " << weights["ptscale_up"]
        << " ptscale Down: " << weights["ptscale_down"]
        << std::endl;
    }
    totalweights["nominal"]         *= weights["nominal"];
    totalweights["etaphibias_up"]   *= weights["etaphibias_up"];
    totalweights["etaphibias_down"] *= weights["etaphibias_down"];
    totalweights["ptscale_up"]      *= weights["ptscale_up"];
    totalweights["ptscale_down"]    *= weights["ptscale_down"];
      
  }
//  }
//  else{
//    std::cerr << "CalculateMuonBiasTest ERROR: we have !=2 leptons which is very strange indeed" << std::endl;
//    abort();
//  }

  return totalweights;

}





/*****************
void TruthEventStruct::connect(TTree* truthTree) {
  if (!truthTree) return;
  m_connected = true;
  truthTree->SetBranchStatus("elePt", 1);
  truthTree->SetBranchAddress("elePt", &elePt);
  truthTree->SetBranchStatus("eleEta", 1);
  truthTree->SetBranchAddress("eleEta", &eleEta);
  truthTree->SetBranchStatus("elePhi", 1);
  truthTree->SetBranchAddress("elePhi", &elePhi);
  truthTree->SetBranchStatus("eleCharge", 1);
  truthTree->SetBranchAddress("eleCharge", &eleCharge);
  truthTree->SetBranchStatus("elePassOR", 1);
  truthTree->SetBranchAddress("elePassOR", &elePassOR);
  truthTree->SetBranchStatus("isBaselineElectron", 1);
  truthTree->SetBranchAddress("isBaselineElectron", &isBaselineElectron);
  truthTree->SetBranchStatus("isSignalElectron", 1);
  truthTree->SetBranchAddress("isSignalElectron", &isSignalElectron);

  truthTree->SetBranchStatus("muonPt", 1);
  truthTree->SetBranchAddress("muonPt", &muPt);
  truthTree->SetBranchStatus("muonEta", 1);
  truthTree->SetBranchAddress("muonEta", &muEta);
  truthTree->SetBranchStatus("muonPhi", 1);
  truthTree->SetBranchAddress("muonPhi", &muPhi);
  truthTree->SetBranchStatus("muonCharge", 1);
  truthTree->SetBranchAddress("muonCharge", &muCharge);
  truthTree->SetBranchStatus("muonPassOR", 1);
  truthTree->SetBranchAddress("muonPassOR", &muPassOR);
  truthTree->SetBranchStatus("isBaselineMuon", 1);
  truthTree->SetBranchAddress("isBaselineMuon", &isBaselineMuon);
  truthTree->SetBranchStatus("isSignalMuon", 1);
  truthTree->SetBranchAddress("isSignalMuon", &isSignalMuon);

  truthTree->SetBranchStatus("jetPt", 1);
  truthTree->SetBranchAddress("jetPt", &jetPt);
  truthTree->SetBranchStatus("jetEta", 1);
  truthTree->SetBranchAddress("jetEta", &jetEta);
  truthTree->SetBranchStatus("jetPhi", 1);
  truthTree->SetBranchAddress("jetPhi", &jetPhi);
  truthTree->SetBranchStatus("jetMass", 1);
  truthTree->SetBranchAddress("jetMass", &jetMass);
  //truthTree->SetBranchStatus("jetPassOR", 1);
  //truthTree->SetBranchAddress("jetPassOR", &jetPassOR);
  //truthTree->SetBranchStatus("isBaselineJet", 1);
  //truthTree->SetBranchAddress("isBaselineJet", &isBaselineJet);
  truthTree->SetBranchStatus("jetIsSignal", 1);
  truthTree->SetBranchAddress("jetIsSignal", &jetIsSignal);
  //truthTree->SetBranchStatus("isBJet", 1);
  //truthTree->SetBranchAddress("isBJet", &isBJet);

  truthTree->SetBranchStatus("photonPt", 1);
  truthTree->SetBranchAddress("photonPt", &photonPt);
  truthTree->SetBranchStatus("photonEta", 1);
  truthTree->SetBranchAddress("photonEta", &photonEta);
  truthTree->SetBranchStatus("photonPhi", 1);
  truthTree->SetBranchAddress("photonPhi", &photonPhi);
  truthTree->SetBranchStatus("photonPassOR", 1);
  truthTree->SetBranchAddress("photonPassOR", &photonPassOR);
  truthTree->SetBranchStatus("isSignalPhoton", 1);
  truthTree->SetBranchAddress("isSignalPhoton", &isSignalPhoton);

  truthTree->SetBranchStatus("tauPt", 1);
  truthTree->SetBranchAddress("tauPt", &tauPt);
  truthTree->SetBranchStatus("tauEta", 1);
  truthTree->SetBranchAddress("tauEta", &tauEta);
  truthTree->SetBranchStatus("tauPhi", 1);
  truthTree->SetBranchAddress("tauPhi", &tauPhi);

  truthTree->SetBranchStatus("EtMiss", 1);
  truthTree->SetBranchAddress("EtMiss", &EtMiss);
  truthTree->SetBranchStatus("EtMissPhi", 1);
  truthTree->SetBranchAddress("EtMissPhi", &EtMissPhi);
}
**************/
