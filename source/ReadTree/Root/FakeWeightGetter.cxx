#include "ReadTree/FakeWeightGetter.h"

TGraph get_variation_graph(TGraphAsymmErrors* graph, std::string var){
  /*******
   * Creates a new TGraph using the points from either the high error values, or the low error values
   * The string "var" must be either "up" or "down" 
   * ******/

  int npoints = graph->GetN();
  Double_t* yarray = graph->GetY();
  Double_t* yerrors_up = graph->GetEYhigh();
  Double_t* yerrors_down = graph->GetEYlow();

  //std::vector<double> new_yarray;
  Double_t* new_yarray = new Double_t[npoints]; // probably introduces small memory leak. Ok since this function isn't called many times? 
  for(int i=0; i<npoints; ++i){
    if(var == "up"){
      //new_yarray.push_back( yarray[i] + yerrors_up[i]); 
      new_yarray[i] =  yarray[i] + yerrors_up[i];
    }
    else if(var == "down"){
      //new_yarray.push_back(yarray[i] - yerrors_down[i]); 
      new_yarray[i] = yarray[i] - yerrors_down[i];
    }
  }
  return TGraph(npoints, graph->GetX(), new_yarray);

}


std::vector<std::string> splitString(const std::string& inStr, char delim){
  // Split a std::string along a delimiter "delim"
  // Returns a vector of the split string
  std::istringstream ss(inStr);
  std::string placeholder;
  std::vector<std::string> ret;
  while (std::getline(ss, placeholder, delim) ) ret.push_back(placeholder);
  return ret;
}

float FakeWeightGetter::BayesianProbability(const PO::Lepton &lep, std::string type){
  /************
  p(fake | tight) = p(tight | fake) * p(fake) / p(tight) 
  p(tight | fake) is the fake efficiency = 0.1

  // type = "real" or "fake" 
  ************/

  std::string charge;
  if(lep.charge>0){ charge = "plus"; }
  else if (lep.charge<0){ charge = "minus"; }

  std::string variable = "lep1Pt";
  if (lep.flavour == "electron") variable = "ele1Pt";
  if (lep.flavour == "muon") variable = "muon1Pt";
  

  std::string type_name = type+"-probability-"+lep.flavour+"-"+charge+"-"+variable;
  std::string tight_name =  "tight-probability-"+lep.flavour+"-"+charge+"-"+variable;
  float p_type  = m_probability_graphs.at(type_name)->Eval(lep.Pt); // p(real) or p(fake)
  float p_tight = m_probability_graphs.at(tight_name)->Eval(lep.Pt);
  float p_tight_type = get_efficiency(type, lep).eff; // p(tight | fake) or p(tight | real)
  float type_probability = p_tight_type * p_type / p_tight; 
  return type_probability;

}

float FakeWeightGetter::BayesianRealProbability(const PO::Lepton &lep){
  return BayesianProbability(lep, "real");
}

float FakeWeightGetter::BayesianFakeProbability(const PO::Lepton& lep){
  return BayesianProbability(lep, "fake");
}

float FakeWeightGetter::WillIdea2ElectronWeight(const PO::Lepton& lep){
  // m_abcd_graphs 
  std::string charge = "";
  float weight = 1.0;
  if(lep.flavour != "electron"){
    std::cout << "ERROR: electron not supplied" << std::endl;
    return -999;
  }
  if(lep.charge > 0) charge = "plus";
  else charge = "minus";
  std::string graph_name = "electron_"+charge+"_fake_rate";
  weight = m_abcd_graphs[graph_name]->Eval(lep.Pt); 
  return weight;
}

float FakeWeightGetter::WillFakeProb2(const PO::Lepton& lep1, const PO::Lepton& lep2){

  float fake_prob_1 = 0.0;
  float fake_prob_2 = 0.0;
  if(lep1.flavour == "electron"){
    fake_prob_1 = WillIdea2ElectronWeight(lep1);
  }
  if(lep2.flavour == "electron"){
    fake_prob_2 = WillIdea2ElectronWeight(lep2);
  }

  float real_prob_1 = 1 - fake_prob_1;
  float real_prob_2 = 1 - fake_prob_2;
  float prob_both_real = real_prob_1 * real_prob_2;
  float prob_not_both_real = 1 - prob_both_real;
  return prob_not_both_real; 

}

void FakeWeightGetter::load_abcd_graphs(){
  TFile* input_file = TFile::Open("/usera/wfawcett/cambridge/emus/OSDFChargeFlavourAsymmCode/fake_estimate/tag_and_probe_method/new_fake_probabilities.root", "READ");
  TIter keyList(input_file->GetListOfKeys());
  TKey* key;
  while ((key = (TKey*)keyList())) {
    std::string graph_name = key->GetName();
    //std::cout << "key: " << graph_name << std::endl;
    TGraphAsymmErrors* graph = 0;
    input_file->GetObject(TString(graph_name), graph);
    if(!graph){ std::cerr << "WARNING: didn't find the object we were looking for " << graph_name << std::endl; }
    else{ m_abcd_graphs[graph_name] = graph; }
  }
  input_file->Close();
}

void FakeWeightGetter::load_probability_graphs(){
  TFile* input_file = TFile::Open("/usera/wfawcett/cambridge/emus/OSDFChargeFlavourAsymmCode/fake_estimate/tag_and_probe_method/09Sept_onlyPrompt/fake_tight_probabilities.root", "READ");

    // Go around the fucking houses to get the graphs out 
    TIter keyList(input_file->GetListOfKeys());
    TKey* key;
    while ((key = (TKey*)keyList())) {
      std::string graph_name = key->GetName();
      //std::cout << "key: " << graph_name << std::endl;
      // Note the "GetObject" function should check that the TObject is of type TGraphAsymmErrors, otherwise will return a null pointer 
      // What about 2D graphs? 
      TGraphAsymmErrors* graph = 0;
      input_file->GetObject(TString(graph_name), graph);
      if(!graph){
        std::cerr << "WARNING: didn't find the object we were looking for " << graph_name << std::endl;
      }
      else{
        m_probability_graphs[graph_name] = graph; 
        //m_efficiency_graphs_down[graph_name] = get_variation_graph(graph, "down"); // could only store pointer to the object
        //m_efficiency_graphs_up[graph_name] = get_variation_graph(graph, "up");
      }
    }
    input_file->Close();
}


void FakeWeightGetter::load_efficiency_graphs(){

    // Test: /r10/atlas/emus/efficiencies/real_and_fake_efficiencies_11thJuly_NoSubtraction_NEWFORMAT.root
    TFile* input_file = TFile::Open( m_efficiency_file.c_str() );
  
    // Go around the fucking houses to get the graphs out 
    TIter keyList(input_file->GetListOfKeys());
    TKey* key;
    while ((key = (TKey*)keyList())) {
      std::string graph_name = key->GetName();
      //std::cout << "key: " << graph_name << std::endl;

      // Note the "GetObject" function should check that the TObject is of type TGraphAsymmErrors, otherwise will return a null pointer 
      // What about 2D graphs? 
      TGraphAsymmErrors* graph = 0;
      input_file->GetObject(TString(graph_name), graph);
      if(!graph){
        std::cerr << "WARNING: didn't find the object we were looking for " << graph_name << std::endl;
      }
      else{

        // Note setting the kIsSortedX flag was tested, such that the binary search tree would be used in the Eval function, but this had no measureable performace improvement
        //graph->SetBit(TGraph::kIsSortedX); // Don't use this 
        
        m_efficiency_graphs[graph_name] = graph; 
        // not 100% sure I want this, but ok for now
        m_efficiency_graphs_down[graph_name] = get_variation_graph(graph, "down"); // could only store pointer to the object
        m_efficiency_graphs_up[graph_name] = get_variation_graph(graph, "up");
      }
    }

    input_file->Close();
}

float FakeWeightGetter::fake_weight_formula(bool tight1, bool tight2, float r1, float r2, float f1, float f2 ) const{
  /********
   * Calculates the per-event fake weight
   * This forumula is dreived from the matrix method
   * m = F x r
   * m: vector of measured number of leptons passing tight and loose {TT, TL, LT, LL}
   * F: matrix of real and fake efficiencies
   * r: vector of true number of leptons passing tight and loose {rr, rf, fr, ff}
   * Inverting the matrix extracts equations for the true number of real and fake leptons, as a funciton of the efficiencies and the measured leptons. 
   *
   *  >>> Setting the "measured" numbers of leptons to 1 yields an equation for the per-event fake weight. See the internal note for more details. <<<
   *  This is important ... 
   *
   * Note, it is anticipated that the real efficiency is greater than the fake efficiency (r1 > f1, r2 > f2). 
   *************/


  float weight = 1.0; 
  float denominator = (r1-f1)*(r2-f2);
  if(tight1 && tight2){
    //weight = ( f1*f2*(1-r1*r2) - f1*(1-r1)*r2 - f2*r1*(1-r2) ) / denominator; 
    //weight = ( f2*r1*( (1-r2) - r2 ) + f1*(1-r1)*f2*r2 ) /denominator; // INCORRECT !! 
    weight = 1 - r1*r2*(1-f1)*(1-f2)/denominator;
  }
  else if (tight1 && !tight2){
    weight = ((1-f1)*f2*r1*r2) / denominator;
  }
  else if (!tight1 && tight2){
    weight = f1*(1-f2)*r1*r2 / denominator;
  }
  else if (!tight1 && !tight2){
    weight = -f1*f2*r1*r2 / denominator;
  }
  else{
    std::cerr << "WARNING: impossible lepton quality condition" << std::endl;
  }



  if(m_debug) std::cout << "fake_weight_formula: " << weight << std::endl;
  return weight;
}

/**********
float FakeWeightGetter::temp(){

  //float denominator = (r1-f1)*(r2-f2);
  //float n_fakes =  (f2 r1 (-TT + r2 (TL + TT)) -  f1 (r2 (-LT r1 + TT - r1 TT) +  f2 (LL r1 r2 + LT r1 r2 + r1 r2 TL - TT + r1 r2 TT)));
  //n_fakes = n_fakes/denominator;

  return 0.0;
}
***********/

EfficiencyTuple FakeWeightGetter::get_efficiency(std::string classification, const PO::Lepton& lepton /*, std::string variable*/){
  
  //classification = "real" or "fake"
  // At the moment, hard-code lepton 1 pT as the variable to use 
  std::string variable = "lep1Pt"; 
  float value = lepton.Pt; 


  // Create key name for this type of lepton, should be of the form "real-efficiency-electron-plus-variable"
  std::string graph_name = classification + "-efficiency-" + lepton.flavour + "-";
  if(lepton.charge > 0){
    graph_name += "plus";
  }
  else if(lepton.charge < 0){
    graph_name += "minus";
  }

  /*******
  //  Variable now depends on flavour
  if(classification == "real"){
    if(lepton.flavour == "electron"){
      if(lepton.charge > 0){
        variable = "elePlusPt";
      }
      else if(lepton.charge < 0){
        variable = "eleMinusPt";
      }
    }
    if(lepton.flavour == "muon"){
      if(lepton.charge > 0){
        variable = "muonPlusPt";
      }
      else if(lepton.charge < 0){
        variable = "muonMinusPt";
      }
    }
  }
  else if(classification == "fake"){
    if(lepton.flavour == "electron"){
      variable = "ele1Pt";
    }
    else if(lepton.flavour == "muon"){
      variable = "lep1Pt";
    }
  }
  ************/
  graph_name += "-" + variable; 

  // Extract the efficiency from the histogram
  if(m_debug){
    std::cout << "About to extact efficiency from graph " << graph_name << std::endl;
  }
  
  //m_efficiency_graphs.at(graph_name)->Eval(value);
  double efficiency = m_efficiency_graphs.at(graph_name)->Eval(value);
  double efficiency_up = m_efficiency_graphs_up[graph_name].Eval(value);
  double eff_down = m_efficiency_graphs_down[graph_name].Eval(value);

  // Make sure there are no negative efficiencies
  if(efficiency < 0){
    std::cout << "Efficiency < 0!! <<<<<<<<<<<<<" <<  std::endl;
    std::cout << "Efficiency < 0!! <<<<<<<<<<<<<" <<  std::endl;
    std::cout << "Efficiency < 0!! <<<<<<<<<<<<<" <<  std::endl;
    std::cout << "Efficiency < 0!! <<<<<<<<<<<<<" <<  std::endl;
    std::cout << "Efficiency < 0!! <<<<<<<<<<<<<" <<  std::endl;
    efficiency = 0;
  }
  if(efficiency_up < 0) efficiency_up = 0;
  if(eff_down < 0) eff_down = 0;


  if(m_debug){
    std::cout << "efficiency from " << graph_name << ", pT=" << value << " is : " << efficiency << std::endl; 
  }

  EfficiencyTuple tup;
  tup.eff = efficiency;
  tup.eff_up = efficiency_up;
  tup.eff_down = eff_down;
  return tup;
}

void FakeWeightGetter::print_histograms(){
  /*******************
   *
   * Diagnostic function to print the information inside the m_eff_histograms map
   *
   * *************/
  std::cout << "Information retrieved from input fake and real efficiencies" << std::endl;
  for(std::map< std::string, TGraphAsymmErrors* >::iterator iter =   m_efficiency_graphs.begin(); iter != m_efficiency_graphs.end(); ++iter)
  {
    std::string name = iter->first;
    TGraphAsymmErrors* hist = iter->second; 
    std::cout << "name: " << name << std::endl;
    hist->Print();
    std::cout << std::endl;
  }
}

std::map<std::string, float> FakeWeightGetter::get_fake_weight(const PO::Lepton& lep1, const PO::Lepton& lep2) {
  // overloaded as a reminder to think about adding njets 
  return get_fake_weight(lep1, lep2, 0);
}


std::map<std::string, float> FakeWeightGetter::get_fake_weight(const PO::Lepton& lep1, const PO::Lepton& lep2, int njets) {
  /********************
   * Get the fake weights for this event
   * Should only make sense in the case of "e+mu" events
   * Args: 
   *  lep1 and lep2 are the leading and subleading leptons in the event
   *  njets is the number of jets in the event
   *  (these could all be replaced with the EventStruct???) 
   * Returns
   *  std::map<std::string, float> of the weights, and the variations for moving the efficiency up and down
   *  **************************/


  EfficiencyTuple r1 = get_efficiency("real", lep1);
  EfficiencyTuple r2 = get_efficiency("real", lep2);

  EfficiencyTuple f1 = get_efficiency("fake", lep1);
  EfficiencyTuple f2 = get_efficiency("fake", lep2);

  //float weight                             = fake_weight_formula(lep1.isTight, lep2.isTight, r1,      r2,      f1,      f2);
  float weight                             = fake_weight_formula(lep1.isTight, lep2.isTight, r1.eff,      r2.eff,      f1.eff,      f2.eff);
  /**********
  float weight_leading_realup              = fake_weight_formula(lep1.isTight, lep2.isTight, r1.eff_up,   r2.eff,      f1.eff,      f2.eff);
  float weight_subleading_realup           = fake_weight_formula(lep1.isTight, lep2.isTight, r1.eff,      r2.eff_up,   f1.eff,      f2.eff);
  float weight_leading_subleading_realup   = fake_weight_formula(lep1.isTight, lep2.isTight, r1.eff_up,   r2.eff_up,   f1.eff,      f2.eff);
  float weight_leading_realdown            = fake_weight_formula(lep1.isTight, lep2.isTight, r1.eff_down, r2.eff,      f1.eff,      f2.eff);
  float weight_subleading_realdown         = fake_weight_formula(lep1.isTight, lep2.isTight, r1.eff,      r2.eff_down, f1.eff,      f2.eff);
  float weight_leading_subleading_realdown = fake_weight_formula(lep1.isTight, lep2.isTight, r1.eff_down, r2.eff_down, f1.eff,      f2.eff);
  float weight_leading_fakeup              = fake_weight_formula(lep1.isTight, lep2.isTight, r1.eff,      r2.eff,      f1.eff_up,   f2.eff);
  float weight_subleading_fakeup           = fake_weight_formula(lep1.isTight, lep2.isTight, r1.eff,      r2.eff,      f1.eff,      f2.eff_up);
  float weight_leading_subleading_fakeup   = fake_weight_formula(lep1.isTight, lep2.isTight, r1.eff,      r2.eff,      f1.eff_up,   f2.eff_up);
  float weight_leading_fakedown            = fake_weight_formula(lep1.isTight, lep2.isTight, r1.eff,      r2.eff,      f1.eff_down, f2.eff);
  float weight_subleading_fakedown         = fake_weight_formula(lep1.isTight, lep2.isTight, r1.eff,      r2.eff,      f1.eff,      f2.eff_down);
  float weight_leading_subleading_fakedown = fake_weight_formula(lep1.isTight, lep2.isTight, r1.eff,      r2.eff,      f1.eff_down, f2.eff_down);
  *****/



  std::map<std::string, float> weight_map; 
  weight_map["weight"]                             = weight;
  /****************
  weight_map["weight_leading_realup"]              = weight_leading_realup;
  weight_map["weight_subleading_realup"]           = weight_subleading_realup;
  weight_map["weight_leading_subleading_realup"]   = weight_leading_subleading_realup;
  weight_map["weight_leading_realdown"]            = weight_leading_realdown;
  weight_map["weight_subleading_realdown"]         = weight_subleading_realdown;
  weight_map["weight_leading_subleading_realdown"] = weight_leading_subleading_realdown;
  weight_map["weight_leading_fakeup"]              = weight_leading_fakeup;
  weight_map["weight_subleading_fakeup"]           = weight_subleading_fakeup;
  weight_map["weight_leading_subleading_fakeup"]   = weight_leading_subleading_fakeup;
  weight_map["weight_leading_fakedown"]            = weight_leading_fakedown;
  weight_map["weight_subleading_fakedown"]         = weight_subleading_fakedown;
  weight_map["weight_leading_subleading_fakedown"] = weight_leading_subleading_fakedown;
  ***********/

  //std::cout << lep1 << " .. " << lep2 << " : weight = " << weight << std::endl;

  return weight_map;

}
