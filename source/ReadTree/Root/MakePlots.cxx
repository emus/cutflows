#include "ReadTree/ReadTree.h"
#include "CutChain/TH1Wrapper.h"
#include "CutChain/TH2Wrapper.h"
#include "CutChain/TH3Wrapper.h"
#include "CutChain/TEfficiencyWrapper.h"
#include "CutChain/TEfficiency2Wrapper.h"

struct HistBinning {
  unsigned int nbins=0;
  float min=0.;
  float max=0.;

  HistBinning(unsigned int _nbins, float _min, float _max) {
    this->nbins = _nbins;
    this->min = _min;
    this->max = _max;
  };
};


// Define histogram binning in one place
static const float GeV = 1.;
static const float TeV = 1e3;
static const HistBinning HTBinning(50,0,10*TeV);
//

void ReadTree::makePlots(CutChain::Cut* cut, CutChain::CutEnum /*passAlong*/)
{
  using namespace CutChain;
  using namespace HistTags; // includes "Vital" and other tags 
  bool onlyveryimportantplots = true;
  if (onlyveryimportantplots) m_plot_extra_lepton_variables = false;
  //auto MET = MET_Slow | MET_Quick;
  //auto Jets = Jets_Slow | Jets_Quick;
  //

    // Group addition regex:
    //   //   makeTaggedPlot(cut, Vital,\r      TH1Wrapper("&",\r         [] () -> TH1* {return new TH1F("&", "&", 100, 0, 1000); },\r         [this] (TH1Wrapper\& th1) {th1.fill(event.&); } ) );\r
    //

    // ele+ pt-eta plot
  makeTaggedPlot(cut, Vital, 
          TH2Wrapper("elePlusPtEta",
              [] () -> TH2* {return new TH2F("elePlusPtEta", "elePlusPtEta", 200, 0, 2000, 250, -4, 4); },
              [this] (TH2Wrapper& th2) {
              for(auto electron : event.loose_electrons){
                  if(electron.charge == 1) th2.fill(electron.Pt, electron.Eta);
              }
              } ) );

  // ele- pt-eta plot
  makeTaggedPlot(cut, Vital, 
      TH2Wrapper("eleMinusPtEta",
        [] () -> TH2* {return new TH2F("eleMinusPtEta", "eleMinusPtEta", 200, 0, 2000, 250, -4, 4); },
        [this] (TH2Wrapper& th2) {
              for(auto electron : event.loose_electrons){
                  if(electron.charge == -1) th2.fill(electron.Pt, electron.Eta);
              }
        } ) );

  
  // mu+ pt-eta plot
  makeTaggedPlot(cut, Vital, 
      TH2Wrapper("muPlusPtEta",
        [] () -> TH2* {return new TH2F("muPlusPtEta", "muPlusPtEta", 200, 0, 2000, 250, -4, 4); },
        [this] (TH2Wrapper& th2) {
              for(auto muon : event.loose_muons){
                  if(muon.charge == 1) th2.fill(muon.Pt, muon.Eta);
              }
        } ) );

  // mu- pt-eta plot
  makeTaggedPlot(cut, Vital, 
      TH2Wrapper("muMinusPtEta",
        [] () -> TH2* {return new TH2F("muMinusPtEta", "muMinusPtEta", 200, 0, 2000, 250, -4, 4); },
        [this] (TH2Wrapper& th2) {
              for(auto muon : event.loose_muons){
                  if(muon.charge == -1) th2.fill(muon.Pt, muon.Eta);
              }
        } ) );


    makeTaggedPlot(cut, Vital,
        TH2Wrapper("sumMtMET_ele1Pt", 
          [] () -> TH2* {return new TH2F("sumMtMET_ele1Pt", "sumMtMET_ele1Pt;sumMtMET;ele1Pt", 100, 0, 2000, 100, 0, 1000); },
          [this] (TH2Wrapper& th2) {
          if (event.loose_electrons.size() > 0) { th2.fill(event.sum_mt_met, event.loose_electrons.at(0).Pt);}
          } ) );

  //////////////////////
  // Sagitta bias  studies  
  //////////////////////

//  makeTaggedPlot(cut, Vital, 
//      TH2Wrapper("muPlusPtPhi",
//        [] () -> TH2* {return new TH2F("muPlusPtPhi", "muPlusPtPhi", 200, 0, 2000, 128, -3.2, 3.2); },
//        [this] (TH2Wrapper& th2) {
//        if (event.loose_muons.size() == 2){ 
//          if(event.loose_muons.at(0).charge == 1) th2.fill(event.loose_muons.at(0).Pt, event.loose_muons.at(0).Phi);
//          if(event.loose_muons.at(1).charge == 1) th2.fill(event.loose_muons.at(1).Pt, event.loose_muons.at(1).Phi);
//        }
//        } ) );
//
//  // mu- eta-phi plot
//  makeTaggedPlot(cut, Vital, 
//      TH2Wrapper("muMinusPtPhi",
//        [] () -> TH2* {return new TH2F("muMinusPtPhi", "muMinusPtPhi", 200, 0, 2000, 128, -3.2, 3.2); },
//        [this] (TH2Wrapper& th2) {
//        if (event.loose_muons.size() == 2){ 
//          if(event.loose_muons.at(0).charge == -1) th2.fill(event.loose_muons.at(0).Pt, event.loose_muons.at(0).Phi);
//          if(event.loose_muons.at(1).charge == -1) th2.fill(event.loose_muons.at(1).Pt, event.loose_muons.at(1).Phi);
//        }
//        } ) );
//
//  // GenFilt HT plot
//  makeTaggedPlot(cut, Vital,
//    TH1Wrapper("GenFiltHT",
//        [] () -> TH1* {return new TH1F("GenFiltHT", "GenFiltHT", 200, 0, 5000); },
//        [this] (TH1Wrapper& th1) { th1.fill(event.GenFiltHT); } ) );
//
//  // GenFilt MET plot
//  makeTaggedPlot(cut, Vital,
//    TH1Wrapper("GenFiltMET",
//        [] () -> TH1* {return new TH1F("GenFiltMET", "GenFiltMET", 200, 0, 2000); },
//        [this] (TH1Wrapper& th1) { th1.fill(event.GenFiltMET); } ) );
//



  // Simple hist the number of events
  makeTaggedPlot(cut, Vital, 
      TH1Wrapper("EventCount", 
        [] () -> TH1* {return new TH1F("EventCount", "EventCount", 1, 1, 2); },
        [this] (TH1Wrapper& th1) { th1.fill(1); } ) );

  makeTaggedPlot(cut, Vital, 
      TH1Wrapper("EtMiss", 
        [] () -> TH1* {return new TH1F("EtMiss", "EtMiss", 200, 0, 2000); },
        [this] (TH1Wrapper& th1) { th1.fill(event.EtMiss); } ) );

  makeTaggedPlot(cut, Vital, 
      TH1Wrapper("EtMissSignificance", 
        [] () -> TH1* {return new TH1F("EtMissSignificance", "EtMissSignificance", 500, 0, 100); },
        [this] (TH1Wrapper& th1) { th1.fill(event.EtMissSignificance); } ) );

  // kinematics for electrons and muons
  for (unsigned int nLep : {0, 1} ) {

    // Electrons 
    std::string nEleNamePt = "ele"+std::to_string(nLep+1)+"Pt";
    makeTaggedPlot(cut, Vital, 
        TH1Wrapper(nEleNamePt,
          [nLep, nEleNamePt] () -> TH1* {return new TH1F(nEleNamePt.c_str(), ("Electron "+std::to_string(nLep+1)+" pT").c_str(), 400, 0, 2000); },
          [this, nLep] (TH1Wrapper& th1) {
            if (event.loose_electrons.size() >= nLep+1) th1.fill(event.loose_electrons.at(nLep).Pt);
          } ) );

    std::string nEleNamePhi = "ele"+std::to_string(nLep+1)+"Phi";
    makeTaggedPlot(cut, Vital, 
        TH1Wrapper(nEleNamePhi,
          [nLep, nEleNamePhi] () -> TH1* {return new TH1F(nEleNamePhi.c_str(), ("Electron "+std::to_string(nLep+1)+" phi").c_str(), 128, -3.2, 3.2); },
          [this, nLep] (TH1Wrapper& th1) {
            if (event.loose_electrons.size() >= nLep+1) th1.fill(event.loose_electrons.at(nLep).Phi);
            } ) );

    std::string nEleNameEta = "ele"+std::to_string(nLep+1)+"Eta";
    makeTaggedPlot(cut, Vital, 
        TH1Wrapper(nEleNameEta,
          [nLep, nEleNameEta] () -> TH1* {return new TH1F(nEleNameEta.c_str(), ("Electron "+std::to_string(nLep+1)+" eta").c_str(), 250, -4, 4); },
          [this, nLep] (TH1Wrapper& th1) {
          if (event.loose_electrons.size() >= nLep+1) th1.fill(event.loose_electrons.at(nLep).Eta);
          } ) );

    std::string nEleNamePtEta = "ele"+std::to_string(nLep+1)+"PtEta";
    makeTaggedPlot(cut, Vital, 
            TH2Wrapper(nEleNamePtEta,
                [nLep, nEleNamePtEta] () -> TH2* {return new TH2F(nEleNamePtEta.c_str(), ("Electron "+std::to_string(nLep+1)+" pT eta").c_str(), 200, 0, 2000, 250, -4, 4); },
                [this, nLep] (TH2Wrapper& th2) {
                if (event.loose_electrons.size() >= nLep+1) th2.fill(event.loose_electrons.at(nLep).Pt, event.loose_electrons.at(nLep).Eta);
                } ) );


    if (!onlyveryimportantplots){
      std::string nEleNameIFFClassification = "ele"+std::to_string(nLep+1)+"IFFClassification";
      makeTaggedPlot(cut, Vital, 
          TH1Wrapper(nEleNameIFFClassification,
            [nLep, nEleNameIFFClassification] () -> TH1* {return new TH1F(nEleNameIFFClassification.c_str(), ("Electron "+std::to_string(nLep+1)+" IFFClassification").c_str(), 20, 0, 20); },
            [this, nLep] (TH1Wrapper& th1) {
              if (event.loose_electrons.size() >= nLep+1) th1.fill(event.loose_electrons.at(nLep).IFFClassification);
            } ) );

      // Track parameters
      std::string nEleNamed0sig = "ele"+std::to_string(nLep+1)+"d0sig";
      makeTaggedPlot(cut, Vital, 
          TH1Wrapper(nEleNamed0sig,
            [nLep, nEleNamed0sig] () -> TH1* {return new TH1F(nEleNamed0sig.c_str(), ("Electron "+std::to_string(nLep+1)+" d0sig").c_str(), 200, -50, 50); },
            [this, nLep] (TH1Wrapper& th1) {
              if (event.loose_electrons.size() >= nLep+1) th1.fill(event.loose_electrons.at(nLep).d0sig);
            } ) );
  
      std::string nEleNamez0sinTheta = "ele"+std::to_string(nLep+1)+"z0sinTheta";
      makeTaggedPlot(cut, Vital, 
          TH1Wrapper(nEleNamez0sinTheta,
            [nLep, nEleNamez0sinTheta] () -> TH1* {return new TH1F(nEleNamez0sinTheta.c_str(), ("Electron "+std::to_string(nLep+1)+" z0sinTheta").c_str(), 400, -20, 20); },
            [this, nLep] (TH1Wrapper& th1) {
              if (event.loose_electrons.size() >= nLep+1) th1.fill(event.loose_electrons.at(nLep).z0sinTheta);
            } ) );
  
  
      std::string nEleNameTEMP = "ele"+std::to_string(nLep+1)+"z0sinTheta_d0sig";
      makeTaggedPlot(cut, Vital, 
          TH2Wrapper(nEleNameTEMP,
            [nLep, nEleNameTEMP] () -> TH2* {return new TH2F(nEleNameTEMP.c_str(), "", 200, -10, 10, 100, -20, 20); },
            [this, nLep] (TH2Wrapper& th2) {
            if (event.loose_electrons.size() >= nLep+1) th2.fill(event.loose_electrons.at(nLep).z0sinTheta, event.loose_electrons.at(nLep).d0sig);
            } ) );
  
      std::string nEleNamez0sinTheta_d0sig = "ele"+std::to_string(nLep+1)+"z0sinTheta_d0sig";
      makeTaggedPlot(cut, Vital,
      TH2Wrapper("",
      [nLep, nEleNamez0sinTheta_d0sig] () -> TH2* {return new TH2F(nEleNamez0sinTheta_d0sig.c_str(),  ("Electron "+std::to_string(nLep+1)+" z0sinTheta_d0sig").c_str(), 200, -10, 10, 100, -20, 20); },
      [this, nLep] (TH2Wrapper& th2){
      if (event.loose_electrons.size() >= nLep+1) th2.fill(event.loose_electrons.at(nLep).z0sinTheta, event.loose_electrons.at(nLep).d0sig);
      } ) );

    }   
 
    if(m_plot_extra_lepton_variables){
      std::string nEleNameMtMet = "ele"+std::to_string(nLep+1) + "mt_met";
      makeTaggedPlot(cut, Vital, 
          TH1Wrapper(nEleNameMtMet, 
            [nLep, nEleNameMtMet] () -> TH1* {return new TH1F(nEleNameMtMet.c_str(), "", 100, 0, 1000); }, 
            [this, nLep] (TH1Wrapper& th1){
            if(event.loose_electrons.size() >= nLep+1) th1.fill(event.loose_electrons.at(nLep).mt_met);
          }));

      std::string eleBaseName = "ele"+std::to_string(nLep+1);

      std::string eleVarName = eleBaseName + "charge";
      makeTaggedPlot(cut, Vital, 
          TH1Wrapper(eleVarName, 
            [nLep, eleVarName] () -> TH1* {return new TH1F(eleVarName.c_str(), eleVarName.c_str(), 3, -1, 2); }, 
            [this, nLep] (TH1Wrapper& th1){
            if(event.loose_electrons.size() >= nLep+1) th1.fill(event.loose_electrons.at(nLep).charge);
          }));

      eleVarName = eleBaseName + "passCFT";
      makeTaggedPlot(cut, Vital, 
          TH1Wrapper(eleVarName, 
            [nLep, eleVarName] () -> TH1* {return new TH1F(eleVarName.c_str(), eleVarName.c_str(), 2, 0, 2); }, 
            [this, nLep] (TH1Wrapper& th1){
            if(event.loose_electrons.size() >= nLep+1) th1.fill(event.loose_electrons.at(nLep).passCFT);
          }));

      eleVarName = eleBaseName + "isIsolated";
      makeTaggedPlot(cut, Vital, 
          TH1Wrapper(eleVarName, 
            [nLep, eleVarName] () -> TH1* {return new TH1F(eleVarName.c_str(), eleVarName.c_str(), 3, -1, 2); }, 
            [this, nLep] (TH1Wrapper& th1){
            if(event.loose_electrons.size() >= nLep+1) th1.fill(event.loose_electrons.at(nLep).isIsolated);
          }));

      eleVarName = eleBaseName + "isLoose";
      makeTaggedPlot(cut, Vital, 
          TH1Wrapper(eleVarName, 
            [nLep, eleVarName] () -> TH1* {return new TH1F(eleVarName.c_str(), eleVarName.c_str(), 3, -1, 2); }, 
            [this, nLep] (TH1Wrapper& th1){
            if(event.loose_electrons.size() >= nLep+1) th1.fill(event.loose_electrons.at(nLep).isLoose);
          }));

      eleVarName = eleBaseName + "isTight";
      makeTaggedPlot(cut, Vital, 
          TH1Wrapper(eleVarName, 
            [nLep, eleVarName] () -> TH1* {return new TH1F(eleVarName.c_str(), eleVarName.c_str(), 3, -1, 2); }, 
            [this, nLep] (TH1Wrapper& th1){
            if(event.loose_electrons.size() >= nLep+1) th1.fill(event.loose_electrons.at(nLep).isTight);
          }));

      eleVarName = eleBaseName + "scaleFactor";
      makeTaggedPlot(cut, Vital,
          TH1Wrapper(eleVarName,
            [nLep, eleVarName] () -> TH1*{return new TH1F(eleVarName.c_str(), eleVarName.c_str(), 100, 0, 3); },
            [this, nLep] (TH1Wrapper& th1){
            if(event.loose_electrons.size() >= nLep+1) th1.fill(event.loose_electrons.at(nLep).scaleFactor);
            }));

      eleVarName = eleBaseName + "truthCharge";
      makeTaggedPlot(cut, Vital,
          TH1Wrapper(eleVarName,
            [nLep, eleVarName] () -> TH1*{return new TH1F(eleVarName.c_str(), eleVarName.c_str(), 3, -1, 2); },
            [this, nLep] (TH1Wrapper& th1){
            if(event.loose_electrons.size() >= nLep+1) th1.fill(event.loose_electrons.at(nLep).truthCharge);
            }));

      eleVarName = eleBaseName + "truthType";
      makeTaggedPlot(cut, Vital,
          TH1Wrapper(eleVarName,
            [nLep, eleVarName] () -> TH1*{return new TH1F(eleVarName.c_str(), eleVarName.c_str(), 100, -50, 50); },
            [this, nLep] (TH1Wrapper& th1){
            if(event.loose_electrons.size() >= nLep+1) th1.fill(event.loose_electrons.at(nLep).truthType);
            }));

      eleVarName = eleBaseName + "truthOrigin";
      makeTaggedPlot(cut, Vital,
          TH1Wrapper(eleVarName,
            [nLep, eleVarName] () -> TH1*{return new TH1F(eleVarName.c_str(), eleVarName.c_str(), 100, -50, 50); },
            [this, nLep] (TH1Wrapper& th1){
            if(event.loose_electrons.size() >= nLep+1) th1.fill(event.loose_electrons.at(nLep).truthOrigin);
            }));

      eleVarName = eleBaseName + "chargeIDScaleFactor";
      makeTaggedPlot(cut, Vital,
          TH1Wrapper(eleVarName,
            [nLep, eleVarName] () -> TH1*{return new TH1F(eleVarName.c_str(), eleVarName.c_str(), 100, 0, 3); },
            [this, nLep] (TH1Wrapper& th1){
            if(event.loose_electrons.size() >= nLep+1) th1.fill(event.loose_electrons.at(nLep).chargeIDScaleFactor);
            }));

      eleVarName = eleBaseName + "chargeFlipTaggerScaleFactor";
      makeTaggedPlot(cut, Vital,
          TH1Wrapper(eleVarName,
            [nLep, eleVarName] () -> TH1*{return new TH1F(eleVarName.c_str(), eleVarName.c_str(), 100, 0, 3); },
            [this, nLep] (TH1Wrapper& th1){
            if(event.loose_electrons.size() >= nLep+1) th1.fill(event.loose_electrons.at(nLep).chargeFlipTaggerScaleFactor);
            }));

//      eleVarName = eleBaseName + "trigScaleFactor";
//      makeTaggedPlot(cut, Vital,
//          TH1Wrapper(eleVarName,
//            [nLep, eleVarName] () -> TH1*{return new TH1F(eleVarName.c_str(), eleVarName.c_str(), 100, 0, 3); },
//            [this, nLep] (TH1Wrapper& th1){
//            if(event.loose_electrons.size() >= nLep+1) th1.fill(event.loose_electrons.at(nLep).trigScaleFactor);
//            }));

      eleVarName = eleBaseName + "looseScaleFactor";
      makeTaggedPlot(cut, Vital,
          TH1Wrapper(eleVarName,
            [nLep, eleVarName] () -> TH1*{return new TH1F(eleVarName.c_str(), eleVarName.c_str(), 100, 0, 3); },
            [this, nLep] (TH1Wrapper& th1){
            if(event.loose_electrons.size() >= nLep+1) th1.fill(event.loose_electrons.at(nLep).looseScaleFactor);
            }));

      eleVarName = eleBaseName + "isolationScaleFactor";
      makeTaggedPlot(cut, Vital,
          TH1Wrapper(eleVarName,
            [nLep, eleVarName] () -> TH1*{return new TH1F(eleVarName.c_str(), eleVarName.c_str(), 100, 0, 3); },
            [this, nLep] (TH1Wrapper& th1){
            if(event.loose_electrons.size() >= nLep+1) th1.fill(event.loose_electrons.at(nLep).isolationScaleFactor);
            }));

      eleVarName = eleBaseName + "isTruthChargeMatched";
      makeTaggedPlot(cut, Vital,
          TH1Wrapper(eleVarName,
            [nLep, eleVarName] () -> TH1*{return new TH1F(eleVarName.c_str(), eleVarName.c_str(), 3, -1, 2); },
            [this, nLep] (TH1Wrapper& th1){
            if(event.loose_electrons.size() >= nLep+1) th1.fill(event.loose_electrons.at(nLep).isTruthChargeMatched);
            }));

    }

    // Muons 
    std::string nMuonNamePt = "muon"+std::to_string(nLep+1)+"Pt";
    makeTaggedPlot(cut, Vital, 
        TH1Wrapper(nMuonNamePt,
          [nLep, nMuonNamePt] () -> TH1* {return new TH1F(nMuonNamePt.c_str(), ("muon "+std::to_string(nLep+1)+" pT").c_str(),  200, 0, 2000); },
          [this, nLep] (TH1Wrapper& th1) {
            if (event.loose_muons.size() >= nLep+1) th1.fill(event.loose_muons.at(nLep).Pt);
          } ) );

    std::string nMuonNamePhi = "muon"+std::to_string(nLep+1)+"Phi";
    makeTaggedPlot(cut, Vital, 
        TH1Wrapper(nMuonNamePhi,
          [nLep, nMuonNamePhi] () -> TH1* {return new TH1F(nMuonNamePhi.c_str(), ("muon "+std::to_string(nLep+1)+" phi").c_str(), 128, -3.2, 3.2); },
          [this, nLep] (TH1Wrapper& th1) {
            if (event.loose_muons.size() >= nLep+1) th1.fill(event.loose_muons.at(nLep).Phi);
            } ) );

    std::string nMuonNameEta = "muon"+std::to_string(nLep+1)+"Eta";
    makeTaggedPlot(cut, Vital, 
        TH1Wrapper(nMuonNameEta,
          [nLep, nMuonNameEta] () -> TH1* {return new TH1F(nMuonNameEta.c_str(), ("muon "+std::to_string(nLep+1)+" eta").c_str(), 250, -4, 4); },
          [this, nLep] (TH1Wrapper& th1) {
          if (event.loose_muons.size() >= nLep+1) th1.fill(event.loose_muons.at(nLep).Eta);
          } ) );

      std::string nMuonNamePtEta = "muon"+std::to_string(nLep+1)+"PtEta";
      makeTaggedPlot(cut, Vital, 
          TH2Wrapper(nMuonNamePtEta,
            [nLep, nMuonNamePtEta] () -> TH2* {return new TH2F(nMuonNamePtEta.c_str(), ("Muon "+std::to_string(nLep+1)+" pT eta").c_str(), 200, 0, 2000, 250, -4, 4); },
            [this, nLep] (TH2Wrapper& th2) {
            if (event.loose_muons.size() >= nLep+1) th2.fill(event.loose_muons.at(nLep).Pt, event.loose_muons.at(nLep).Eta);
            } ) );

    if (!onlyveryimportantplots){
      std::string nMuonNameIFFClassification = "muon"+std::to_string(nLep+1)+"IFFClassification";
      makeTaggedPlot(cut, Vital, 
          TH1Wrapper(nMuonNameIFFClassification,
            [nLep, nMuonNameIFFClassification] () -> TH1* {return new TH1F(nMuonNameIFFClassification.c_str(), ("Muon "+std::to_string(nLep+1)+" IFFClassification").c_str(), 20, 0, 20); },
            [this, nLep] (TH1Wrapper& th1) {
              if (event.loose_muons.size() >= nLep+1) th1.fill(event.loose_muons.at(nLep).IFFClassification);
            } ) );


      // tracking parameters
      std::string nMuonNamed0sig = "muon"+std::to_string(nLep+1)+"d0sig";
      makeTaggedPlot(cut, Vital, 
          TH1Wrapper(nMuonNamed0sig,
            [nLep, nMuonNamed0sig] () -> TH1* {return new TH1F(nMuonNamed0sig.c_str(), ("Muon "+std::to_string(nLep+1)+" d0sig").c_str(), 200, -50, 50); },
            [this, nLep] (TH1Wrapper& th1) {
              if (event.loose_muons.size() >= nLep+1) th1.fill(event.loose_muons.at(nLep).d0sig);
            } ) );
  
      std::string nMuonNamez0sinTheta = "muon"+std::to_string(nLep+1)+"z0sinTheta";
      makeTaggedPlot(cut, Vital, 
          TH1Wrapper(nMuonNamez0sinTheta,
            [nLep, nMuonNamez0sinTheta] () -> TH1* {return new TH1F(nMuonNamez0sinTheta.c_str(), ("Muon "+std::to_string(nLep+1)+" z0sinTheta").c_str(), 400, -20, 20); },
            [this, nLep] (TH1Wrapper& th1) {
              if (event.loose_muons.size() >= nLep+1) th1.fill(event.loose_muons.at(nLep).z0sinTheta);
            } ) );
  
      std::string nMuonNameTEMP = "muon"+std::to_string(nLep+1)+"z0sinTheta_d0sig";
      makeTaggedPlot(cut, Vital, 
          TH2Wrapper(nMuonNameTEMP,
            [nLep, nMuonNameTEMP] () -> TH2* {return new TH2F(nMuonNameTEMP.c_str(), "", 200, -10, 10, 100, -20, 20); },
            [this, nLep] (TH2Wrapper& th2) {
            if (event.loose_muons.size() >= nLep+1) th2.fill(event.loose_muons.at(nLep).z0sinTheta, event.loose_muons.at(nLep).d0sig);
            } ) );
    }

    if(m_plot_extra_lepton_variables){

      std::string muonBaseName = "muon"+std::to_string(nLep+1);

      std::string muonVarName = muonBaseName + "charge";
      makeTaggedPlot(cut, Vital, 
          TH1Wrapper(muonVarName, 
            [nLep, muonVarName] () -> TH1* {return new TH1F(muonVarName.c_str(), "", 3, -1, 2); }, 
            [this, nLep] (TH1Wrapper& th1){
            if(event.loose_muons.size() >= nLep+1) th1.fill(event.loose_muons.at(nLep).charge);
          }));

      muonVarName = muonBaseName + "isIsolated";
      makeTaggedPlot(cut, Vital, 
          TH1Wrapper(muonVarName, 
            [nLep, muonVarName] () -> TH1* {return new TH1F(muonVarName.c_str(), "", 3, -1, 2); }, 
            [this, nLep] (TH1Wrapper& th1){
            if(event.loose_muons.size() >= nLep+1) th1.fill(event.loose_muons.at(nLep).isIsolated);
          }));

      muonVarName = muonBaseName + "isLoose";
      makeTaggedPlot(cut, Vital, 
          TH1Wrapper(muonVarName, 
            [nLep, muonVarName] () -> TH1* {return new TH1F(muonVarName.c_str(), "", 3, -1, 2); }, 
            [this, nLep] (TH1Wrapper& th1){
            if(event.loose_muons.size() >= nLep+1) th1.fill(event.loose_muons.at(nLep).isLoose);
          }));

      muonVarName = muonBaseName + "isTight";
      makeTaggedPlot(cut, Vital, 
          TH1Wrapper(muonVarName, 
            [nLep, muonVarName] () -> TH1* {return new TH1F(muonVarName.c_str(), "", 3, -1, 2); }, 
            [this, nLep] (TH1Wrapper& th1){
            if(event.loose_muons.size() >= nLep+1) th1.fill(event.loose_muons.at(nLep).isTight);
          }));

      muonVarName = muonBaseName + "scaleFactor";
      makeTaggedPlot(cut, Vital,
          TH1Wrapper(muonVarName,
            [nLep, muonVarName] () -> TH1*{return new TH1F(muonVarName.c_str(), muonVarName.c_str(), 100, 0, 3); },
            [this, nLep] (TH1Wrapper& th1){
            if(event.loose_muons.size() >= nLep+1) th1.fill(event.loose_muons.at(nLep).scaleFactor);
            }));

      muonVarName = muonBaseName + "truthCharge";
      makeTaggedPlot(cut, Vital,
          TH1Wrapper(muonVarName,
            [nLep, muonVarName] () -> TH1*{return new TH1F(muonVarName.c_str(), muonVarName.c_str(), 3, -1, 2); },
            [this, nLep] (TH1Wrapper& th1){
            if(event.loose_muons.size() >= nLep+1) th1.fill(event.loose_muons.at(nLep).truthCharge);
            }));

      muonVarName = muonBaseName + "truthType";
      makeTaggedPlot(cut, Vital,
          TH1Wrapper(muonVarName,
            [nLep, muonVarName] () -> TH1*{return new TH1F(muonVarName.c_str(), muonVarName.c_str(), 100, -50, 50); },
            [this, nLep] (TH1Wrapper& th1){
            if(event.loose_muons.size() >= nLep+1) th1.fill(event.loose_muons.at(nLep).truthType);
            }));

      muonVarName = muonBaseName + "truthOrigin";
      makeTaggedPlot(cut, Vital,
          TH1Wrapper(muonVarName,
            [nLep, muonVarName] () -> TH1*{return new TH1F(muonVarName.c_str(), muonVarName.c_str(), 100, -50, 50); },
            [this, nLep] (TH1Wrapper& th1){
            if(event.loose_muons.size() >= nLep+1) th1.fill(event.loose_muons.at(nLep).truthOrigin);
            }));

      muonVarName = muonBaseName + "isolationScaleFactor";
      makeTaggedPlot(cut, Vital,
          TH1Wrapper(muonVarName,
            [nLep, muonVarName] () -> TH1*{return new TH1F(muonVarName.c_str(), muonVarName.c_str(), 100, 0, 3); },
            [this, nLep] (TH1Wrapper& th1){
            if(event.loose_muons.size() >= nLep+1) th1.fill(event.loose_muons.at(nLep).isolationScaleFactor);
            }));

      muonVarName = muonBaseName + "isTruthChargeMatched";
      makeTaggedPlot(cut, Vital,
          TH1Wrapper(muonVarName,
            [nLep, muonVarName] () -> TH1*{return new TH1F(muonVarName.c_str(), muonVarName.c_str(), 3, -1, 2); },
            [this, nLep] (TH1Wrapper& th1){
            if(event.loose_muons.size() >= nLep+1) th1.fill(event.loose_muons.at(nLep).isTruthChargeMatched);
            }));



    } 

    // Generic leptons
    std::string nLepNamePt = "lep"+std::to_string(nLep+1)+"Pt";
    makeTaggedPlot(cut, Vital, 
        TH1Wrapper(nLepNamePt,
          [nLep, nLepNamePt] () -> TH1* {return new TH1F(nLepNamePt.c_str(), ("Lepton "+std::to_string(nLep+1)+" pT").c_str(), 200, 0, 2000); },
          [this, nLep] (TH1Wrapper& th1) {
            if (event.loose_leptons.size() >= nLep+1) th1.fill(event.loose_leptons.at(nLep).get().Pt);
          } ) );

    std::string nLepNamePhi = "lep"+std::to_string(nLep+1)+"Phi";
    makeTaggedPlot(cut, Vital, 
        TH1Wrapper(nLepNamePhi,
          [nLep, nLepNamePhi] () -> TH1* {return new TH1F(nLepNamePhi.c_str(), ("Lepton "+std::to_string(nLep+1)+" phi").c_str(), 128, -3.2, 3.2); },
          [this, nLep] (TH1Wrapper& th1) {
            if (event.loose_leptons.size() >= nLep+1) th1.fill(event.loose_leptons.at(nLep).get().Phi);
            } ) );

    std::string nLepNameEta = "lep"+std::to_string(nLep+1)+"Eta";
    makeTaggedPlot(cut, Vital, 
        TH1Wrapper(nLepNameEta,
          [nLep, nLepNameEta] () -> TH1* {return new TH1F(nLepNameEta.c_str(), ("Lepton "+std::to_string(nLep+1)+" eta").c_str(), 250, -4, 4); },
          [this, nLep] (TH1Wrapper& th1) {
          if (event.loose_leptons.size() >= nLep+1) th1.fill(event.loose_leptons.at(nLep).get().Eta);
          } ) );

    if (!onlyveryimportantplots){
      std::string nLeptonNamePtEta = "lepton"+std::to_string(nLep+1)+"PtEta";
      makeTaggedPlot(cut, Vital, 
          TH2Wrapper(nLeptonNamePtEta,
            [nLep, nLeptonNamePtEta] () -> TH2* {return new TH2F(nLeptonNamePtEta.c_str(), ("Lepton "+std::to_string(nLep+1)+" pT eta").c_str(), 200, 0, 2000, 250, -4, 4); },
            [this, nLep] (TH2Wrapper& th2) {
            if (event.loose_leptons.size() >= nLep+1) th2.fill(event.loose_leptons.at(nLep).get().Pt, event.loose_leptons.at(nLep).get().Eta);
            } ) );
  
      std::string nLeptonNamed0sig = "lepton"+std::to_string(nLep+1)+"d0sig";
      makeTaggedPlot(cut, Vital, 
          TH1Wrapper(nLeptonNamed0sig,
            [nLep, nLeptonNamed0sig] () -> TH1* {return new TH1F(nLeptonNamed0sig.c_str(), ("Lepton "+std::to_string(nLep+1)+" d0sig").c_str(), 200, -50, 50); },
            [this, nLep] (TH1Wrapper& th1) {
              if (event.loose_leptons.size() >= nLep+1) th1.fill(event.loose_leptons.at(nLep).get().d0sig);
            } ) );
      std::string nLeptonNamez0sinTheta = "lepton"+std::to_string(nLep+1)+"z0sinTheta";
      makeTaggedPlot(cut, Vital, 
          TH1Wrapper(nLeptonNamez0sinTheta,
            [nLep, nLeptonNamez0sinTheta] () -> TH1* {return new TH1F(nLeptonNamez0sinTheta.c_str(), ("Lepton "+std::to_string(nLep+1)+" z0sinTheta").c_str(), 400, -20, 20); },
            [this, nLep] (TH1Wrapper& th1) {
              if (event.loose_leptons.size() >= nLep+1) th1.fill(event.loose_leptons.at(nLep).get().z0sinTheta);
            } ) );
  
      std::string nLeptonNameTEMP = "lepton"+std::to_string(nLep+1)+"z0sinTheta_d0sig";
      makeTaggedPlot(cut, Vital, 
          TH2Wrapper(nLeptonNameTEMP,
            [nLep, nLeptonNameTEMP] () -> TH2* {return new TH2F(nLeptonNameTEMP.c_str(), "", 200, -10, 10, 100, -20, 20); },
            [this, nLep] (TH2Wrapper& th2) {
            if (event.loose_leptons.size() >= nLep+1) th2.fill(event.loose_leptons.at(nLep).get().z0sinTheta, event.loose_leptons.at(nLep).get().d0sig);
            } ) );
    }
//
  } // end loop over nLep

  if (!onlyveryimportantplots){
    //loose-not-tight leptons
    //should only be relevant in the case there are exactly 2 leptons 
    //(all loose_leptons are Loose)
    makeTaggedPlot(cut, Vital, 
        TH1Wrapper("looseNotTightLeptonsPt",
          [] () -> TH1* {return new TH1F("looseNotTightLeptonsPt", "looseNotTightLeptonsPt", 200, 0, 2000); },
          [this] (TH1Wrapper& th1) {
            for(auto lep : event.loose_leptons){
              if(!lep.get().isTight) th1.fill(lep.get().Pt);
            }
          } ) );
  
    // tight leptons 
    makeTaggedPlot(cut, Vital, 
        TH1Wrapper("tightLeptonsPt",
          [] () -> TH1* {return new TH1F("tightLeptonsPt", "tightLeptonsPt", 200, 0, 2000); },
          [this] (TH1Wrapper& th1) {
            for(auto lep : event.loose_leptons){
              if(!lep.get().isTight) th1.fill(lep.get().Pt);
            }
          } ) );
  
    // positive electrons
    makeTaggedPlot(cut, Vital, 
        TH1Wrapper("elePlusPt",
          [] () -> TH1* {return new TH1F("elePlusPt", "elePlusPt", 200, 0, 2000); },
          [this] (TH1Wrapper& th1) {
            for(auto ele : event.loose_electrons){
              if(ele.charge > 0) th1.fill(ele.Pt);
            }
          } ) );
    makeTaggedPlot(cut, Vital, 
        TH1Wrapper("eleMinusPt",
          [] () -> TH1* {return new TH1F("eleMinusPt", "eleMinusPt", 200, 0, 2000); },
          [this] (TH1Wrapper& th1) {
            for(auto ele : event.loose_electrons){
              if(ele.charge < 0) th1.fill(ele.Pt);
            }
          } ) );
    makeTaggedPlot(cut, Vital, 
        TH1Wrapper("muonPlusPt",
          [] () -> TH1* {return new TH1F("muonPlusPt", "muonPlusPt", 200, 0, 2000); },
          [this] (TH1Wrapper& th1) {
            for(auto muon : event.loose_muons){
              if(muon.charge > 0) th1.fill(muon.Pt);
            }
          } ) );
    makeTaggedPlot(cut, Vital, 
        TH1Wrapper("muonMinusPt",
          [] () -> TH1* {return new TH1F("muonMinusPt", "muonMinusPt", 200, 0, 2000); },
          [this] (TH1Wrapper& th1) {
            for(auto muon : event.loose_muons){
              if(muon.charge < 0) th1.fill(muon.Pt);
            }
          } ) );
  } 

//  makeTaggedPlot(cut, Vital, 
//      TH1Wrapper("sum_lepPt",
//        [] () -> TH1* {return new TH1F("sum_lepPt", "sum_lepPt", 200, 0, 2000); },
//        [this] (TH1Wrapper& th1) {
//          if (event.loose_leptons.size() >1) th1.fill(event.loose_leptons.at(1).get().Pt + event.loose_leptons.at(1).get().Pt);
//        } ) );

  makeTaggedPlot(cut, Vital, 
      TH1Wrapper("sum_lepPt_jet1Pt",
        [] () -> TH1* {return new TH1F("sum_lepPt_jet1Pt", "sum_lepPt_jet1Pt", 250, 0, 2500); },
        [this] (TH1Wrapper& th1) {
          if (event.loose_leptons.size() >1 && event.signalJets.size()>0) th1.fill( event.sum_pt_leps_jet1 );
        } ) );

  // Jets
  makeTaggedPlot(cut, Vital,
      TH1Wrapper("NJets",
        [] () -> TH1* {return new TH1F("nJets", "nJets", 50, 0, 50);},
        [this] (TH1Wrapper& th1) {th1.fill(event.signalJets.size()); } ) );

    // only care about the leading jet 
    makeTaggedPlot(cut, Vital, 
        TH1Wrapper("jet1Pt",
          [] () -> TH1* {return new TH1F("jet1Pt", "jet1Pt", 250, 0, 2500); },
          [this] (TH1Wrapper& th1) {
          if (event.signalJets.size() > 0) th1.fill( event.signalJets.at(0).Pt ); } ) );

    makeTaggedPlot(cut, Vital, 
        TH1Wrapper("jet1Eta",
          [] () -> TH1* {return new TH1F("jet1Eta", "jet1Eta", 250, -4, 4); },
          [this] (TH1Wrapper& th1) {
          if (event.signalJets.size() > 0) th1.fill( event.signalJets.at(0).Eta ); } ) );

    makeTaggedPlot(cut, Vital, 
        TH1Wrapper("jet1Phi",
          [] () -> TH1* {return new TH1F("jet1Phi", "jet1Phi", 128, -3.2, 3.2); },
          [this] (TH1Wrapper& th1) {
          if (event.signalJets.size() > 0) th1.fill( event.signalJets.at(0).Phi ); } ) );

    // only care about the leading jet .. except when veto-ing PPLQ events 
    if (!onlyveryimportantplots){
      makeTaggedPlot(cut, Vital, 
          TH1Wrapper("jet2Pt",
            [] () -> TH1* {return new TH1F("jet2Pt", "jet2Pt", 100, 0, 1000); },
            [this] (TH1Wrapper& th1) {
            if (event.signalJets.size() > 1) th1.fill( event.signalJets.at(1).Pt ); } ) );
  
      makeTaggedPlot(cut, Vital, 
          TH1Wrapper("jet2Eta",
            [] () -> TH1* {return new TH1F("jet2Eta", "jet2Eta", 250, -4, 4); },
            [this] (TH1Wrapper& th1) {
            if (event.signalJets.size() > 1) th1.fill( event.signalJets.at(1).Eta ); } ) );
  
      makeTaggedPlot(cut, Vital, 
          TH1Wrapper("jet2Phi",
            [] () -> TH1* {return new TH1F("jet2Phi", "jet2Phi", 128, -3.2, 3.2); },
            [this] (TH1Wrapper& th1) {
            if (event.signalJets.size() > 1) th1.fill( event.signalJets.at(1).Phi ); } ) );
    }


  if (!onlyveryimportantplots){
  // Event-level
    makeTaggedPlot(cut, Vital, 
        TH1Wrapper("ttbarweight", 
          [] () -> TH1* {return new TH1F("ttbarweight", "ttbarweight", 1000, 0, 2); },
          [this] (TH1Wrapper& th1) {th1.fill(event.ttbar_weight); } ) );
  
    makeTaggedPlot(cut, Vital, 
        TH1Wrapper("muon_triggerweight", 
          [] () -> TH1* {return new TH1F("muon_triggerweight", "muon_triggerweight", 100, 0, 2); },
          [this] (TH1Wrapper& th1) {th1.fill(event.m_muon_triggerweight); } ) );
  
    makeTaggedPlot(cut, Vital, 
        TH1Wrapper("muon_recoweight", 
          [] () -> TH1* {return new TH1F("muon_recoweight", "muon_recoweight", 100, 0, 2); },
          [this] (TH1Wrapper& th1) {th1.fill(event.m_muon_recoweight); } ) );

    makeTaggedPlot(cut, Vital, 
        TH1Wrapper("HTjets", 
          [] () -> TH1* {return new TH1F("HTjets", "HTjets", 200, 0, 4000); },
          [this] (TH1Wrapper& th1) {th1.fill(event.HT_jets); } ) );
  
    makeTaggedPlot(cut, Vital, 
        TH1Wrapper("HTall", 
          [] () -> TH1* {return new TH1F("HTall", "HTall", 200, 0, 4000); },
          [this] (TH1Wrapper& th1) {th1.fill(event.HT_all); } ) );

    /*****
    makeTaggedPlot(cut, Vital, 
        TH1Wrapper("fake_weight", 
          [] () -> TH1* {return new TH1F("fake_weight", "fake_weight", 100, -2, 2); },
          [this] (TH1Wrapper& th1) {th1.fill(event.m_fake_weight); } ) );
    ******/
    makeTaggedPlot(cut, Vital, 
        TH1Wrapper("Mll", 
          [] () -> TH1* {return new TH1F("Mll", "Mll", 300, 0, 3000); },
          [this] (TH1Wrapper& th1) {th1.fill(event.mll); } ) );
  
    makeTaggedPlot(cut, Vital, 
        TH1Wrapper("Mll_zoom", 
          [] () -> TH1* {return new TH1F("Mll", "Mll", 2000, 0, 200); },
          [this] (TH1Wrapper& th1) {th1.fill(event.mll); } ) );

    makeTaggedPlot(cut, Vital, 
        TH2Wrapper("sumMtMET_MT2Leptons", 
          [] () -> TH2* {return new TH2F("sumMtMET_MT2Leptons", "sumMtMET_MT2Leptons;sumMtMet;MT2Leptons", 100, 0, 2000, 100, 0, 1000); },
          [this] (TH2Wrapper& th2) {          
          th2.fill(event.sum_mt_met, event.squirrel_leptons.MT2 );
          } ) );
  
    makeTaggedPlot(cut, Vital,
        TH2Wrapper("sumMtMET_lep1Pt", 
          [] () -> TH2* {return new TH2F("sumMtMET_lep1Pt", "sumMtMET_lep1Pt;sumMtMET;lep1Pt", 100, 0, 2000, 100, 0, 1000); },
          [this] (TH2Wrapper& th2) {
          if (event.loose_leptons.size() > 0) { th2.fill(event.sum_mt_met, event.loose_leptons.at(0).get().Pt);}
          } ) );
    makeTaggedPlot(cut, Vital,
        TH3Wrapper("lep1Pt_lep2Pt_EtMiss",
       [] () -> TH3* {return new TH3F("lep1Pt_lep2Pt_EtMiss", "lep1Pt_lep2Pt_EtMiss;lep1Pt;lep2Pt;EtMiss", 200, 0., 2000., 200, 0., 2000., 200, 0., 2000.);},
          [this] (TH3Wrapper& th3) {
          if (event.loose_leptons.size() > 1) th3.fill(event.loose_leptons.at(0).get().Pt, event.loose_leptons.at(1).get().Pt, event.EtMiss);
          } ) );
  }


  makeTaggedPlot(cut, Vital,
      TH1Wrapper("sumMtMET",
         [] () -> TH1* {return new TH1F("sumMtMET", "sum_mt_met", 250, 0, 2500); }, // unusual binning to be consistent with previous histograms 
         [this] (TH1Wrapper& th1) {th1.fill(event.sum_mt_met); } ) );

//  makeTaggedPlot(cut, Vital,
//      TH1Wrapper("dPhiBoost",
//         [] () -> TH1* {return new TH1F("dPhiBoost", "dPhiBoost", 20, 0, 4); }, // unusual binning to be consistent with previous histograms 
//         [this] (TH1Wrapper& th1) {th1.fill(event.dPhiBoost); } ) );
//
//  makeTaggedPlot(cut, Vital,
//      TH1Wrapper("max_mt_met",
//         [] () -> TH1* {return new TH1F("max_mt_met", "max_mt_met", 250, 0, 2500); },
//         [this] (TH1Wrapper& th1) {th1.fill(event.max_mt_met); } ) );

  makeTaggedPlot(cut, Vital,
      TH2Wrapper("max_mt_met_v_mt2",
         [] () -> TH2* {return new TH2F("max_mt_met_v_mt2", "max_mt_met_v_mt2;max_mt_met;mt2", 250, 0., 2500., 250, 0.0, 500.); },
         [this] (TH2Wrapper& th2) {th2.fill(event.max_mt_met, event.squirrel_leptons.MT2); } ) );

  makeTaggedPlot(cut, Vital,
      TH2Wrapper("sumljpt_v_metsig",
         [] () -> TH2* {return new TH2F("sumljpt_v_metsig", "sumljpt_v_metsig;sumljpt;metsig", 250, 0., 2500., 100, 0., 100.); },
         [this] (TH2Wrapper& th2) {  
          if (event.loose_leptons.size() >1) th2.fill( event.sum_pt_leps_jet1, event.EtMissSignificance );
         } ) );

  makeTaggedPlot(cut, Vital,
      TH2Wrapper("metsig_v_mt2",
         [] () -> TH2* {return new TH2F("metsig_v_mt2", "metsig_v_mt2;metsig;mt2", 500, 0., 100., 100, 0., 500.); },
         [this] (TH2Wrapper& th2) { th2.fill( event.EtMissSignificance, event.squirrel_leptons.MT2 ); } ) );

//  makeTaggedPlot(cut, Vital,
//      TH2Wrapper("lep1Pt_EtMiss",
//         [] () -> TH2* {return new TH2F("lep1Pt_EtMiss", "lep1Pt_EtMiss;lep1Pt;EtMiss", 200, 0., 2000., 200, 0., 2000.); },
//         [this] (TH2Wrapper& th2) {
//           if (event.loose_leptons.size() > 1) th2.fill(event.loose_leptons.at(0).get().Pt, event.EtMiss);
//         } ) );

//  makeTaggedPlot(cut, Vital,
//      TH1Wrapper("sumMtJet1",
//         [] () -> TH1* {return new TH1F("sumMtJet1", "sumMtJet1", 250, 0, 5000); },
//         [this] (TH1Wrapper& th1) {th1.fill(event.sum_mt_jet1); } ) );

  makeTaggedPlot(cut, Vital,
      TH1Wrapper("sum_mass_lepton_jet1",
         [] () -> TH1* {return new TH1F("sum_mass_lepton_jet1", "sum_mass_lepton_jet1", 500, 0, 10000); },
         [this] (TH1Wrapper& th1) {th1.fill(event.sum_mass_lepton_jet1); } ) );

  makeTaggedPlot(cut, Vital,
      TH1Wrapper("mass_electron_jet1",
         [] () -> TH1* {return new TH1F("mass_electron_jet1", "mass_electron_jet1", 500, 0, 5000); },
         [this] (TH1Wrapper& th1) {th1.fill(event.mass_electron_jet1); } ) );

  makeTaggedPlot(cut, Vital,
      TH1Wrapper("mass_muon_jet1",
         [] () -> TH1* {return new TH1F("mass_muon_jet1", "mass_muon_jet1", 500, 0, 5000); },
         [this] (TH1Wrapper& th1) {th1.fill(event.mass_muon_jet1); } ) );

//  makeTaggedPlot(cut, Vital,
//      TH1Wrapper("max_mt_jet1",
//         [] () -> TH1* {return new TH1F("max_mt_jet1", "max_mt_jet1", 250, 0, 2500); },
//         [this] (TH1Wrapper& th1) {th1.fill(event.max_mt_jet1); } ) );
//
//  makeTaggedPlot(cut, Vital,
//      TH1Wrapper("max_mass_lepton_jet1",
//         [] () -> TH1* {return new TH1F("max_mass_lepton_jet1", "max_mass_lepton_jet1", 250, 0, 5000); },
//         [this] (TH1Wrapper& th1) {th1.fill(event.max_mass_lepton_jet1); } ) );


  // Diagnostic , could add a flag to turn these off 
  /********
  makeTaggedPlot(cut, Vital,
      TH1Wrapper("count_loose_electrons", 
        [] () -> TH1* {return new TH1F("count_loose_electrons", "count_loose_electrons", 10, 0, 10);},
        [this] (TH1Wrapper& th1) {th1.fill(event.count_loose_electrons); } ) );

  makeTaggedPlot(cut, Vital,
      TH1Wrapper("count_loose_muons", 
        [] () -> TH1* {return new TH1F("count_loose_muons", "count_loose_muons", 10, 0, 10);},
        [this] (TH1Wrapper& th1) {th1.fill(event.count_loose_muons); } ) );

  makeTaggedPlot(cut, Vital,
      TH1Wrapper("count_loose_leptons", 
        [] () -> TH1* {return new TH1F("count_loose_leptons", "count_loose_leptons", 10, 0, 10);},
        [this] (TH1Wrapper& th1) {th1.fill(event.count_loose_muons + event.count_loose_electrons); } ) );

  makeTaggedPlot(cut, Vital,
      TH1Wrapper("electronCharge", 
        [] () -> TH1* {return new TH1F("electronCharge", "electronCharge", 3, -1, 2);},
        [this] (TH1Wrapper& th1) {for (const auto& el : event.loose_electrons) th1.fill(el.charge); } ) );

  makeTaggedPlot(cut, Vital,
      TH1Wrapper("muonCharge", 
        [] () -> TH1* {return new TH1F("muonCharge", "muonCharge", 3, -1, 2);},
        [this] (TH1Wrapper& th1) {for (const auto& mu : event.loose_muons) th1.fill(mu.charge); } ) );
        ************************/

  // Squirrel plots


  // possibly more interesting squirrel plots
  makeTaggedPlot(cut, Squirrel,
      TH1Wrapper("MT2Leptons",
        [] () -> TH1* {return new TH1F("MT2Leptons", "MT2", 250, 0, 500); },
        [this] (TH1Wrapper& th1) {th1.fill(event.squirrel_leptons.MT2);} ) );
  /****************************************
  makeTaggedPlot(cut, Squirrel,
      TH1Wrapper("M2TLeptons",
        [] () -> TH1* {return new TH1F("M2TLeptons", "M2T", 250, 0, 500); },
        [this] (TH1Wrapper& th1) {th1.fill(event.squirrel_leptons.M2T);} ) );
  makeTaggedPlot(cut, Squirrel,
      TH1Wrapper("Mo2Leptons",
        [] () -> TH1* {return new TH1F("Mo1Leptons", "Mo2", 250, 0, 500); },
        [this] (TH1Wrapper& th1) {th1.fill(event.squirrel_leptons.Mo2);} ) );
  makeTaggedPlot(cut, Squirrel,
      TH1Wrapper("M2oLeptons",
        [] () -> TH1* {return new TH1F("M2oLeptons", "M2o", 250, 0, 500); },
        [this] (TH1Wrapper& th1) {th1.fill(event.squirrel_leptons.M2o);} ) );
  makeTaggedPlot(cut, Squirrel,
      TH1Wrapper("MCTLeptons",
        [] () -> TH1* {return new TH1F("MCTLeptons", "M2o", 250, 0, 500); },
        [this] (TH1Wrapper& th1) {th1.fill(event.squirrel_leptons.MCT);} ) );

  for(int ii=0; ii<6; ++ii){
    std::string plot_name = "AsymMT2Leptons_"+std::to_string(ii);
    makeTaggedPlot(cut, Squirrel,
       TH1Wrapper(plot_name, 
          [plot_name, ii] () -> TH1* {return new TH1F(plot_name.c_str(), "aMT2", 400, 0, 2000); },
          [this, ii] (TH1Wrapper& th1) {th1.fill(event.squirrel_leptons.aMT2.at(ii));} ) );
  }


  makeTaggedPlot(cut, Squirrel,
      TH1Wrapper("M2TAll",
        [] () -> TH1* {return new TH1F("M2TAll", "M2T", 250, 0, 2500); },
        [this] (TH1Wrapper& th1) {th1.fill(event.squirrel_all.M2T);} ) );
  makeTaggedPlot(cut, Squirrel,
      TH1Wrapper("MT2All",
        [] () -> TH1* {return new TH1F("MT2All", "MT2", 250, 0, 2500); },
        [this] (TH1Wrapper& th1) {th1.fill(event.squirrel_all.MT2);} ) );
  makeTaggedPlot(cut, Squirrel,
      TH1Wrapper("Mo2All",
        [] () -> TH1* {return new TH1F("Mo1All", "Mo2", 250, 0, 2500); },
        [this] (TH1Wrapper& th1) {th1.fill(event.squirrel_all.Mo2);} ) );
  makeTaggedPlot(cut, Squirrel,
      TH1Wrapper("M2oAll",
        [] () -> TH1* {return new TH1F("M2oAll", "M2o", 250, 0, 2500); },
        [this] (TH1Wrapper& th1) {th1.fill(event.squirrel_all.M2o);} ) );
  makeTaggedPlot(cut, Squirrel,
      TH1Wrapper("MCTAll",
        [] () -> TH1* {return new TH1F("MCTAll", "MCT", 250, 0, 2500); },
        [this] (TH1Wrapper& th1) {th1.fill(event.squirrel_all.MCT);} ) );

  // possibly less interesting squirrel plots 
  makeTaggedPlot(cut, Squirrel,
      TH1Wrapper("Mside1",
        [] () -> TH1* {return new TH1F("Mside1", "M_side1", 100, 0, 2500); },
        [this] (TH1Wrapper& th1) {th1.fill(event.squirrel_all.M_side1);} ) );
  makeTaggedPlot(cut, Squirrel,
      TH1Wrapper("Mside2",
        [] () -> TH1* {return new TH1F("Mside2", "M_side2", 100, 0, 2500); },
        [this] (TH1Wrapper& th1) {th1.fill(event.squirrel_all.M_side2);} ) );
  makeTaggedPlot(cut, Squirrel,
      TH1Wrapper("sumMside",
        [] () -> TH1* {return new TH1F("sumMside", "Sum of side masses", 100, 0, 5000); },
        [this] (TH1Wrapper& th1) {th1.fill(event.squirrel_all.M_side1+event.squirrel_all.M_side2);} ) );
  makeTaggedPlot(cut, Squirrel,
      TH1Wrapper("asymMside",
        [] () -> TH1* {return new TH1F("asymMside", "Asymmetry of side masses", 100, 0, 1); },
        [this] (TH1Wrapper& th1) {th1.fill(fabs(event.squirrel_all.M_side1-event.squirrel_all.M_side2)/(event.squirrel_all.M_side1+event.squirrel_all.M_side2));} ) );
  makeTaggedPlot(cut, Squirrel,
      TH1Wrapper("minMside",
        [] () -> TH1* {return new TH1F("minMside", "minimum side mass", 100, 0, 2500); },
        [this] (TH1Wrapper& th1) {th1.fill(std::min(event.squirrel_all.M_side1,event.squirrel_all.M_side2));} ) );
  makeTaggedPlot(cut, Squirrel,
      TH2Wrapper("Mside1_Mside2",
        [] () -> TH2* {return new TH2F("Mside1_Mside2", "M_side1 vs M_side2", 100, 0, 2500, 100, 0, 2500); },
        [this] (TH2Wrapper& th2) {th2.fill(event.squirrel_all.M_side1,event.squirrel_all.M_side2);} ) );
  makeTaggedPlot(cut, Squirrel,
      TH2Wrapper("Nside1_Nside2",
        [] () -> TH2* {return new TH2F("Nside1_Nside2", "Nvis_side1 vs Nvis_side2", 10, 0, 10, 10, 0, 10); },
        [this] (TH2Wrapper& th2) {th2.fill(event.squirrel_all.Nvis_side1,event.squirrel_all.Nvis_side2);} ) );
  makeTaggedPlot(cut, Squirrel,
      TH2Wrapper("minMside_sumMside",
        [] () -> TH2* {return new TH2F("minMside_sumMside", "min(Mside) vs sum(Mside)", 100, 0, 2500, 100, 0, 5000); },
        [this] (TH2Wrapper& th2) {th2.fill(event.squirrel_all.M_side1+event.squirrel_all.M_side2,
          std::min(event.squirrel_all.M_side1,event.squirrel_all.M_side2));} ) );
  makeTaggedPlot(cut, Squirrel,
      TH2Wrapper("asymMside_sumMside",
        [] () -> TH2* {return new TH2F("asymMside_sumMside", "asym(Mside) vs sum(Mside)", 100, 0, 5000, 100, 0, 1); },
        [this] (TH2Wrapper& th2) {th2.fill(event.squirrel_all.M_side1+event.squirrel_all.M_side2,
          fabs(event.squirrel_all.M_side1-event.squirrel_all.M_side2)/(event.squirrel_all.M_side1+event.squirrel_all.M_side2));} ) );

  *****************************************/

}
