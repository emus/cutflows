#include "ReadTree/EventStruct.h"

// FakeBkgTool 
#include "xAODBase/IParticle.h"
#include "xAODBase/IParticleContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/ElectronAuxContainer.h"
#include "AthContainers/ConstDataVector.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonAuxContainer.h"


//enum class LLSR 
//{
//}

StatusCode EventStruct::InitializeFakeBkgTools(){

  ///////////////////////////
  // Initialize the IFF tools
  ///////////////////////////
  
  std::cout << "About to initialize the FakeBkgTool ... " << std::endl;


  // configure the input file for the MM tool 
  //std::string fakeInputFile = "/r10/atlas/emus/fakes/efficiencies/2020-10-21_efficiencies/";
  //std::string fakeInputFile = "/r10/atlas/emus/fakes/efficiencies/2019-09-09_efficiencies/";
  //std::string fakeInputFile = "/r10/atlas/emus/fakes/efficiencies/2021-01-21_efficiencies/";
  std::string fakeInputFile = "/r10/atlas/emus/fakes/efficiencies/2021-01-27_efficiencies/"; // same as 21st Jan, except with finer binning 
  if(fakeSyst == "nominal") fakeInputFile += "nominal_RealFakeEff2D.root";
  //else if(fakeSyst == "upper") fakeInputFile += "upper_bound_RealFakeEff2D.root";
  //else if(fakeSyst == "lower") fakeInputFile += "lower_bound_RealFakeEff2D.root";
  else if(fakeSyst == "upper") fakeInputFile += "upper_syst_RealFakeEff2D.root";
  else if(fakeSyst == "lower") fakeInputFile += "lower_syst_RealFakeEff2D.root";
  else{
    std::cerr << "FATAL: matrix method efficiency file not found in path: " << fakeInputFile << std::endl;
    throw std::invalid_argument("Make sure the argument to fakeSyst is one of: nominal, upper or lower.");
  }

  std::cout << "InitializeFakeBkgTools:: reading " << fakeSyst << " input efficiency file: " << fakeInputFile << std::endl;

  ///////////////////////////
  // First the asymptotic tool
  ///////////////////////////
  std::cout << "Asym tool" << std::endl;
  ATH_CHECK(m_asmTool.setProperty("InputFiles", std::vector<std::string>{fakeInputFile}));
  // would need to check the nomenclature 
  ATH_CHECK(m_asmTool.setProperty("Selection", "=2T")); /// region of interest = events with >= 1 tight lepton
  ATH_CHECK(m_asmTool.setProperty("Process", ">=1F")); /// fake lepton background = all events with >= 1 fake lepton (tight, by default)
  //ATH_CHECK(m_asmTool.setProperty("EnergyUnit", "MeV")); // energy units are actually GeV, but this option is for the case where one uses xAOD electrons with MeV and has efficiencies in GeV, and therefore performs a conversion. We don't want a conversion.
  ATH_CHECK(m_asmTool.initialize());

  
  InitialiseLikelihoodMMTool( &m_lhmTool, "ePmuM", fakeInputFile );

  LL_event_count = 0;

  return StatusCode::SUCCESS;
}

StatusCode EventStruct::InitialiseLikelihoodMMTool(CP::LhoodMM_tools* lhmTool, std::string tag, std::string  fakeInputFile){

  std::cout << " LL tool for " << tag << std::endl;

  ATH_CHECK(lhmTool->setProperty("InputFiles", std::vector<std::string>{fakeInputFile}));

  ///////////////////////////////////////////////////////////////
  // Settings for the LL MM tool
  ///////////////////////////////////////////////////////////////
  // Default settings (estimating the fake yield in the SRs)
  // Selection : =2T
  // Process : >=1F (this is equivalent to >=1F[T], that is the fakes need to be tight)
  //
  // Settings for VR estimation (work in progress)
  // Selection : >=0T (or something to represent no selection)
  // Process : >=1F[L] (number of loose fake leptons ... is this "loose" or "anti-tight")
  //
  //
  // Note, the point of the process parameter is to fine-tune what is meant by a fake lepton in the region you are estimating for. 
  // Note, the selection parameter is somewhat irrelevnt, since we only pass the leptons to the tool that we're interested in.
  ///////////////////////////////////////////////////////////////
  
  std::cout << "LL SR code chosen: " << LLSR << std::endl;

  std::string selection = "";
  std::string process = "";
  if(LLSR == 7 || LLSR == 8 || LLSR == 14 || LLSR == 15 || LLSR==24 || LLSR==25 || LLSR==26){ // not sure if this is quite rught for LLSR:15
    // This selection needed for validation regions, which are loose
    selection = ">=0T";
    process   = ">=1F[L]";
  }
  else{
    selection = ">=2T";
    process = ">=1F";
  }
  std::cout << "Using selection: " << selection << "\t and pocess: " << process << " for LL MM fake estimate." <<  std::endl;
  ATH_CHECK(lhmTool->setProperty("Selection", selection));
  ATH_CHECK(lhmTool->setProperty("Process", process)); 


  // Energy units are actually GeV, but this option is for the case where one uses xAOD electrons with MeV and has efficiencies in GeV, and therefore performs a conversion. 
  // We don't want a conversion, do don't use this!
  // ATH_CHECK(lhmTool->setProperty("EnergyUnit", "MeV")); 


  // This forces the integral of the output histograms to be the same as the total yield. They aren't by default (a result of how the LL MM works)
  ATH_CHECK(lhmTool->setProperty("FixHistogramNormalization", true)); 


  ATH_CHECK(lhmTool->initialize());

  // register histogram for LL tool
  LL_lep1Pt = new TH1F("LL_lep1Pt", "LL_lep1Pt", 200, 0, 2000);
  lhmTool->register1DHistogram(LL_lep1Pt, &m_lep1Pt);
  LL_ele1Pt = new TH1F("LL_ele1Pt", "LL_ele1Pt", 200, 0, 2000);
  lhmTool->register1DHistogram(LL_ele1Pt, &m_ele1Pt);
  LL_muon1Pt = new TH1F("LL_muon1Pt", "LL_muon1Pt", 200, 0, 2000);
  lhmTool->register1DHistogram(LL_muon1Pt, &m_muon1Pt);
  LL_sumMT_histo = new TH1F("LL_sumMtMET", "LL_sumMtMET", 250, 0, 2500);
  lhmTool->register1DHistogram(LL_sumMT_histo, &m_sum_mt_met);
  

  // 2D histos with channel 
  LL_EventCount_channel = new TH2F("LL_EventCount_channel", "LL_EventCount_channel", 1, 1, 2, 2,0,2);
  lhmTool->register2DHistogram(LL_EventCount_channel, &m_EventCount, &m_channel);
  //
  LL_lep1Pt_channel = new TH2F("LL_lep1Pt_channel", "LL_lep1Pt_channel", 200, 0, 2000, 2,0,2);
  lhmTool->register2DHistogram(LL_lep1Pt_channel, &m_lep1Pt, &m_channel);
  //
  LL_lep2Pt_channel = new TH2F("LL_lep2Pt_channel", "LL_lep2Pt_channel", 100, 0, 2000, 2, 0,2);
  lhmTool->register2DHistogram(LL_lep2Pt_channel, &m_lep2Pt, &m_channel);
  //
  LL_ele1Pt_channel = new TH2F("LL_ele1Pt_channel", "LL_ele1Pt_channel", 400, 0, 2000, 2,0,2);
  lhmTool->register2DHistogram(LL_ele1Pt_channel, &m_ele1Pt, &m_channel);
  //
  LL_muon1Pt_channel = new TH2F("LL_muon1Pt_channel", "LL_muon1Pt_channel", 200, 0, 2000, 2,0,2);
  lhmTool->register2DHistogram(LL_muon1Pt_channel, &m_muon1Pt, &m_channel);
  //
  LL_sumMT_channel = new TH2F("LL_sumMT_channel", "LL_sumMT_channel", 125, 0, 2500, 2,0,2);
  lhmTool->register2DHistogram(LL_sumMT_channel, &m_sum_mt_met, &m_channel);
  //
  LL_MT2_channel = new TH2F("LL_MT2_channel", "LL_MT2_channel", 250, 0, 500, 2,0,2);
  lhmTool->register2DHistogram(LL_MT2_channel, &m_mt2, &m_channel);
  //
  LL_jet1Pt_channel = new TH2F("LL_jet1Pt_channel", "LL_jet1Pt_channel", 125, 0, 2500, 2, 0,2);
  lhmTool->register2DHistogram(LL_jet1Pt_channel, &m_jet1Pt, &m_channel);
  //
  LL_jet2Pt_channel = new TH2F("LL_jet2Pt_channel", "LL_jet2Pt_channel", 125, 0, 2500, 2, 0,2);
  lhmTool->register2DHistogram(LL_jet2Pt_channel, &m_jet2Pt, &m_channel);
  //
  LL_EtMissSignificance_channel = new TH2F("LL_EtMissSignificance_channel", "LL_EtMissSignificance_channel", 500, 0, 100, 2, 0,2);
  lhmTool->register2DHistogram(LL_EtMissSignificance_channel, &m_metsig, &m_channel);
  //
  LL_EtMiss_channel = new TH2F("LL_EtMiss_channel", "LL_EtMiss_channel", 100, 0, 1000, 2, 0,2);
  lhmTool->register2DHistogram(LL_EtMiss_channel, &m_met, &m_channel);
  //
  LL_sum_pt_leps_jet1_channel = new TH2F("LL_sum_pt_leps_jet1_channel", "LL_sum_pt_leps_jet1_channel", 250, 0, 2500, 2, 0,2);
  lhmTool->register2DHistogram(LL_sum_pt_leps_jet1_channel, &sum_pt_leps_jet1, &m_channel);


  LL_max_mt_met_channel = new TH2F("LL_max_mt_met_channel", "LL_max_mt_met_channel", 250, 0, 2500, 2, 0, 2);
  lhmTool->register2DHistogram(LL_max_mt_met_channel, &max_mt_met, &m_channel);
  //
  LL_lep1Phi_channel = new TH2F("LL_lep1Phi_channel", "LL_lep1Phi_channel", 128, -3.2, 3.2, 2, 0, 2);
  lhmTool->register2DHistogram(LL_lep1Phi_channel, &m_lep1Phi, &m_channel);
  //
  LL_lep2Phi_channel = new TH2F("LL_lep2Phi_channel", "LL_lep2Phi_channel", 128, -3.2, 3.2, 2, 0, 2);
  lhmTool->register2DHistogram(LL_lep2Phi_channel, &m_lep2Phi, &m_channel);
  //
  LL_jet1Phi_channel = new TH2F("LL_jet1Phi_channel", "LL_jet1Phi_channel", 128, -3.2, 3.2, 2, 0, 2);
  lhmTool->register2DHistogram(LL_jet1Phi_channel, &m_jet1Phi, &m_channel);
  // 
  LL_jet1Eta_channel = new TH2F("LL_jet1Eta_channel", "LL_jet1Eta_channel", 250, -4, 4, 2, 0, 2);
  lhmTool->register2DHistogram(LL_jet1Eta_channel, &m_jet1Eta, &m_channel);
  //
  LL_lep1Eta_channel = new TH2F("LL_lep1Eta_channel", "LL_lep1Eta_channel", 250, -4, 4, 2, 0, 2); 
  lhmTool->register2DHistogram(LL_lep1Eta_channel, &m_lep1Eta, &m_channel);
  //
  LL_lep2Eta_channel = new TH2F("LL_lep2Eta_channel", "LL_lep2Eta_channel", 250, -4, 4, 2, 0, 2);
  lhmTool->register2DHistogram(LL_lep2Eta_channel, &m_lep2Eta, &m_channel);
  // - 1
  //
  //LL_sumMtJet1_channel = new TH2F("LL_sumMtJet1_channel", "LL_sumMtJet1_channel", 250, 0, 5000, 2, 0, 2);
  LL_sumMtJet1_channel = new TH2F("LL_sumMtJet1_channel", "LL_sumMtJet1_channel", 250, 0, 5000, 2, 0, 2);
  lhmTool->register2DHistogram(LL_sumMtJet1_channel, &m_sum_mt_jet1, &m_channel);
  //
  LL_sum_mass_lepton_jet1_channel = new TH2F("LL_sum_mass_lepton_jet1_channel", "LL_sum_mass_lepton_jet1_channel", 500, 0, 10000, 2, 0, 2);
  lhmTool->register2DHistogram(LL_sum_mass_lepton_jet1_channel, &m_sum_mass_lepton_jet1, &m_channel);
  //
  LL_max_mt_jet1_channel = new TH2F("LL_max_mt_jet1_channel", "LL_max_mt_jet1_channel", 125, 0, 2500, 2, 0, 2);
  lhmTool->register2DHistogram(LL_max_mt_jet1_channel, &m_max_mt_jet1, &m_channel);
  // 
  LL_max_mass_lepton_jet1_channel = new TH2F("LL_max_mass_lepton_jet1_channel", "LL_max_mass_lepton_jet1_channel", 250, 0, 5000, 2, 0, 2);
  lhmTool->register2DHistogram(LL_max_mass_lepton_jet1_channel, &m_max_mass_lepton_jet1, &m_channel);
  //
  LL_NJets_channel = new TH2F("LL_NJets_channel", "LL_NJets_channel", 50, 0, 50, 2, 0, 2);
  lhmTool->register2DHistogram(LL_NJets_channel, &m_NJets, &m_channel);
  
  // 3D histograms 
  LL_lep1Pt_EtMiss_channel = new TH3F("LL_lep1Pt_EtMiss_channel", "LL_lep1Pt_EtMiss_channel;lep1Pt;EtMiss", 200, 0, 2000, 200, 0, 2000, 2, 0, 2);
  lhmTool->register3DHistogram(LL_lep1Pt_EtMiss_channel, &m_lep1Pt, &m_met, &m_channel); 

  LL_HTjets_EtMiss_channel = new TH3F("LL_HTjets_EtMiss_channel", "LL_HTjets_EtMiss_channel;HTjets;EtMiss", 200, 0, 4000, 200, 0, 2000, 2, 0, 2);
  lhmTool->register3DHistogram(LL_HTjets_EtMiss_channel, &HT_jets, &m_met, &m_channel); 

  LL_HTall_EtMiss_channel = new TH3F("LL_HTall_EtMiss_channel", "LL_HTall_EtMiss_channel;HTall;EtMiss", 200, 0, 4000, 200, 0, 2000, 2, 0, 2);
  lhmTool->register3DHistogram(LL_HTall_EtMiss_channel, &HT_all, &m_met, &m_channel); 

  LL_max_mt_met_v_mt2_channel = new TH3F("LL_max_mt_met_v_mt2_channel", "LL_max_mt_met_v_mt2_channel;max_mt_met;MT2",  250., 0., 2500., 250, 0.0, 500., 2, 0, 2);
  lhmTool->register3DHistogram(LL_max_mt_met_v_mt2_channel, &max_mt_met, &m_mt2, &m_channel); 

  LL_sumljpt_v_metsig_channel = new TH3F("LL_sumljpt_v_metsig_channel", "LL_sumljpt_v_metsig_channel;sumljpt;metsig",  250., 0., 2500., 100, 0.0, 100., 2, 0, 2);
  lhmTool->register3DHistogram(LL_sumljpt_v_metsig_channel, &sum_pt_leps_jet1, &m_metsig, &m_channel);

  LL_metsig_v_mt2_channel = new TH3F("LL_metsig_v_mt2_channel", "LL_metsig_v_mt2_channel;metsig;mt2",  500, 0., 100., 100, 0., 500., 2, 0, 2);
  lhmTool->register3DHistogram(LL_metsig_v_mt2_channel, &m_metsig, &m_mt2, &m_channel); 


  // separated by charge assignment 
  LL_lep1Pt_ePmuM = new TH1F("LL_lep1pt_ePmuM", "LL_lep1pt_ePmuM", 200, 0, 2000);
  lhmTool->register1DHistogram(LL_lep1Pt_ePmuM, &m_lep1Pt_ePmuM);
  LL_lep1Pt_eMmuP = new TH1F("LL_lep1pt_eMmuP", "LL_lep1pt_eMmuP", 200, 0, 2000);
  lhmTool->register1DHistogram(LL_lep1Pt_eMmuP, &m_lep1Pt_eMmuP);

  LL_yield = new TH1F("LL_yield", "LL_yield", 2,0,2);
  lhmTool->register1DHistogram(LL_yield, &m_channel);

  std::cout << "Finished initializing the FakeBkgTool" << std::endl;

  return StatusCode::SUCCESS;

}


StatusCode EventStruct::ExecuteMatrixMethod(){

  // IFF tool provides normal matrix method (aka asymptotic) as well as the likelyhood matrix method. 
  // m_asmTool : asymptotic matrix method, sets m_IFF_fake_weight
  // m_lhmTool : likelyhood matrix method 

  m_IFF_fake_weight = 1.0;

  // note channel here refers to charge assignment (ePmuM or eMmuP)
  m_channel = -1;
  m_lep1Pt_ePmuM = -1;
  m_lep1Pt_eMmuP = -1;

  // make sure there are exactly 2 leptons
  if(loose_leptons.size() != 2){
    return StatusCode::SUCCESS;
  }

  // make sure there is 1 electron and 1 muon
  if(!(loose_muons.size() == 1 and loose_electrons.size() == 1)){
    return StatusCode::SUCCESS;
  }

  // my leptons
  PO::Electron& ele = loose_electrons.at(0);
  PO::Muon& mu    = loose_muons.at(0);


  if(LLSR == 14 || LLSR==15 || LLSR==24){
    // attempt at fake validation regions
    // Make sure leptons have the same charge
    if(ele.charge != mu.charge) return StatusCode::SUCCESS; 
  
  }
  else{
    // make sure leptons have opposite charge! 
    if(ele.charge == mu.charge) return StatusCode::SUCCESS; 
  }



  xAOD::IParticleContainer particles(SG::VIEW_ELEMENTS);

  // Set electron properties
  xAOD::Electron* elePtr = new xAOD::Electron(); 
  elePtr->makePrivateStore();
  elePtr->setPt( ele.Pt );
  elePtr->setEta( ele.Eta );
  elePtr->setPhi( ele.Phi );
  elePtr->setCharge( ele.charge ); 
  elePtr->auxdata<char>("Tight") = ele.isTight; 
  elePtr->auxdata<float>("charge") = ele.charge;
  particles.push_back(static_cast<xAOD::IParticle*>(elePtr));

  // Set  muon properties
  xAOD::Muon* muonPtr = new xAOD::Muon();
  muonPtr->makePrivateStore();
  muonPtr->setP4( mu.Pt, mu.Eta, mu.Phi );
  muonPtr->setCharge( mu.charge ); 
  muonPtr->auxdata<char>("Tight") = mu.isTight; 
  muonPtr->auxdata<float>("charge") = mu.charge;
  particles.push_back(static_cast<xAOD::IParticle*>(muonPtr)); 

  ///////////////
  // WARNING: don't want any return statements until the particle vector has been cleared!
  ///////////////

  ///////////////////////////
  // asymptotic matrix method     
  ///////////////////////////
  ATH_CHECK(m_asmTool->addEvent( particles ) );
  if(m_asmTool->getEventWeight(m_IFF_fake_weight, "2T", ">=1F") != StatusCode::SUCCESS) { 
    std::cout << "ERROR: AsymptMatrixTool::getEventWeightAndUncertainties() failed" << std::endl;
    exit(2); 
  }


  ///////////////////////////
  // likelyhood matrix method
  ///////////////////////////

  // Set variables for LL MM plots 
  m_EventCount = 1.0; // set and never changed, only added if the histogram is filled 
  m_lep1Pt = loose_leptons.at(0).get().Pt;
  m_lep2Pt = loose_leptons.at(1).get().Pt;
  m_lep1Phi = loose_leptons.at(0).get().Phi;
  m_lep2Phi = loose_leptons.at(1).get().Phi;
  m_lep1Eta = loose_leptons.at(0).get().Eta;
  m_lep2Eta = loose_leptons.at(1).get().Eta;

  m_muon1Pt = mu.Pt;
  m_ele1Pt = ele.Pt; 

  m_sum_mt_met = sum_mt_met;
  m_max_mt_met = max_mt_met;

  m_mt2 = squirrel_leptons.MT2; 

  m_metsig = EtMissSignificance;
  m_met = EtMiss;

  m_NJets = signalJets.size();

  if(signalJets.size()>0){
    m_jet1Pt = signalJets.at(0).Pt;
    m_jet1Phi = signalJets.at(0).Phi;
    m_jet1Eta = signalJets.at(0).Eta;
    m_sum_mt_jet1 = sum_mt_jet1;
    m_sum_mass_lepton_jet1 = sum_mass_lepton_jet1;
    m_max_mt_jet1 = max_mt_jet1;
    m_max_mass_lepton_jet1 = max_mass_lepton_jet1;
  }
  else{
    m_jet1Pt = -999; 
    m_jet1Phi = -999;
    m_jet1Eta = -999;
    m_sum_mt_jet1 = -999;
    m_sum_mass_lepton_jet1 = -999;
    m_max_mt_jet1 = -999;
    m_max_mass_lepton_jet1 = -999;
  }

  if(signalJets.size()>1) m_jet2Pt = signalJets.at(1).Pt;
  else m_jet2Pt = -1;

  // Additional selection for LL tool

  // make sure the leptons are both tight! 
  // Note: DON'T want this cut, this is taken into account by the "selection" part of the LL MM tool 
  //if(!ele.isTight) return StatusCode::SUCCESS;
  //if(!mu.isTight)  return StatusCode::SUCCESS;


  // e+ mu-
  bool iseMmuP(false);
  if(ele.charge > 0 && mu.charge < 0){
    m_channel = 0.5;
    m_lep1Pt_ePmuM = m_lep1Pt;
  }

  // e- mu+
  if(ele.charge < 0 && mu.charge > 0){
    m_channel = 1.5; 
    m_lep1Pt_eMmuP = m_lep1Pt;
  }

  if(LLSR == 14 || LLSR == 15 || LLSR==24){
    // for SS leptons
    if(ele.charge < 0 && mu.charge < 0){
      m_channel = 0.5;
    }
    else if(ele.charge > 0 && mu.charge > 0){
      m_channel = 1.5;
    }
    else{
      std::cerr << "ERROR: wrong charge assignment for SS leptons" << std::endl;
      //throw std::invalid_argument("bad charge assignment"); 
      exit(1);
    }
  }

  float sumMT_cut = 250;

  // SR-MET 
  bool isSRMET(false);
  bool isVRMET(false);
  if(m_sum_mt_met > sumMT_cut){
    isSRMET = true;
    isVRMET = true;
  }

  // SR-JET
  bool isSRJET(false);
  if(m_sum_mt_met > sumMT_cut){
    //if(
    //(signalJets.size() == 1) || 
    //(signalJets.size()>=2 && signalJets.at(1).Pt<300)
    //){ 
    // WJF 2020-11-26: change of definition of regions
    if(signalJets.size() >= 1) isSRJET = true;
  }

  // SR-RPV-1
  bool isSRRPV(false);
  //if(m_sum_mt_met > 200 && m_mt2>120 && m_max_mt_met>220.0){
  // WJF 2020-11-26: change of definition of regions
  if(m_sum_mt_met > sumMT_cut && m_mt2 > 120 && m_metsig > 10){
    isSRRPV = true;
  }

  // SR-LQ
  bool isSRLQ1(false);
  bool isSRLQ2(false);
  // SRLQ2 no longer used, it would seem. 
  if(m_sum_mt_met > sumMT_cut){
    if( 
      ((signalJets.size() > 1 && signalJets.at(1).Pt<300.0 ) || signalJets.size()==1)
      && m_metsig < 6 
      && sum_pt_leps_jet1>1000
      ){
      isSRLQ1 = true;
    }
  }

  bool isCRRatio(false);
  bool isVRRatio(false);
  if( m_sum_mt_met < 200){
    isVRRatio = true;
    if( m_lep1Pt < 400) isCRRatio = true;
  }

  // WJF 2020-11-26: change of definition of regions
  bool isCRJET(false);
  if(m_sum_mt_met < 200 && m_lep1Pt < 400 && signalJets.size() >= 1){
      isCRJET = true;
  }

  // Will B regions 
  bool isWillB1(false), isWillB2(false), isWillB3(false), isWillB4(false);
  if(m_sum_mt_met > sumMT_cut && m_max_mt_met > 220 && m_mt2 > 120) isWillB1 = true;
  if(m_sum_mt_met > sumMT_cut && m_max_mt_met < 220 && m_mt2 > 120) isWillB2 = true;
  if(m_sum_mt_met > sumMT_cut && m_max_mt_met > 220 && m_mt2 < 120) isWillB3 = true;
  if(m_sum_mt_met > sumMT_cut && m_max_mt_met < 220 && m_mt2 < 120) isWillB4 = true;

  // Fake validation regions (note implicit tagged muon requirement)
  //
  bool isOneLooseElectron(false); // two different flavour, same sign leptons. One tagged muon 
  bool isOneLooseMuon(false); // two different flavour, same sign leptons. One tagged muon 
  bool isTwoTightLeptons(false); // isOneLoose and one tight electron
  if(mu.isTagged){
    isOneLooseElectron=true;
    if(ele.isTight) isTwoTightLeptons = true;
  }

  if(ele.isTight) isOneLooseMuon=true;

  // Holly's new regions
  bool is_RPV_MT2100_METSig10(false), is_RPV_MT2100_10METSig(false), is_RPV_100MT2_METSig10(false), is_RPV_100MT2_10METSig(false);
  bool is_LQ_6METSig_sumlpt1000(false), is_LQ_METSig6_sumlpt1000(false), is_LQ_6METSig_1000sumlpt(false), is_LQ_METSig6_1000sumlpt(false);
  bool isSecondJetAndLowPt = (signalJets.size() > 1 && signalJets.at(1).Pt < 300);
  if(sum_mt_met > sumMT_cut){
    if(m_mt2 > 100 && m_metsig > 10) is_RPV_MT2100_METSig10 = true;
    if(m_mt2 > 100 && m_metsig < 10) is_RPV_MT2100_10METSig = true;
    if(m_mt2 < 100 && m_metsig > 10) is_RPV_100MT2_METSig10 = true;
    if(m_mt2 < 100 && m_metsig < 10) is_RPV_100MT2_10METSig = true;

    if( m_metsig < 6 && sum_pt_leps_jet1 > 1000 && ( isSecondJetAndLowPt || signalJets.size() == 1) ) is_LQ_6METSig_sumlpt1000 = true;
    if( m_metsig > 6 && sum_pt_leps_jet1 > 1000 && ( isSecondJetAndLowPt || signalJets.size() == 1) ) is_LQ_METSig6_sumlpt1000 = true;
    if( m_metsig < 6 && sum_pt_leps_jet1 < 1000 && ( isSecondJetAndLowPt || signalJets.size() == 1) ) is_LQ_6METSig_1000sumlpt = true;
    if( m_metsig > 6 && sum_pt_leps_jet1 < 1000 && ( isSecondJetAndLowPt || signalJets.size() == 1) ) is_LQ_METSig6_1000sumlpt = true;
  }

  // one lepton exclusive Loose 
  // Two opposite sign different flavour leptons, 
  bool is_OrthoVRMET = false;
  if(sum_mt_met > sumMT_cut){
      if (! (mu.isPerfect && ele.isPerfect) ) is_OrthoVRMET = true;
  }

  bool is_LooseVRMET = false;
  if(sum_mt_met > sumMT_cut){
      if (!mu.isPerfect && !ele.isPerfect) is_LooseVRMET = true;
  }


  // Extra CRs and VRs for the new sumMTMet cut of 250
  bool is_sumMtMET150_CRRatio(false);
  bool is_sumMtMET150_CRJET(false);
  if( m_sum_mt_met < 150 && m_lep1Pt < 400){
      is_sumMtMET150_CRRatio = true;
      if( signalJets.size() >= 1) is_sumMtMET150_CRJET = true;
  }

  bool is_sumMtMET150250_CRRatio(false);
  bool is_sumMtMET150250_CRJET(false);
  if( m_sum_mt_met > 150 && m_sum_mt_met < 250){
    is_sumMtMET150250_CRRatio = true; 
    if(signalJets.size() >= 1) is_sumMtMET150250_CRJET = true;
  }



  bool chosenSR = true;

  if(LLSR == 1) chosenSR = isSRMET;
  else if(LLSR == 2) chosenSR = isSRJET;
  else if(LLSR == 3) chosenSR = isSRRPV;
  else if (LLSR == 4) chosenSR = isSRLQ1;
  else if (LLSR == 5) chosenSR = isSRLQ2;
  else if (LLSR == 6) chosenSR = isCRRatio;
  else if (LLSR == 7) chosenSR = isVRMET;
  else if (LLSR == 8) chosenSR = isVRRatio;
  else if (LLSR == 9) chosenSR = isCRJET;
  else if (LLSR == 10) chosenSR = isWillB1;
  else if (LLSR == 11) chosenSR = isWillB2;
  else if (LLSR == 12) chosenSR = isWillB3;
  else if (LLSR == 13) chosenSR = isWillB4;
  else if (LLSR == 14) chosenSR = isOneLooseElectron; // SS
  else if (LLSR == 15) chosenSR = isTwoTightLeptons; // SS
  //
  else if (LLSR == 16) chosenSR = is_RPV_MT2100_METSig10;
  else if (LLSR == 17) chosenSR = is_RPV_MT2100_10METSig; 
  else if (LLSR == 18) chosenSR = is_RPV_100MT2_METSig10;
  else if (LLSR == 19) chosenSR = is_RPV_100MT2_10METSig;
  //
  else if (LLSR == 20) chosenSR = is_LQ_6METSig_sumlpt1000;
  else if (LLSR == 21) chosenSR = is_LQ_METSig6_sumlpt1000;
  else if (LLSR == 22) chosenSR = is_LQ_6METSig_1000sumlpt;
  else if (LLSR == 23) chosenSR = is_LQ_METSig6_1000sumlpt;
  //
  else if (LLSR == 24) chosenSR = isOneLooseMuon; // SS, for extra fakes validation
  else if (LLSR == 25) chosenSR = is_OrthoVRMET; 
  else if (LLSR == 26) chosenSR = is_LooseVRMET; 
  // New regions with sumMT < 150 for the CRs, 150<sumMT<250 for the VR and sumMT>250 for the SR
  else if (LLSR == 27) chosenSR = is_sumMtMET150_CRRatio;
  else if (LLSR == 28) chosenSR = is_sumMtMET150_CRJET;
  else if (LLSR == 29) chosenSR = is_sumMtMET150250_CRRatio;
  else if (LLSR == 30) chosenSR = is_sumMtMET150250_CRJET;



  // Add SR cut cut
  if(chosenSR){
    LL_event_count += 1;
    ATH_CHECK(m_lhmTool.addEvent( particles ) );
  }

  // After add event, can delete the pointers
  for (xAOD::IParticleContainer::iterator it = particles.begin(); it != particles.end(); it++) {
    if (*it != nullptr) delete *it;
  }
  particles.clear();


  return StatusCode::SUCCESS;
}

std::string get_name(int LLSR){
  if(LLSR ==0) return "TwoTight";
  if(LLSR ==1) return "SRMET";
  if(LLSR ==2) return "SRJET";
  if(LLSR ==3) return "SRRPV";
  if(LLSR ==4) return "SRLQ1";
  if(LLSR ==5) return "SRLQ2";
  if(LLSR ==6) return "CRRatio";
  if(LLSR ==7) return "VRMET";
  if(LLSR ==8) return "VRRatio";
  if(LLSR ==9) return "CRJET";
  // Will B regions
  if(LLSR ==10) return "MaxMtMET220_MT2120";
  if(LLSR ==11) return "220MaxMtMET_MT2120";
  if(LLSR ==12) return "MaxMtMET220_120MT2";
  if(LLSR ==13) return "220MaxMtMET_120MT2";
  // Fake validation regions?
  if(LLSR ==14) return "oneLoose";
  if(LLSR ==15) return "twoTightLeptons";
  // new Holly regions
  if(LLSR == 16) return "RPV_MT2100_METSig10"; 
  if(LLSR == 17) return "RPV_MT2100_10METSig";
  if(LLSR == 18) return "RPV_100MT2_METSig10";
  if(LLSR == 19) return "RPV_100MT2_10METSig";
  //
  if(LLSR == 20) return "LQ_6METSig_sumlpt1000";
  if(LLSR == 21) return "LQ_METSig6_sumlpt1000";
  if(LLSR == 22) return "LQ_6METSig_1000sumlpt";
  if(LLSR == 23) return "LQ_METSig6_1000sumlpt";
  //
  if(LLSR == 24) return "oneLooseMuon";
  //
  if(LLSR == 25) return "OrthoVRMET"; 
  if(LLSR == 26) return "LooseVRMET";
  //
  if(LLSR == 27) return "sumMtMET150_CRRatio";
  if(LLSR == 28) return "sumMtMET150_CRJET";
  if(LLSR == 29) return "sumMtMET150250_CRRatio"; 
  if(LLSR == 30) return "sumMtMET150250_CRJET";
  return "";
}


StatusCode EventStruct::FinalizeFakeBkgTools(){ 
  
  std::cout << "Try to finalize FakeBkgTool" << std::endl;

  float lhmYield, lhmYieldStatUp, lhmYieldStatDown;
  if(m_lhmTool.getTotalYield(lhmYield, lhmYieldStatUp, lhmYieldStatDown) != StatusCode::SUCCESS){
    std::cout << "ERROR: LhoodMM_tools::getTotalYieldAndUncertainties() failed" << std::endl;
    exit(2); 
  }

  std::cout << "FinalizeFakeBkgTools::Number of events added to LL MM tool: " << LL_event_count << std::endl;

  //ATH_CHECK( m_lhmTool.fillHistograms() );
  std::string fake_output_name = "LLfakes_"+get_name(LLSR)+"_"+fakeSyst+".root";
  TFile f(fake_output_name.c_str(), "RECREATE");
  std::cout << "FinalizeFakeBkgTools::Will write fakes to output root file: " << fake_output_name << std::endl;
  LL_lep1Pt->Write();
  LL_sumMT_histo->Write();
  LL_ele1Pt->Write();
  LL_muon1Pt->Write();
  LL_yield->Write();

  LL_lep1Pt_ePmuM->Write();
  LL_lep1Pt_eMmuP->Write();

  LL_EventCount_channel->Write();
  LL_lep1Pt_channel->Write();
  LL_lep2Pt_channel->Write();
  LL_ele1Pt_channel->Write();
  LL_muon1Pt_channel->Write();
  LL_sumMT_channel->Write();
  LL_MT2_channel->Write();
  LL_jet1Pt_channel->Write();
  LL_jet2Pt_channel->Write();
  LL_EtMissSignificance_channel->Write();
  LL_EtMiss_channel->Write();
  LL_sum_pt_leps_jet1_channel->Write();

  LL_max_mt_met_channel->Write();
  LL_lep1Phi_channel->Write();
  LL_lep2Phi_channel->Write();
  LL_jet1Phi_channel->Write();
  LL_jet1Eta_channel->Write();
  LL_lep1Eta_channel->Write();
  LL_lep2Eta_channel->Write();

  LL_sumMtJet1_channel->Write();
  LL_sum_mass_lepton_jet1_channel->Write();
  LL_max_mt_jet1_channel->Write();
  LL_max_mass_lepton_jet1_channel->Write();
  LL_NJets_channel->Write();
  LL_lep1Pt_EtMiss_channel->Write();
  LL_HTjets_EtMiss_channel->Write();
  LL_HTall_EtMiss_channel->Write();

  LL_max_mt_met_v_mt2_channel->Write();
  LL_sumljpt_v_metsig_channel->Write();
  LL_metsig_v_mt2_channel->Write();

  f.Close();

  std::cout << "LL yield from fakes: " << lhmYield << std::endl;
  std::cout << "LL stat up yield: " << lhmYieldStatUp <<  std::endl;
  std::cout << "LL stat down yield: " << lhmYieldStatDown <<  std::endl;

  return StatusCode::SUCCESS;
}
