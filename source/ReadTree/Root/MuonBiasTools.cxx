#include "ReadTree/MuonBiasTools.h"

std::string find_root_type(TH1* obj){
  // Function to get around ROOT;s STUPID STUPID STUPIDITY
  // list of the ROOT types, in reverse order of possible inheritance
  std::vector<std::string> types = { "TH2Poly", "TH2D", "TH2F", "TH2", "TH1D", "TH1F", "TH1" }; 
  std::string dominant_type = "";
  for(const auto& type : types){
    if(obj->InheritsFrom(type.c_str())){
      dominant_type = type;
      break;
    }
  }
  return dominant_type;
}

EfficiencyPair binomial_efficiency_and_error(double matches, double probes){
  double eff(0);
  double eff_err(0);
  if(probes != 0){ // TODO: double?? 
    eff = matches / probes;
    eff_err = sqrt( eff * (1-eff) / probes);
  }
  else{
    eff = 1;
    eff_err =  1; //std::numeric_limits<double>::infinity();
  }
  return EfficiencyPair(eff, eff_err);
}


std::ostream& operator<<(std::ostream& os, const HistEfficiencyTuple& tup)
{
  os << "HistEfficiencyTuple, type: " << find_root_type(tup.eff_hist) << ". "
      << "EffHist: (" << tup.eff_hist->GetName() << ", " << tup.eff_hist->GetTitle() << ")" << "\tErrHist: (" << tup.eff_hist_err->GetName() << ", " << tup.eff_hist_err->GetTitle() << ")";
  return os;
}

std::pair<double, double> efficiency_ratio_and_error(EfficiencyPair eff_num, EfficiencyPair eff_denom, bool debug){
  if (isnan(eff_num.eff_err)){ eff_num.eff_err = 0; }
  if (isnan(eff_denom.eff_err)){ eff_denom.eff_err = 0; }
  if (eff_num.eff==-0.0){ eff_num.eff = 0; }
  if (eff_denom.eff==-0.0){ eff_denom.eff = 0; }
  if(debug) std::cout << "efficiency_ratio_and_error(" << eff_num << ", " << eff_denom << ")";
  double ratio;
  double ratio_err;
    if( eff_num.eff > 0 && eff_denom.eff > 0){
        ratio = eff_num.eff / eff_denom.eff;
        ratio_err = ratio * sqrt(   pow(eff_num.eff_err/eff_num.eff,2) + pow(eff_denom.eff_err/eff_denom.eff, 2) );
    }
    else if(eff_denom.eff > 0){ // and numerator zero
        ratio = 1.;
        ratio_err = 1.;
//        std::cout << "WARNING: efficiency_ratio_and_error: Zero in the numerator!" << std::endl;
    }
    else if(eff_num.eff > 0) { //  and denominator zero
      // should be careful if this condition is ever encountered
        ratio = 1; //std::numeric_limits<double>::infinity(); 
        ratio_err = 1;// std::numeric_limits<double>::infinity();
//        std::cout << "WARNING: efficiency_ratio_and_error: Zero in the denominator!" << std::endl;
//        std::cout << "Note: special care needs to be taken in this condition, investigage!" << std::endl;
    }
    else { //  both numerator and denominator zero
        ratio = 1.;
        ratio_err = 1.;
//        std::cout << "WARNING: efficiency_ratio_and_error: Both numerator and denominator are zero!" << std::endl; // not sure how much of a warning this should be, maybe remove 
    }
    if(debug){
      std::pair<double, double> temp(ratio, ratio_err);
      std::cout << " -> " << ratio << ", " <<  ratio_err << std::endl;
    }
    return std::make_pair(ratio, ratio_err);
}


TH1* EfficiencyTool::load_histogram(std::string hist_name) const{
  if(m_debug) std::cout << "EfficiencyTool::load_histogram(" << hist_name << ")" << std::endl;
  // Load histogram hist_name from the ROOT file
  TH1* hist = dynamic_cast<TH1*>(m_input_file->Get(hist_name.c_str())); // dynamic cast, as this could be TH1, TH2 or TH2Poly
  if(hist == NULL){
    std::cout << "EfficiencyTool::load_histogram: ERROR: failed to load histogram " << hist_name << std::endl;
  }
  return hist;
}


std::string EfficiencyTool::get_analysis() const{
  if( m_efficiency_type == "Reco") return "ZmumuTPReco";
  else if( m_efficiency_type == "Trig") return "ZmumuTPMuon";
  else{
    std::cout << "ERROR: Incorrect efficiency type specified" << std::endl;
    return "";
  }
}

std::string EfficiencyTool::get_probes() const{
  if( m_efficiency_type == "Reco") return "CaloProbes";
  else if( m_efficiency_type == "Trig") return "MediumProbes";
  else{
    std::cout << "ERROR: Incorrect efficiency type specified" << std::endl;
    return "";
  }
}

std::string EfficiencyTool:: get_matches() const{
  if( m_efficiency_type == "Reco") return "MediumMuons";
  else if( m_efficiency_type == "Trig") return "MediumMuons_mu14"; // TODO Change back 0 mu14 when available
  else{
    std::cout << "ERROR: Incorrect efficiency type specified" << std::endl;
    return "";
  }
}

std::string EfficiencyTool::get_histogram_name(std::string probe_type, std::string match_type, std::string probe_match, std::string hist_tag, int charge, std::pair<double, double> pt_bin) const{
  // override to set full_path to true by default
  return get_histogram_name(probe_type, match_type, probe_match, hist_tag, charge, pt_bin, true);
}

std::string EfficiencyTool::get_histogram_name(std::string probe_type, std::string match_type, std::string probe_match, std::string hist_tag, int charge, std::pair<double, double> pt_bin, bool full_path) const{
  /*****************
   * Returns the histogram path calculated from the input parameters
   *
   * the pt_bin is a pair of doubles with some special entries
   * (-1, -1) corresponds to "" in the python code
   *
   * *******************/


  std::string region_tag = "All"; // case for charge == 0, or pt_bins = (-1, -1) or ("all", "all") 
  
  // decode the region tag 
  //if( pt_bin.first != -1 && pt_bin.first != "all"){
  std::string pt_min = std::to_string((int)pt_bin.first);
  if(pt_bin.first > 0 && pt_bin.second < 99999){ 
    std::string pt_max = std::to_string((int)pt_bin.second);
    region_tag += "Pt" + pt_min + pt_max;
  }
  else if(pt_bin.first > 0 && pt_bin.second > 99999){ // "Inf" case
    region_tag += "Ptgt"+pt_min;
  }


  if(charge > 0) region_tag += "qplus";
  else if(charge < 0) region_tag += "qminus";

  std::string hist_name = probe_type + "_" + region_tag + "_OC_" + match_type + "_" + probe_match + "_" + hist_tag; 
  if(full_path){
    hist_name = get_analysis() + "/" + probe_type + "_" + region_tag + "/OC/" + match_type + "/" + probe_match + "/" + hist_name;
    //analysis = self.get_analysis()
    //hist_name = '%s/%s_%s/OC/%s/%s/' % (analysis, probe_type, region_tag, match_type, probe_match) + hist_name
  }

  if(m_debug){
    std::cout << "get_histogram_name(" << probe_type << ", " << match_type << ", " << probe_match << ", " << hist_tag << ", " << charge << ", (" << pt_bin.first << ", " << pt_bin.second << "), " << full_path << ")";
    std::cout << " -> " << hist_name << std::endl;
  }

  return hist_name;
}

std::string EfficiencyTool::get_short_hist_name(std::string hist_type, int charge, std::pair<double, double> pt_bin) const{
  return get_histogram_name("dummy", "dummy", hist_type, "dummy", charge, pt_bin, false);
}



std::map<std::string, HistEfficiencyTuple> EfficiencyTool::load_efficiency_histograms(std::string hist_tag) const{
  return load_efficiency_histograms(hist_tag, std::make_pair(-1.0, -1.0) );
}


std::map<std::string, HistEfficiencyTuple> EfficiencyTool::load_efficiency_histograms(std::string hist_tag, std::pair<double, double> pt_bin) const{
  if(m_debug) std::cout << "EfficiencyTool::load_efficiency_histograms(" << hist_tag << ", (" << pt_bin.first << ", " << pt_bin.second << "))" << std::endl;
  //
  // 
  //

  std::map<std::string, HistEfficiencyTuple> eff_histograms;

  std::string probe_type = get_probes();
  std::string match_type = get_matches();
  for(int charge : m_charges){

    TH1* probe_calo = load_histogram( get_histogram_name(probe_type, match_type, "Probe", hist_tag, charge, pt_bin) ); 
    TH1* match_calo = load_histogram( get_histogram_name(probe_type, match_type, "Match", hist_tag, charge, pt_bin) );

    TH1* probe_ms = NULL;
    TH1* match_ms = NULL;
    if(m_efficiency_type == "Reco" && m_do_id_correction){ // true by default
      probe_ms = load_histogram(get_histogram_name("MSProbes", "IDTracks", "Probe", hist_tag, charge, pt_bin));
      match_ms = load_histogram(get_histogram_name("MSProbes", "IDTracks", "Match", hist_tag, charge, pt_bin));
    } 

    std::string eff_hist_name = get_short_hist_name("Eff", charge, pt_bin);
    eff_histograms[eff_hist_name] = combined_efficiency_from_histograms(probe_calo, match_calo, probe_ms, match_ms); 
  }

  return eff_histograms;
  
}


HistEfficiencyTuple EfficiencyTool::combined_efficiency_from_histograms(TH1* probe_calo, TH1* match_calo, TH1* probe_ms, TH1* match_ms) const{
  if(m_debug){
    std::cout << "combined_efficiency_from_histograms(" << std::endl;
    probe_calo->Print();
    match_calo->Print();
    if(probe_ms != NULL) probe_ms->Print();
    if(match_ms != NULL) match_ms->Print();
    std::cout << ") " <<std::endl;
  }
  
  // 
  if(probe_ms == NULL && match_ms == NULL){
    if(m_efficiency_type == "Reco"){
      std::cout << "WARNING: computing reco efficiency without correction for ID efficiency" << std::endl;
    }
    return one_component_efficiency_from_histograms(probe_calo, match_calo);
  }
  else if (probe_ms != NULL && match_ms != NULL){
    HistEfficiencyTuple eff_hist_calo = one_component_efficiency_from_histograms(probe_calo, match_calo);
    HistEfficiencyTuple eff_hist_ms = one_component_efficiency_from_histograms(probe_ms, match_ms);
    return eff_hist_calo*eff_hist_ms;
  }

  std::cout << "ERROR!!! Do not know how to compute efficiency from given histograms!!" << std::endl;
  HistEfficiencyTuple tup;
  return tup;
}

HistEfficiencyTuple EfficiencyTool::one_component_efficiency_from_histograms(TH1* probe, TH1* match) const{
  if(m_debug){
    std::cout << "EfficiencyTool::one_component_efficiency_from_histograms() " << std::endl;
    probe->Print();
    match->Print();
  }
  // TODO: deal with the different types, perhaps overload or template? 
  // TODO: may want to implement RTTI to perform below calculations
  TH1* eff_hist = dynamic_cast<TH1*>(probe->Clone("eff_hist"));
  TH1* eff_hist_err = dynamic_cast<TH1*>(probe->Clone("eff_hist_err")); // TODO: WJF: copied from Ben's code, could be a bug. Possibly implemented just to get the binning/other histogram properties right? 

  TH2Poly* eff_poly = dynamic_cast<TH2Poly*>(probe->Clone("eff_poly"));
  TH2Poly* eff_poly_err = dynamic_cast<TH2Poly*>(probe->Clone("eff_poly_err")); // TODO: WJF: copied from Ben's code, could be a bug. Possibly implemented just to get the binning/other polyogram properties right? 

  //if( probe->InheritsFrom("TH2Poly") && match->InheritsFrom("TH2Poly") ){
  if(eff_poly && eff_poly_err){
    if(m_debug) std::cout << "Probe inherits from TH2Poly" << std::endl;
    for(int i=1; i<eff_poly->GetNumberOfBins(); ++i){
      EfficiencyPair effs = binomial_efficiency_and_error( match->GetBinContent(i), probe->GetBinContent(i) );
      eff_hist->SetBinContent(i, effs.eff);
      eff_hist_err->SetBinContent(i, effs.eff_err);
    }
  }
  else if(probe->InheritsFrom("TH2") && match->InheritsFrom("TH2")){
    if(m_debug) std::cout << "Probe inherits from TH2" << std::endl;
    for(int i=1; i<probe->GetNbinsX()+1; ++i){
      for(int j=1; j<probe->GetNbinsY()+1; ++j){
        EfficiencyPair effs = binomial_efficiency_and_error( match->GetBinContent(i,j), probe->GetBinContent(i,j) );
        eff_hist->SetBinContent(i, j, effs.eff);
        eff_hist_err->SetBinContent(i, j, effs.eff_err);
      }
    }
  }
  else{ // assume 1D histogram 
    if(m_debug) std::cout << "Probe inherits from TH1" << std::endl;
    for(int i=1; i<probe->GetNbinsX(); ++i){
      EfficiencyPair effs = binomial_efficiency_and_error( match->GetBinContent(i), probe->GetBinContent(i) );
      eff_hist->SetBinContent(i, effs.eff);
      eff_hist_err->SetBinContent(i, effs.eff_err);
    }
  }

  return HistEfficiencyTuple(eff_hist, eff_hist_err);
}

void EtaPhiEfficiencyTool::print() const{
  std::cout << "\nEtaPhiEfficiencyTool(" << m_efficiency_type << ", " << m_do_id_correction << ", " << m_do_poly_hist << ")" << std::endl;
  std::cout << "Member variables:" << std::endl;

  std::cout << "m_efficiency_histograms" << std::endl;
  for(auto it=m_efficiency_histograms.begin(); it != m_efficiency_histograms.end(); ++it){
    std::cout << "\t" << it->first << " : " << it->second << std::endl; 
  }

  std::cout << "m_bin_template_hist" << std::endl;
  std::cout << "type: " << find_root_type(m_bin_template_hist) 
    << " name: " << m_bin_template_hist->GetName() 
    << " title: " << m_bin_template_hist->GetTitle() << std::endl;
  m_bin_template_hist->Print();
  std::cout << std::endl;

}

bool EtaPhiEfficiencyTool::out_of_bounds(double eta, double phi){

  /**************************
   *
   * FindBins returns the global bin index (a single number, even for 2D/3D hists).
   * For 1D, bin_index=0 indicates underflow and bin_index=nbins+1 indicates overflow.
   * For 2D, bins 0 to nbinsx+1 are underflow in y.
   *         bins numbered i*(nbinsx+2) for i=0..nbinsy+1 are underflow in x
   *         bins numbered i*(nbinsx+2) + nbinsx+1 for i=0..nbinsy+1 are overflow in x
   *         bins numbered (nbinsy+1)*(nbinsx+2) to (nbinsy+1)*(nbinsx+2) + nbinsx+1 are overflow in y
   * For 2DPoly, overflow bins are numbered as
   * -1 | -2 | -3
   * ---+----+----
   * -4 | -5 | -6
   * ---+----+----
   * -7 | -8 | -9
   * where -5 is the "sea": unbinned areas.
   *
   **************************/
  if(m_debug) std::cout << "out_of_bounds(" << eta << ", " << phi << ")" << std::endl;;

  int bin_index = get_bin_index(eta, phi);
  bool bound = false;
  
  if(m_bin_template_hist->InheritsFrom("TH2Poly")){
    bound = bin_index<0;
  }
  else if( m_bin_template_hist->InheritsFrom("TH2") ){
    int nbinsx = m_bin_template_hist->GetNbinsX();
    int nbinsy = m_bin_template_hist->GetNbinsY();
    
    if( bin_index <= nbinsx+1) bound=true;
    else if(bin_index >= (nbinsy+1)*(nbinsx+2)) bound=true;
    else if(bin_index % (nbinsx+2) == 0) bound=true;
    else if(bin_index % (nbinsx+2) == nbinsx+1) bound=true;
    else bound=false;

  }
  else if(m_bin_template_hist->InheritsFrom("TH1")){
    bound = ((bin_index == 0) || (bin_index == m_bin_template_hist->GetNbinsX()+1));
  }
  else{
    std::cout << "ERROR: template histogram does not seem to be of a type I recognise!!" << std::endl;
    bound= false;
  }
  
  if(m_debug) std::cout << "out_of_bounds() -> " << bound << std::endl;
  return bound; 

}

TH1* EtaPhiEfficiencyTool::get_bin_template_hist(){
  // Function to get the first efficiency histogram that satisfies the name requirements. Could use a [0] if there was  more than one element in this list
  std::string key = get_short_hist_name("Eff", +1, std::make_pair(-1, -1) );

  if(m_debug){
    std::cout << "About to print info for m_bin_template_hist" << std::endl; 
    m_efficiency_histograms[key].eff_hist->Print("all");
  }

  return m_efficiency_histograms[key].eff_hist;
}

int FindTheGodDamnBin(TH1* hist, double x, double y){
  TAxis* xaxis = hist->GetXaxis();
  TAxis* yaxis = hist->GetYaxis();
  int nx   = xaxis->GetNbins()+2;
  int binx = xaxis->FindBin(x);
  int biny = yaxis->FindBin(y);
  biny++; // bug somewhere 
  std::cout << "nx: " << nx << "\tbinx: " << binx << "\tbiny: " << biny << std::endl;
  return binx + nx*biny;
}

int EtaPhiEfficiencyTool::get_bin_index(double eta, double phi) {
  // Return the bin index (int) from the histogram, given the eta and phi coordiates
  // Cache the result for faster access
  static int FindBinCounter(1);
  int bin_index(0);
  if( m_last_bin_index_fetch.eta == eta && m_last_bin_index_fetch.phi == phi){
    bin_index = m_last_bin_index_fetch.bin_index;
    if(m_debug) std::cout << "get_bin_index() uses cached index" << std::endl;
  }
  else{
    bin_index = m_bin_template_hist->FindBin(eta, phi); // returns the global bin index (a single number, even for 2D/3D histograms)
    //bin_index = FindTheGodDamnBin(m_bin_template_hist, eta, phi);
    FindBinCounter++;

    m_last_bin_index_fetch.eta = eta;
    m_last_bin_index_fetch.phi = phi;
    m_last_bin_index_fetch.bin_index = bin_index;
    if(m_debug) std::cout << "get_bin_index() uses histogram: " << m_bin_template_hist->GetName() << " of type " << find_root_type(m_bin_template_hist) << " " << m_bin_template_hist->GetTitle() <<  std::endl;
  }
  if(m_debug) std::cout << "EtaPhiEfficiencyTool::get_bin_index(" << eta << ", " << phi << ") -> " << bin_index << std::endl;
  return bin_index; 
}

EfficiencyPair EtaPhiEfficiencyTool::get_efficiency(double eta, double phi, int charge) {
  if(m_debug) std::cout << "EtaPhiEfficiencyTool::get_efficiency(" << eta << ", " << phi << ", " << charge << ")" <<  std::endl;

  // get key for cache 
  int bin_index = get_bin_index(eta, phi);
  std::string key = std::to_string(bin_index) + "_" + std::to_string(charge);


  // Check cached efficiencies
  auto it = m_efficiency_cache.find(key);
  if( it != m_efficiency_cache.end() ){
    if(m_debug) std::cout << "EtaPhiEfficiencyTool::get_efficiency used cache, found " << m_efficiency_cache[key] << std::endl;
    return m_efficiency_cache[key];
  }
  else{
    // otherwise extract efficiency
    std::string eff_hist_name = get_short_hist_name("Eff", charge, std::make_pair(-1, -1));
    double eff = m_efficiency_histograms[eff_hist_name].eff_hist->GetBinContent(bin_index);
    double eff_err = m_efficiency_histograms[eff_hist_name].eff_hist_err->GetBinContent(bin_index);
    if (isnan(eff_err)){eff_err = 0.0;}
    if (eff<0.0){eff = 0.0;}
    EfficiencyPair temp(eff, eff_err);
    m_efficiency_cache[key] = temp;
    if(m_debug) std::cout << "EtaPhiEfficiencyTool::get_efficiency calculated: " << temp << std::endl;
    return temp;
  }
}

// print info on the member variables of this class
void PtBiasScaleTool::print() const{
  std::cout << "\nPtBiasScaleTool(" << m_efficiency_type << ", " << m_do_id_correction << ", " << m_do_poly_hist << ")" << std::endl;
  std::cout << "Member variables:" << std::endl;
  std::cout << "m_pt_bin_scale: " << std::endl;  
  for(auto it = m_pt_bin_scale.begin(); it  != m_pt_bin_scale.end(); ++it){
    std::pair<double, double> key = it->first;
    std::pair<double, double> val = it->second;
    std::cout << "\t" << key.first << ", " << key.second << " : " << val.first << ", " << val.second << std::endl; 
  }
  std::cout << std::endl;
}

std::vector< std::pair<double, double> > PtBiasScaleTool::get_pt_bins() const{

  std::vector<std::pair<double, double>> bins = {
    std::make_pair(-10., -10.), // replacement for "all"
    std::make_pair(25., 30.000001), 
    std::make_pair(30., 40.000001),
    std::make_pair(40., 50.000001),
    std::make_pair(50., 60.000001),
    std::make_pair(60., 75.000001),
    std::make_pair(75., 100.000001),
    std::make_pair(100., std::numeric_limits<double>::infinity()),
  };

  return bins; 
}

std::map<std::string, HistEfficiencyTuple> PtBiasScaleTool::get_bias_histograms(std::map<std::string, HistEfficiencyTuple> eff_hists) const{

  if(m_debug) std::cout << "PtBiasScaleTool::get_bias_histograms()" << std::endl;
  std::map<std::string, HistEfficiencyTuple> bias_hists;

  // get pt bins
  std::vector< std::pair<double, double> > bins = get_pt_bins();
  //bins.push_back( std::make_pair(-10, -10) );


  for(const std::pair<double, double> pt_bin : bins){

    std::string qminus_hist_name = get_short_hist_name("Eff", -1, pt_bin);
    std::string qplus_hist_name = get_short_hist_name("Eff", 1, pt_bin);
    
    // Check if keys are in map
    auto it = eff_hists.find(qminus_hist_name); // if not found, then it == eff_hists.end() 
    if(it == eff_hists.end()){
      std::cout << "ERROR: cannot find histogram in efficinecy map: " << qminus_hist_name << std::endl;
      // Should exit here! 
      exit(1);
    }
    if(eff_hists.find(qplus_hist_name) == eff_hists.end()){
      std::cout << "ERROR: cannot find histogram in efficinecy map: " << qplus_hist_name << std::endl;
      // Should exit here!
      exit(1);
    }

    HistEfficiencyTuple qminus_hist = eff_hists[qminus_hist_name];
    HistEfficiencyTuple qplus_hist = eff_hists[qplus_hist_name];
    
    std::string bias_hist_name = get_short_hist_name("Bias", 0, pt_bin);

    TH1* bias_hist = dynamic_cast<TH1*>(qminus_hist.eff_hist->Clone(bias_hist_name.c_str()));
    TH1* bias_hist_err = dynamic_cast<TH1*>(qminus_hist.eff_hist->Clone( (bias_hist_name+"err").c_str() ));

    int n_bins = qminus_hist.GetNbinsSimple();
    if(m_debug) std::cout << "\tn_bins: " << n_bins << std::endl;
    //TH2Poly* bias_poly = dynamic_cast<TH2Poly*>( bias_hist->Clone() );
    //if(bias_poly)  n_bins = bias_poly->GetNumberOfBins();
    //else           n_bins = bias_hist->GenBbins()+1;

    // Fill the bias histograms
    for(int i=1; i<n_bins;++i){
      EfficiencyPair qminus = qminus_hist.GetBinContent(i);
      EfficiencyPair qplus  = qplus_hist.GetBinContent(i);
      std::pair<double, double> bias_and_error = efficiency_ratio_and_error(qminus, qplus, m_debug);
      bias_hist->SetBinContent(i, bias_and_error.first);
      bias_hist_err->SetBinContent(i, bias_and_error.second);
    }

    bias_hists[bias_hist_name] = HistEfficiencyTuple(bias_hist, bias_hist_err);

  }

  return bias_hists;
}

std::map<std::string, HistEfficiencyTuple> PtBiasScaleTool::get_efficiency_histograms_ptbins(std::string hist_tag) const{
  if(m_debug) std::cout << "get_efficiency_histograms_ptbins(" << hist_tag << ")" << std::endl;

  std::map<std::string, HistEfficiencyTuple> eff_hists;

  // get the pt bins 
  std::vector< std::pair<double, double> > bins = get_pt_bins();
  //bins.push_back( std::make_pair(-10, -10) );

  for(const std::pair<double, double> pt_bin : bins){
    std::map<std::string, HistEfficiencyTuple> temp = load_efficiency_histograms(hist_tag, pt_bin); // returns a map 
    eff_hists.insert(temp.begin(), temp.end()); // concatenate maps 
  }


  if(m_debug){
    std::cout << "\tEfficiency histograms" << std::endl;
    for(auto it=eff_hists.begin(); it!=eff_hists.end(); ++it){
      std::cout << "\t" << it->first << " : " << it->second << std::endl;
    }
    
  }
  return eff_hists; 
}

std::map<std::pair<double, double>, std::pair<double, double> > PtBiasScaleTool::get_mean_sd_by_ptbin(std::map<std::string, HistEfficiencyTuple> bias_hists) const{

  if(m_debug) std::cout << "get_mean_sd_by_ptbin():" << std::endl;
  std::map<std::pair<double, double>, std::pair<double, double> > pt_bin_mean_weights;

  for(const std::pair<double, double> pt_bin : get_pt_bins()){
    HistEfficiencyTuple bias_hist_thisbin = bias_hists[ get_short_hist_name("Bias", 0, pt_bin) ];
    HistEfficiencyTuple bias_hist_overall = bias_hists[ get_short_hist_name("Bias", 0, std::make_pair(-10, -10))];
    
    int n_bins = bias_hist_overall.GetNbinsSimple();
    double sum_wx(0), sum_wxsq(0), sum_of_weights(0);

    if(m_debug){
      std::cout << "\tpt_bin = " << pt_bin.first << ", " << pt_bin.second << " nbins = " << n_bins << std::endl;
      std::cout << bias_hist_thisbin << std::endl;
      std::cout << bias_hist_overall << std::endl;
      bias_hist_overall.eff_hist->Print();
    }

    for(int i=1; i<n_bins; ++i){
      double overall_bias = bias_hist_overall.eff_hist->GetBinContent(i) - 1;
      double thisptbin_bias = bias_hist_thisbin.eff_hist->GetBinContent(i) -1;
      double thisptbin_err = bias_hist_thisbin.eff_hist_err->GetBinContent(i); 

      double scale = thisptbin_bias / overall_bias;
      double scale_err = thisptbin_err / fabs(overall_bias);


      if(scale_err < 0){
        std::cout << "ERROR: negative bin error " << scale_err << std::endl;
        exit(1);
      }

      if(scale > 0 || scale < 0){
        double bin_weight = (1.0 / pow(scale_err, 2));
        sum_wx += (bin_weight * scale);
        sum_wxsq += (bin_weight * pow(scale,2));
        sum_of_weights += bin_weight;
      }

      if(m_debug){
        std::cout << "\t" << i << " : " << overall_bias << ", " << thisptbin_bias << ", " << thisptbin_err << ", " << scale << ", " << scale_err << std::endl;
      }
    }
    double weighted_mean = sum_wx / sum_of_weights;
    double weighted_stdev = sqrt( sum_wxsq/sum_of_weights - pow(sum_wx/sum_of_weights, 2) );
    pt_bin_mean_weights[pt_bin] = std::make_pair(weighted_mean, weighted_stdev);
  }
  return pt_bin_mean_weights;
}

std::pair<double, double> PtBiasScaleTool::ptscale_and_error(double pt_gev) const{
  if(m_debug) std::cout << "PtBiasScaleTool::ptscale_and_error( " << pt_gev << ")" << std::endl;

  bool value_found(false);
  std::pair<double, double> value;


  for(auto it = m_pt_bin_scale.begin(); it!=m_pt_bin_scale.end(); ++it){
    std::pair<double, double> pt_bin = it->first;
    std::pair<double, double> values = it->second;
    //double mean = values.first;
    //double sd   = values.second;
    if(pt_gev > pt_bin.first && pt_gev < pt_bin.second){ 
      if(pt_bin.first < 0) std::cout << "WARNING: ptbin < 0 accepted here, not sure what this will do ... " << std::endl;
      value = values;
      value_found=true;
    }
  }
  if(!value_found){
    std::cout << "Failed to find pT scale for muon of pT = " << pt_gev << " GeV." << std::endl;
    exit(1);
  }

  return value;
}

std::map<std::string, double> MuonBiasCorrectionTool::get_weights(double eta, double phi, double pt_gev, int charge){
  // TODO 
  EfficiencyPair eff_etaphi, eff_etaphi_plus, eff_etaphi_minus;
  std::pair<double, double> bias_etaphi;
  
  bool out_of_bounds = m_etaPhiTool.out_of_bounds(eta, phi);
  if(out_of_bounds){
    eff_etaphi.eff = -10; eff_etaphi.eff_err = -10;
    //eff_etaphi_plus.eff = -10; eff_etaphi_plus.eff_err = -10;
    //eff_etaphi_minus.eff = -10; eff_etaphi_minus.eff_err = -10;
    bias_etaphi.first = -10; bias_etaphi.second = -10;
  }else{
    eff_etaphi = m_etaPhiTool.get_efficiency(eta, phi, charge);
    eff_etaphi_plus = m_etaPhiTool.get_efficiency(eta, phi, +1);
    eff_etaphi_minus = m_etaPhiTool.get_efficiency(eta, phi, -1);
    bias_etaphi = efficiency_ratio_and_error(eff_etaphi_minus, eff_etaphi_plus, m_debug);
    //bias_etaphi.eff = bias_etaph_pair.first;
    //bias_etaphi.eff_err = bias_etaph_pair.second;

  }

  std::pair<double, double> pt_scale = m_ptBiasTool.ptscale_and_error(pt_gev);
  //if(m_debug)

  // Fill the weights map 
  std::map<std::string, double> weights;


  if(out_of_bounds){
    weights["nominal"]         = 1.0;
    weights["etaphibias_up"]   = 1.0;
    weights["etaphibias_down"] = 1.0;
    weights["ptscale_up"]      = 1.0;
    weights["ptscale_down"]    = 1.0;
  }
  else{
    double wplus_over_wminus = exp(log(bias_etaphi.first)*pt_scale.first);
    if(charge > 0){
      weights["nominal"]         = sqrt(wplus_over_wminus);
      weights["etaphibias_up"]   = weights["nominal"] * (1 + 0.5*pt_scale.first*bias_etaphi.second / bias_etaphi.first );
      weights["etaphibias_down"] = weights["nominal"] * (1 - 0.5*pt_scale.first*bias_etaphi.second / bias_etaphi.first );
      weights["ptscale_up"]      = weights["nominal"] * (pow(bias_etaphi.first, 0.5*pt_scale.second) );
      weights["ptscale_down"]    = weights["nominal"] * (pow(bias_etaphi.first, -0.5*pt_scale.second) );
//      weights["nominal"]         = sqrt(wplus_over_wminus);
//      weights["etaphibias_up"]   = weights["nominal"] * (1 + 0.5*pt_scale.first*bias_etaphi.second / pow(weights["nominal" ],2) );
//      weights["etaphibias_down"] = weights["nominal"] * (1 - 0.5*pt_scale.first*bias_etaphi.second / pow(weights["nominal" ],2) );
//      weights["ptscale_up"]      = weights["nominal"] * (1 + 0.5*(bias_etaphi.first-1)*pt_scale.second / pow(weights["nominal" ],2) );
//      weights["ptscale_down"]    = weights["nominal"] * (1 - 0.5*(bias_etaphi.first-1)*pt_scale.second / pow(weights["nominal" ],2) );
    }
    else if(charge < 0){
      weights["nominal"] = 1./sqrt(wplus_over_wminus);
      weights["etaphibias_up"]     = weights["nominal"] * (1 - 0.5*pt_scale.first*bias_etaphi.second / bias_etaphi.first );
      weights["etaphibias_down"]   = weights["nominal"] * (1 + 0.5*pt_scale.first*bias_etaphi.second / bias_etaphi.first );
      weights["ptscale_up"]       = weights["nominal"] * ( pow(bias_etaphi.first, -0.5*pt_scale.second) );
      weights["ptscale_down"]       = weights["nominal"] * ( pow(bias_etaphi.first, 0.5*pt_scale.second) );
//      weights["nominal"] = 1./sqrt(wplus_over_wminus);
//      weights["etaphibias_up"]     = weights["nominal"] * (1 - 0.5*pt_scale.first*bias_etaphi.second / pow(weights["nominal" ],4) );
//      weights["etaphibias_down"]   = weights["nominal"] * (1 + 0.5*pt_scale.first*bias_etaphi.second / pow(weights["nominal" ],4) );
//      weights["ptscale_up"]       = weights["nominal"] * (1 + 0.5*(bias_etaphi.first-1)*pt_scale.second / pow(weights["nominal" ],4) );
//      weights["ptscale_down"]       = weights["nominal"] * (1 - 0.5*(bias_etaphi.first-1)*pt_scale.second / pow(weights["nominal" ],4) );
    }
    else{
      std::cout << "ERROR: Not sure what to do with charge " << charge << std::endl;
      exit(1);
    }
//    std::cout<<"MUON WEIGHTS: nominal: "<<weights["nominal"]<<", etaphiup: "<<weights["etaphibias_up"]<<", etaphidown: "<<weights["etaphibias_down"]<<", ptup: "<<weights["ptscale_up"]<<", ptdown: "<<weights["ptscale_down"]<<std::endl;
  }

  // This entry does not seem to be used .. maybe remove if extra performance is needed?
  weights["eff"] = eff_etaphi.eff;
  weights["eff_err"] = eff_etaphi.eff_err;

  return weights;
}
