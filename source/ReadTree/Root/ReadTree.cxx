#include "ReadTree/ReadTree.h"
#include <iostream>
#include <sstream>
#include <TSystem.h>
#include <chrono>
#include <iomanip>

inline float invariant_mass_squared(const PO::Lepton& lep1,  const PO::Lepton& lep2){
  // Because the ROOT version of this is clearly SHIT
  // Assumes ultra-relativistic limit
  float d_eta = cosh( lep1.Eta - lep2.Eta );
  float d_phi = cos( lep1.Phi - lep2.Phi );
  return 2 * lep1.Pt * lep2.Pt * (d_eta - d_phi);
}

//PFlow: - implement baseline jets
//       - JVTSF
//       - BtagSF
//       - isSignalJet
namespace {
  std::vector<std::string> splitString(const std::string& inStr, char delim){
    // Split a std::string along a delimiter "delim"
    // Returns a vector of the split string 
    std::istringstream ss(inStr);
    std::string placeholder;
    std::vector<std::string> ret;
    while (std::getline(ss, placeholder, delim) ) ret.push_back(placeholder);
    return ret;
  }
  void printHistTags(ReadTree::bitflag_t mask) {
    HistTags::HistTags bit(static_cast<HistTags::HistTags>(1ul) );
    std::string tag(ReadTreeBitFlags::toString(bit) );
    std::cout << "Activated histogram flags: " << std::boolalpha << std::endl;
    while (tag != "unknown") {
      std::cout << "\t" <<  std::setw(15) << std::left << tag << ": " << bool(mask & bit) << std::endl;
      bit = static_cast<HistTags::HistTags>(bit << 1);
      tag = ReadTreeBitFlags::toString(bit);
    }
  }
  void printCutTags(ReadTree::bitflag_t mask) {
    CutPositionTags::CutPositionTags bit(static_cast<CutPositionTags::CutPositionTags>(1ul) );
    std::string tag(ReadTreeBitFlags::toString(bit) );
    std::cout << "Activated cut flags: " << std::boolalpha << std::endl;
    while (tag != "unknown") {
      std::cout << "\t" <<  std::setw(15) << std::left << tag << ": " << bool(mask & bit) << std::endl;
      bit = static_cast<CutPositionTags::CutPositionTags>(bit << 1);
      tag = ReadTreeBitFlags::toString(bit);
    }
  }
  template <typename U>
    bool setMask(const std::string& maskString, ReadTree::bitflag_t& mask, const std::map<std::string, ReadTree::bitflag_t> tagSets)
    {
      using namespace ReadTreeBitFlags;
      bitflag_t tempMask(0);
      for (const std::string& tagStr : splitString(maskString, ',') ) {
        if(tagStr.empty()) {continue;}
        auto mapItr = tagSets.find(tagStr);
        if (mapItr == tagSets.end() ) {
          U tag = fromString<U>(tagStr);
          if (tag) tempMask |= tag;
          else return false;
        }
        else {
          tempMask |= mapItr->second;
        }
      }
      mask = tempMask;
      return true;
    }
}



bool ReadTree::setHistMask(const std::string& maskString)
{
  std::map<std::string, bitflag_t> tagSets;
  tagSets["All"] = ~bitflag_t(0);
  return setMask<HistTags::HistTags>(maskString, m_histMask, tagSets);
}

bool ReadTree::setCutPositionMask(const std::string& maskString)
{
  std::map<std::string, bitflag_t> tagSets;
  tagSets["All"] = ~bitflag_t(0);
  tagSets["DefaultCuts"] = CutPositionTags::First | CutPositionTags::Penultimate | CutPositionTags::Final;
  return setMask<CutPositionTags::CutPositionTags>(maskString, m_cutMask, tagSets);
}

// Constructor
ReadTree::ReadTree(TTree* tree, TTree* configTree, TTree* truthTree, std::string muon_efficiency_histos, std::string muon_efficiency_trig_histos, std::string inputTreeName) :
  // Take care to get the initializer list correct ... 
  m_event("Reco","Trig", muon_efficiency_histos, muon_efficiency_trig_histos),
  m_inputTreeName(inputTreeName),
  event(m_event),
  startEvent(0),
  debug(false),
  LLSR(0),
  nEvents(-1),
  doSquirrels(false),
  applyHighPtIso(false),
  fakeSyst("nominal"),
  applyFakeWeights(false),
  applyttbarWeights(false),
  applyMuonBias(false),
  applyDibosonScaleWeight(false),
  applyPDFWeight(false),
  applyISRWeight(false),
  applyPromptLeptonCut(false),
  applyPromptRejectionCut(false),
  useLikelihoodMM(false),
  m_plot_extra_lepton_variables(false),
  //disableWeights(false),
  skipPRW(false),
  m_scaling(1.0),
  isTruth(false),
  onlyOutputFinal(false),
  tikzFile(""),
  fakeWeightHistos(""),
  ttbarWeightHistFile(""),
  m_input(tree),
  m_configTree(configTree),
  m_truthTree(truthTree),
  //lregEnumVec( {Region::VETO, Region::ONE_E, Region::ONE_MU, Region::TWO_E, Region::TWO_MU} ),
  m_cutchainOutputMode(OutputSetting::Full)  {}
  // Properly set cutchainOutputMode


  void ReadTree::Loop() {

    // Make sure that we're always outputting the histograms marked as vital
    m_histMask |= HistTags::Vital;

    printHistTags(m_histMask);
    printCutTags(m_cutMask);

    using namespace std::chrono;

    m_event.scaling = m_scaling;
    std::cout << "ReadTree::Loop(): scaling = " << m_scaling << std::endl;
    chain.onlyOutputFinal = onlyOutputFinal;
    DefineChain();
    if (tikzFile != "") {
      std::ofstream fout(tikzFile);
      chain.printTikzFile(fout);
    }
    m_event.debug = debug;
    m_event.LLSR = LLSR;
    m_event.fakeSyst = fakeSyst;
    m_event.connect(m_input.get() );

    if(useLikelihoodMM){
      m_event.InitializeFakeBkgTools(); 
    }


    // WJF 2019 remove truth tree
    /*******
    //if (m_truthTree) m_event.connectTruth(m_truthTree);

    if (m_histMask & HistTags::Truth && !m_truthTree) {
    std::cerr << "Truth histograms requested but the ntuple doesn't have truth information! Turning off truth histograms." << std::endl;
    m_histMask &= ~HistTags::Truth;
    }
     *******/

    std::cout << "About to initialize the chain" << std::endl;
    chain.initializeAll();

    std::cout << "getting nEntries" << std::endl;

    Long64_t nEntries = m_input->GetEntries();
    Long64_t nEntries_todo = (nEvents > 0 && nEvents < (nEntries - startEvent) ? nEvents : nEntries - startEvent);
    Long64_t defaultStep = pow(10, floor(log10(nEntries_todo) - 1) );
    Long64_t countStep = defaultStep > 1 ? defaultStep : 1;

    // Timing for running of ReadTree 
    system_clock::time_point time_start = system_clock::now();
    std::time_t timer = system_clock::to_time_t(time_start);
    std::cout << "Started loop at " << std::ctime(&timer) << std::endl;
    std::cout << "Going to start running over " << nEntries_todo << " entries." << std::endl;

    // Open TFile for muons
    bool write_muon_root_file = false; // write an extra "muons.root" file that contains only the muons, origionally for the sagitta bias studies. 
    int temp_muon_event_count(0);
    if(write_muon_root_file){
      std::string muon_filename = m_outDir;
      std::string toReplace(".root");
      size_t pos = muon_filename.find(toReplace);
      muon_filename.replace(pos, toReplace.length(), "_muons.root");
      m_event.muon_file = TFile::Open(muon_filename.c_str(), "RECREATE");
      m_event.muon_tree = new TTree("tree", "tree");
    }




    ////////////////////////////////////////////////////////////// 
    // Loop over events
    ////////////////////////////////////////////////////////////// 
    for (Long64_t nProcessed = 0; nProcessed < nEntries_todo; ++nProcessed) {
      if (debug) std::cout << "nProcessed: " << nProcessed << std::endl;
      if ( nProcessed !=0 && nProcessed % countStep == 0) {
        // float processed_pc = 100*((float)(nProcessed-startEvent)/nEntries_todo);
        std::cout << "Event " << nProcessed << "/" << nEntries_todo << " (" << /*std::fixed << std::setprecision(2) <<*/ float(nProcessed)/nEntries_todo * 100 << "%) ";
        system_clock::time_point time_now = system_clock::now();
        seconds time_elapsed = duration_cast<seconds>(time_now - time_start);
        //std::cout << time_elapsed << std::endl;
        seconds time_till_end = time_elapsed * nEntries_todo / nProcessed;
        system_clock::time_point time_projected = time_start + time_till_end;
        timer = system_clock::to_time_t(time_projected);
        auto h = duration_cast<hours>(time_elapsed);
        time_elapsed -= h;
        auto m = duration_cast<minutes>(time_elapsed);
        time_elapsed -= m;
        std::string projected_end_time = std::string(std::ctime(&timer));
        projected_end_time.erase(std::remove(projected_end_time.begin(), projected_end_time.end(), '\n'), projected_end_time.end());
        std::cout << "Time elapsed: " << h.count() << " hours, " << m.count() << " minutes, " << time_elapsed.count() << " seconds. Projected end time " << projected_end_time << "." << std::endl;
      }

      // Load the entry! 
      if (debug) std::cout << "Going to load entry " << nProcessed << "/" << nEntries << std::endl;
      m_input->GetEntry(nProcessed+startEvent);
      if (m_truthTree) m_truthTree->GetEntry(nProcessed+startEvent);

      // Calculate event values! 
      bool success = m_event.CalculateEventValues(applyHighPtIso);
      if (!success) throw "Oh Gosh, I'm afraid I've rather failed to calculate the event values! Terribly sorry.";
      //    m_event.CalculateMuonBiasTest();
      if(doSquirrels) m_event.CalculateSquirrels(); // only calculate squirrels if requested


      // Fake weights 
      // Calculate event weights MUST BE DONE LAST *******:
      m_event.CalculateEventWeight(skipPRW, applyFakeWeights, applyMuonBias, m_inputTreeName, useLikelihoodMM, applyttbarWeights, applyPDFWeight, applyISRWeight, applyDibosonScaleWeight);

      if (debug) std::cout << "Done calculate event values!" << std::endl;

      //m_event.passGRL_CutChain = 1;

      chain.passEvent(1., debug);

      // Stuff for extra muon tree! (After calculate event values)
      if(write_muon_root_file){
        double muPlusPt(0), muMinusPt(0); 
        double muPlusPhi(0), muMinusPhi(0); 
        double muPlusEta(0), muMinusEta(0); 
        int isJetInEvent(0);
        double branch_mll(0);
        ULong64_t copy_event_number(0);
        
        if(nProcessed == 0){
          m_event.muon_tree->Branch("muPlusPt", &muPlusPt);
          m_event.muon_tree->Branch("muMinusPt", &muMinusPt);
          m_event.muon_tree->Branch("muPlusEta", &muPlusEta);
          m_event.muon_tree->Branch("muMinusEta", &muMinusEta);
          m_event.muon_tree->Branch("muPlusPhi", &muPlusPhi);
          m_event.muon_tree->Branch("muMinusPhi", &muMinusPhi);
          m_event.muon_tree->Branch("isJetInEvent", &isJetInEvent);
          m_event.muon_tree->Branch("fromDSID", &m_dsid); 
          m_event.muon_tree->Branch("EventNumber", &copy_event_number); 
          m_event.muon_tree->Branch("mll", &branch_mll);
        }

        if(m_event.count_tight_muons == 2 && m_event.count_loose_electrons==0){
          if( m_event.mll < 92+5 && m_event.mll>92-5){
            temp_muon_event_count++; // two muons per event 

            int plusIndex(-1), minusIndex(-1);
            if(m_event.tight_muons.at(0).charge > 0){
              plusIndex = 0;
              minusIndex = 1;
            }else{
              plusIndex = 1;
              minusIndex = 0;
            }

            PO::Muon muPlus = m_event.tight_muons.at(plusIndex);
            PO::Muon muMinus = m_event.tight_muons.at(minusIndex);

            muPlusPt = muPlus.Pt;
            muPlusEta = muPlus.Eta;
            muPlusPhi = muPlus.Phi;

            muMinusPt = muMinus.Pt;
            muMinusEta = muMinus.Eta;
            muMinusPhi = muMinus.Phi;

            branch_mll = m_event.mll;

            copy_event_number = m_event.EventNumber;

            if( m_event.signalJets.size() > 0) isJetInEvent = 1;
            else isJetInEvent = 0;

            m_event.muon_tree->Fill();
          }
        } 

      } // end of if write_muon_root_file 

    } //// End loop over events ///////// 

   if(write_muon_root_file)  m_event.muon_file->Write();

    /////////////////////////////////////

    if(useLikelihoodMM){
      StatusCode result = m_event.FinalizeFakeBkgTools();
    }
  }

bool ReadTree::configure(TFile* oFile, std::string outDir, float lumiweight, std::string e_tag, float xsection, float kfactor, float efficiency) {
  //////////////////////////////////////////////////////
  //
  // Configuration for the ReadTree object.
  // Reads the config tree through use of a TTreeReader object, 
  // extracts the cross-section scaling values. 
  //
  // Args:
  //  oFile: output *TFile* to be written to.
  //  outDir: name of the output TFile. 
  // Returns:
  //  true if configuration is successful 
  //  
  //////////////////////////////////////////////////////

  std::cout << "Entered ReadTree::configure" << std::endl;

  m_event.m_isTruth = isTruth;
  m_outDir = outDir;

  // FakeWeights
  if(applyFakeWeights) m_event.initializeFakeWeightHistos(fakeWeightHistos);
  ///////////////////////////////
  // Read the config tree
  ///////////////////////////////

  TTreeReader configReader(m_configTree);
  TTreeReaderValue<int> c_isData(configReader, "isData");
  TTreeReaderValue<int> c_DSID(configReader, "DSID");
  TTreeReaderValue<TObjString> c_gitRevisionSha(configReader, "git_revision_sha");
  TTreeReaderValue<TObjString> c_amiTag(configReader, "amiTag");
  TTreeReaderValue<TObjString> c_subCampaign(configReader, "subCampaign"); // mc16{a,d,e} or data1{5,6,7,8}
  TTreeReaderValue<TObjString> c_treeBaseName(configReader, "treeBaseName");
  TTreeReaderValue<TObjString> c_outputName(configReader, "outputName");

  configReader.Next(); // Load the first entry

  std::cout << "Reading the config tree... " << std::endl;
  // Extract data from the config tree
  m_isData = *c_isData;
  if(m_isData) m_isMC = false;
  else m_isMC = true;
  m_dsid = *c_DSID;

  std::string amiTag       = (*c_amiTag).String().Data();
  std::string subCampaign  = (*c_subCampaign).String().Data();
  std::string treeBaseName = (*c_treeBaseName).String().Data();
  std::string outputName   = (*c_outputName).String().Data();

  std::cout << "\tisData: " << m_isData << std::endl;
  std::cout << "\tTherefore setting isMC to: " << m_isMC << std::endl;
  std::cout << "\tDSID: " << m_dsid << std::endl;
  std::cout << "\tamiTag: " << amiTag << std::endl;
  std::cout << "\tsubCampaign: " << subCampaign << std::endl;
  std::cout << "\ttreeBaseName: " << treeBaseName << std::endl;
  std::cout << "\toutputName: " << outputName << std::endl;

  std::cout << "\te_tag from filename: " << e_tag << std::endl;



  // extract lumi weight for MC capmaign
  m_lumiweight = lumiweight;

  // initialise the ttbar reweighting!
  bool isttbar = (m_dsid==410472 || m_dsid==410470 || m_dsid==407342 || m_dsid==407343 || m_dsid==407344 || m_dsid==407345 || m_dsid==407346 || m_dsid==407347 || m_dsid==142857 || m_dsid==101232 || m_dsid==407341 );
  if (!isttbar) {
    applyttbarWeights=false;
  }
  if(applyttbarWeights) m_event.InitialisettbarWeight2D(ttbarWeightHistFile);
//  if(applyttbarWeights) m_event.InitialisettbarWeight(ttbarWeightHistFile);


  // Extract the XStimesBR
  extractXS(oFile, e_tag, xsection, kfactor, efficiency);


  // Special change for ttbar MC 
  // WJF: change the dsid if equal to 410470,
  // *** MUST be done AFTER extractXS() ***
  if(m_dsid==410470) {
    if(outDir.find("407341") != std::string::npos){
      std::cout << std::endl;
      std::cout << "INFORMATION: You are running on the sample 410470. \
        \nBut you have deliberately requested to create a HT-sliced sub-sample by including in the\
        \noutput filename the number 407341. This will now create a new DSID with only the events\
        \npassing HT<600 GeV. This is created to ensure that there is no double counting after \
        \nadding runs 407342, 407343, 407344 (these are the HT sliced ttbar)." << std::endl;
      m_dsid = 407341;
      std::cout << std::endl;
    }
  }

  // Special change for ttbar MC when you want MET slicing to eb activated
  if(m_dsid == 410470){
    if(outDir.find("142857") != std::string::npos){
      std::cout << std::endl;
      std::cout << "INFORMATION: You are running on the sample 410470. \
        \nBut you have deliberately requested to create a HT-sliced sub-sample by including in the\
        \noutput filename the number 142857. This will now create a new DSID with only the events\
        \npassing MET<200 GeV. This is created to ensure that there is no double counting after \
        \nadding runs 407345, 407346, 407347 (these are the MET sliced ttbar)." << std::endl;
      m_dsid = 142857;
      std::cout << std::endl;
    }
  }

  if(m_dsid == 410472){ // special change for 2L ttbar sample
    if(outDir.find("101232") != std::string::npos){
      std::cout << std::endl;
      std::cout << "INFORMATION: You are running on the sample 410472. \
        \nBut you have deliberately requested to create a HT-sliced sub-sample by including in the\
        \noutput filename the number 101232. This will now create a new DSID with only the events\
        \npassing HT<600 GeV. This is created to ensure that there is no double counting after \
        \nadding runs 407342, 407343, 407344 (these are the HT sliced ttbar)." << std::endl;
      m_dsid = 101232;
      std::cout << std::endl;
    }
  }

  //  std::cout<<"FLAMINGO DSID:  "<<m_dsid<<std::endl;

  // WJF, need some duplication of information here. There are event-level variables, and overall file-level variables
  m_event.m_dsid   = m_dsid;
  m_event.m_isMC   = m_isMC;
  m_event.m_lumiweight = m_lumiweight;
  //m_event.m_disableWeights = disableWeights;
  m_event.m_isData = m_isData;
  //m_event.m_isSherpa = m_isSherpa;

  //m_event.m_outputMode = OutputSetting(*outputMode);
  //m_event.m_dataSource = DataSource(*dataSource);
  //m_event.m_prwConfName = (*prwConfName).String().Data();
  //m_event.m_prwLumiCalcName = (*prwLumiCalcName).String().Data();
  //m_event.m_systOption = (*systOption).String().Data();
  //m_event.m_doTruth = *doTruth;
  m_event.m_doTruth = bool(m_truthTree);
  //m_event.m_isTruth = *isTruth;
  //m_event.m_isTruth = false;
  //m_event.m_STConfFile = (*STConfFile).String().Data();
  //m_event.m_JESNPSet = *JESNPSet;
  //m_event.m_JESNPSet = 1;
  //m_event.m_fatJetName = (*fatJetName).String().Data();
  //m_event.m_hasFatJets = *hasFatJets;

  // WJF: as there are different per-event level scale factors applied, this totals in these histograms may not be the same as the totals in the output histograms
  m_event.m_gitCommitID = c_gitRevisionSha->String().Data();


  // Here load the configuration file and make sure that input is suitable for it (and the chain that has been defined!)


  std::cout << "ReadTree::configure configured correctly " << std::endl;

  return true; // if it gets to here, then it worked :)

} // end of configure

std::vector<float> ReadTree::read_xsection_file(std::string e_tag){

    std::cout << "\nWill read xsection information from a file on cvmfs ... " << std::endl;

    std::string fullPathToBkgXsection_mc16 = PathResolverFindCalibFile("dev/PMGTools/PMGxsecDB_mc16.txt"); // note, probably /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/PMGTools/PMGxsecDB_mc16.txt

    std::cout << "Using background x-sections from path resolver (for mc16): " << fullPathToBkgXsection_mc16 << std::endl;

    std::vector<std::string> xsection_files;
    // x-sections for mc16a (using PathResolver -- keep an eye on these x-sections)
    xsection_files.push_back(fullPathToBkgXsection_mc16);  // use the cross-section file from SUSYTools directly

    // Read all cross-sections files, push lines into a vector
    std::vector<std::string> xsection_lines;
    for(auto xsecfile : xsection_files){
        std::cout << "Opening xsection file: " << xsecfile << std::endl;
        std::ifstream infile(xsecfile);
        std::string temp_line;
        const char* t = " \t\n\r\f\v";
        while (std::getline(infile, temp_line)) {
            temp_line.erase(0, temp_line.find_first_not_of(t)); // guard against leading whitespace
            xsection_lines.push_back(temp_line);
        }
    }

    // Extract the line containing the dsid of this channel
    std::string xsection_line = "";
    for(auto line : xsection_lines){

        bool match_dsid = std::to_string(m_dsid) == line.substr(0,6);
        bool match_etag = e_tag == line.substr( line.size() - 5);

        if(match_dsid && match_etag){
            xsection_line = line;
            if(debug){
                std::cout << "line: " << line << std::endl;
                std::cout << "Extracted e-tag from file: " << line.substr( line.size() - 5) << std::endl;
            }
            break;
        }
    }

    // Check there has been a match
    if(xsection_line == ""){
        std::cerr << "No matching cross-section line found for DSID: " << m_dsid << " and e-tag" << e_tag << std::endl;
        exit(1);
    }

    // Extract the cross-section information from the correct line
    std::cout<<"cross-section line read as: "<<xsection_line<<std::endl;
    std::stringstream is;
    is << xsection_line;
    int id(0);
    float xsection, kfactor, efficiency, relative_uncertainty;
    std::string name;
    is >> id >> name >> xsection >> kfactor >> efficiency >> relative_uncertainty;

    // Check that ID is the same as DSID
    if( m_dsid == id ){
        std::cout << "DSID: " << m_dsid << " id: " << id << " are the same :)" << std::endl;
    }
    else{
        std::cerr << "DSID: " << m_dsid << " id: " << id << " are the NOT same :( !! " << std::endl;
        std::cerr << "ERROR: exit" << std::endl;
        abort();
    }

    // Print info extracted from xsection file
    std::cout << "Information from cross-section file:" << std::endl;
    std::cout << "\tid:\t" << id << std::endl;
    std::cout << "\tname:\t" << name << std::endl;
    std::cout << "\txsect:\t" << xsection << std::endl;
    std::cout << "\tkfactor:\t" << kfactor << std::endl;
    std::cout << "\tefficiency:\t" << efficiency << std::endl;
    std::cout << "\trelative_uncertainty:\t" << relative_uncertainty << std::endl;
    std::cout << std::endl;

    std::vector<float> xsection_info = {xsection, kfactor, efficiency, relative_uncertainty, static_cast<float>(id)};
    return xsection_info;
}

void ReadTree::extractXS(TFile* oFile, std::string e_tag, float xsection, float kfactor, float efficiency){

  int id = 0;
  float relative_uncertainty = 0;
  float XStimesBR = 0.0;
  double scaling_to_1ipb = 1.0;

  bool read_xsection_from_file = true; 
  if(xsection > 0 && kfactor > 0 && efficiency > 0) read_xsection_from_file = false;

  if(m_isMC){

      if(read_xsection_from_file){
          std::vector<float> xsection_info = read_xsection_file(e_tag);
          m_xsection   = xsection_info.at(0);
          m_kfactor    = xsection_info.at(1);
          m_efficiency = xsection_info.at(2);
          relative_uncertainty = xsection_info.at(3);
          id = static_cast<int>(xsection_info.at(4)); // not really needed
      }
      else{
          std::cout << "Will use xsection, kfactor and efficiency supplied from the command line." << std::endl;
          m_xsection = xsection;
          m_kfactor = kfactor;
          m_efficiency = efficiency; 
          relative_uncertainty = 0; // irrelevant here  
          id = m_dsid; 
      }


    // Calcualte XS*BR
    XStimesBR = m_xsection * m_kfactor * m_efficiency;
    if (applyISRWeight & (m_inputTreeName.find("up")!=std::string::npos)){
      scaling_to_1ipb = XStimesBR / totinitialSumWeights_muR05_muF05;
    } else {
      scaling_to_1ipb = XStimesBR / totinitialSumWeights;
    }
    std::cout<<"Scaling to 1 inverse picobarn by applying weight "<<scaling_to_1ipb<<std::endl;
    if (scaling_to_1ipb<=0){
      std::cerr << "It seems I couldn't find this DSID in the file. Exiting." <<std::endl;
      abort();
    }

  } // end of isMC
  else {
    XStimesBR = totinitialNevts;
  }


  m_scaling = scaling_to_1ipb;


  std::cout << "Calculated scale factors:" << std::endl;
  std::cout << "\tXStimesBR: "  << XStimesBR << std::endl;
  std::cout << "\totinitialSumWeights: " << totinitialSumWeights << std::endl;
  std::cout << "\tscaling_to_1ipb: " << scaling_to_1ipb << std::endl;



  // Create this histo for MC and data, even if the MC numbers don't mean anything
  TH1* h_xsec = new TH1F("h_xsec", "", 1, 0, 1);
  h_xsec->SetDirectory(0);
  h_xsec->Fill("id", id);
  h_xsec->Fill("xsec", m_xsection);
  h_xsec->Fill("kfactor", m_kfactor);
  h_xsec->Fill("efficiency", m_efficiency);
  h_xsec->Fill("relative_uncertainty", relative_uncertainty);
  h_xsec->Fill("XStimesBR", XStimesBR);
  h_xsec->Fill("scaling_to_1ipb", scaling_to_1ipb);
  oFile->WriteTObject(h_xsec);

}
