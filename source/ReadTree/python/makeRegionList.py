from region import Region

from sys import argv

fout = open(argv[1], 'w')

types = ["SR", "CR1e", "CR2e", "CR1mu", "CR2mu"]
jetCombinations = [ [6, False, 50, ""],
                    [7, False, 50, ""],
                    [8, False, 50, ""],
                    [9, False, 50, ""],
                    [10, True, 50, ""],
                    [5, False, 80, ""],
                    [6, False, 80, ""],
                    [7, False, 80, ""],
                    [8, True, 80, ""] ]
bJetCombinations = [ [0, True, 70],
                     [0, False, 70],
                     [1, False, 70],
                     [2, True, 70] ]

for typ in types:
  for jetList in jetCombinations:
    for bJetList in bJetCombinations:
      region = Region()
      region.type = typ;
      region.jetMulti = jetList[0]
      region.isInclusive = jetList[1]
      region.pt = str(jetList[2])
      region.jetType = jetList[3]
      region.bjetMulti = bJetList[0]
      region.isBjetInclusive = bJetList[1]
      region.bJetWP = str(bJetList[2])
      fout.write(region.getName() + "\n")
