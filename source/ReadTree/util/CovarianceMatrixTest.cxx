#include "ReadTree/CovarianceMatrix.h"
#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>

int main(int argc, char* argv[])
{
  CovarianceMatrix matrix(8,5,2);
  CovarianceEllipse ellipse(matrix);
  std::cout << "cov_xx: 8, " << matrix.covXX() << std::endl
            << "cov_yy: 5, " << matrix.covYY() << std::endl
            << "cov_xy: 2, " << matrix.covXY() << std::endl
            << std::boolalpha << (matrix == CovarianceMatrix(ellipse) ) << std::endl;
  std::cout << "Ellipse:" << std::endl
            << "  Major: " << ellipse.major() << std::endl
            << "  Minor: " << ellipse.minor() << std::endl
            << "  Phi  : " << ellipse.phi()/M_PI << "pi" << std::endl
            << "  tan(phi) : " << tan(ellipse.phi() ) << std::endl;
  std::cout << "Original: " << std::endl << matrix << std::endl  
            << "Reconstructed: " << std::endl << CovarianceMatrix(ellipse) << std::endl;
  return 0;
}
