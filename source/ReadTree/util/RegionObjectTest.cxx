#include "ReadTree/Region/RegionType.h"
#include "ReadTree/Region/RegionJets.h"
#include "ReadTree/Region/RegionBjets.h"
#include "ReadTree/Region/RegionMJSigma.h"
#include "ReadTree/Region/RegionMETSig.h"
#include "ReadTree/Region/RegionPhoton.h"

#include <vector>
#include <string>

int main() {
  std::vector<std::string> names;

  // Test type
  names.clear();
  names.push_back("SR");
  names.push_back("CR2mu");
  for (const auto& name : names) {
    RegionType region(name);
    region.Print();
    //std::cout << region.getPath() << std::endl;
    std::cout << std::endl;
  }

  // Test jets 
  names.clear();
  names.push_back("8ij50");
  names.push_back("6ej70");
  for (const auto& name : names) {
    RegionJets region(name);
    region.Print();
    std::cout << std::endl;
  }

  // Test b-jets 
  names.clear();
  names.push_back("0ib");
  names.push_back("1eb");
  for (const auto& name : names) {
    RegionBjets region(name);
    region.Print();
    std::cout << std::endl;
  }

  // Test MJSigma
  names.clear();
  names.push_back("MJ500");
  names.push_back("MJ340");
  names.push_back("MJ000");
  for (const auto& name : names) {
    RegionMJSigma region(name);
    region.Print();
    std::cout << std::endl;
  }
  
  // Test photons 
  names.clear();
  names.push_back("0ig");
  names.push_back("1eg");
  for (const auto& name : names) {
    RegionPhoton region(name);
    region.Print();
    std::cout << std::endl;
  }

  return 0;
}

