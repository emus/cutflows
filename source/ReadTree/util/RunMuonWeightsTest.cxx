
#include "ReadTree/MuonBiasTools.h"

int main(){

  std::cout << "Constructing MuonBiasTools" << std::endl;
  MuonBiasCorrectionTool muon_bias_correction_tool("Reco", "/r10/atlas/hpacey/ChargeFlavourAsymm_Muons/histograms/output_histograms_data_v55_highPt.root", "/r10/atlas/hpacey/ChargeFlavourAsymm_Muons/histograms/output_histograms_data_trigger_v55_highPt.root");

  // Code to test the muon bias tool 
  std::vector< std::vector<double> > test = {
      {-0.05, 2.6,  41, -1},
      {-0.05, 2.6,  41, +1},
      {-0.05, 2.6, 101, +1},
      {1.02, -2.8,  41, -1},
      {1.02, -2.8,  41, +1},
  };


  //muon_bias_correction_tool.activate_debug();
  muon_bias_correction_tool.print();

  for(auto vec : test){ 
    double eta, phi, pt;
    int charge;
    eta = vec.at(0);
    phi = vec.at(1);
    pt = vec.at(2);
    charge = (int)vec.at(3);

    std::cout << "\n*** about to calculate weights ***" << std::endl;
    std::map<std::string, double> weights = muon_bias_correction_tool.get_weights(eta, phi, pt, charge);

    std::cout << "For muon at eta " << eta << " phi " << phi << " with pT " << pt << " GeV and charge " << charge; // << std::endl;
    std::cout << " returned eta-phi efficiency " << weights["eff"] << " pm " << weights["eff_err"] << ", "
      << "weight of " << weights["nominal"]  
      << " etaphi Up: " << weights["etaphibias_up"] 
      << " etaphi Down: " << weights["etaphibias_down"]
      << " ptscale Up: " << weights["ptscale_up"]
      << " ptscale Down: " << weights["ptscale_down"]
      << std::endl;

  }

}
