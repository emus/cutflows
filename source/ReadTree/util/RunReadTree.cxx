#include "ReadTree/ReadTree.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip> //for phi string

#include <getopt.h>
#include <TFile.h>
#include <TString.h>
#include <TDirectory.h>
#include <TSystem.h>

#include <vector>
#include <string>

#include <memory>


// forward declarations 
int get_dsid(std::string);
bool get_isData(std::string);
float get_campaign_weight(std::string, float);
std::string get_subcampaign(std::string);
std::string get_etag(std::string);
//-------------------------
// Main function
//-------------------------



int main(int argc, char* argv[]){


  bool USE_COMMON = false;
  bool util_debug = false; // debug for this file only

  // Initialising default options
  std::string outDir = "hist-out";
  std::string fakeSyst = "nominal";
  std::string inputTreeName = "Nominal";
  std::string fileList = "";
  bool overwrite = 0;
  int nEvents = -1;
  int startEvent = 0;
  int oc;
  int debug = 0;
  int LLSR = 0;
  bool doSquirrels = false; // Don't do squirrel variables by default
  bool applyHighPtIso = true; //default to true for updated fakes in 2021;
  bool applyFakeWeights = false; 
  bool useLikelihoodMM = false; // Use the likelihood matrix method if doing the fake calculation
  bool applyMuonBias = false;
  bool applyPromptLeptonCut = false;
  bool applyPromptRejectionCut = false;
  bool applyttbarWeights = false;
  bool applyDibosonScaleWeight = false;
  bool applyPDFWeight = false;
  bool applyISRWeight = false;
  //bool disableWeights = false; // Disable the application of weights and scale factors
  //bool skipPRW = false; // Do pileup reweighting by default
  //bool isSystematic = false; // Add the Nominal and common trees as friends of the systematic tree
  //Can deduce this from the name of the input tree
  bool verbose = false;
  bool onlyOutputFinal = false;
  bool isTruth = false;
  std::string histMaskString = "";
  std::string cutMaskString = "DefaultCuts";
  float xsecscale = 1.0; // way to get LQ weights in sneakily 
  //std::string resolutionMode = "";
  
  // WJF: november 2021, add arguments to pass in xsection and other normalisation information so this can be passed in the RECAST system without needing to read the file on CVMFS
  float xsection(-1);
  float kfactor(-1);
  float efficiency(-1);

  struct option longopts[] = {
    {"output",         required_argument,    0,        'o'},
    {"fakeSyst",       required_argument,    0,        'a'}, 
    {"overwrite",      no_argument,          0,        'w'},
    {"nEvents",        required_argument,    0,        'n'},
    {"startEvent",     required_argument,    0,        'p'},
    {"inTree",         required_argument,    0,        't'},
    {"xscale",         required_argument,    0,        'X'},
    {"xsection",       required_argument,    0,        'x'},
    {"kfactor",        required_argument,    0,        'k'},
    {"efficiency",     required_argument,    0,        'e'},
    {"debug",          no_argument,          0,        'd'},
    {"LLSR",           no_argument,          0,        'Q'},
    {"doSquirrels",    no_argument,          0,        's'},
    {"applyHighPtIso", no_argument,          0,        'I'},
    {"fakeWeights",    no_argument,          0,        'F'},
    {"likelihoodMM",   no_argument,          0,        'L'},
    {"muonBias",       no_argument,          0,        'M'},
    {"applyttbarWeights",       no_argument,          0,        'T'},
    {"applyPromptLeptonCut", no_argument,    0,        'R'},
    {"applyPromptRejectionCut", no_argument,    0,     'J'},
    //{"disableWeights", no_argument,          0,        'D'},
    //{"isSystematic",   no_argument,          0,        'V'},
    {"verbose",        no_argument,          0,        'v'},
    {"onlyFinal",      no_argument,          0,        'f'},
    {"fileList",       required_argument,    0,        'l'},
    {"isTruth",        no_argument,          0,        '0'},
    {"histMask",       required_argument,    0,        'H'},
    {"cutMask",        required_argument,    0,        'C'},
    {"eventWeightBranch", required_argument, 0,        'W'},
    {0,0,0,0}
  };
  int option_index = 0;
  while((oc = getopt_long(argc, argv, ":o:a:wn:p:t:d:Q:sIFLTMRJ:Vvfl:0H:C:X:x:k:e:", longopts, &option_index)) != -1){
    switch(oc){
      case 'o':
        outDir = optarg;
        break;
      case 'a':
        fakeSyst = optarg;
        break;
      case 'w':
        overwrite = true;
        break;
      case 'n':
        nEvents = std::atoi(optarg);
        break;
      case 'p':
        startEvent = std::atoi(optarg);
        break;
      case 't':
        inputTreeName = optarg;
        break;
      case 'X':
        xsecscale = std::atof(optarg);
        break;
      case 'x':
        xsection = std::atof(optarg);
        break;
      case 'k':
        kfactor = std::atof(optarg);
        break;
      case 'e': 
        efficiency = std::atof(optarg);
        break;
      case 'd':
        debug = std::atoi(optarg);
        break;
      case 'Q':
        LLSR = std::atoi(optarg);
        break;
      case 's':
        doSquirrels = true;
        break;
      case 'I':
        applyHighPtIso = true;
        break;
      case 'F':
        applyFakeWeights = true;
        break;
      case 'L':
        useLikelihoodMM = true;
        break;
      case 'T':
        applyttbarWeights = true;
        break;
      case 'M':
        applyMuonBias = true;
        break;
      case 'R':
        applyPromptLeptonCut = true;
        break;
      case 'J':
        applyPromptRejectionCut = true;
        break;
      case 'v':
        verbose = true;
        break;
      case 'f':
        onlyOutputFinal = true;
        break;
      case 'l':
        fileList = optarg;
        break;
      case '0':
        isTruth = true;
        break;
      case 'H':
        histMaskString = optarg;
        break;
      case 'C':
        cutMaskString = optarg;
        break;
      case ':':
        std::cout << "Warning - missing argument" << std::endl;
        break;
      case '?':
        std::cout << "Warning - unknown option" << std::endl;
        break;
    }
  }

  bool isSystematic = (inputTreeName != "Nominal");

  CutChain::TObjWrapper::s_printDebug = debug > 1000000;

  // Make sure there's at least one file 
  if(optind == argc && fileList==""){
    std::cout << argv[0] << " requires at least one input file. Exiting" << std::endl;
    return 1;
  }

  // Make sure that if any of {xsection, kfactor, efficiency} have been entered, then they all have
  bool all_entered = xsection > 0 && kfactor > 0 && efficiency > 0;
  bool none_entered = xsection < 0 && kfactor < 0 && efficiency < 0;
  if(!(all_entered || none_entered)){
      std::cerr << "If you enter any of {xsection, kfactor, efficiency}, you must enter them all!" << std::endl;
      return 1;
  }


  // Create output folder
  if(outDir.at(0) != '/') outDir = gSystem->WorkingDirectory() + ("/" + outDir);
  TFile* oFile;
  if(outDir.find(".root") == std::string::npos) outDir = outDir+".root";
  if(overwrite) oFile = new TFile((outDir).c_str(), "RECREATE");
  else oFile = new TFile((outDir).c_str(), "NEW");
  if(oFile->IsZombie()){
    std::cerr << "Error opening output file" << std::endl;
    return 1;
  }

  if (doSquirrels) histMaskString += ",Squirrel";


  if (onlyOutputFinal) cutMaskString = "Final";

  //
  // Only use Likelihood Matrix Method if -"F" for fakes is set
  if(useLikelihoodMM && !applyFakeWeights){
    std::cerr << "ERROR: Fake weights must be turned on to use Likelihood matrix method" << std::endl;
    return 1;
  }


  // print out input arguments
  std::cout << std::endl;
  std::cout << "Printing input arguments." << std::endl;
  std::cout << "\toutput:\t" << outDir << std::endl;
  std::cout << "\tfakeSyst:\t" << fakeSyst << std::endl;
  std::cout << "\toverwrite:\t" << overwrite << std::endl;
  std::cout << "\tnEvents:\t" << nEvents << std::endl;
  std::cout << "\tstartEvent:\t" << startEvent << std::endl;
  std::cout << "\tinTree:\t" << inputTreeName << std::endl;
  std::cout << "\tdebug:\t" << debug << std::endl;
  std::cout << "\tLLSR:\t" << LLSR << std::endl;
  std::cout << "\tdoSquirrels:\t" << doSquirrels << std::endl;
  std::cout << "\tapplyHighPtIso:\t" << applyHighPtIso << std::endl;
  std::cout << "\tfakeWeights:\t" << applyFakeWeights << std::endl;
  std::cout << "\tLikelihood matrix method:\t" << useLikelihoodMM << std::endl;
  std::cout << "\tmuonBias:\t" << applyMuonBias << std::endl;
  std::cout << "\tttbarWeight:\t" << applyttbarWeights << std::endl;
  std::cout << "\tpromptLeptonCut:\t" << applyPromptLeptonCut << std::endl;
  std::cout << "\tpromptRejectionCut:\t" << applyPromptRejectionCut << std::endl;
  std::cout << "\tisSystematic:\t" << isSystematic << std::endl;
  std::cout << "\tverbose:\t" << verbose << std::endl;
  std::cout << "\txscale:\t" << xsecscale << std::endl;

  std::cout << "\txsection:\t" << xsection << std::endl;
  std::cout << "\tkfactor:\t" << kfactor << std::endl;
  std::cout << "\tefficiency:\t" << efficiency << std::endl;

  std::cout << std::endl;

  if(applyPromptRejectionCut == true && applyPromptLeptonCut == true){
    std::cout << "ERROR: Cannot have both of these cuts applied at the same time! " << std::endl;
    return 1;
  }


  // If we are running a muon bias syst or ttbar modelling syst we want the inputTreeName to still be nominal... so set that and define another string name we can pass to the alg to apply the muon bias syst weight
  std::string pretendTreeName = "";
  if (inputTreeName.find("MUON_RECO")!=std::string::npos || inputTreeName.find("MUON_TRIGGER")!=std::string::npos || inputTreeName.compare("ttbar_WeightClosure")==0 || inputTreeName.find("ttbar_PDF")!=std::string::npos || inputTreeName.find("singletop_PDF")!=std::string::npos || inputTreeName.find("ttbar_ISR")!=std::string::npos || inputTreeName.find("diboson_scale")!=std::string::npos ){
//    std::cout<<"Setting the pretendTreeName, and making the inputTreeName Nominal"<<std::endl;
    pretendTreeName = inputTreeName;
    inputTreeName = "Nominal";
  }


  // Create vector of paths+names of ROOT files to add to the TChain
  std::vector<std::string> fileNames;
  if(fileList != ""){
    // read in files from fileList
    std::cout << "Using list of files: " << fileList << std::endl;
    std::ifstream infile(fileList);
    std::string temp_line;
    const char* t = " \t\n\r\f\v";
    while (std::getline(infile, temp_line)) {
      temp_line.erase(0, temp_line.find_first_not_of(t)); // guard against leading whitespace
      fileNames.push_back(temp_line);
    }
  }
  else{
    // read in files from command line
      for (int i = optind; i < argc; ++i){
        fileNames.push_back(argv[i]);
      }
  }
  std::cout << "The number of input files is: " << fileList.size() << std::endl;
  for(const auto& fname : fileNames) std::cout << fname << std::endl;


  // Check that the first file contains the inputTreeName
  std::unique_ptr<TFile> first_tfile(TFile::Open(fileNames.at(0).c_str(),"r") );
  if (!(first_tfile->GetListOfKeys()->Contains(inputTreeName.c_str() ))) {
    std::cerr << "File doesn't contain '" << inputTreeName << "' - exiting." << std::endl;
    return 1;
  }
  else {
    std::cout<<"found '" << inputTreeName << "' tree, all is well :-)"<<std::endl;
  }

  //
  std::vector<std::string> friends_names;
  if(isSystematic){
    friends_names.push_back("Nominal");
  }
  for (auto & friendname : friends_names) {
    if (!(first_tfile->GetListOfKeys()->Contains(friendname.c_str() ))) {
      std::cerr << "File doesn't contain '" << friendname << "' - exiting." << std::endl;
      return 1;
    }
  }
  bool hasTruth = first_tfile->GetListOfKeys()->Contains("Truth");

  // Getting InitialSumOfWeights info
  // Procedure assumes run over *all* files in dataset to get normalisation right. Due to speed of code this should be OK
  // At some point, make it scan for all of them to make sure

  int totinitialNevts = 0;
  double totinitialSumWeights = 0.0;
  double totinitialSumWeights_muR05_muF05 = 0.0;
  double totinitialSumWeightsSquared = 0.0;
  int totrunNevts = 0;
  double totrunSumWeights = 0;

  int total_nevents_executed = 0;
  int total_nevents_noPV = 0;
  int total_clean_events = 0;


  double totSumWeightsPRW = 0.0;

  int Nfiles = 0;
  int NevtsChain = 0;

  // copy of the summed cutflow histogram
  TH1D * h_nEvents_sum = new TH1D();
  h_nEvents_sum->SetDirectory(0); // may cause memory leak? only one histogram

  TH1D* h_cutflow_syst_sum = new TH1D();
  h_cutflow_syst_sum->SetDirectory(0);

  TH1D* h_cutflow_common_sum = new TH1D();
  h_cutflow_common_sum->SetDirectory(0);

  TH1D * h_weighted_cutflow_syst_sum = new TH1D();
  h_weighted_cutflow_syst_sum->SetDirectory(0); // may cause memory leak? only one histogram

  TH1D * h_weighted_cutflow_common_sum = new TH1D();
  h_weighted_cutflow_common_sum->SetDirectory(0); // may cause memory leak? only one histogram


  if(util_debug) std::cout << "About to set name" << std::endl;

  // Add all chains for trees 
  TChain * input = new TChain(inputTreeName.c_str());
  TChain * commonInput;
  if(USE_COMMON) commonInput = new TChain("commonValues");
  TChain * truth = hasTruth ? new TChain("Truth") : 0;
  std::vector<TChain*> syst_friends;
  if(isSystematic) {
     for (auto friendname : friends_names) {
         syst_friends.push_back(new TChain(friendname.c_str()));
     }
  }


  for(std::string fileName : fileNames){
    Nfiles ++;
    if(verbose) std::cout<<"adding file "<<Nfiles<<": "<<fileName<<std::endl;
    input->Add(fileName.c_str());
    if(USE_COMMON) commonInput->Add(fileName.c_str());
    if (truth) truth->Add(fileName.c_str() );
    if(syst_friends.size() > 0) {
      for(auto friend_chain : syst_friends) friend_chain->Add(fileName.c_str());
    }

    first_tfile.reset(TFile::Open(fileName.c_str()) );
    if (first_tfile->IsZombie() ) {
      std::cerr << "Failed to open file " << fileName << std::endl;
      return 1;
    }
    TH1* h_nevents;
    first_tfile->GetObject("h_numberOfEvents", h_nevents);
    TH1* cutflow_syst_temp; // syst can mean nominal
    TH1* cutflow_common_temp;
    TH1* weighted_syst_temp; // syst can mean nominal 
    TH1* weighted_common_temp;

    first_tfile->GetObject(("cutflow_"+inputTreeName).c_str(), cutflow_syst_temp);
    first_tfile->GetObject("cutflow_common", cutflow_common_temp);

    first_tfile->GetObject(("weighted_cutflow_"+inputTreeName).c_str(), weighted_syst_temp);
    first_tfile->GetObject("weighted_cutflow_common", weighted_common_temp);

    if(Nfiles==1) {
      totSumWeightsPRW = h_nevents->GetBinContent(h_nevents->GetXaxis()->FindBin("Sum of weights PRW"));

      if(verbose) std::cout << "about to clone" << std::endl;

      if(verbose) std::cout << "h_numberOfEvents_sum" << std::endl;
      h_nEvents_sum = dynamic_cast<TH1D*>( h_nevents->Clone("h_numberOfEvents_sum") );
      h_nEvents_sum->SetDirectory(0);

      if(verbose) std::cout << "cutflow_"+inputTreeName+"_sum" << std::endl;
      h_cutflow_syst_sum = dynamic_cast<TH1D*>( cutflow_syst_temp->Clone( ("cutflow_"+inputTreeName+"_sum").c_str()) );
      h_cutflow_syst_sum->SetDirectory(0);

      if(verbose) std::cout << "cutflow_common_sum" << std::endl;
      h_cutflow_common_sum = dynamic_cast<TH1D*>( cutflow_common_temp->Clone("cutflow_common_sum"));
      h_cutflow_common_sum->SetDirectory(0);

      if(verbose) std::cout << "weighted_cutflow_"+inputTreeName+"_sum" << std::endl;
      h_weighted_cutflow_syst_sum = dynamic_cast<TH1D*>( weighted_syst_temp->Clone( ("weighted_cutflow_"+inputTreeName+"_sum").c_str()) );
      h_weighted_cutflow_syst_sum->SetDirectory(0); 

      if(verbose) std::cout << "weighted_cutflow_common_sum" << std::endl;
      h_weighted_cutflow_common_sum = dynamic_cast<TH1D*>( weighted_common_temp->Clone("weighted_cutflow_common_sum") );
      h_weighted_cutflow_common_sum->SetDirectory(0);

      if(util_debug) std::cout << "Cloned" << std::endl;

    }
    else {
      //std::cout << "About to add" << std::endl;
      h_nEvents_sum->Add(h_nevents);
      h_cutflow_syst_sum->Add(cutflow_syst_temp);
      h_cutflow_common_sum->Add(cutflow_common_temp);
      
      h_weighted_cutflow_syst_sum->Add(weighted_syst_temp);
      h_weighted_cutflow_common_sum->Add(weighted_common_temp);
    }

    std::cout << "Extracting info from nevents histograms" << std::endl; 
    int initialNevts                = h_nevents->GetBinContent(h_nevents->GetXaxis()->FindBin("Nevents Generated"));
    double initialSumWeights        = h_nevents->GetBinContent(h_nevents->GetXaxis()->FindBin("Sum of weights Generated"));
    double initialSumWeights_muR05_muF05        = h_nevents->GetBinContent(h_nevents->GetXaxis()->FindBin("muR05,muF05 Sum of weights Generated"));
    double initialSumWeightsSquared = h_nevents->GetBinContent(h_nevents->GetXaxis()->FindBin("Sum of weights squared Generated"));
    int runNevts                    = h_nevents->GetBinContent(h_nevents->GetXaxis()->FindBin("Nevents run over"));
    double runSumWeights            = h_nevents->GetBinContent(h_nevents->GetXaxis()->FindBin("Sum of weights run over"));
    //nevents_executed             = h_nevents->GetBinContent(h_nevents->GetXaxis()->FindBin("Nevents executed"));
    //nevents_noPV                 = h_nevents->GetBinContent(h_nevents->GetXaxis()->FindBin("NEvents with no PV"));
    int nevents_executed            = cutflow_common_temp->GetBinContent(cutflow_common_temp->GetXaxis()->FindBin("NoCut"));
    int nevents_noPV                = cutflow_common_temp->GetBinContent(cutflow_common_temp->GetXaxis()->FindBin("PrimaryVertex"));
    int nevents_clean = -1;

    // If using fakes, ExactlyTwoLeptons is the last cut. "using fakes" = calculating histograms for the fake estimate 
    nevents_clean = cutflow_syst_temp->GetBinContent(cutflow_syst_temp->GetXaxis()->FindBin("ExactlyTwoLeptons"));

    // If NOT using fakes, the last cut isOppositeChargeLeptons:
    //nevents_clean = cutflow_syst_temp->GetBinContent(cutflow_syst_temp->GetXaxis()->FindBin("OppositeChargeLeptons"));
    

    
    totinitialNevts             += initialNevts;
    totinitialSumWeights        += initialSumWeights;
    totinitialSumWeights_muR05_muF05        += initialSumWeights_muR05_muF05;
    totinitialSumWeightsSquared += initialSumWeightsSquared;
    totrunNevts                 += runNevts;
    totrunSumWeights            += runSumWeights;
    total_nevents_executed      += nevents_executed;
    total_nevents_noPV          += nevents_noPV;
    total_clean_events          += nevents_clean; 

    if(verbose) std::cout<<"Initial Nevts = "<<initialNevts<<", SumWeights = "<<initialSumWeights<<"; Run over Nevts = "<<runNevts<<", SumWeights = "<<runSumWeights;//<<std::endl;
    if(verbose) std::cout<<", from chain: "<< input->GetEntries()-NevtsChain <<" (NevtsChain= = "<< NevtsChain <<")"<<std::endl;
    NevtsChain = input->GetEntries();
    first_tfile->Close();
  } // end loop over input files 

  oFile->WriteTObject(h_nEvents_sum);
  // Fix weird friend thing
  if (input->GetListOfFriends() ) input->GetListOfFriends()->RemoveAll();
  if(USE_COMMON) input->AddFriend(commonInput);
  //input->AddFriend(commonInput);
  for(auto friendchain : syst_friends) input->AddFriend(friendchain);
  if(USE_COMMON) input->GetListOfFriends()->ls();




  std::cout << "About to read the configuration tree..."; 
  bool isData;
  int DSID;
  std::string subCampaign;
  TTree* configTree = dynamic_cast<TTree*>( (TFile::Open(fileNames.at(0).c_str()) )->Get("ConfigurationTree") );
  if (configTree != 0 ) {
      TTreeReader configReader(configTree);
      TTreeReaderValue<int> c_isData(configReader, "isData");
      TTreeReaderValue<int> c_DSID(configReader, "DSID");
      TTreeReaderValue<TObjString> c_subCampaign(configReader, "subCampaign"); // mc16{a,d,e} or data1{5,6,7,8}
      configReader.Next(); // Load the first entry
      isData = *c_isData;
      DSID = *c_DSID;
      subCampaign = (*c_subCampaign).String().Data();
      std::cout << " ... success" << std::endl;
  }
  else{

      std::cout << std::endl;
      std::cerr << "Failed to read 'ConfigTree' from input file: " << fileNames.at(0) << std::endl;
      std::cout << "Will extract DSID and isData from the filename..." << std::endl;
      // Hack to extract DSID and isData if configTree isn't working 
      isData = get_isData(fileNames.at(0));
      DSID   = get_dsid(fileNames.at(0));
      subCampaign = get_subcampaign(fileNames.at(0));

  }

  // Only possible to extract e-tag from the filename unfortunately  
  std::string e_tag = "";
  if(!isData) e_tag = get_etag(fileNames.at(0));

  float lumiweight = get_campaign_weight(subCampaign, xsecscale);


  // Create ReadTree object, and add information from command line/config
  std::cout << "about to run ReadTree alg" << std::endl;

  std::string readtree_dataprefix  = static_cast<std::string>(gSystem->ExpandPathName("$WorkDir_DIR")) + "/data/ReadTree";
  std::cout << "ReadTree data path prefix: " << readtree_dataprefix << std::endl;

  std::string muon_efficiency_histos = readtree_dataprefix+"/muonbias/output_histograms_data_v55.root";
  std::string muon_efficiency_trig_histos = readtree_dataprefix+"/muonbias/output_histograms_data_trigger_v55.root";

  ReadTree alg(input, configTree, truth, muon_efficiency_histos, muon_efficiency_trig_histos, pretendTreeName);


  // make sure that the mask strings are correct
  if (!alg.setHistMask(histMaskString) ) {
    std::cerr << "Invalid hist mask string " << histMaskString << " supplied!" << std::endl;
    return 1;
  }
  if (!alg.setCutPositionMask(cutMaskString) ) {
    std::cerr << "Invalid cut mask string " << cutMaskString << " supplied!" << std::endl;
    return 1;
  }



  /////////////////////////////////
  // Define other ReadTree settings
  /////////////////////////////////

  alg.m_plot_extra_lepton_variables = true; // turn on/off some lepton diagnostic plots
  alg.nEvents = nEvents;
  alg.startEvent = startEvent;
  //alg.m_nEvents_NTUP = totrunNevts;
  alg.debug = debug;
  alg.LLSR = LLSR;
  alg.doSquirrels = doSquirrels;
  alg.applyHighPtIso = applyHighPtIso;
  alg.fakeSyst = fakeSyst;
  alg.applyFakeWeights = applyFakeWeights;
  alg.useLikelihoodMM = useLikelihoodMM;
  alg.applyMuonBias = applyMuonBias;
  alg.applyttbarWeights = applyttbarWeights;
  alg.applyPromptLeptonCut = applyPromptLeptonCut;
  alg.applyPromptRejectionCut = applyPromptRejectionCut;
  //alg.disableWeights = disableWeights;
  //alg.skipPRW = skipPRW;
  alg.onlyOutputFinal = false;
  alg.isTruth = isTruth;



  // Updated for RECAST 2021-08-21
  // Was /usera/wfawcett/cambridge/emus/OSDFChargeFlavourAsymmCode/fake_estimate/tag_and_probe_method/21Oct_p4series/efficiencies_combined.root 
  alg.fakeWeightHistos    = readtree_dataprefix +  "weightfiles/efficiencies_combined.root";

  // Updated for RECAST 2021-08-12
  // Was /r10/atlas/emus/Plots/ttbarWeights/191020/ttbarMisModellingWeight_lep1Pt_EtMiss_measCRRatio_valVRMET_mc16ade_testgraph2.root
  alg.ttbarWeightHistFile = readtree_dataprefix + "weightfiles/ttbarMisModellingWeight_lep1Pt_EtMiss_measCRRatio_valVRMET_mc16ade_testgraph2.root";

  alg.totinitialSumWeights = totinitialSumWeights;
  alg.totinitialSumWeights_muR05_muF05 = totinitialSumWeights_muR05_muF05;
  alg.totinitialNevts = totinitialNevts;
  

  std::cout << "Read " << Nfiles << " files, "
            << "with Nevts = " << totinitialNevts
            << ", SumWeights = " << totinitialSumWeights
            << ", SumWeightsPRW = " << totSumWeightsPRW
            << std::endl;
  if(totSumWeightsPRW != 0) {
    std::cout << "Ratio PRW / CutBookkeeper = " << totSumWeightsPRW / totinitialSumWeights << std::endl;
  }

  std::cout<< "NTUP ran over Nevts = " << total_nevents_executed
    << ", SumWeights = " << totrunSumWeights
    << ", from chain: " << input->GetEntries()
    << std::endl;

  std::cout << "There were " << totrunNevts << " in the orignal DxAOD, and " << total_nevents_noPV << " had no PV" << std::endl;
  std::cout << "There should be " << total_nevents_noPV << " events stored in the ntuple" << std::endl;

  // Check consistent number of events
  if( total_clean_events != input->GetEntries() ){
    std::cout << "ERROR: Inconsistent number of events in ntuple and in chain!" << std::endl;
    std::cout << "ERROR: For tree: " << inputTreeName << std::endl;
    std::cout << "ERROR: total_clean_events: " << total_clean_events << " \t input->GetEntries(): " << input->GetEntries() << std::endl;
    return 1;
  }


  ////////////////////////////////////////////////
  // Now start ReadTree configuration
  ////////////////////////////////////////////////


  // set ReadTree to apply the PDF/ISR/Scale weight if we have a ttbar/singletop sample and an appropriate systname
  //
  std::vector<int> ISR_samples = {410472, 410482};
  std::vector<int> PDF4LHC_samples = {407341, 407342, 407343, 407344, 407345, 407346, 410470, 410472, 410558, 410482, 410465, 101232, 142857, 410644, 410645, 410646, 410647, 410658, 410659, 410648, 410649};
  std::vector<int> diboson_samples = {364250, 364253, 364254, 364255, 364288, 364289, 364290, 363356, 363358, 363359, 363360, 363489, 364283, 364284, 364285, 364287};
  //
  //
  if (pretendTreeName.find("ttbar_ISR")!=std::string::npos){
    bool is_ISR_sample = std::find(ISR_samples.begin(), ISR_samples.end(), DSID) != ISR_samples.end();
    if(is_ISR_sample){
      std::cout<< "Will do ISR weights to sample: " << DSID << std::endl;
      applyISRWeight = true;
    }
  }
  alg.applyISRWeight = applyISRWeight;

  if (pretendTreeName.find("diboson_scale")!=std::string::npos || pretendTreeName.find("diboson_scale")!=std::string::npos){
    bool is_DibosonScale_sample = std::find(diboson_samples.begin(), diboson_samples.end(), DSID) != diboson_samples.end();
    if(is_DibosonScale_sample){
      std::cout<< "Will do Diboson Scale weights to sample: " << DSID << std::endl;
      applyDibosonScaleWeight = true;
    }
  }
  alg.applyDibosonScaleWeight = applyDibosonScaleWeight;

  if (pretendTreeName.find("ttbar_PDF")!=std::string::npos || pretendTreeName.find("singletop_PDF")!=std::string::npos){
    bool is_PDF4LHC_sample = std::find(PDF4LHC_samples.begin(), PDF4LHC_samples.end(), DSID) != PDF4LHC_samples.end();
    if(is_PDF4LHC_sample){
      std::cout<< "Will do PDF weights to sample: " << DSID << std::endl;
      applyPDFWeight = true;
    }
  }
  alg.applyPDFWeight = applyPDFWeight;

  if( !alg.configure(oFile, outDir,  lumiweight, e_tag, xsection, kfactor, efficiency) ){
    std::cerr << "ERROR: could not configure ReadTree" << std::endl;
    abort();
  }

  // WJF: as there are different per-event level scale factors applied, this totals in these histograms may not be the same as the totals in the output histograms
  //h_cutflowWeighted_sum->Scale(alg.m_scaling);
  h_weighted_cutflow_common_sum->Scale(alg.m_scaling);
  h_weighted_cutflow_syst_sum->Scale(alg.m_scaling);



  ////////////////////////////////////////////////
  // Begin the Loop
  ////////////////////////////////////////////////

  alg.Loop();
  alg.chain.saveOutput(false, oFile, h_cutflow_common_sum, h_weighted_cutflow_common_sum);
  std::cout << "Done writing to " << outDir << std::endl;
  oFile->Close();
  std::cout << "I successfully completed :-)" << std::endl;

  return 0;
}

std::vector<std::string> splitString(const std::string& inStr, char delim)
{
  // Split a string along a delimiter
  std::istringstream ss(inStr);
  std::string placeholder;
  std::vector<std::string> ret;
  while (std::getline(ss, placeholder, delim) ) ret.push_back(placeholder);
  return ret;
}

int get_dsid(std::string filepath){
  // poor man's function to extract the DSID from the filename until it can be extracted from the metadata
  
  std::cout << "will attempt to extract DSID from filepath: " << filepath << std::endl;
  std::vector<std::string> toks = splitString(filepath, '/');
  // second-to-last entry 
  std::string filename = toks.at(toks.size() - 2); 

  // e.g: user.wfawcett.1902251350.data17.325713.r10260_p3399_p3704_NTUP
  toks = splitString(filename, '.');
  std::string dsid = toks.at(4);
  std::cout << "extracted DSID " << dsid << std::endl;
  return std::stoi(dsid);
}

std::string get_subcampaign(std::string filepath){
  // Get the sub-campaign from the filepath 
  // e.g: user.wfawcett.1902251350.data17.325713.r10260_p3399_p3704_NTUP
  // will return e.g. "mc16d" or "data15"
  std::cout << "will attempt to extract sub-campaign from filepath: " << filepath << std::endl;
  std::vector<std::string> toks = splitString(filepath, '/');
  std::string filename = toks.at(toks.size() - 2); 
  toks = splitString(filename, '.');
  std::string subCampaign = toks.at(3);
  std::cout << "Extracted campaign: " << subCampaign << std::endl;
  return subCampaign;
}

float get_campaign_weight(std::string subCampaign, float xsecscale=1.0){



  // Extract lumi weight depending on campaign
  std::map<std::string, float> campaign_map = {
    { "mc16a", 36215. }, // mc16a should correspond to data15+data16 lumi
    { "mc16d", 44307. }, // mc16d should correspond to 2017
    { "mc16e", 58450. }, // mc16e should correspond to 2018
    { "data15", 1. },
    { "data16", 1. },
    { "data17", 1. },
    { "data18", 1. },
  };

 float campaign_weight = campaign_map[subCampaign];
 return campaign_weight*xsecscale; //also sneaking in the LQ scaling here
}

bool get_isData(std::string filepath){
  bool isData(false);
  std::cout << "will attempt to extract isData from filepath: " << filepath << std::endl;
  std::vector<std::string> toks = splitString(filepath, '/');
  // second-to-last entry 
  std::string filename = toks.at(toks.size() - 2); 
  toks = splitString(filename, '.');
  std::string subCampaign = toks.at(3); // should be "d" or "m"
  const char* dataOrMC = subCampaign.c_str(); 
  auto dORm = dataOrMC[0];
  
  if(dORm == 'd') isData = true;
  //if(subCampaign == "d") isData = true;
  //else isData=false;
  std::cout << "found isData" << isData << std::endl;
  return isData;
}

std::string get_etag(std::string filepath){
  // poor man's function to extract the e-tag from the filename until it can be extracted from the metadata

  std::cout << "will attempt to extract e-tag from filepath: " << filepath << std::endl;
  std::vector<std::string> toks = splitString(filepath, '/');
  // second-to-last entry
  std::string filename = toks.at(toks.size() - 2);

  // e.g: user.wfawcett.1902251350.data17.325713.r10260_p3399_p3704_NTUP
  // user.wfawcett.2011161102.mc16a.361023.Py8EG_N23LO_jetjet_JZ3W.e3668_s3126_r9364_p4237_NTUP
  toks = splitString(filename, '.');
  std::string ami_tag = toks.at(6);
  std::cout << "extracted AMI tag: " << ami_tag << std::endl;
  std::string e_tag = splitString(ami_tag, '_').at(0);
  std::cout << "Extracted e-tag: " << e_tag << std::endl;

  return e_tag;
}
