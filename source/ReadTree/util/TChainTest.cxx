#include <TFile.h>
#include <TChain.h>
#include <iostream>

int main(int argc, char* argv[]) {
  TChain input("Nominal");
  for (int i = 1; i < argc; ++i) {
    input.Add(argv[i]);
    std::cout << "Adding file " << argv[i] << std::endl;
  }
  std::cout << "About to call GetEntries" << std::endl;
  std::cout << input.GetEntries() << std::endl;
  return 0;
}
